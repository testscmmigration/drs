package com.moneygram.drs.service.v1.mappers;

import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.service.v1.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v1.QueryRegistrationByCustomerReceiveNumberResponse;
import com.moneygram.drs.service.v1.RegistrationFieldInfo;
import com.moneygram.drs.util.ObjectMaster;

public class QueryRegistrationByCustomerReceiveNumberMapperTest extends TestCase {
	public void testMapToCommandRequest() throws Exception {
		QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest = new QueryRegistrationByCustomerReceiveNumberRequest();
		queryRegistrationByCustomerReceiveNumberRequest.setMgCustomerReceiveNumber("MG1234");
		queryRegistrationByCustomerReceiveNumberRequest.setSuperUser(true);
		QueryRegistrationByCustomerReceiveNumberMapper queryRegistrationByCustomerReceiveNumberMapper = new QueryRegistrationByCustomerReceiveNumberMapper();
		com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest ejbReq = (com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest) queryRegistrationByCustomerReceiveNumberMapper
				.mapToCommandRequest(queryRegistrationByCustomerReceiveNumberRequest);
		assertEquals(ejbReq.getMgCustomerReceiveNumber(),
				queryRegistrationByCustomerReceiveNumberRequest.getMgCustomerReceiveNumber());
		assertEquals(ejbReq.isSuperUser(), queryRegistrationByCustomerReceiveNumberRequest
				.isSuperUser());
	}

	public void testMapToCommandResponse() throws Exception {
		QueryRegistrationByCustomerReceiveNumberMapper queryRegistrationByCustomerReceiveNumberMapper = new QueryRegistrationByCustomerReceiveNumberMapper();
		Object commandResponse = ObjectMaster.getRegistrationData();
		QueryRegistrationByCustomerReceiveNumberResponse byCustomerReceiveNumberResponse = (QueryRegistrationByCustomerReceiveNumberResponse) queryRegistrationByCustomerReceiveNumberMapper
				.mapToServiceResponse(commandResponse);
		RegistrationData registrationData = (RegistrationData) commandResponse;
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getDeliveryOption(),
				registrationData.getFqdoInfo().getDeliveryOption());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getDeliveryOptionDisplayName(),
				registrationData.getFqdoInfo().getDeliveryOptionDisplayName());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getReceiveAgentAbbreviation(),
				registrationData.getFqdoInfo().getReceiveAgentAbbreviation());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getReceiveAgentID(),
				registrationData.getFqdoInfo().getReceiveAgentID());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getReceiveAgentName(),
				registrationData.getFqdoInfo().getReceiveAgentName());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getReceiveCountry(),
				registrationData.getFqdoInfo().getReceiveCountry());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo().getReceiveCurrency(),
				registrationData.getFqdoInfo().getReceiveCurrency());
		assertEquals(byCustomerReceiveNumberResponse.getFqdoInfo()
				.getRegistrationAuthorizationText(), registrationData.getFqdoInfo()
				.getRegistrationAuthorizationText());
		assertEquals(byCustomerReceiveNumberResponse.getOfacExceptionStateCode(), registrationData
				.getOfacExceptionStateCode());
		assertEquals(byCustomerReceiveNumberResponse.getRegistrationSubStatus().getValue(),
				registrationData.getRegistrationSubStatus());
		assertEquals(byCustomerReceiveNumberResponse.getRegistrationStatus().getValue(),
				registrationData.getRegistrationStatus().getName());
		RegistrationFieldInfo[] fieldInfos = byCustomerReceiveNumberResponse
				.getRegistrationFieldInfo();
		for (int i = 0; i < fieldInfos.length; i++) {
			List regFieldExpected = (List) registrationData.getRegistrationFieldInfo();
			assertEquals(fieldInfos[i].getDefaultValue(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getDefaultValue());
			assertEquals(fieldInfos[i].getDisplayOrder(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getDisplayOrder());
			assertEquals(fieldInfos[i].getExampleFormat(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getExampleFormat());
			assertEquals(fieldInfos[i].getFieldLabel(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldLabel());
			assertEquals(fieldInfos[i].getFieldMax(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldMax());
			assertEquals(fieldInfos[i].getFieldMin(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldMin());
			assertEquals(fieldInfos[i].getFieldScale(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldScale());
			assertEquals(fieldInfos[i].getFieldValue(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldValue());
			assertEquals(fieldInfos[i].getValidationRegEx(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getValidationRegEx());
			assertEquals(fieldInfos[i].getXmlTag(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getXmlTag());
			// TODO add enumerated values check.... currently enumerated values
			// is null
		}
	}
}
