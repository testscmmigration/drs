package com.moneygram.drs.service.v1.mappers;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.service.v1.FQDOsForCountryRequest;

public class FQDOsForCountryrMapperTest extends TestCase {
	public void testMapToCommandRequest() {
		FQDOsForCountryRequest fqdOsForCountryRequest = createRequest();
		FQDOsForCountryrMapper fqdOsForCountryrMapper = new FQDOsForCountryrMapper();
		com.moneygram.drs.request.FQDOsForCountryRequest fqdosForCountryRequestEJB = (com.moneygram.drs.request.FQDOsForCountryRequest) fqdOsForCountryrMapper
				.mapToCommandRequest(fqdOsForCountryRequest);
		assertEquals("USA", fqdosForCountryRequestEJB.getReceiveCountry());
		assertEquals("Eng", fqdosForCountryRequestEJB.getLanguage());
	}

	private FQDOsForCountryRequest createRequest() {
		FQDOsForCountryRequest fqdOsForCountryRequest = new FQDOsForCountryRequest();
		fqdOsForCountryRequest.setReceiveCountry("USA");
		fqdOsForCountryRequest.setLanguage("Eng");
		return fqdOsForCountryRequest;
	}

	public void testMapToCommandResponse() {
		List fqdOsForCountryResponse = createResponse();
		FQDOsForCountryrMapper fqdOsForCountryrMapper = new FQDOsForCountryrMapper();
		com.moneygram.drs.service.v1.FQDOsForCountryResponse fqdOsForCountryResponseWeb = (com.moneygram.drs.service.v1.FQDOsForCountryResponse) fqdOsForCountryrMapper
				.mapToServiceResponse(fqdOsForCountryResponse);
		com.moneygram.drs.service.v1.FQDOInfo[] fqdoInfoWeb = fqdOsForCountryResponseWeb
				.getFqdoInfo();
		assertEquals("USD", fqdoInfoWeb[0].getReceiveCurrency());
		assertEquals("The company", fqdoInfoWeb[0].getReceiveAgentName());
		assertEquals("1", fqdoInfoWeb[0].getReceiveAgentID());
		assertEquals("Account deposit", fqdoInfoWeb[0].getDeliveryOptionDisplayName());
		assertEquals("option1", fqdoInfoWeb[0].getDeliveryOption());
	}

	private List createResponse() {
		List fqdOsForCountryResponse = new ArrayList();
		FQDOInfo fqdoInfo = new FQDOInfo();
		fqdoInfo.setDeliveryOption("option1");
		fqdoInfo.setDeliveryOptionDisplayName("Account deposit");
		fqdoInfo.setReceiveAgentID("1");
		fqdoInfo.setReceiveAgentName("The company");
		fqdoInfo.setReceiveCurrency("USD");
		fqdOsForCountryResponse.add(fqdoInfo);
		return fqdOsForCountryResponse;
	}
}
