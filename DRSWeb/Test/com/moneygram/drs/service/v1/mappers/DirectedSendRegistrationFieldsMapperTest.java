package com.moneygram.drs.service.v1.mappers;

import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.service.v1.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.service.v1.DirectedSendRegistrationFieldsResponse;
import com.moneygram.drs.service.v1.RegistrationFieldInfo;
import com.moneygram.drs.util.ObjectMaster;

public class DirectedSendRegistrationFieldsMapperTest extends TestCase {
	public void testMapToCommandRequest() {
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = createRequest();
		DirectedSendRegistrationFieldsMapper directedSendRegistrationFieldsMapper = new DirectedSendRegistrationFieldsMapper();
		com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequestEJB = (com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest) directedSendRegistrationFieldsMapper
				.mapToCommandRequest(directedSendRegistrationFieldsRequest);
		assertEquals(directedSendRegistrationFieldsRequestEJB.getDeliveryOption(), "DelOption");
		assertEquals(directedSendRegistrationFieldsRequestEJB.getReceiveAgentID(), "AgentID");
		assertEquals(directedSendRegistrationFieldsRequestEJB.getReceiveCountry(), "USA");
		assertEquals(directedSendRegistrationFieldsRequestEJB.getReceiveCurrency(), "USD");
	}

	private DirectedSendRegistrationFieldsRequest createRequest() {
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = new DirectedSendRegistrationFieldsRequest();
		directedSendRegistrationFieldsRequest.setDeliveryOption("DelOption");
		directedSendRegistrationFieldsRequest.setReceiveAgentID("AgentID");
		directedSendRegistrationFieldsRequest.setReceiveCountry("USA");
		directedSendRegistrationFieldsRequest.setReceiveCurrency("USD");
		return directedSendRegistrationFieldsRequest;
	}

	public void testMapToCommandResponse() throws Exception {
		DirectedSendRegistrationInfo directedSendRegistrationInfo = createResponse();
		DirectedSendRegistrationFieldsMapper directedSendRegistrationFieldsMapper = new DirectedSendRegistrationFieldsMapper();
		DirectedSendRegistrationFieldsResponse directedSendRegistrationFieldsResponse = (DirectedSendRegistrationFieldsResponse) directedSendRegistrationFieldsMapper
				.mapToServiceResponse(directedSendRegistrationInfo);
		assertEquals(directedSendRegistrationFieldsResponse.getFqdoInfo().getDeliveryOption(),
				directedSendRegistrationInfo.getFqdoInfo().getDeliveryOption());
		assertEquals(directedSendRegistrationFieldsResponse.getFqdoInfo().getReceiveAgentID(),
				directedSendRegistrationInfo.getFqdoInfo().getReceiveAgentID());
		assertEquals(directedSendRegistrationFieldsResponse.getFqdoInfo().getReceiveCountry(),
				directedSendRegistrationInfo.getFqdoInfo().getReceiveCountry());
		assertEquals(directedSendRegistrationFieldsResponse.getFqdoInfo().getReceiveCurrency(),
				directedSendRegistrationInfo.getFqdoInfo().getReceiveCurrency());
		RegistrationFieldInfo[] fieldInfos = directedSendRegistrationFieldsResponse
				.getRegistrationFieldInfo();
		for (int i = 0; i < fieldInfos.length; i++) {
			List regFieldExpected = (List) directedSendRegistrationInfo.getRegistrationFieldData();
			assertEquals(fieldInfos[i].getDefaultValue(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getDefaultValue());
			assertEquals(fieldInfos[i].getDisplayOrder(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getDisplayOrder());
			assertEquals(fieldInfos[i].getExampleFormat(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getExampleFormat());
			assertEquals(fieldInfos[i].getFieldLabel(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldLabel());
			assertEquals(fieldInfos[i].getFieldMax(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldMax());
			assertEquals(fieldInfos[i].getFieldMin(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldMin());
			assertEquals(fieldInfos[i].getFieldScale(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldScale());
			assertEquals(fieldInfos[i].getFieldValue(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getFieldValue());
			assertEquals(fieldInfos[i].getValidationRegEx(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getValidationRegEx());
			assertEquals(fieldInfos[i].getXmlTag(),
					((com.moneygram.drs.bo.RegistrationFieldInfo) regFieldExpected.get(i))
							.getXmlTag());
			// TODO add enumerated values check.... currently enumerated values
			// is null
		}
	}

	private DirectedSendRegistrationInfo createResponse() throws Exception {
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		FQDOInfo infoExpected = ObjectMaster.getFQDOInfo();
		DirectedSendRegistrationInfo directedSendRegistrationInfo = new DirectedSendRegistrationInfo();
		directedSendRegistrationInfo.setFqdoInfo(infoExpected);
		/*directedSendRegistrationInfo.setRegistrationFieldData(expectedRegistrationFieldsCollection
				.getFullList());*/
		return directedSendRegistrationInfo;
	}
}
