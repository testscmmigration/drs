package com.moneygram.drs.service.v1.mappers;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.CountryInfo;

public class CountryInfoMapperTest extends TestCase {
	public void testMapToCommandResponse() {
		List countryInfoList = createResponse();
		CountryInfoMapper countryInfoMapper = new CountryInfoMapper();
		com.moneygram.drs.service.v1.CountryInfoResponse countryInfoResponseWeb = (com.moneygram.drs.service.v1.CountryInfoResponse) countryInfoMapper
				.mapToServiceResponse(countryInfoList);
		com.moneygram.drs.service.v1.CountryInfo[] countryInfoWeb = countryInfoResponseWeb
				.getConutryInfoArray();
		assertEquals(countryInfoList.size(), countryInfoWeb.length);
		assertEquals("TESTCOUNTRYCODE", countryInfoWeb[0].getCountryCode());
		assertEquals("TESTCOUNTRYLEGACYCODE", countryInfoWeb[0].getCountryLegacyCode());
		assertEquals("TESTCOUNTRYNAME", countryInfoWeb[0].getCountryName());
		assertTrue(countryInfoWeb[0].isDirectedSendCountry());
		assertTrue(countryInfoWeb[0].isMgDirectedSendCountry());
		assertTrue(countryInfoWeb[0].isSendActive());
		assertTrue(countryInfoWeb[0].isReceiveActive());
	}

	private List createResponse() {
		List countryInfoList = new ArrayList();
		CountryInfo countryInfo = new CountryInfo();
		countryInfo.setCountryCode("TESTCOUNTRYCODE");
		countryInfo.setCountryLegacyCode("TESTCOUNTRYLEGACYCODE");
		countryInfo.setCountryName("TESTCOUNTRYNAME");
		countryInfo.setDirectedSendCountry(true);
		countryInfo.setMgDirectedSendCountry(true);
		countryInfo.setReceiveActive(true);
		countryInfo.setSendActive(true);
		countryInfoList.add(countryInfo);
		return countryInfoList;
	}
}
