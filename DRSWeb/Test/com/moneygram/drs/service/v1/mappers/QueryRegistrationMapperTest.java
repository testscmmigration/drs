package com.moneygram.drs.service.v1.mappers;

import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.RegistrationInfo;
import com.moneygram.drs.service.v1.QueryRegistrationsRequest;
import com.moneygram.drs.service.v1.QueryRegistrationsResponse;
import com.moneygram.drs.util.ObjectMaster;

public class QueryRegistrationMapperTest extends TestCase {
	public void testMapToCommandRequest() {
		QueryRegistrationsRequest queryRegistrationsRequest = createRequest();
		QueryRegistrationMapper queryRegistrationMapper = new QueryRegistrationMapper();
		com.moneygram.drs.request.QueryRegistrationsRequest queryRegistrationsRequestEJB = (com.moneygram.drs.request.QueryRegistrationsRequest) queryRegistrationMapper
				.mapToCommandRequest(queryRegistrationsRequest);
		assertEquals("1234", queryRegistrationsRequestEJB.getAccountNumberLast4());
		assertEquals("receiveAgentID", queryRegistrationsRequestEJB.getReceiveAgentID());
		assertEquals("deliveryOption", queryRegistrationsRequestEJB.getDeliveryOption());
		assertEquals("receiveCountry", queryRegistrationsRequestEJB.getReceiveCountry());
		assertEquals("USD", queryRegistrationsRequestEJB.getReceiveCurrency());
		assertEquals("receiverFirstName", queryRegistrationsRequestEJB.getReceiverFirstName());
		assertEquals("receiverLastName", queryRegistrationsRequestEJB.getReceiverLastName());
		assertEquals("123456789", queryRegistrationsRequestEJB.getReceiverPhoneNumber());
	}

	private QueryRegistrationsRequest createRequest() {
		QueryRegistrationsRequest queryRegistrationsRequest = new QueryRegistrationsRequest();
		queryRegistrationsRequest.setAccountNumberLast4("1234");
		queryRegistrationsRequest.setReceiveAgentID("receiveAgentID");
		queryRegistrationsRequest.setDeliveryOption("deliveryOption");
		queryRegistrationsRequest.setReceiveCountry("receiveCountry");
		queryRegistrationsRequest.setReceiveCurrency("USD");
		queryRegistrationsRequest.setReceiverFirstName("receiverFirstName");
		queryRegistrationsRequest.setReceiverLastName("receiverLastName");
		queryRegistrationsRequest.setReceiverPhoneNumber("123456789");
		return queryRegistrationsRequest;
	}

	public void testMapToCommandResponse() {
		int i = 0;
		List registrationInfoEJBList = ObjectMaster.getRegistrationInfo();
		QueryRegistrationMapper queryRegistrationMapper = new QueryRegistrationMapper();
		QueryRegistrationsResponse queryRegistrationsResponse = (QueryRegistrationsResponse) queryRegistrationMapper
				.mapToServiceResponse(registrationInfoEJBList);
		com.moneygram.drs.service.v1.RegistrationInfo[] registrationInfoWeb = queryRegistrationsResponse
				.getRegistrationInfo();
		for (Iterator iter = registrationInfoEJBList.iterator(); iter.hasNext();) {
			RegistrationInfo registrationInfoEJB = (RegistrationInfo) iter.next();
			com.moneygram.drs.service.v1.RegistrationInfo registrationInfo = registrationInfoWeb[i++];
			assertEquals(registrationInfoEJB.getCreatorFirstName(), registrationInfo
					.getCreatorFirstName());
			assertEquals(registrationInfoEJB.getCreatorLastName(), registrationInfo
					.getCreatorLastName());
			assertEquals(registrationInfoEJB.getMgCustomerReceiveNumber(), registrationInfo
					.getMgCustomerReceiveNumber());
			assertEquals(registrationInfoEJB.getMgCustomerReceiveNumberVersion(), registrationInfo
					.getMgCustomerReceiveNumberVersion());
			assertEquals(registrationInfoEJB.getReceiverFirstName(), registrationInfo
					.getReceiverFirstName());
			assertEquals(registrationInfoEJB.getReceiverLastName(), registrationInfo
					.getReceiverLastName());
			assertEquals(registrationInfoEJB.getReceiverPhoneNumber(), registrationInfo
					.getReceiverPhoneNumber());
			FQDOInfo fqdoInfoEJB = registrationInfoEJB.getFqdoInfo();
			com.moneygram.drs.service.v1.FQDOInfo fqdoInfoWeb = registrationInfo.getFqdoInfoResp();
			assertEquals(fqdoInfoEJB.getDeliveryOption(), fqdoInfoWeb.getDeliveryOption());
		}
	}
}
