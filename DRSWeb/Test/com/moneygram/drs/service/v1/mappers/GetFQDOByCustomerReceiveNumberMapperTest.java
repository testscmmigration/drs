package com.moneygram.drs.service.v1.mappers;

import junit.framework.TestCase;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.service.v1.FQDOInfo;
import com.moneygram.drs.service.v1.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v1.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.drs.util.ObjectMaster;

public class GetFQDOByCustomerReceiveNumberMapperTest extends TestCase {
	public void testMapToCommandRequest() {
		GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest = new GetFQDOByCustomerReceiveNumberRequest();
		getFQDOByCustomerReceiveNumberRequest.setMgCustomerReceiveNumber("MG12345");
		getFQDOByCustomerReceiveNumberRequest.setLanguage("Eng");
		GetFQDOByCustomerReceiveNumberMapper getFQDOByCustomerReceiveNumberMapper = new GetFQDOByCustomerReceiveNumberMapper();
		com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequestEJB = (com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest) getFQDOByCustomerReceiveNumberMapper
				.mapToCommandRequest(getFQDOByCustomerReceiveNumberRequest);
		assertEquals("MG12345", getFQDOByCustomerReceiveNumberRequestEJB
				.getMgCustomerReceiveNumber());
		assertEquals("Eng", getFQDOByCustomerReceiveNumberRequestEJB.getLanguage());
	}

	public void testMapToCommandResponse() {
		FullFQDOWithRegistrationStatus fqdoByCustomerReceiveNumber = ObjectMaster
				.getFqdoByCustomerReceiveNumber();
		GetFQDOByCustomerReceiveNumberMapper fqdoByCustomerReceiveNumberMapper = new GetFQDOByCustomerReceiveNumberMapper();
		GetFQDOByCustomerReceiveNumberResponse getFQDOByCustomerReceiveNumberResponse = (GetFQDOByCustomerReceiveNumberResponse) fqdoByCustomerReceiveNumberMapper
				.mapToServiceResponse(fqdoByCustomerReceiveNumber);
		FQDOInfo fqdoInfoWeb = getFQDOByCustomerReceiveNumberResponse.getFqdoInfo();
		assertEquals("USD", fqdoInfoWeb.getReceiveCurrency());
		assertEquals("Rec Agent Name", fqdoInfoWeb.getReceiveAgentName());
		assertEquals("12345678", fqdoInfoWeb.getReceiveAgentID());
		assertEquals("Foo Display", fqdoInfoWeb.getDeliveryOptionDisplayName());
		assertEquals("CAMB_PLUS", fqdoInfoWeb.getDeliveryOption());
	}
}
