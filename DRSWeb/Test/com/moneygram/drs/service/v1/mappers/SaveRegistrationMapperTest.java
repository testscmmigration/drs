package com.moneygram.drs.service.v1.mappers;

import junit.framework.TestCase;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.bo.SaveRegistrationCommandResponse;
import com.moneygram.drs.service.v1.KeyValuePair;
import com.moneygram.drs.service.v1.RegistrationStatus;
import com.moneygram.drs.service.v1.SaveRegistrationRequest;
import com.moneygram.drs.service.v1.SaveRegistrationResponse;

public class SaveRegistrationMapperTest extends TestCase {
	public void testMapToCommandRequest() {
		SaveRegistrationRequest saveRegistrationRequest = createRequest();
		SaveRegistrationMapper saveRegistrationMapper = new SaveRegistrationMapper();
		com.moneygram.drs.request.SaveRegistrationRequest saveRegistrationRequestEJB = (com.moneygram.drs.request.SaveRegistrationRequest) saveRegistrationMapper
				.mapToCommandRequest(saveRegistrationRequest);
		assertEquals("12345678", saveRegistrationRequestEJB.getCustomerReceiveNumber());
		assertEquals("DeliveryOption", saveRegistrationRequestEJB.getDeliveryOption());
		assertEquals("ENG", saveRegistrationRequestEJB.getLanguage());
		assertEquals("RecvAgentID", saveRegistrationRequestEJB.getReceiveAgentID());
		assertEquals("USA", saveRegistrationRequestEJB.getReceiveCountry());
		assertEquals("USD", saveRegistrationRequestEJB.getReceiveCurrency());
		assertEquals(RegistrationStatusCode.ACTIVE.getName(), saveRegistrationRequestEJB
				.getRegistrationStatus());
		assertEquals(Boolean.TRUE.booleanValue(), saveRegistrationRequestEJB.isSuperUser());
	}

	private SaveRegistrationRequest createRequest() {
		SaveRegistrationRequest saveRegistrationRequest = new SaveRegistrationRequest();
		saveRegistrationRequest.setCustomerReceiveNumber("12345678");
		saveRegistrationRequest.setSuperUser(true);
		saveRegistrationRequest.setDeliveryOption("DeliveryOption");
		saveRegistrationRequest.setLanguage("ENG");
		saveRegistrationRequest.setReceiveAgentID("RecvAgentID");
		saveRegistrationRequest.setReceiveCountry("USA");
		saveRegistrationRequest.setReceiveCurrency("USD");
		saveRegistrationRequest.setRegistrationStatus(RegistrationStatus.ACTIVE);
		saveRegistrationRequest.setSuperUser(true);
		KeyValuePair[] keyValuePairsArray = new KeyValuePair[3];
		for (int i = 0; i < 3; i++) {
			KeyValuePair keyValuePair = new KeyValuePair();
			keyValuePair.setFieldValue("value" + i);
			keyValuePair.setXmlTag("Tag1" + i);
			keyValuePairsArray[i] = keyValuePair;
		}
		saveRegistrationRequest.setFieldValues(keyValuePairsArray);
		return saveRegistrationRequest;
	}

	public void testMapToCommandResponse() {
		SaveRegistrationCommandResponse saveRegistrationCommandResponse = new SaveRegistrationCommandResponse();
		saveRegistrationCommandResponse.setMgCustomerReceiveNumber("12345678");
		saveRegistrationCommandResponse.setMgCustomerReceiveNumberVersion(1);
		saveRegistrationCommandResponse.setOfacSourceID("sourceID");
		saveRegistrationCommandResponse.setOfacStatus(true);
		saveRegistrationCommandResponse.setRegistrationStatusCode(RegistrationStatusCode.ACTIVE);
		saveRegistrationCommandResponse.setVerificationRequiredForUse(true);
		SaveRegistrationMapper saveRegistrationMapper = new SaveRegistrationMapper();
		SaveRegistrationResponse saveRegistrationResponse = (SaveRegistrationResponse) saveRegistrationMapper
				.mapToServiceResponse(saveRegistrationCommandResponse);
		assertEquals(saveRegistrationCommandResponse.getMgCustomerReceiveNumber(),
				saveRegistrationResponse.getMgCustomerReceiveNumber());
		assertEquals(saveRegistrationCommandResponse.getMgCustomerReceiveNumberVersion(),
				saveRegistrationResponse.getMgCustomerReceiveNumberVersion().intValue());
		assertEquals(saveRegistrationCommandResponse.getOfacSourceID(), saveRegistrationResponse
				.getOfacSourceID());
		assertEquals(saveRegistrationCommandResponse.getRegistrationStatusCode().getName(),
				saveRegistrationResponse.getRegistrationStatusCode().getValue());
	}
}
