package com.moneygram.drs.service.v4.mappers;

import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.SaveRegistrationResponse;
import com.moneygram.drs.service.v4.SaveRegistrationResponse1808;

public class SaveRegistrationMapper1808 extends SaveRegistrationMapper
		implements DRSMapper {
	public SaveRegistrationRequest mapToCommandRequest(Request2 serviceRequest) {
		SaveRegistrationRequest ejbRequest = super
				.mapToCommandRequest(serviceRequest);
		ejbRequest.setSupportsMultipleErrors(true);
		return ejbRequest;
	}

	protected SaveRegistrationResponse createResponse() {
		SaveRegistrationResponse1808 saveRegistrationResponse = new SaveRegistrationResponse1808();
		return saveRegistrationResponse;
	}
}
