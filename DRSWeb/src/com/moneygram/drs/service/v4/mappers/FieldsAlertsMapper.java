package com.moneygram.drs.service.v4.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.request.FieldsAlertsRequest;
import com.moneygram.drs.service.v4.AlertFieldsInfo;
import com.moneygram.drs.service.v4.DataTypeCodeEnum;
import com.moneygram.drs.service.v4.FieldsAlertsResponse;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.Response;

public class FieldsAlertsMapper implements DRSMapper {

	@Override
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		com.moneygram.drs.service.v4.FieldsAlertsRequest v4FieldAlertsRequest = (com.moneygram.drs.service.v4.FieldsAlertsRequest)serviceRequest;
		FieldsAlertsRequest fieldsAlertsRequest = new FieldsAlertsRequest(
				v4FieldAlertsRequest.getLanguage(),
				v4FieldAlertsRequest.getReceiveCountry(),
				v4FieldAlertsRequest.getDeliveryOption(),
				v4FieldAlertsRequest.getReceiveAgentID(),
				v4FieldAlertsRequest.getReceiveCurrency(),
				v4FieldAlertsRequest.getCustomerReceiveNumber(),
				v4FieldAlertsRequest.isExternalReceiverProfile());
		return fieldsAlertsRequest;
	}

	@Override
	public Response mapToServiceResponse(Object commandResponse) {
		Collection<com.moneygram.drs.service.v4.ErrorFieldInfo> errorFieldInfo = new ArrayList<com.moneygram.drs.service.v4.ErrorFieldInfo>();
		AlertRegistrationInfo alertRegistrationInfo = (AlertRegistrationInfo) commandResponse;
		FieldsAlertsResponse fieldsAlertsResponse = new FieldsAlertsResponse();

		Collection alertErrorInfo = alertRegistrationInfo.getErrorData();
		for (Iterator iter = alertErrorInfo.iterator(); iter.hasNext();) {
			AlertErrorInfo alertErrorInfoEJB = (AlertErrorInfo) iter.next();
			com.moneygram.drs.service.v4.ErrorFieldInfo errorFieldInfoWeb = new com.moneygram.drs.service.v4.ErrorFieldInfo();
			errorFieldInfoWeb.setErrorCode(alertErrorInfoEJB.getErrorCode());
			errorFieldInfoWeb.setErrorSource(alertErrorInfoEJB.getErrorSource());
			errorFieldInfoWeb.setErrorCategoryCode(alertErrorInfoEJB.getErrorCategoryCode());
			errorFieldInfoWeb.setAlertReasonCode(alertErrorInfoEJB.getAlertReasonCode());
			errorFieldInfoWeb.setPersistenceFlag(alertErrorInfoEJB.getPersistenceFlag());
		
			
			RegistrationFieldInfo extendedRegistrationFieldInfoEJB = (RegistrationFieldInfo) alertErrorInfoEJB.getRegistrationFieldData();
			com.moneygram.drs.service.v4.RegistrationFieldInfo registrationFieldInfoWeb = new com.moneygram.drs.service.v4.RegistrationFieldInfo();
				registrationFieldInfoWeb.setDefaultValue(extendedRegistrationFieldInfoEJB
						.getDefaultValue());
				registrationFieldInfoWeb.setDisplayOrder(extendedRegistrationFieldInfoEJB
						.getDisplayOrder());
				registrationFieldInfoWeb.setEnumerated(extendedRegistrationFieldInfoEJB.isEnumerated());
				registrationFieldInfoWeb.setExampleFormat(extendedRegistrationFieldInfoEJB
						.getExampleFormat());
				registrationFieldInfoWeb
				        .setExampleFormatID(extendedRegistrationFieldInfoEJB.getExampleFormatID());
				registrationFieldInfoWeb
						.setExtendedHelpTextID(extendedRegistrationFieldInfoEJB.getExtendedHelpTextID());
				registrationFieldInfoWeb
						.setFieldLabel(extendedRegistrationFieldInfoEJB.getFieldLabel());
				registrationFieldInfoWeb.setFieldMax(extendedRegistrationFieldInfoEJB.getFieldMax());
				registrationFieldInfoWeb.setFieldMin(extendedRegistrationFieldInfoEJB.getFieldMin());
				registrationFieldInfoWeb
						.setFieldScale(extendedRegistrationFieldInfoEJB.getFieldScale());
				registrationFieldInfoWeb
						.setFieldValue(extendedRegistrationFieldInfoEJB.getFieldValue());
				registrationFieldInfoWeb.setHidden(extendedRegistrationFieldInfoEJB.isHidden());
				registrationFieldInfoWeb.setReadOnly(extendedRegistrationFieldInfoEJB.isReadOnly());
				registrationFieldInfoWeb.setRequired(extendedRegistrationFieldInfoEJB.isRequired());
				registrationFieldInfoWeb.setValidationRegEx(extendedRegistrationFieldInfoEJB
						.getValidationRegEx());
				registrationFieldInfoWeb.setXmlTag(extendedRegistrationFieldInfoEJB.getXmlTag());
				registrationFieldInfoWeb.setDataTypeCodeEnum(DataTypeCodeEnum
						.fromString(extendedRegistrationFieldInfoEJB.getDataTypeCodeEnum().getName()));
				registrationFieldInfoWeb.setCategory(extendedRegistrationFieldInfoEJB.getCategory());
				registrationFieldInfoWeb.setIncludeInInterTransaction(extendedRegistrationFieldInfoEJB.getIncludeInInterTransaction());
				
				Collection enumeratedValueListEJB = extendedRegistrationFieldInfoEJB
						.getEnumeratedValues();
				if (enumeratedValueListEJB != null) {
					Collection<com.moneygram.drs.service.v4.EnumeratedValueInfo> enumeratedValueWebList = new ArrayList<com.moneygram.drs.service.v4.EnumeratedValueInfo>();
					for (Iterator iterator = enumeratedValueListEJB.iterator(); iterator.hasNext();) {
						EnumeratedValueInfo enumeratedValueInfoEJB = (EnumeratedValueInfo) iterator
								.next();
						com.moneygram.drs.service.v4.EnumeratedValueInfo enumeratedValueInfoWeb = new com.moneygram.drs.service.v4.EnumeratedValueInfo();
						enumeratedValueInfoWeb.setLabel(enumeratedValueInfoEJB.getLabel());
						enumeratedValueInfoWeb.setValue(enumeratedValueInfoEJB.getValue());
						enumeratedValueWebList.add(enumeratedValueInfoWeb);
					}
					registrationFieldInfoWeb
							.setEnumeratedValues((com.moneygram.drs.service.v4.EnumeratedValueInfo[]) enumeratedValueWebList
									.toArray(new com.moneygram.drs.service.v4.EnumeratedValueInfo[enumeratedValueListEJB
											.size()]));
				
				}
				if ((registrationFieldInfoWeb.getXmlTag()) != null) {
				errorFieldInfoWeb.setRegistrationFieldInfo(registrationFieldInfoWeb);
				errorFieldInfo.add(errorFieldInfoWeb);						
				}
			
		}	
		AlertFieldsInfo alertFieldInfo = new AlertFieldsInfo();
		alertFieldInfo.setAlertActionCode(alertRegistrationInfo.getAlertActionCode());
		alertFieldInfo.setAlertCategoryCode(alertRegistrationInfo.getAlertCategoryCode());
		alertFieldInfo.setAlertSubCategoryCode(alertRegistrationInfo.getAlertSubCategoryCode());
		alertFieldInfo.setErrorFieldInfo((com.moneygram.drs.service.v4.ErrorFieldInfo[]) errorFieldInfo
				.toArray(new com.moneygram.drs.service.v4.ErrorFieldInfo[errorFieldInfo.size()]));
		
		fieldsAlertsResponse.setAlertFieldsInfo(alertFieldInfo);	
	
		
		return fieldsAlertsResponse;
	}

}
