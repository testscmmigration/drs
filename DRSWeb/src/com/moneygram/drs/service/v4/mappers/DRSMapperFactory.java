package com.moneygram.drs.service.v4.mappers;

import java.util.HashMap;
import java.util.Map;

import com.moneygram.drs.service.v4.CountryInfoRequest;
import com.moneygram.drs.service.v4.CustomerProfileRequest;
import com.moneygram.drs.service.v4.DirectedSendFieldsRequest;
import com.moneygram.drs.service.v4.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.service.v4.FQDOsForCountryRequest;
import com.moneygram.drs.service.v4.FieldsAlertsRequest;
import com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v4.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v4.QueryRegistrationsRequest;
import com.moneygram.drs.service.v4.RegistrationFieldsAlertsRequest;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.SaveRegistrationRequest;
import com.moneygram.drs.service.v4.SaveRegistrationRequest1808;
import com.moneygram.drs.service.v4.SimplifiedQueryRegistrationRequest;

public class DRSMapperFactory {
	private static Map<Class, DRSMapper> mapOfMappers = new HashMap<Class, DRSMapper>();
	static {
		mapOfMappers.put(DirectedSendRegistrationFieldsRequest.class,
				new DirectedSendRegistrationFieldsMapper());		
		mapOfMappers.put(FQDOsForCountryRequest.class, new FQDOsForCountryrMapper());
		mapOfMappers.put(GetFQDOByCustomerReceiveNumberRequest.class,
				new GetFQDOByCustomerReceiveNumberMapper());
		mapOfMappers
				.put(SimplifiedQueryRegistrationRequest.class, new SimplifiedQueryRegistrationMapper());
		mapOfMappers.put(SaveRegistrationRequest.class, new SaveRegistrationMapper());
		mapOfMappers.put(QueryRegistrationByCustomerReceiveNumberRequest.class,
				new QueryRegistrationByCustomerReceiveNumberMapper());
		mapOfMappers.put(QueryRegistrationsRequest.class, new QueryRegistrationMapper());
		mapOfMappers.put(CountryInfoRequest.class, new CountryInfoMapper());
		mapOfMappers.put(CustomerProfileRequest.class, new CustomerProfileMapper());
		mapOfMappers.put(RegistrationFieldsAlertsRequest.class,
				new RegistrationFieldsAlertsMapper());		
		mapOfMappers.put(DirectedSendFieldsRequest.class,
				new DirectedSendFieldsMapper());	
		mapOfMappers.put(FieldsAlertsRequest.class,
				new FieldsAlertsMapper());		
		mapOfMappers.put(SaveRegistrationRequest1808.class,
				new SaveRegistrationMapper1808());		
	}

	/**
	 * @param com.moneygram.consumerservice.webservice.nmf.v1.Request
	 * @return
	 */
	public static DRSMapper getMapper(Request2 request) {
		return (DRSMapper) mapOfMappers.get(request.getClass());
	}
}
