package com.moneygram.drs.service.v4.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v4.DataTypeCodeEnum;
import com.moneygram.drs.service.v4.DirectedSendFieldsRequest;
import com.moneygram.drs.service.v4.DirectedSendFieldsResponse;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.Response;
import com.moneygram.drs.util.ObjectUtils;

public class DirectedSendFieldsMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		
		DirectedSendFieldsRequest directedSendRegistrationRequest = (DirectedSendFieldsRequest) serviceRequest;
		com.moneygram.drs.request.DirectedSendFieldsRequest directedSendFieldsRequestEJB = new com.moneygram.drs.request.DirectedSendFieldsRequest();
		directedSendFieldsRequestEJB
				.setDeliveryOption(directedSendRegistrationRequest.getDeliveryOption());
		directedSendFieldsRequestEJB
				.setReceiveAgentID(directedSendRegistrationRequest.getReceiveAgentID());
		directedSendFieldsRequestEJB
				.setReceiveCountry(directedSendRegistrationRequest.getReceiveCountry());
		directedSendFieldsRequestEJB
				.setReceiveCurrency(directedSendRegistrationRequest.getReceiveCurrency());
		directedSendFieldsRequestEJB.setLanguage(directedSendRegistrationRequest
				.getLanguage());
		directedSendFieldsRequestEJB.setCustomerReceiverNumber(directedSendRegistrationRequest.getCustomerReceiverNumber());
		directedSendFieldsRequestEJB.setDisableRegistrationFields(directedSendRegistrationRequest.isDisableRegistrationFields());
		directedSendFieldsRequestEJB.setSupportsComplianceFields(directedSendRegistrationRequest.isSupportsComplianceFields());
		directedSendFieldsRequestEJB.setSkipReceiverPhoneCountryCode(directedSendRegistrationRequest.isSkipReceiverPhoneCountryCode());
		
		
		return directedSendFieldsRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		Collection<com.moneygram.drs.service.v4.RegistrationFieldInfo> extendedRegInfoList = new ArrayList<com.moneygram.drs.service.v4.RegistrationFieldInfo>();
		DirectedSendRegistrationInfo directedSendRegistrationInfo = (DirectedSendRegistrationInfo) commandResponse;
		DirectedSendFieldsResponse directedSendFieldsResponse = new DirectedSendFieldsResponse();
		ObjectUtils.copyProperties(directedSendFieldsResponse.getFqdoInfo(),
				directedSendRegistrationInfo.getFqdoInfo());
		Collection registrationFieldInfo = directedSendRegistrationInfo.getRegistrationFieldData();
		for (Iterator iter = registrationFieldInfo.iterator(); iter.hasNext();) {
			RegistrationFieldInfo extendedRegistrationFieldInfoEJB = (RegistrationFieldInfo) iter
					.next();
			com.moneygram.drs.service.v4.RegistrationFieldInfo registrationFieldInfoWeb = new com.moneygram.drs.service.v4.RegistrationFieldInfo();
			registrationFieldInfoWeb.setDefaultValue(extendedRegistrationFieldInfoEJB
					.getDefaultValue());
			registrationFieldInfoWeb.setDisplayOrder(extendedRegistrationFieldInfoEJB
					.getDisplayOrder());
			registrationFieldInfoWeb.setEnumerated(extendedRegistrationFieldInfoEJB.isEnumerated());
			registrationFieldInfoWeb.setExampleFormat(extendedRegistrationFieldInfoEJB
					.getExampleFormat());
			//PRF 54351: To Add extendedHelpTextID and exampleFormatID in response
			registrationFieldInfoWeb
			        .setExampleFormatID(extendedRegistrationFieldInfoEJB.getExampleFormatID());
			registrationFieldInfoWeb
					.setExtendedHelpTextID(extendedRegistrationFieldInfoEJB.getExtendedHelpTextID());
			registrationFieldInfoWeb
					.setFieldLabel(extendedRegistrationFieldInfoEJB.getFieldLabel());
			registrationFieldInfoWeb.setFieldMax(extendedRegistrationFieldInfoEJB.getFieldMax());
			registrationFieldInfoWeb.setFieldMin(extendedRegistrationFieldInfoEJB.getFieldMin());
			registrationFieldInfoWeb
					.setFieldScale(extendedRegistrationFieldInfoEJB.getFieldScale());
			registrationFieldInfoWeb
					.setFieldValue(extendedRegistrationFieldInfoEJB.getFieldValue());
			registrationFieldInfoWeb.setHidden(extendedRegistrationFieldInfoEJB.isHidden());
			registrationFieldInfoWeb.setReadOnly(extendedRegistrationFieldInfoEJB.isReadOnly());
			registrationFieldInfoWeb.setRequired(extendedRegistrationFieldInfoEJB.isRequired());
			registrationFieldInfoWeb.setValidationRegEx(extendedRegistrationFieldInfoEJB
					.getValidationRegEx());
			registrationFieldInfoWeb.setXmlTag(extendedRegistrationFieldInfoEJB.getXmlTag());
			registrationFieldInfoWeb.setDataTypeCodeEnum(DataTypeCodeEnum
					.fromString(extendedRegistrationFieldInfoEJB.getDataTypeCodeEnum().getName()));
			registrationFieldInfoWeb.setCategory(extendedRegistrationFieldInfoEJB.getCategory());
			registrationFieldInfoWeb.setIncludeInInterTransaction(extendedRegistrationFieldInfoEJB.getIncludeInInterTransaction());
			
			Collection enumeratedValueListEJB = extendedRegistrationFieldInfoEJB
					.getEnumeratedValues();
			if (enumeratedValueListEJB != null) {
				Collection<com.moneygram.drs.service.v4.EnumeratedValueInfo> enumeratedValueWebList = new ArrayList<com.moneygram.drs.service.v4.EnumeratedValueInfo>();
				for (Iterator iterator = enumeratedValueListEJB.iterator(); iterator.hasNext();) {
					EnumeratedValueInfo enumeratedValueInfoEJB = (EnumeratedValueInfo) iterator
							.next();
					com.moneygram.drs.service.v4.EnumeratedValueInfo enumeratedValueInfoWeb = new com.moneygram.drs.service.v4.EnumeratedValueInfo();
					enumeratedValueInfoWeb.setLabel(enumeratedValueInfoEJB.getLabel());
					enumeratedValueInfoWeb.setValue(enumeratedValueInfoEJB.getValue());
					enumeratedValueWebList.add(enumeratedValueInfoWeb);
				}
				registrationFieldInfoWeb
						.setEnumeratedValues((com.moneygram.drs.service.v4.EnumeratedValueInfo[]) enumeratedValueWebList
								.toArray(new com.moneygram.drs.service.v4.EnumeratedValueInfo[enumeratedValueListEJB
										.size()]));
				extendedRegInfoList.add(registrationFieldInfoWeb);
			}
		}
		directedSendFieldsResponse
				.setRegistrationFieldInfo((com.moneygram.drs.service.v4.RegistrationFieldInfo[]) extendedRegInfoList
						.toArray(new com.moneygram.drs.service.v4.RegistrationFieldInfo[extendedRegInfoList
								.size()]));
		
		//TODO: set supplementalFlag
		directedSendFieldsResponse.setSupplementalFlag(Boolean.valueOf(directedSendRegistrationInfo.isSupplementalFlag()));
		
		return directedSendFieldsResponse;
	}

}
