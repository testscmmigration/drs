package com.moneygram.drs.service.v4.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v4.DataTypeCodeEnum;
import com.moneygram.drs.service.v4.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v4.QueryRegistrationByCustomerReceiveNumberResponse;
import com.moneygram.drs.service.v4.RegistrationStatus;
import com.moneygram.drs.service.v4.RegistrationSubStatus;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.Response;
import com.moneygram.drs.util.ObjectUtils;

public class QueryRegistrationByCustomerReceiveNumberMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest = (QueryRegistrationByCustomerReceiveNumberRequest) serviceRequest;
		com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequestEJB = new com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest();
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setMgCustomerReceiveNumber(queryRegistrationByCustomerReceiveNumberRequest
						.getMgCustomerReceiveNumber());
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setSuperUser(queryRegistrationByCustomerReceiveNumberRequest.isSuperUser());
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setLanguage(queryRegistrationByCustomerReceiveNumberRequest.getLanguage());
		queryRegistrationByCustomerReceiveNumberRequestEJB
		.setDisplayFullAccountNumber(queryRegistrationByCustomerReceiveNumberRequest.isDisplayFullAccountNumber());
		return queryRegistrationByCustomerReceiveNumberRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		Collection enumeratedValueListEJB=null;
		com.moneygram.drs.service.v4.RegistrationFieldInfo registrationFieldInfoWeb=null;
		Collection<com.moneygram.drs.service.v4.RegistrationFieldInfo> registrationFieldInfoWebList = new ArrayList<com.moneygram.drs.service.v4.RegistrationFieldInfo>();
		RegistrationData registrationData = (RegistrationData) commandResponse;
		QueryRegistrationByCustomerReceiveNumberResponse queryRegistrationByCustomerReceiveNumberResponse = new QueryRegistrationByCustomerReceiveNumberResponse();
		ObjectUtils.copyProperties(queryRegistrationByCustomerReceiveNumberResponse.getFqdoInfo(),
				registrationData.getFqdoInfo());
		queryRegistrationByCustomerReceiveNumberResponse.setOfacExceptionStateCode(registrationData
				.getOfacExceptionStateCode());
		queryRegistrationByCustomerReceiveNumberResponse.setRegistrationStatus(RegistrationStatus
				.fromValue(registrationData.getRegistrationStatus().getName()));
		queryRegistrationByCustomerReceiveNumberResponse
				.setRegistrationSubStatus(RegistrationSubStatus.fromValue(registrationData
						.getRegistrationSubStatus()));
		queryRegistrationByCustomerReceiveNumberResponse.setCustTranFreqCode(registrationData.getCustTranFreqCode());
		//Added for 30665j AGG3 - Send Infra and Rules
		//queryRegistrationByCustomerReceiveNumberResponse.setRuleProcessAccountNumber(registrationData.getRuleProcessAccountNumber());
		//queryRegistrationByCustomerReceiveNumberResponse.setAccountNumberSensitveID(registrationData.getAccountNumberSensitveID());
		Collection fieldInfoListEJB = registrationData.getRegistrationFieldInfo();
		for (Iterator iter = fieldInfoListEJB.iterator(); iter.hasNext();) {
			List<RegistrationFieldInfo> elementList =  (List<RegistrationFieldInfo>) iter.next();
			for(RegistrationFieldInfo element:elementList){
			 registrationFieldInfoWeb = new com.moneygram.drs.service.v4.RegistrationFieldInfo();
			registrationFieldInfoWeb.setDefaultValue(element.getDefaultValue());
			registrationFieldInfoWeb.setDisplayOrder(element.getDisplayOrder());
			registrationFieldInfoWeb.setEnumerated(element.isEnumerated());
			registrationFieldInfoWeb.setExampleFormat(element.getExampleFormat());
			//PRF 54351: To Add Extended Help Text in response
			registrationFieldInfoWeb.setExampleFormatID(element.getExampleFormatID());
			registrationFieldInfoWeb.setExtendedHelpTextID(element.getExtendedHelpTextID());
			registrationFieldInfoWeb.setFieldLabel(element.getFieldLabel());
			registrationFieldInfoWeb.setFieldMax(element.getFieldMax());
			registrationFieldInfoWeb.setFieldMin(element.getFieldMin());
			registrationFieldInfoWeb.setFieldScale(element.getFieldScale());
			registrationFieldInfoWeb.setFieldValue(element.getFieldValue());
			registrationFieldInfoWeb.setHidden(element.isHidden());
			registrationFieldInfoWeb.setReadOnly(element.isReadOnly());
			registrationFieldInfoWeb.setRequired(element.isRequired());
			registrationFieldInfoWeb.setValidationRegEx(element.getValidationRegEx());
			registrationFieldInfoWeb.setXmlTag(element.getXmlTag());
			registrationFieldInfoWeb.setDataTypeCodeEnum(DataTypeCodeEnum.fromString(element
					.getDataTypeCodeEnum().getName()));
			registrationFieldInfoWeb.setCategory(element.getCategory());
			registrationFieldInfoWeb.setLanguageCode(element.getLanguageCode());
			 enumeratedValueListEJB = element.getEnumeratedValues();
			
			if (enumeratedValueListEJB != null) {
				Collection<com.moneygram.drs.service.v4.EnumeratedValueInfo> enumeratedValueWebList = new ArrayList<com.moneygram.drs.service.v4.EnumeratedValueInfo>();
				for (Iterator iterator = enumeratedValueListEJB.iterator(); iterator.hasNext();) {
					EnumeratedValueInfo enumeratedValueInfoEJB = (EnumeratedValueInfo) iterator
							.next();
					com.moneygram.drs.service.v4.EnumeratedValueInfo enumeratedValueInfoWeb = new com.moneygram.drs.service.v4.EnumeratedValueInfo();
					enumeratedValueInfoWeb.setLabel(enumeratedValueInfoEJB.getLabel());
					enumeratedValueInfoWeb.setValue(enumeratedValueInfoEJB.getValue());
					enumeratedValueWebList.add(enumeratedValueInfoWeb);
				}
				registrationFieldInfoWeb
						.setEnumeratedValues((com.moneygram.drs.service.v4.EnumeratedValueInfo[]) enumeratedValueWebList
								.toArray(new com.moneygram.drs.service.v4.EnumeratedValueInfo[enumeratedValueListEJB
										.size()]));
			}
			registrationFieldInfoWebList.add(registrationFieldInfoWeb);
		}
		}
		queryRegistrationByCustomerReceiveNumberResponse
				.setRegistrationFieldInfo((com.moneygram.drs.service.v4.RegistrationFieldInfo[]) registrationFieldInfoWebList
						.toArray(new com.moneygram.drs.service.v4.RegistrationFieldInfo[fieldInfoListEJB
								.size()]));
		return queryRegistrationByCustomerReceiveNumberResponse;
	}
}
