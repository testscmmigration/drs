package com.moneygram.drs.service.v4.mappers;

import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.drs.service.v4.RegistrationStatus;
import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.Response;
import com.moneygram.drs.util.ObjectUtils;

public class GetFQDOByCustomerReceiveNumberMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest = (com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberRequest) serviceRequest;
		GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequestEJB = new GetFQDOByCustomerReceiveNumberRequest();
		getFQDOByCustomerReceiveNumberRequestEJB
				.setMgCustomerReceiveNumber(getFQDOByCustomerReceiveNumberRequest
						.getMgCustomerReceiveNumber());
		getFQDOByCustomerReceiveNumberRequestEJB.setLanguage(getFQDOByCustomerReceiveNumberRequest
				.getLanguage());
		return getFQDOByCustomerReceiveNumberRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		FullFQDOWithRegistrationStatus fqdoByCustomerReceiveNumber = (FullFQDOWithRegistrationStatus) commandResponse;
		GetFQDOByCustomerReceiveNumberResponse fqdoByCustomerReceiveNumberResponse = new GetFQDOByCustomerReceiveNumberResponse();
		ObjectUtils.copyProperties(fqdoByCustomerReceiveNumberResponse.getFqdoInfo(),
				fqdoByCustomerReceiveNumber.getFqdoInfo());
		fqdoByCustomerReceiveNumberResponse.setRegistrationStatusCode(RegistrationStatus
				.fromValue(fqdoByCustomerReceiveNumber.getRegistrationStatusCode()));
		return fqdoByCustomerReceiveNumberResponse;
	}
}
