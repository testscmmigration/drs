package com.moneygram.drs.service.v4.mappers;

import com.moneygram.drs.request.CommandRequest;

import com.moneygram.drs.service.v4.Request2;
import com.moneygram.drs.service.v4.Response;

public interface DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest);

	public Response mapToServiceResponse(Object commandResponse);
}