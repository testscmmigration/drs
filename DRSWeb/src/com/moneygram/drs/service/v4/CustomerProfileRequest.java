package com.moneygram.drs.service.v4;

public class CustomerProfileRequest extends Request2 implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private String customerReceiverNumber;
	private int customerProfileVersionNumber;
	private boolean displayFullAccountNumber;


	public String getCustomerReceiverNumber() {
		return customerReceiverNumber;
	}

	public void setCustomerReceiverNumber(String customerReceiverNumber) {
		this.customerReceiverNumber = customerReceiverNumber;
	}

	public int getCustomerProfileVersionNumber() {
		return customerProfileVersionNumber;
	}

	public void setCustomerProfileVersionNumber(int customerProfileVersionNumber) {
		this.customerProfileVersionNumber = customerProfileVersionNumber;
	}


	public boolean isDisplayFullAccountNumber() {
		return displayFullAccountNumber;
	}

	public void setDisplayFullAccountNumber(boolean displayFullAccountNumber) {
		this.displayFullAccountNumber = displayFullAccountNumber;
	}
}
