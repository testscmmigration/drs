package com.moneygram.drs.service.v4;

import java.io.Serializable;

/**
 * 
 */
public class CustomerProfile implements Serializable  {
	private static final long serialVersionUID = 1L;
	private String customerReceiveNumber;
	private int customerReceiveNumberVersion;
	private String agentID;
	private int deliveryOptionID;
	private RegistrationStatusCode registrationStatus;
	private boolean ofacStatus;
	private String rcvAgentCustAcctName;
	private String rcvAgentCustAcctNbr;
	private String rcvAgentCustAcctNbrMask;
	private String rcvAgentBankID;
	private String registrationSubStatus;
	private String rcvAgentCustAcctNbrPanFlag = "N";
	private String accountNickname;
	private int poeSourceSystem;
	private CustomerProfilePerson creator;
	private CustomerProfilePerson receiver;
	private CustomerProfilePerson[] jointAccountHolders;
	private CustomerProfileCustomField[] customFields;
	//Added for 30665j AGG3 - Send Infra and Rules
	private String ruleProcessAccountNumber;
	private String accountNumberSensitveID;

	public CustomerProfilePerson getCreator() {
		return creator;
	}

	public void setCreator(CustomerProfilePerson creator) {
		this.creator = creator;
	}

	public CustomerProfilePerson getReceiver() {
		return receiver;
	}

	public void setReceiver(CustomerProfilePerson receiver) {
		this.receiver = receiver;
	}

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public int getCustomerReceiveNumberVersion() {
		return customerReceiveNumberVersion;
	}

	public void setCustomerReceiveNumberVersion(int customerReceiveNumberVersion) {
		this.customerReceiveNumberVersion = customerReceiveNumberVersion;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public int getDeliveryOptionID() {
		return deliveryOptionID;
	}

	public void setDeliveryOptionID(int deliveryOptionID) {
		this.deliveryOptionID = deliveryOptionID;
	}

	public RegistrationStatusCode getRegistrationStatus() {
		return registrationStatus;
	}

	public void setRegistrationStatus(RegistrationStatusCode registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	public boolean getOfacStatus() {
		return ofacStatus;
	}

	public void setOfacStatus(boolean ofacStatus) {
		this.ofacStatus = ofacStatus;
	}

	public String getRcvAgentCustAcctName() {
		return rcvAgentCustAcctName;
	}

	public void setRcvAgentCustAcctName(String rcvAgentCustAcctName) {
		this.rcvAgentCustAcctName = rcvAgentCustAcctName;
	}

	public String getRcvAgentCustAcctNbr() {
		return rcvAgentCustAcctNbr;
	}

	public void setRcvAgentCustAcctNbr(String rcvAgentCustAcctNbr) {
		this.rcvAgentCustAcctNbr = rcvAgentCustAcctNbr;
	}

	public String getRcvAgentCustAcctNbrMask() {
		return rcvAgentCustAcctNbrMask;
	}

	public void setRcvAgentCustAcctNbrMask(String rcvAgentCustAcctNbrMask) {
		this.rcvAgentCustAcctNbrMask = rcvAgentCustAcctNbrMask;
	}

	public String getRcvAgentBankID() {
		return rcvAgentBankID;
	}

	public void setRcvAgentBankID(String rcvAgentBankID) {
		this.rcvAgentBankID = rcvAgentBankID;
	}

	public String getRegistrationSubStatus() {
		return registrationSubStatus;
	}

	public void setRegistrationSubStatus(String registrationSubStatus) {
		this.registrationSubStatus = registrationSubStatus;
	}

	public String getRcvAgentCustAcctNbrPanFlag() {
		return rcvAgentCustAcctNbrPanFlag;
	}

	public void setRcvAgentCustAcctNbrPanFlag(String rcvAgentCustAcctNbrPanFlag) {
		this.rcvAgentCustAcctNbrPanFlag = rcvAgentCustAcctNbrPanFlag;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}

	public int getPoeSourceSystem() {
		return poeSourceSystem;
	}

	public void setPoeSourceSystem(int poeSourceSystem) {
		this.poeSourceSystem = poeSourceSystem;
	}

	public CustomerProfilePerson[] getJointAccountHolders() {
		return jointAccountHolders;
	}

	public void setJointAccountHolders(
			CustomerProfilePerson[] jointAccountHolders) {
		this.jointAccountHolders = jointAccountHolders;
	}

	public CustomerProfileCustomField[] getCustomFields() {
		return customFields;
	}

	public void setCustomFields(CustomerProfileCustomField[] customFields) {
		this.customFields = customFields;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getAccountNumberSensitveID() {
		return accountNumberSensitveID;
	}

	public void setAccountNumberSensitveID(String accountNumberSensitveID) {
		this.accountNumberSensitveID = accountNumberSensitveID;
	}

	public String getRuleProcessAccountNumber() {
		return ruleProcessAccountNumber;
	}

	public void setRuleProcessAccountNumber(String ruleProcessAccountNumber) {
		this.ruleProcessAccountNumber = ruleProcessAccountNumber;
	}
}
