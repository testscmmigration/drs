package com.moneygram.drs.service.v4;

import java.io.Serializable;

public class CustomerProfileCustomField implements Serializable {
	private String xmlTag;
	private String value;

	public CustomerProfileCustomField() {
		super();
	}

	public CustomerProfileCustomField(String xmlTag, String value) {
		this.xmlTag = xmlTag;
		this.value = value;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return
	 */
	public String getXmlTag() {
		return xmlTag;
	}

	/**
	 * @param i
	 */
	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}

	/**
	 * @param string
	 */
	public void setXmlTag(String string) {
		xmlTag = string;
	}
}
