package com.moneygram.drs.service.v4;

public class FieldsAlertsResponse extends com.moneygram.drs.service.v4.Response
		implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private com.moneygram.drs.service.v4.AlertFieldsInfo alertFieldsInfo;

	public FieldsAlertsResponse() {
	}

	public FieldsAlertsResponse(AlertFieldsInfo alertFieldsInfo) {
		super();
		this.alertFieldsInfo = alertFieldsInfo;
	}

	public com.moneygram.drs.service.v4.AlertFieldsInfo getAlertFieldsInfo() {
		return alertFieldsInfo;
	}

	public void setAlertFieldsInfo(
			com.moneygram.drs.service.v4.AlertFieldsInfo alertFieldsInfo) {
		this.alertFieldsInfo = alertFieldsInfo;
	}

}
