package com.moneygram.drs.service.v4;

public class ErrorInfo extends org.apache.axis.AxisFault implements java.io.Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private com.moneygram.drs.service.v4.Error[] errorInfo;
	
	public ErrorInfo(){
	}
	
	public ErrorInfo(com.moneygram.drs.service.v4.Error[] errorInfo) {
		this.errorInfo = errorInfo;
	}

	
	
	 // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ErrorInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/DRSV4", "ErrorInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.moneygram.com/DRSV4", "errorInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/DRSV4", "Error"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.moneygram.com/DRSV4", "item"));
        typeDesc.addFieldDesc(elemField);
    }
	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Writes the exception data to the faultDetails
	 */
	public void writeDetails(javax.xml.namespace.QName qname,
			org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
		context.serialize(qname, null, this);
	}

	public com.moneygram.drs.service.v4.Error[] getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(com.moneygram.drs.service.v4.Error[] errorInfo) {
		this.errorInfo = errorInfo;
	}


}
