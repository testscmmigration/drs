/**
 * FQDOsForCountryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

public class FQDOsForCountryRequest extends Request implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiveCountry;

	public FQDOsForCountryRequest() {
	}

	public FQDOsForCountryRequest(java.lang.String language, java.lang.String receiveCountry) {
		super(language);
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the receiveCountry value for this FQDOsForCountryRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this FQDOsForCountryRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}
}
