/**
 * GetFQDOByCustomerReceiveNumberRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

public class GetFQDOByCustomerReceiveNumberRequest extends com.moneygram.drs.service.v4.Request
		implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String mgCustomerReceiveNumber;
	private java.lang.String language;

	public GetFQDOByCustomerReceiveNumberRequest() {
	}

	public GetFQDOByCustomerReceiveNumberRequest(java.lang.String language,
			java.lang.String mgCustomerReceiveNumber) {
		super(language);
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	/**
	 * Gets the mgCustomerReceiveNumber value for this
	 * GetFQDOByCustomerReceiveNumberRequest.
	 * 
	 * @return mgCustomerReceiveNumber
	 */
	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * Sets the mgCustomerReceiveNumber value for this
	 * GetFQDOByCustomerReceiveNumberRequest.
	 * 
	 * @param mgCustomerReceiveNumber
	 */
	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
