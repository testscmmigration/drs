/**
 * DRSBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Calendar;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.ejb.DRSLocal;
import com.moneygram.drs.ejb.DRSLocalHome;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionWithErrors;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v4.mappers.DRSMapper;
import com.moneygram.drs.service.v4.mappers.DRSMapperFactory;

public class DRSBindingImpl {
	private static Logger log = LogFactory.getInstance().getLogger(DRSBindingImpl.class);

	public com.moneygram.drs.service.v4.DirectedSendRegistrationFieldsResponse directedSendRegistrationFields(
			com.moneygram.drs.service.v4.DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (DirectedSendRegistrationFieldsResponse) satisfyRequest(directedSendRegistrationFieldsRequest);
	}
	
	public com.moneygram.drs.service.v4.DirectedSendFieldsResponse directedSendFields(
			com.moneygram.drs.service.v4.DirectedSendFieldsRequest directedSendFieldsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (DirectedSendFieldsResponse) satisfyRequest(directedSendFieldsRequest);
	}
	
	public com.moneygram.drs.service.v4.FQDOsForCountryResponse getFQDOsForCountry(
			com.moneygram.drs.service.v4.FQDOsForCountryRequest FQDOsForCountryRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (FQDOsForCountryResponse) satisfyRequest(FQDOsForCountryRequest);
	}

	public com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberResponse getFQDOByCustomerReceiveNumber(
			com.moneygram.drs.service.v4.GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (GetFQDOByCustomerReceiveNumberResponse) satisfyRequest(getFQDOByCustomerReceiveNumberRequest);
	}

	public com.moneygram.drs.service.v4.SimplifiedQueryRegistrationResponse simplifiedQueryRegistration(
			com.moneygram.drs.service.v4.SimplifiedQueryRegistrationRequest simplifiedQueryRegistrationRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (SimplifiedQueryRegistrationResponse) satisfyRequest(simplifiedQueryRegistrationRequest);
	}

	public com.moneygram.drs.service.v4.SaveRegistrationResponse saveRegistration(
			com.moneygram.drs.service.v4.SaveRegistrationRequest saveRegistrationRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (SaveRegistrationResponse) satisfyRequest(saveRegistrationRequest);
	}

	public com.moneygram.drs.service.v4.QueryRegistrationByCustomerReceiveNumberResponse queryRegistrationByCustomerReceiveNumber(
			com.moneygram.drs.service.v4.QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest)


			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (QueryRegistrationByCustomerReceiveNumberResponse) satisfyRequest(queryRegistrationByCustomerReceiveNumberRequest);
	}

	public com.moneygram.drs.service.v4.QueryRegistrationsResponse queryRegistrations(
			com.moneygram.drs.service.v4.QueryRegistrationsRequest queryRegistrationsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (QueryRegistrationsResponse) satisfyRequest(queryRegistrationsRequest);


	}

	public CountryInfoResponse getCountryInfo(CountryInfoRequest countryInfoRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (CountryInfoResponse) satisfyRequest(countryInfoRequest);
	}

	public CustomerProfileResponse getCustomerProfile(
			CustomerProfileRequest customerProfileRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (CustomerProfileResponse) satisfyRequest(customerProfileRequest);
	}
	
	public com.moneygram.drs.service.v4.RegistrationFieldsAlertsResponse registrationFieldsAlerts(
			com.moneygram.drs.service.v4.RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (RegistrationFieldsAlertsResponse) satisfyRequest(registrationFieldsAlertsRequest);
		
	}
	
	public com.moneygram.drs.service.v4.FieldsAlertsResponse fieldsAlerts(
			com.moneygram.drs.service.v4.FieldsAlertsRequest fieldsAlertsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (FieldsAlertsResponse) satisfyRequest(fieldsAlertsRequest);
		
	}

	public com.moneygram.drs.service.v4.SaveRegistrationResponse1808 saveRegistration1808(
			com.moneygram.drs.service.v4.SaveRegistrationRequest1808 saveRegistrationRequest1808)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v4.Error {
		return (SaveRegistrationResponse1808) satisfyRequest(saveRegistrationRequest1808);
		
	}

	private Response satisfyRequest(Request2 request) throws com.moneygram.drs.service.v4.Error, com.moneygram.drs.service.v4.ErrorInfo, IllegalArgumentException {
		Response response = null;
		try {
			DRSMapper mapper = DRSMapperFactory.getMapper(request);
			CommandRequest commandRequest = mapper.mapToCommandRequest(request);
			DRSLocal drsLocal = createDRSBean();
			Object result = drsLocal.execute(commandRequest);
			response = mapper.mapToServiceResponse(result);
		}catch (DRSExceptionWithErrors drsExWithErrors) {
			buildAndThrowException(drsExWithErrors);
		} 
		//TODO: Check this is reachable in other flows
		catch (DRSException drs) {
			buildAndThrowException(drs);
		} 
		 catch (Exception e) {
			log.error("Error during satisfyRequest (requestType: " + request.getClass().getName()
					+ "): ", e);
			throw new RuntimeException(e);
		}
		return response;
	}
	

	private void buildAndThrowException(DRSExceptionWithErrors be) throws IllegalArgumentException, ErrorInfo {
		if(be.getExceptionList().isEmpty()){
			return;
		}
		ErrorInfo errors = new ErrorInfo();
		ArrayList<Error> errList = new ArrayList<Error>();
		for(DRSException drsException:be.getExceptionList()){
			Error exception = new Error();
			exception.setDetailString(drsException.getMessage());
			exception.setOffendingField(drsException.getOffendingField());
			exception.setErrorString(drsException.getErrorString());
			exception.setTimeStamp(Calendar.getInstance());
			exception.setFaultString(drsException.getDebugString());
			errors.setFaultString(drsException.getDebugString());
			exception.setLanguage("ENG");
			exception.setFaultActor(drsException.getFaultCode());
			exception.setErrorCode(new BigInteger(new Integer(drsException.getErrorCode()).toString()));
			errList.add(exception);
		}
		Error[] serviceErr = new Error[errList.size()];
		serviceErr = errList.toArray(serviceErr);
		errors.setErrorInfo(serviceErr);
		errors.setFaultActor("Client");
		throw errors;
	}
	
	private void buildAndThrowException(DRSException be) throws IllegalArgumentException, Error {
		Error exception = new Error();
		exception.setDetailString(be.getMessage());
		exception.setOffendingField(be.getOffendingField());
		exception.setErrorString(be.getErrorString());
		exception.setTimeStamp(Calendar.getInstance());
		exception.setFaultString(be.getDebugString());
		exception.setLanguage("ENG");
		exception.setFaultActor(be.getFaultCode());
		exception.setErrorCode(new BigInteger(new Integer(be.getErrorCode()).toString()));
		throw exception;
	}

	private DRSLocal createDRSBean() throws Exception {
		DRSLocalHome drsHome = null;
		DRSLocal drsLocal = null;
		Context ctx = new InitialContext();
		Object o = ctx.lookup("java:comp/env/ejb/DRSHome");
		if (o instanceof String)
			ctx.unbind("java:comp/env/ejb/DRSHome");
		drsHome = (DRSLocalHome) PortableRemoteObject.narrow(o, DRSLocalHome.class);
		drsLocal = drsHome.create();
		return drsLocal;
	}
}
