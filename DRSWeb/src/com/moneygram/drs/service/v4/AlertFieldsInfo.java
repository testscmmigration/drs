/**
 * AlertFieldsInfo.Java
 *
 * This file was created Manually to generate WSDL from server-config.wsdd
 * @author bai2
 */
package com.moneygram.drs.service.v4;


public class AlertFieldsInfo implements  java.io.Serializable  {

	private static final long serialVersionUID = 1L;
	private String alertActionCode;
	private String alertCategoryCode;
	private String alertSubCategoryCode;
	private com.moneygram.drs.service.v4.ErrorFieldInfo[] errors;

	public AlertFieldsInfo() {
		// TODO Auto-generated constructor stub
	}

	public AlertFieldsInfo(java.lang.String alertActionCode,
			java.lang.String alertCategoryCode,
			java.lang.String alertSubCategoryCode,
			ErrorFieldInfo[] errors) {
		this.alertActionCode = alertActionCode;
		this.alertCategoryCode = alertCategoryCode;
		this.alertSubCategoryCode = alertSubCategoryCode;
		this.errors = errors;

	}

	/**
	 * @return the alertActionCode
	 */
	public String getAlertActionCode() {
		return alertActionCode;
	}

	/**
	 * @param alertActionCode
	 *            the alertActionCode to set
	 */
	public void setAlertActionCode(String alertActionCode) {
		this.alertActionCode = alertActionCode;
	}

	/**
	 * @return the alertCategoryCode
	 */
	public String getAlertCategoryCode() {
		return alertCategoryCode;
	}

	/**
	 * @param alertCategoryCode
	 *            the alertCategoryCode to set
	 */
	public void setAlertCategoryCode(String alertCategoryCode) {
		this.alertCategoryCode = alertCategoryCode;
	}

	/**
	 * @return the alertSubCategoryCode
	 */
	public String getAlertSubCategoryCode() {
		return alertSubCategoryCode;
	}

	/**
	 * @param alertSubCategoryCode
	 *            the alertSubCategoryCode to set
	 */
	public void setAlertSubCategoryCode(String alertSubCategoryCode) {
		this.alertSubCategoryCode = alertSubCategoryCode;
	}

	/**
	 * @return the errorFieldInfo
	 */
	public com.moneygram.drs.service.v4.ErrorFieldInfo[] getErrorFieldInfo() {
		return errors;
	}

	/**
	 * @param errors 
	 * the errors to set
	 */
	public void setErrorFieldInfo(
			com.moneygram.drs.service.v4.ErrorFieldInfo[] errors) {
		this.errors = errors;
	}

}
