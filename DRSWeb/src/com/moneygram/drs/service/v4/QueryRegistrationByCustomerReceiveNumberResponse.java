/**
 * QueryRegistrationByCustomerReceiveNumberResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

public class QueryRegistrationByCustomerReceiveNumberResponse extends
		com.moneygram.drs.service.v4.Response implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private com.moneygram.drs.service.v4.FQDOInfo fqdoInfo = new FQDOInfo();
	private java.lang.String ofacExceptionStateCode;
	private RegistrationStatus registrationStatus;
	private RegistrationSubStatus registrationSubStatus;
	private java.lang.String custTranFreqCode;
	private com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo;

	public QueryRegistrationByCustomerReceiveNumberResponse() {
	}

	public QueryRegistrationByCustomerReceiveNumberResponse(
			com.moneygram.drs.service.v4.FQDOInfo fqdoInfo,
			java.lang.String ofacExceptionStateCode, RegistrationStatus registrationStatus,
			RegistrationSubStatus registrationSubStatus,
			com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo,
			java.lang.String custTranFreqCode) {
		this.fqdoInfo = fqdoInfo;
		this.ofacExceptionStateCode = ofacExceptionStateCode;
		this.registrationStatus = registrationStatus;
		this.registrationSubStatus = registrationSubStatus;
		this.registrationFieldInfo = registrationFieldInfo;
		this.custTranFreqCode = custTranFreqCode;
	}

	/**
	 * Gets the fqdoInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return fqdoInfo
	 */
	public com.moneygram.drs.service.v4.FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * Sets the fqdoInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param fqdoInfo
	 */
	public void setFqdoInfo(com.moneygram.drs.service.v4.FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	/**
	 * Gets the ofacExceptionStateCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return ofacExceptionStateCode
	 */
	public java.lang.String getOfacExceptionStateCode() {
		return ofacExceptionStateCode;
	}

	/**
	 * Sets the ofacExceptionStateCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param ofacExceptionStateCode
	 */
	public void setOfacExceptionStateCode(java.lang.String ofacExceptionStateCode) {
		this.ofacExceptionStateCode = ofacExceptionStateCode;
	}

	/**
	 * Gets the registrationStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationStatus
	 */
	public RegistrationStatus getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * Sets the registrationStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationStatus
	 */
	public void setRegistrationStatus(RegistrationStatus registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	/**
	 * Gets the registrationSubStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationSubStatus
	 */
	public RegistrationSubStatus getRegistrationSubStatus() {
		return registrationSubStatus;
	}

	/**
	 * Sets the registrationSubStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationSubStatus
	 */
	public void setRegistrationSubStatus(RegistrationSubStatus registrationSubStatus) {
		this.registrationSubStatus = registrationSubStatus;
	}

	/**
	 * Gets the registrationFieldInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationFieldInfo
	 */
	public com.moneygram.drs.service.v4.RegistrationFieldInfo[] getRegistrationFieldInfo() {
		return registrationFieldInfo;
	}

	/**
	 * Sets the registrationFieldInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationFieldInfo
	 */
	public void setRegistrationFieldInfo(
			com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo) {
		this.registrationFieldInfo = registrationFieldInfo;
	}

	public com.moneygram.drs.service.v4.RegistrationFieldInfo getRegistrationFieldInfo(int i) {
		return this.registrationFieldInfo[i];
	}

	public void setRegistrationFieldInfo(int i,
			com.moneygram.drs.service.v4.RegistrationFieldInfo _value) {
		this.registrationFieldInfo[i] = _value;
	}

	/**
	 * @return the custTranFreqCode
	 */
	public java.lang.String getCustTranFreqCode() {
		return custTranFreqCode;
	}

	/**
	 * @param custTranFreqCode the custTranFreqCode to set
	 */
	public void setCustTranFreqCode(java.lang.String custTranFreqCode) {
		this.custTranFreqCode = custTranFreqCode;
	}
	
}
