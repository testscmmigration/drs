/**
 * DirectedSendRegistrationFieldsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

public class DirectedSendRegistrationFieldsResponse extends com.moneygram.drs.service.v4.Response
		implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private com.moneygram.drs.service.v4.FQDOInfo fqdoInfo = new FQDOInfo();
	private com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo;
	
	private Boolean supplementalFlag;

	public DirectedSendRegistrationFieldsResponse() {
	}

	public DirectedSendRegistrationFieldsResponse(com.moneygram.drs.service.v4.FQDOInfo fqdoInfo,
			com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo) {
		this.fqdoInfo = fqdoInfo;
		this.registrationFieldInfo = registrationFieldInfo;
	}

	public Boolean getSupplementalFlag() {
		return supplementalFlag;
	}

	public void setSupplementalFlag(Boolean supplementalFlag) {
		this.supplementalFlag = supplementalFlag;
	}

	/**
	 * Gets the fqdoInfo value for this DirectedSendRegistrationFieldsResponse.
	 * 
	 * @return fqdoInfo
	 */
	public com.moneygram.drs.service.v4.FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * Sets the fqdoInfo value for this DirectedSendRegistrationFieldsResponse.
	 * 
	 * @param fqdoInfo
	 */
	public void setFqdoInfo(com.moneygram.drs.service.v4.FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	/**
	 * Gets the registrationFieldInfo value for this
	 * DirectedSendRegistrationFieldsResponse.
	 * 
	 * @return registrationFieldInfo
	 */
	public com.moneygram.drs.service.v4.RegistrationFieldInfo[] getRegistrationFieldInfo() {
		return registrationFieldInfo;
	}

	/**
	 * Sets the registrationFieldInfo value for this
	 * DirectedSendRegistrationFieldsResponse.
	 * 
	 * @param registrationFieldInfo
	 */
	public void setRegistrationFieldInfo(
			com.moneygram.drs.service.v4.RegistrationFieldInfo[] registrationFieldInfo) {
		this.registrationFieldInfo = registrationFieldInfo;
	}

	public com.moneygram.drs.service.v4.RegistrationFieldInfo getRegistrationFieldInfo(int i) {
		return this.registrationFieldInfo[i];
	}

	public void setRegistrationFieldInfo(int i,
			com.moneygram.drs.service.v4.RegistrationFieldInfo _value) {
		this.registrationFieldInfo[i] = _value;
	}
}
