package com.moneygram.drs.service.v4;

public class RegistrationFieldsAlertsResponse extends
		com.moneygram.drs.service.v4.Response implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private com.moneygram.drs.service.v4.AlertFieldsInfo alertFieldsInfo;



	public RegistrationFieldsAlertsResponse() {
	}

	public RegistrationFieldsAlertsResponse(
			com.moneygram.drs.service.v4.AlertFieldsInfo alertFieldsInfo) {
		this.alertFieldsInfo = alertFieldsInfo;
	}

	/**
	 * @return the alertFieldsInfo
	 */
	public com.moneygram.drs.service.v4.AlertFieldsInfo getAlertFieldsInfo() {
		return alertFieldsInfo;
	}

	/**
	 * @param alertFieldsInfo
	 *            the alertFieldsInfo to set
	 */
	public void setAlertFieldsInfo(
			com.moneygram.drs.service.v4.AlertFieldsInfo alertFieldsInfo) {
		this.alertFieldsInfo = alertFieldsInfo;
	}

}
