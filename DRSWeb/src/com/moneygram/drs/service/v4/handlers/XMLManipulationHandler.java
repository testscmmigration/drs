package com.moneygram.drs.service.v4.handlers;

import javax.xml.soap.SOAPMessage;

import javax.xml.soap.SOAPPart;
import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.message.MessageElement;
import org.apache.axis.utils.ByteArrayOutputStream;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * Fix Axis error which Adds xmlns="" for all its child elements. Creates new
 * MessageElements and adds namespaces to all child elements and replace the
 * existing MessageElements with newly created ones for all parents.
 * 
 * @author Prasad
 * 
 */

public class XMLManipulationHandler extends BasicHandler {

	private static Logger log = LogFactory.getInstance().getLogger(
			XMLManipulationHandler.class);

	private static final long serialVersionUID = 1L;

	public void invoke(MessageContext context) throws AxisFault {
		log.info(" XmlManipulationHandler V4 Entering");
		SOAPMessage message = context.getResponseMessage();
		SOAPPart soapPart = (SOAPPart) message.getSOAPPart();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String nameSpace = null;

		try {
			message.writeTo(baos);
			baos.flush();
			baos.close();
			log.info("Processing request to remove empty namespaces in XmlManipulationHandler");
			/*
			 * Get all the child elements of errorFieldInfo in the response and
			 * add namespace to each child to remove xmlns=""
			 */
			nameSpace = (message.getSOAPBody()).getFirstChild()
					.getNamespaceURI();
			NodeList it = soapPart.getElementsByTagName("item");

			for (int nodeIndex = 0; nodeIndex < it.getLength(); nodeIndex++) {
				MessageElement element = (MessageElement) it.item(nodeIndex);
				MessageElement errorFieldInfoElement = (MessageElement) element
						.getParentElement();
				processChilds(element, errorFieldInfoElement, nameSpace);
			}

			message.saveChanges();
			baos.reset();

			message.writeTo(baos);
			baos.flush();
			baos.close();
			log.info("Processing completed to remove empty namespaces in XmlManipulationHandler");
		} catch (Exception e) {
			throw new AxisFault(
					"Error Caught processing document in XMLManipulationHandler",
					e);
		}
		log.info(" XmlManipulationHandler V4 Finish");
	}

	public static MessageElement newMessageElement(MessageElement msgElement,
			String nameSpace) throws Exception {

		Element e = ((MessageElement) msgElement).getAsDOM();
		e.removeAttribute("xmlns");
		MessageElement me = new MessageElement(e);
		me.setNamespaceURI(nameSpace);
		return me;
	}

	// Adds namespaces to all child elements in hierarchy
	public void processChilds(MessageElement msgElement,
			MessageElement parentMsgElement, String nameSpace) throws Exception {
		parentMsgElement.removeChild(msgElement);
		MessageElement me = newMessageElement(msgElement, nameSpace);

		if (((me.getChildElements().hasNext()) && (me.getChildren().size() > 1))
				|| ((me.getChildElements().hasNext())
						&& (me.getTagName() == "enumeratedValues") && (me
						.getChildren().size() >= 1))) {
			NodeList nodeList = me.getChildNodes();
			for (int childNodeIndex = 0; childNodeIndex < nodeList.getLength(); childNodeIndex++) {
				MessageElement aChildMsgElement = (MessageElement) nodeList
						.item(childNodeIndex);
				processChilds(aChildMsgElement, me, nameSpace);

			}

		}

		//parentMsgElement.replaceChild(me, msgElement); 
		//parentMsgElement.addTextNode(me.toString());
		parentMsgElement.addChildElement(me);

	}

}
