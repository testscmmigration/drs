/**
 * SaveRegistrationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v4;

import java.math.BigDecimal;

public class SaveRegistrationRequest extends com.moneygram.drs.service.v4.Request implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private boolean superUser;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String customerReceiveNumber;
	private RegistrationStatus registrationStatus;
	private com.moneygram.drs.service.v4.KeyValuePair[] fieldValues;
	private java.lang.String language;
	private int poeSourceSystem;
	private BigDecimal receiveAmount;
	private AgentProfileInfo sendAgentInfo;
	private boolean intraTransaction;
	private java.lang.String apiVersion;
	private String externalReceiverProfileId;
	private String externalReceiverProfileType;

	public SaveRegistrationRequest() {
	}

	public SaveRegistrationRequest(java.lang.String language, java.lang.String receiveCountry,
			java.lang.String deliveryOption, java.lang.String receiveAgentID,
			java.lang.String receiveCurrency, java.lang.String customerReceiveNumber,
			RegistrationStatus registrationStatus,
			com.moneygram.drs.service.v4.KeyValuePair[] fieldValues, boolean superUser,
			int poeSourceSystem, BigDecimal receiveAmount, AgentProfileInfo sendAgentInfo, boolean intraTransaction,java.lang.String apiVersion,String externalReceiverProfileId, String externalReceiverProfileType) {
		super(language);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
		this.customerReceiveNumber = customerReceiveNumber;
		this.registrationStatus = registrationStatus;
		this.fieldValues = fieldValues;
		this.superUser = superUser;
		this.poeSourceSystem = poeSourceSystem;
		this.receiveAmount = receiveAmount;
		this.sendAgentInfo = sendAgentInfo;
		this.intraTransaction = intraTransaction;
		this.apiVersion = apiVersion;
		this.externalReceiverProfileId = externalReceiverProfileId;
		this.externalReceiverProfileType = externalReceiverProfileType;
	}

	/**
	 * Gets the receiveCountry value for this SaveRegistrationRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this SaveRegistrationRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the deliveryOption value for this SaveRegistrationRequest.
	 * 
	 * @return deliveryOption
	 */
	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * Sets the deliveryOption value for this SaveRegistrationRequest.
	 * 
	 * @param deliveryOption
	 */
	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * Gets the receiveAgentID value for this SaveRegistrationRequest.
	 * 
	 * @return receiveAgentID
	 */
	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * Sets the receiveAgentID value for this SaveRegistrationRequest.
	 * 
	 * @param receiveAgentID
	 */
	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	/**
	 * Gets the receiveCurrency value for this SaveRegistrationRequest.
	 * 
	 * @return receiveCurrency
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * Sets the receiveCurrency value for this SaveRegistrationRequest.
	 * 
	 * @param receiveCurrency
	 */
	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * Gets the customerReceiveNumber value for this SaveRegistrationRequest.
	 * 
	 * @return customerReceiveNumber
	 */
	public java.lang.String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	/**
	 * Sets the customerReceiveNumber value for this SaveRegistrationRequest.
	 * 
	 * @param customerReceiveNumber
	 */
	public void setCustomerReceiveNumber(java.lang.String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	/**
	 * Gets the registrationStatus value for this SaveRegistrationRequest.
	 * 
	 * @return registrationStatus
	 */
	public RegistrationStatus getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * Sets the registrationStatus value for this SaveRegistrationRequest.
	 * 
	 * @param registrationStatus
	 */
	public void setRegistrationStatus(RegistrationStatus registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	/**
	 * Gets the fieldValues value for this SaveRegistrationRequest.
	 * 
	 * @return fieldValues
	 */
	public com.moneygram.drs.service.v4.KeyValuePair[] getFieldValues() {
		return fieldValues;
	}

	/**
	 * Sets the fieldValues value for this SaveRegistrationRequest.
	 * 
	 * @param fieldValues
	 */
	public void setFieldValues(com.moneygram.drs.service.v4.KeyValuePair[] fieldValues) {
		this.fieldValues = fieldValues;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	public int getPoeSourceSystem() {
		return poeSourceSystem;
	}

	public void setPoeSourceSystem(int poeSourceSystem) {
		this.poeSourceSystem = poeSourceSystem;
	}

	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}
	
	public AgentProfileInfo getSendAgentInfo() {
		return sendAgentInfo;
	}

	public void setSendAgentInfo(AgentProfileInfo sendAgentInfo) {
		this.sendAgentInfo = sendAgentInfo;
	}
	
	public boolean isIntraTransaction() {
		return intraTransaction;
	}

	public void setIntraTransaction(boolean intraTransaction) {
		this.intraTransaction = intraTransaction;
	}

	/**
	 * @return the apiVersion
	 */
	public java.lang.String getApiVersion() {
		return apiVersion;
	}

	/**
	 * @param apiVersion the apiVersion to set
	 */
	public void setApiVersion(java.lang.String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getExternalReceiverProfileId() {
		return externalReceiverProfileId;
	}

	public void setExternalReceiverProfileId(String externalReceiverProfileId) {
		this.externalReceiverProfileId = externalReceiverProfileId;
	}

	/**
	 * @param externalReceiverProfileType the externalReceiverProfileType to set
	 */
	public void setExternalReceiverProfileType(
			String externalReceiverProfileType) {
		this.externalReceiverProfileType = externalReceiverProfileType;
	}

	/**
	 * @return the externalReceiverProfileType
	 */
	public String getExternalReceiverProfileType() {
		return externalReceiverProfileType;
	}
}
