package com.moneygram.drs.service.v4;

import java.io.Serializable;

public class ReceiverInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	private String accountNickname;
	private String receiverPhoneNumber;
	private String receiverFirstName;
	private String receiverLastName;
	private String mgCustomerReceiveNumber;
	private String accountNumberLastFour;
	private RegistrationStatus registrationStatusCode;
	
	public ReceiverInformation() {
	}

	public ReceiverInformation(String accountNickname,
			String receiverPhoneNumber, String receiverFirstName,
			String receiverLastName, String mgCustomerReceiveNumber,
			String accountNumberLastFour, RegistrationStatus registrationStatusCode) {
		this.accountNickname = accountNickname;
		this.receiverPhoneNumber = receiverPhoneNumber;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		this.accountNumberLastFour = accountNumberLastFour;
		this.registrationStatusCode = registrationStatusCode;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}

	public String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	public void setReceiverPhoneNumber(String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public String getAccountNumberLastFour() {
		return accountNumberLastFour;
	}

	public void setAccountNumberLastFour(String accountNumberLastFour) {
		this.accountNumberLastFour = accountNumberLastFour;
	}

	public RegistrationStatus getRegistrationStatusCode() {
		return registrationStatusCode;
	}

	public void setRegistrationStatusCode(RegistrationStatus registrationStatusCode) {
		this.registrationStatusCode = registrationStatusCode;
	}
}
