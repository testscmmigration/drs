package com.moneygram.drs.service.v3.mappers;

import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v3.Request2;
import com.moneygram.drs.service.v3.Response;

public interface DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest);

	public Response mapToServiceResponse(Object commandResponse);
}