package com.moneygram.drs.service.v3.mappers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.beanutils.BeanUtils;

import com.moneygram.drs.bo.AgentProfileInfo;
import com.moneygram.drs.bo.SaveRegistrationCommandResponse;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v3.KeyValuePair;
import com.moneygram.drs.service.v3.RegistrationStatus;
import com.moneygram.drs.service.v3.Request2;
import com.moneygram.drs.service.v3.Response;
import com.moneygram.drs.service.v3.SaveRegistrationRequest;
import com.moneygram.drs.service.v3.SaveRegistrationResponse;

public class SaveRegistrationMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		SaveRegistrationRequest saveRegistrationRequest = (SaveRegistrationRequest) serviceRequest;
		com.moneygram.drs.request.SaveRegistrationRequest saveRegistrationRequestEJB = new com.moneygram.drs.request.SaveRegistrationRequest();
		saveRegistrationRequestEJB.setSuperUser(saveRegistrationRequest.isSuperUser());
		saveRegistrationRequestEJB.setLanguage(saveRegistrationRequest.getLanguage());
		saveRegistrationRequestEJB.setCustomerReceiveNumber(saveRegistrationRequest
				.getCustomerReceiveNumber());
		saveRegistrationRequestEJB.setDeliveryOption(saveRegistrationRequest.getDeliveryOption());
		saveRegistrationRequestEJB.setLanguage(saveRegistrationRequest.getLanguage());
		saveRegistrationRequestEJB.setReceiveAgentID(saveRegistrationRequest.getReceiveAgentID());
		saveRegistrationRequestEJB.setReceiveCountry(saveRegistrationRequest.getReceiveCountry());
		saveRegistrationRequestEJB.setReceiveCurrency(saveRegistrationRequest.getReceiveCurrency());
		if (saveRegistrationRequest.getRegistrationStatus() != null) {
			saveRegistrationRequestEJB.setRegistrationStatus(saveRegistrationRequest
					.getRegistrationStatus().getValue());
		}
		saveRegistrationRequestEJB.setIntraTransaction(saveRegistrationRequest.isIntraTransaction());
		KeyValuePair[] keyValuePairWebArray = saveRegistrationRequest.getFieldValues();
		Collection<com.moneygram.drs.bo.KeyValuePair> keyValuePairEJBList = new ArrayList<com.moneygram.drs.bo.KeyValuePair>();
		for (int i = 0; i < keyValuePairWebArray.length; i++) {
			KeyValuePair keyValuePair = keyValuePairWebArray[i];
			com.moneygram.drs.bo.KeyValuePair keyValuePairEJB = new com.moneygram.drs.bo.KeyValuePair();
			keyValuePairEJB.setValue(keyValuePair.getFieldValue());
			keyValuePairEJB.setKey(keyValuePair.getXmlTag());
			keyValuePairEJBList.add(keyValuePairEJB);
		}
		saveRegistrationRequestEJB.setFieldValues(keyValuePairEJBList);
		saveRegistrationRequestEJB.setLanguage(saveRegistrationRequest.getLanguage());
		saveRegistrationRequestEJB.setPoeSourceSystem(saveRegistrationRequest.getPoeSourceSystem());
		saveRegistrationRequestEJB.setReceiveAmount(saveRegistrationRequest.getReceiveAmount());
		if(saveRegistrationRequest.getSendAgentInfo()!=null){
			AgentProfileInfo ejbSendAgentInfo = new AgentProfileInfo();
			 ejbSendAgentInfo.setAgentAddressLine1(saveRegistrationRequest.getSendAgentInfo().getAgentAddressLine1());
				ejbSendAgentInfo.setAgentAddressLine2(saveRegistrationRequest.getSendAgentInfo().getAgentAddressLine2());
				ejbSendAgentInfo.setAgentAddressLine3(saveRegistrationRequest.getSendAgentInfo().getAgentAddressLine3());
				ejbSendAgentInfo.setAgentCity(saveRegistrationRequest.getSendAgentInfo().getAgentCity());
				ejbSendAgentInfo.setAgentCountryName(saveRegistrationRequest.getSendAgentInfo().getAgentCountryName());
				ejbSendAgentInfo.setAgentCounty(saveRegistrationRequest.getSendAgentInfo().getAgentCounty());
				ejbSendAgentInfo.setAgentDBA(saveRegistrationRequest.getSendAgentInfo().getAgentDBA());
				ejbSendAgentInfo.setAgentDMA(saveRegistrationRequest.getSendAgentInfo().getAgentDMA());
				ejbSendAgentInfo.setAgentHierarchyLevel(saveRegistrationRequest.getSendAgentInfo().getAgentHierarchyLevel());
				ejbSendAgentInfo.setAgentIso2Country(saveRegistrationRequest.getSendAgentInfo().getAgentIso2Country());
				ejbSendAgentInfo.setAgentIsoCountry(saveRegistrationRequest.getSendAgentInfo().getAgentIsoCountry());
				ejbSendAgentInfo.setAgentName(saveRegistrationRequest.getSendAgentInfo().getAgentName());
				ejbSendAgentInfo.setAgentPhoneNumber(saveRegistrationRequest.getSendAgentInfo().getAgentPhoneNumber());
				ejbSendAgentInfo.setAgentState(saveRegistrationRequest.getSendAgentInfo().getAgentState());
				ejbSendAgentInfo.setAgentStatusCode(saveRegistrationRequest.getSendAgentInfo().getAgentStatusCode());
				ejbSendAgentInfo.setAgentTaxID(saveRegistrationRequest.getSendAgentInfo().getAgentTaxID());
				ejbSendAgentInfo.setAgentTimeZone(saveRegistrationRequest.getSendAgentInfo().getAgentTimeZone());
				ejbSendAgentInfo.setAgentTypeID(saveRegistrationRequest.getSendAgentInfo().getAgentTypeID());
				ejbSendAgentInfo.setAgentZipCode(saveRegistrationRequest.getSendAgentInfo().getAgentZipCode());
				ejbSendAgentInfo.setCustAcctSiteId(saveRegistrationRequest.getSendAgentInfo().getCustAcctSiteId());
				ejbSendAgentInfo.setHQPartyID(saveRegistrationRequest.getSendAgentInfo().getHQPartyID());
				ejbSendAgentInfo.setIsRetailCreditFlag(saveRegistrationRequest.getSendAgentInfo().getIsRetailCreditFlag());
				ejbSendAgentInfo.setLegacyAgentID(saveRegistrationRequest.getSendAgentInfo().getLegacyAgentID());
				ejbSendAgentInfo.setLegacyUnitOffice(saveRegistrationRequest.getSendAgentInfo().getLegacyUnitOffice());
				ejbSendAgentInfo.setLmsAgentID(saveRegistrationRequest.getSendAgentInfo().getLmsAgentID());
				ejbSendAgentInfo.setMainOfficeID(saveRegistrationRequest.getSendAgentInfo().getMainOfficeID());
				ejbSendAgentInfo.setParentAgentID(saveRegistrationRequest.getSendAgentInfo().getParentAgentID());
			saveRegistrationRequestEJB.setSendAgentInfo(ejbSendAgentInfo);
			
		}
		saveRegistrationRequestEJB.setApiVersion(saveRegistrationRequest.getApiVersion());
		return saveRegistrationRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		SaveRegistrationResponse saveRegistrationResponse = new SaveRegistrationResponse();
		SaveRegistrationCommandResponse saveRegistrationCommandResponse = (SaveRegistrationCommandResponse) commandResponse;
		saveRegistrationResponse.setMgCustomerReceiveNumber(saveRegistrationCommandResponse
				.getMgCustomerReceiveNumber());
		saveRegistrationResponse.setMgCustomerReceiveNumberVersion(new BigInteger(new Integer(
				saveRegistrationCommandResponse.getMgCustomerReceiveNumberVersion()).toString()));
		saveRegistrationResponse.setOfacSourceID(saveRegistrationCommandResponse.getOfacSourceID());
		saveRegistrationResponse.setRegistrationStatusCode(RegistrationStatus
				.fromValue(saveRegistrationCommandResponse.getRegistrationStatusCode().getName()));
		saveRegistrationResponse.setVerificationRequiredForUse(saveRegistrationCommandResponse
				.isVerificationRequiredForUse());
		//Commented Against Defect#821
		//saveRegistrationResponse.setOfacStatus(saveRegistrationCommandResponse.isOfacStatus());
		saveRegistrationResponse.setOfacStatus(true);
		saveRegistrationResponse.setAccountNickname(saveRegistrationCommandResponse.getAccountNickname());
		saveRegistrationResponse.setAccountNumberLastFour(saveRegistrationCommandResponse.getAccountNumberLastFour());
		saveRegistrationResponse.setReceiverCustomerSequenceID(saveRegistrationCommandResponse.getReceiverCustomerSequenceID());
		return saveRegistrationResponse;
	}
}
