package com.moneygram.drs.service.v3.mappers;

import java.util.List;

import com.moneygram.drs.bo.ReceiverInformation;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v3.RegistrationStatus;
import com.moneygram.drs.service.v3.Request2;
import com.moneygram.drs.service.v3.Response;
import com.moneygram.drs.service.v3.SimplifiedQueryRegistrationRequest;
import com.moneygram.drs.service.v3.SimplifiedQueryRegistrationResponse;

public class SimplifiedQueryRegistrationMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		SimplifiedQueryRegistrationRequest request = (SimplifiedQueryRegistrationRequest) serviceRequest;
		
		com.moneygram.drs.request.SimplifiedQueryRegistrationRequest queryRequest = new com.moneygram.drs.request.SimplifiedQueryRegistrationRequest();
		queryRequest.setReceiverFirstName(request.getReceiverFirstName());
		queryRequest.setReceiverLastName(request.getReceiverLastName());
		queryRequest.setReceiveCountry(request.getReceiveCountry());
		queryRequest.setDeliveryOption(request.getDeliveryOption());
		queryRequest.setReceiverPhoneNumber(request.getReceiverPhoneNumber());
		queryRequest.setReceiveAgentID(request.getReceiveAgentID());
		queryRequest.setReceiveCurrency(request.getReceiveCurrency());
		queryRequest.setRegistrationCreatorFirstName(request.getRegistrationCreatorFirstName());
		queryRequest.setRegistrationCreatorLastName(request.getRegistrationCreatorLastName());
		queryRequest.setRegistrationCreatorPhoneNumber(request.getRegistrationCreatorPhoneNumber());
		queryRequest.setMgCustomerReceiveNumber(request.getMgCustomerReceiveNumber());
		queryRequest.setMaxRowsToReturn(request.getMaxRowsToReturn());
		queryRequest.setActiveRecordsOnly(request.getActiveRecordsOnly());
		return queryRequest;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		List<ReceiverInformation> records = (List<ReceiverInformation>)commandResponse;
		com.moneygram.drs.service.v3.ReceiverInformation receivers[] = new com.moneygram.drs.service.v3.ReceiverInformation[records.size()];
		
		for (int i = 0; i < records.size(); i++) {
			ReceiverInformation element = records.get(i);
			
			com.moneygram.drs.service.v3.RegistrationStatus status = RegistrationStatus.fromValue(element.getRegistrationStatusCode().getValue());
			
			com.moneygram.drs.service.v3.ReceiverInformation receiverInformation = new com.moneygram.drs.service.v3.ReceiverInformation();
			receiverInformation.setMgCustomerReceiveNumber(element.getMgCustomerReceiveNumber());
			receiverInformation.setAccountNickname(element.getAccountNickname());
			receiverInformation.setReceiverPhoneNumber(element.getReceiverPhoneNumber());
			receiverInformation.setReceiverFirstName(element.getReceiverFirstName());
			receiverInformation.setReceiverLastName(element.getReceiverLastName());
			receiverInformation.setAccountNumberLastFour(element.getAccountNumberLastFour());
			receiverInformation.setRegistrationStatusCode(status);
			receivers[i] = receiverInformation;
		}

		SimplifiedQueryRegistrationResponse queryResponse = new SimplifiedQueryRegistrationResponse();
		queryResponse.setReceiverInformation(receivers);
		return queryResponse;
	}
}
