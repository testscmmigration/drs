package com.moneygram.drs.service.v3.mappers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.moneygram.drs.bo.RegistrationInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v3.QueryRegistrationsRequest;
import com.moneygram.drs.service.v3.QueryRegistrationsResponse;
import com.moneygram.drs.service.v3.Request2;
import com.moneygram.drs.service.v3.Response;
import com.moneygram.drs.util.ObjectUtils;

public class QueryRegistrationMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		QueryRegistrationsRequest queryRegistrationsRequest = (QueryRegistrationsRequest) serviceRequest;
		com.moneygram.drs.request.QueryRegistrationsRequest queryRegistrationsRequestEJB = new com.moneygram.drs.request.QueryRegistrationsRequest();
		queryRegistrationsRequestEJB.setReceiverFirstName(queryRegistrationsRequest
				.getReceiverFirstName());
		queryRegistrationsRequestEJB.setReceiverLastName(queryRegistrationsRequest
				.getReceiverLastName());
		queryRegistrationsRequestEJB.setReceiveCurrency(queryRegistrationsRequest
				.getReceiveCurrency());
		queryRegistrationsRequestEJB.setReceiverPhoneNumber(queryRegistrationsRequest
				.getReceiverPhoneNumber());
		queryRegistrationsRequestEJB.setReceiveCountry(queryRegistrationsRequest
				.getReceiveCountry());
		queryRegistrationsRequestEJB.setReceiveAgentID(queryRegistrationsRequest
				.getReceiveAgentID());
		queryRegistrationsRequestEJB.setDeliveryOption(queryRegistrationsRequest
				.getDeliveryOption());
		// TODO this value is not sent from Client... need to check US
		queryRegistrationsRequestEJB.setAccountNumberLast4(queryRegistrationsRequest
				.getAccountNumberLast4());
		queryRegistrationsRequestEJB.setLanguage(queryRegistrationsRequest.getLanguage());
		return queryRegistrationsRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		QueryRegistrationsResponse queryRegistrationsResponse = new QueryRegistrationsResponse();
		List registrationInfoList = (List) commandResponse;
		List<com.moneygram.drs.service.v3.RegistrationInfo> registraionInfoWebList = new ArrayList<com.moneygram.drs.service.v3.RegistrationInfo>();
		for (Iterator iter = registrationInfoList.iterator(); iter.hasNext();) {
			RegistrationInfo registrationInfo = (RegistrationInfo) iter.next();
			com.moneygram.drs.service.v3.RegistrationInfo registrationInfoWeb = new com.moneygram.drs.service.v3.RegistrationInfo();
			ObjectUtils.copyProperties(registrationInfoWeb.getFqdoInfoResp(), registrationInfo
					.getFqdoInfo());
			ObjectUtils.copyProperties(registrationInfoWeb, registrationInfo);
			registraionInfoWebList.add(registrationInfoWeb);
		}
		queryRegistrationsResponse
				.setRegistrationInfo((com.moneygram.drs.service.v3.RegistrationInfo[]) registraionInfoWebList
						.toArray(new com.moneygram.drs.service.v3.RegistrationInfo[registraionInfoWebList
								.size()]));
		return queryRegistrationsResponse;
	}
}
