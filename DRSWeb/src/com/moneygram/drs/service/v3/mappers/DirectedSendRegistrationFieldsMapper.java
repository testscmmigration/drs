package com.moneygram.drs.service.v3.mappers;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Iterator;
import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v3.DataTypeCodeEnum;
import com.moneygram.drs.service.v3.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.service.v3.DirectedSendRegistrationFieldsResponse;
import com.moneygram.drs.service.v3.Request2;
import com.moneygram.drs.service.v3.Response;
import com.moneygram.drs.util.ObjectUtils;

public class DirectedSendRegistrationFieldsMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = (DirectedSendRegistrationFieldsRequest) serviceRequest;
		com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequestEJB = new com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest();
		directedSendRegistrationFieldsRequestEJB
				.setDeliveryOption(directedSendRegistrationFieldsRequest.getDeliveryOption());
		directedSendRegistrationFieldsRequestEJB
				.setReceiveAgentID(directedSendRegistrationFieldsRequest.getReceiveAgentID());
		directedSendRegistrationFieldsRequestEJB
				.setReceiveCountry(directedSendRegistrationFieldsRequest.getReceiveCountry());
		directedSendRegistrationFieldsRequestEJB
				.setReceiveCurrency(directedSendRegistrationFieldsRequest.getReceiveCurrency());
		directedSendRegistrationFieldsRequestEJB.setLanguage(directedSendRegistrationFieldsRequest
				.getLanguage());
		return directedSendRegistrationFieldsRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		Collection<com.moneygram.drs.service.v3.RegistrationFieldInfo> extendedRegInfoList = new ArrayList<com.moneygram.drs.service.v3.RegistrationFieldInfo>();
		DirectedSendRegistrationInfo directedSendRegistrationInfo = (DirectedSendRegistrationInfo) commandResponse;
		DirectedSendRegistrationFieldsResponse directedSendRegistrationFieldsResponse = new DirectedSendRegistrationFieldsResponse();
		ObjectUtils.copyProperties(directedSendRegistrationFieldsResponse.getFqdoInfo(),
				directedSendRegistrationInfo.getFqdoInfo());
		Collection registrationFieldInfo = directedSendRegistrationInfo.getRegistrationFieldData();
		for (Iterator iter = registrationFieldInfo.iterator(); iter.hasNext();) {
			RegistrationFieldInfo extendedRegistrationFieldInfoEJB = (RegistrationFieldInfo) iter
					.next();
			com.moneygram.drs.service.v3.RegistrationFieldInfo registrationFieldInfoWeb = new com.moneygram.drs.service.v3.RegistrationFieldInfo();
			registrationFieldInfoWeb.setDefaultValue(extendedRegistrationFieldInfoEJB
					.getDefaultValue());
			registrationFieldInfoWeb.setDisplayOrder(extendedRegistrationFieldInfoEJB
					.getDisplayOrder());
			registrationFieldInfoWeb.setEnumerated(extendedRegistrationFieldInfoEJB.isEnumerated());
			registrationFieldInfoWeb.setExampleFormat(extendedRegistrationFieldInfoEJB
					.getExampleFormat());
			//PRF 54351: To Add extendedHelpTextID and exampleFormatID in response
			registrationFieldInfoWeb
			        .setExampleFormatID(extendedRegistrationFieldInfoEJB.getExampleFormatID());
			registrationFieldInfoWeb
					.setExtendedHelpTextID(extendedRegistrationFieldInfoEJB.getExtendedHelpTextID());
			registrationFieldInfoWeb
					.setFieldLabel(extendedRegistrationFieldInfoEJB.getFieldLabel());
			registrationFieldInfoWeb.setFieldMax(extendedRegistrationFieldInfoEJB.getFieldMax());
			registrationFieldInfoWeb.setFieldMin(extendedRegistrationFieldInfoEJB.getFieldMin());
			registrationFieldInfoWeb
					.setFieldScale(extendedRegistrationFieldInfoEJB.getFieldScale());
			registrationFieldInfoWeb
					.setFieldValue(extendedRegistrationFieldInfoEJB.getFieldValue());
			registrationFieldInfoWeb.setHidden(extendedRegistrationFieldInfoEJB.isHidden());
			registrationFieldInfoWeb.setReadOnly(extendedRegistrationFieldInfoEJB.isReadOnly());
			registrationFieldInfoWeb.setRequired(extendedRegistrationFieldInfoEJB.isRequired());
			registrationFieldInfoWeb.setValidationRegEx(extendedRegistrationFieldInfoEJB
					.getValidationRegEx());
			registrationFieldInfoWeb.setXmlTag(extendedRegistrationFieldInfoEJB.getXmlTag());
			registrationFieldInfoWeb.setDataTypeCodeEnum(DataTypeCodeEnum
					.fromString(extendedRegistrationFieldInfoEJB.getDataTypeCodeEnum().getName()));
			registrationFieldInfoWeb.setCategory(extendedRegistrationFieldInfoEJB.getCategory());
			registrationFieldInfoWeb.setIncludeInInterTransaction(extendedRegistrationFieldInfoEJB.getIncludeInInterTransaction());
			
			Collection enumeratedValueListEJB = extendedRegistrationFieldInfoEJB
					.getEnumeratedValues();
			if (enumeratedValueListEJB != null) {
				Collection<com.moneygram.drs.service.v3.EnumeratedValueInfo> enumeratedValueWebList = new ArrayList<com.moneygram.drs.service.v3.EnumeratedValueInfo>();
				for (Iterator iterator = enumeratedValueListEJB.iterator(); iterator.hasNext();) {
					EnumeratedValueInfo enumeratedValueInfoEJB = (EnumeratedValueInfo) iterator
							.next();
					com.moneygram.drs.service.v3.EnumeratedValueInfo enumeratedValueInfoWeb = new com.moneygram.drs.service.v3.EnumeratedValueInfo();
					enumeratedValueInfoWeb.setLabel(enumeratedValueInfoEJB.getLabel());
					enumeratedValueInfoWeb.setValue(enumeratedValueInfoEJB.getValue());
					enumeratedValueWebList.add(enumeratedValueInfoWeb);
				}
				registrationFieldInfoWeb
						.setEnumeratedValues((com.moneygram.drs.service.v3.EnumeratedValueInfo[]) enumeratedValueWebList
								.toArray(new com.moneygram.drs.service.v3.EnumeratedValueInfo[enumeratedValueListEJB
										.size()]));
				extendedRegInfoList.add(registrationFieldInfoWeb);
			}
		}
		directedSendRegistrationFieldsResponse
				.setRegistrationFieldInfo((com.moneygram.drs.service.v3.RegistrationFieldInfo[]) extendedRegInfoList
						.toArray(new com.moneygram.drs.service.v3.RegistrationFieldInfo[extendedRegInfoList
								.size()]));
		
		//TODO: set supplementalFlag
		directedSendRegistrationFieldsResponse.setSupplementalFlag(Boolean.valueOf(directedSendRegistrationInfo.isSupplementalFlag()));
		
		return directedSendRegistrationFieldsResponse;
	}
}
