package com.moneygram.drs.service.v3.handler;

//import org.apache.axis.components.logger.LogFactory;

import java.util.ArrayList;

import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.message.MessageElement;
import org.apache.axis.utils.ByteArrayOutputStream;
import org.w3c.dom.NodeList;

//import javax.xml.soap.Node;
//import org.w3c.dom.Document;
//import org.apache.axis.message.SOAPEnvelope;
//import org.apache.axis.message.SOAPBodyElement;
//import com.moneygram.common.log.Logger;

public class XMLManipulationHandler extends BasicHandler {
	// private static Log log = LogFactory.getLog(XMLManipulationHandler.class);
	



	/*
	 * protected void treeWalk(Element element) { for (int i = 0, size =
	 * element.nodeCount(); i < size; i++) { Node node = element.node(i); for
	 * (int handlerIndex = 0; handlerIndex < processingHandlers.size();
	 * handlerIndex++) { ProcessingHandler handler = (ProcessingHandler)
	 * processingHandlers.get(handlerIndex); //handler.process(node); } if (node
	 * instanceof Element) { treeWalk((Element) node); } } }
	 */

	/*
	 * protected void treeWalk(SOAPElement element) { //Node node = (Node)
	 * element;
	 * 
	 * NamespaceRemovalHandler handler = new NamespaceRemovalHandler();
	 * handler.process(element);
	 * 
	 * 
	 * 
	 * }
	 */

	public void invoke(MessageContext context) throws AxisFault {

		SOAPMessage message = context.getResponseMessage();
		// message.getSOAPBody().getChildNodes().item(0).getNodeName()
		SOAPPart soapPart = (SOAPPart) message.getSOAPPart();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			message.writeTo(baos);
			// System.out.println(baos.toString());

			// baos.flush();
			// baos.close();
			// NodeList it = message.getSOAPBody().getElementsByTagName("item");

			NodeList it = soapPart.getElementsByTagName("item");
			for (int nodeIndex = 0; nodeIndex < it.getLength(); nodeIndex++) {

				MessageElement element = (MessageElement) it.item(nodeIndex);
				// SOAPElement element = (SOAPElement) it.item(nodeIndex) ;
				MessageElement errorFieldInfoElement = (MessageElement) element
						.getParentElement();
				// SOAPElement errorFieldInfoElement =
				// element.getParentElement();
				// MessageElement newMe = newMessageElement("item", element);
				errorFieldInfoElement.removeChild(element);

				org.w3c.dom.Element e = ((MessageElement) element).getAsDOM();
				e.removeAttribute("xmlns");
				MessageElement me = new MessageElement(e);
				me.setNamespaceURI("http://www.moneygram.com/DRSV3");
				NodeList it1 = me.getChildNodes();
				for (int childNodeIndex = 0; childNodeIndex < it1.getLength(); childNodeIndex++) {
					SOAPElement childElement = (SOAPElement) it1
							.item(childNodeIndex);
					me.removeChild(childElement);
					org.w3c.dom.Element ce = ((MessageElement) childElement)
							.getAsDOM();
					MessageElement cme = new MessageElement(ce);
					cme.setNamespaceURI("http://www.moneygram.com/DRSV3");
					if (childElement.getTagName() == "registrationFieldInfo") {
						NodeList it2 = childElement.getChildNodes();
						for (int regChildNodeIndex = 0; regChildNodeIndex < it2
								.getLength(); regChildNodeIndex++) {
							SOAPElement regChildElement = (SOAPElement) it2
									.item(regChildNodeIndex);
							// cme.removeChild(regChildElement);
							org.w3c.dom.Element regCe = ((MessageElement) regChildElement)
									.getAsDOM();
							MessageElement regCme = new MessageElement(regCe);
							regCme.setNamespaceURI("http://www.moneygram.com/DRSV3");
							cme.replaceChild(regCme, regChildElement);
							// cme.addChildElement(regCme);

						}
					}

					me.addChildElement(cme);
				}
				errorFieldInfoElement.addChildElement(me);
				// errorFieldInfoElement.addTextNode(me.toString());

			}
			message.getSOAPBody().getChildElements().next();

			message.saveChanges();
			baos.reset();

			message.writeTo(baos);
			System.out.println("AFTER" + baos.toString());

		} catch (Exception e) {
			throw new AxisFault(
					"Error Caught processing document in XMLManipulationHandler",
					e);
		}

	}
}
