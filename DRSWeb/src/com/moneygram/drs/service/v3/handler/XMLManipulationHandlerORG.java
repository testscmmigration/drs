package com.moneygram.drs.service.v3.handler;

//import org.apache.axis.components.logger.LogFactory;

import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.SOAPPart;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.message.MessageElement;
import org.apache.axis.utils.ByteArrayOutputStream;
import org.w3c.dom.NodeList;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
/*import java.util.Iterator;

 import javax.xml.soap.SOAPException;
 import javax.xml.soap.SOAPHeader;
 import javax.xml.soap.SOAPHeaderElement;
 import javax.xml.soap.Node;
 import javax.xml.soap.SOAPElement;
 import javax.xml.soap.SOAPPart;
 import org.w3c.dom.Document;
 import org.xml.sax.Attributes;
 import org.xml.sax.InputSource;


 import javax.xml.namespace.QName;
 import javax.xml.soap.MessageFactory;
 import javax.xml.soap.Name;
 import javax.xml.soap.SOAPBody;
 import javax.xml.soap.SOAPBodyElement;
 import javax.xml.soap.SOAPEnvelope;
 import javax.xml.soap.SOAPMessage;*
 import javax.xml.soap.SOAPPart;
 import javax.xml.soap.Text;*/
//import org.jdom.Element;
//import org.dom4j.Document;
//import org.dom4j.Node;
//import org.dom4j.Element;  
//import org.dom4j.io.DocumentSource;
//import org.dom4j.io.SAXReader;
//import com.moneygram.common.log.Logger;

public class XMLManipulationHandlerORG extends BasicHandler {

	private static final long serialVersionUID = 1L;
	private static List processingHandlers;
	private static Logger log = LogFactory.getInstance().getLogger(
			XMLManipulationHandlerORG.class);

	public static void setProcessingHandlers(List handlers) {
		processingHandlers = handlers;
	}

	public void invoke(MessageContext context) throws AxisFault {
		if (context.getPastPivot()) {

			SOAPMessage message = context.getResponseMessage();
			// message.getSOAPBody().getChildNodes().item(0).getNodeName()
			SOAPPart soapPart = (SOAPPart) message.getSOAPPart();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			try {
				message.writeTo(baos);
				// System.out.println(baos.toString());

				// baos.flush();
				// baos.close();
				// NodeList it =
				// message.getSOAPBody().getElementsByTagName("item");

				NodeList it = soapPart.getElementsByTagName("item");
				for (int nodeIndex = 0; nodeIndex < it.getLength(); nodeIndex++) {
					// MessageElement element = (MessageElement)
					// it.item(nodeIndex) ;
					SOAPElement element = (SOAPElement) it.item(nodeIndex);
					// MessageElement errorFieldInfoElement = (MessageElement)
					// element.getParentElement();
					SOAPElement errorFieldInfoElement = element
							.getParentElement();
					// MessageElement newMe = newMessageElement("item",
					// element);
					errorFieldInfoElement.removeChild(element);
					org.w3c.dom.Element e = ((MessageElement) element)
							.getAsDOM();
					e.removeAttribute("xmlns");
					MessageElement me = new MessageElement(e);
					// errorFieldInfoElement.addChildElement(me);
					errorFieldInfoElement.addTextNode(me.toString());
				}

				message.getSOAPBody().getChildElements().next();

				message.saveChanges();
				baos.reset();
				message.writeTo(baos);

			} catch (Exception e) {
				throw new AxisFault(
						"Error Caught processing document in XMLManipulationHandler",
						e);
			}

		}
	}

	/*
	 * @Override public void invoke(MessageContext arg0) throws AxisFault { //
	 * TODO Auto-generated method stub
	 * 
	 * }
	 */

	public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
		return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
		replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
		.replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
		.replaceAll("(</)(\\w+:)(.*?>)", "$1$3"); /* remove closing tags prefix */
	}

	public static MessageElement newMessageElement(String name, Object value)
			throws Exception {
		MessageElement me = new MessageElement("", name);
		me.setObjectValue(value);
		// org.w3c.dom.Element e = me.getAsDOM();
		// e.removeAttribute("xmlns");
		// me = new MessageElement((Name) e);
		return me;
	}

}
