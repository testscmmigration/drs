/*
 * Created on Feb 18, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.moneygram.drs.service.v3;

import java.io.Serializable;

/**
 * @author T958
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TransactionAttributeType implements Serializable {
	private static final long serialVersionUID = 1L;
	private String description;
	private char code;

	private TransactionAttributeType(char code, String description) {
		this.code = code;
		this.description = description;
	}

	public char getCode() {
		return this.code;
	}

	public String getDescription() {
		return this.description;
	}

	public boolean equals(Object other) {
		if (other instanceof TransactionAttributeType) {
			return this.code == ((TransactionAttributeType) other).getCode();
		}
		return false;
	}
	private static final TransactionAttributeType[] allTypes = {
			new TransactionAttributeType('A', "NON-Standard"),
			new TransactionAttributeType('S', "Standard"),
			new TransactionAttributeType('T', "Transaction-Only") };
	public static final TransactionAttributeType NON_STANDARD_TYPE = allTypes[0];
	public static final TransactionAttributeType STANDARD_TYPE = allTypes[1];
	public static final TransactionAttributeType TRAN_ONLY_TYPE = allTypes[2];

	public static TransactionAttributeType getTypeFromCode(char code) {
		for (int i = 0; i < allTypes.length; i++) {
			if (code == allTypes[i].getCode()) {
				return allTypes[i];
			}
		}
		throw new RuntimeException("Unable to find TransactionAttributeType for code " + code);
	}
}
