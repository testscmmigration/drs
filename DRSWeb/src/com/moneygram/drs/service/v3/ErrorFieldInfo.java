/**
 * ErrorFieldInfo.Java
 *
 * This file was created Manually to generate WSDL from server-config.wsdd
 * @author bai2
 */
package com.moneygram.drs.service.v3;

public class ErrorFieldInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String errorCode;
	private java.lang.String errorSource;
	private java.lang.String errorCategoryCode;
	private com.moneygram.drs.service.v3.RegistrationFieldInfo registrationFieldInfo;
	private java.lang.String alertReasonCode;
	private boolean persistenceFlag;

	public ErrorFieldInfo() {
	}

	public ErrorFieldInfo(java.lang.String errorCode,
			java.lang.String errorSource, java.lang.String errorCategoryCode,
			RegistrationFieldInfo registrationFieldInfo,
			String alertReasonCode, Boolean persistenceFlag) {
		this.errorCode = errorCode;
		this.errorSource = errorSource;
		this.errorCategoryCode = errorCategoryCode;
		this.registrationFieldInfo = registrationFieldInfo;
		this.alertReasonCode = alertReasonCode;
		this.persistenceFlag = persistenceFlag;

	}

	/**
	 * @return the errorCode
	 */
	public java.lang.String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(java.lang.String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorSource
	 */
	public java.lang.String getErrorSource() {
		return errorSource;
	}

	/**
	 * @param errorSource
	 *            the errorSource to set
	 */
	public void setErrorSource(java.lang.String errorSource) {
		this.errorSource = errorSource;
	}

	/**
	 * @return the errorCategoryCode
	 */
	public java.lang.String getErrorCategoryCode() {
		return errorCategoryCode;
	}

	/**
	 * @param errorCategoryCode
	 *            the errorCategoryCode to set
	 */
	public void setErrorCategoryCode(java.lang.String errorCategoryCode) {
		this.errorCategoryCode = errorCategoryCode;
	}

	/**
	 * @return the registrationFieldInfo
	 */
	public com.moneygram.drs.service.v3.RegistrationFieldInfo getRegistrationFieldInfo() {
		return registrationFieldInfo;
	}

	/**
	 * @param registrationFieldInfo
	 *            the registrationFieldInfo to set
	 */
	public void setRegistrationFieldInfo(
			com.moneygram.drs.service.v3.RegistrationFieldInfo registrationFieldInfo) {
		this.registrationFieldInfo = registrationFieldInfo;
	}

	/**
	 * @return the alertReasonCode
	 */
	public java.lang.String getAlertReasonCode() {
		return alertReasonCode;
	}

	/**
	 * @param alertReasonCode
	 *            the alertReasonCode to set
	 */
	public void setAlertReasonCode(java.lang.String alertReasonCode) {
		this.alertReasonCode = alertReasonCode;
	}

	/**
	 * @return the persistenceFlag
	 */
	public boolean isPersistenceFlag() {
		return persistenceFlag;
	}

	/**
	 * @param persistenceFlag
	 *            the persistenceFlag to set
	 */
	public void setPersistenceFlag(boolean persistenceFlag) {
		this.persistenceFlag = persistenceFlag;
	}

}
