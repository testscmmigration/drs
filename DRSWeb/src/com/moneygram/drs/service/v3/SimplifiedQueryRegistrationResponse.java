/**
 * SimplifiedQueryRegistrationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v3;

public class SimplifiedQueryRegistrationResponse extends com.moneygram.drs.service.v3.Response
		implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private ReceiverInformation receiverInformation[];
	
	public SimplifiedQueryRegistrationResponse() {
	}

	public SimplifiedQueryRegistrationResponse(ReceiverInformation receiverInformation[]) {
		this.receiverInformation = receiverInformation;
	}

	public ReceiverInformation[] getReceiverInformation() {
		return receiverInformation;
	}

	public void setReceiverInformation(ReceiverInformation[] receiverInformation) {
		this.receiverInformation = receiverInformation;
	}
	
	public ReceiverInformation getReceiverInformation(int i) {
		return receiverInformation[i];
	}
	
	public void setReceiverInformation(int i, ReceiverInformation receiverInformation) {
		this.receiverInformation[i] = receiverInformation;
	}
}
