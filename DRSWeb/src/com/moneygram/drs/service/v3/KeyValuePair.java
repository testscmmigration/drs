/**
 * KeyValuePair.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v3;

public class KeyValuePair implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String xmlTag;
	private java.lang.String fieldValue;

	public KeyValuePair() {
	}

	public KeyValuePair(java.lang.String xmlTag, java.lang.String fieldValue) {
		this.xmlTag = xmlTag;
		this.fieldValue = fieldValue;
	}

	/**
	 * Gets the xmlTag value for this KeyValuePair.
	 * 
	 * @return xmlTag
	 */
	public java.lang.String getXmlTag() {
		return xmlTag;
	}

	/**
	 * Sets the xmlTag value for this KeyValuePair.
	 * 
	 * @param xmlTag
	 */
	public void setXmlTag(java.lang.String xmlTag) {
		this.xmlTag = xmlTag;
	}

	/**
	 * Gets the fieldValue value for this KeyValuePair.
	 * 
	 * @return fieldValue
	 */
	public java.lang.String getFieldValue() {
		return fieldValue;
	}

	/**
	 * Sets the fieldValue value for this KeyValuePair.
	 * 
	 * @param fieldValue
	 */
	public void setFieldValue(java.lang.String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
