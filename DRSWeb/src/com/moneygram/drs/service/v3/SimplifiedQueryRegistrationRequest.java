/**
 * SimplifiedQueryRegistrationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v3;

public class SimplifiedQueryRegistrationRequest extends com.moneygram.drs.service.v3.Request implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String receiverFirstName;
	private java.lang.String receiverLastName;
	private java.lang.String receiverPhoneNumber;
	private java.lang.String registrationCreatorFirstName;
	private java.lang.String registrationCreatorLastName;
	private java.lang.String registrationCreatorPhoneNumber;
	private java.lang.String mgCustomerReceiveNumber;
	private Boolean activeRecordsOnly;
	private Integer maxRowsToReturn;

	public SimplifiedQueryRegistrationRequest() {
	}

	public SimplifiedQueryRegistrationRequest(java.lang.String language,
			java.lang.String receiveCountry, java.lang.String deliveryOption,
			java.lang.String receiveAgentID, java.lang.String receiveCurrency,
			java.lang.String receiverFirstName, java.lang.String receiverLastName,
			java.lang.String receiverPhoneNumber, java.lang.String registrationCreatorFirstName,
			java.lang.String registrationCreatorLastName,
			java.lang.String registrationCreatorPhoneNumber, String mgCustomerReceiveNumber,
			Boolean activeRecordsOnly, Integer maxRowsToReturn) {
		super(language);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.receiverPhoneNumber = receiverPhoneNumber;
		this.registrationCreatorFirstName = registrationCreatorFirstName;
		this.registrationCreatorLastName = registrationCreatorLastName;
		this.registrationCreatorPhoneNumber = registrationCreatorPhoneNumber;
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		this.activeRecordsOnly = activeRecordsOnly;
		this.maxRowsToReturn = maxRowsToReturn;
	}
	
	/**
	 * Gets the receiveCountry value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the deliveryOption value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @return deliveryOption
	 */
	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * Sets the deliveryOption value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @param deliveryOption
	 */
	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * Gets the receiveAgentID value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiveAgentID
	 */
	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * Sets the receiveAgentID value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiveAgentID
	 */
	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	/**
	 * Gets the receiveCurrency value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiveCurrency
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * Sets the receiveCurrency value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiveCurrency
	 */
	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * Gets the receiverFirstName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiverFirstName
	 */
	public java.lang.String getReceiverFirstName() {
		return receiverFirstName;
	}

	/**
	 * Sets the receiverFirstName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiverFirstName
	 */
	public void setReceiverFirstName(java.lang.String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	/**
	 * Gets the receiverLastName value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiverLastName
	 */
	public java.lang.String getReceiverLastName() {
		return receiverLastName;
	}

	/**
	 * Sets the receiverLastName value for this SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiverLastName
	 */
	public void setReceiverLastName(java.lang.String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	/**
	 * Gets the receiverPhoneNumber value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @return receiverPhoneNumber
	 */
	public java.lang.String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	/**
	 * Sets the receiverPhoneNumber value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @param receiverPhoneNumber
	 */
	public void setReceiverPhoneNumber(java.lang.String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	/**
	 * Gets the registrationCreatorFirstName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @return registrationCreatorFirstName
	 */
	public java.lang.String getRegistrationCreatorFirstName() {
		return registrationCreatorFirstName;
	}

	/**
	 * Sets the registrationCreatorFirstName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @param registrationCreatorFirstName
	 */
	public void setRegistrationCreatorFirstName(java.lang.String registrationCreatorFirstName) {
		this.registrationCreatorFirstName = registrationCreatorFirstName;
	}

	/**
	 * Gets the registrationCreatorLastName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @return registrationCreatorLastName
	 */
	public java.lang.String getRegistrationCreatorLastName() {
		return registrationCreatorLastName;
	}

	/**
	 * Sets the registrationCreatorLastName value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @param registrationCreatorLastName
	 */
	public void setRegistrationCreatorLastName(java.lang.String registrationCreatorLastName) {
		this.registrationCreatorLastName = registrationCreatorLastName;
	}

	/**
	 * Gets the registrationCreatorPhoneNumber value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @return registrationCreatorPhoneNumber
	 */
	public java.lang.String getRegistrationCreatorPhoneNumber() {
		return registrationCreatorPhoneNumber;
	}

	/**
	 * Sets the registrationCreatorPhoneNumber value for this
	 * SimplifiedQueryRegistrationRequest.
	 * 
	 * @param registrationCreatorPhoneNumber
	 */
	public void setRegistrationCreatorPhoneNumber(java.lang.String registrationCreatorPhoneNumber) {
		this.registrationCreatorPhoneNumber = registrationCreatorPhoneNumber;
	}

	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public Boolean getActiveRecordsOnly() {
		return activeRecordsOnly;
	}

	public void setActiveRecordsOnly(Boolean activeRecordsOnly) {
		this.activeRecordsOnly = activeRecordsOnly;
	}

	public Integer getMaxRowsToReturn() {
		return maxRowsToReturn;
	}

	public void setMaxRowsToReturn(Integer maxRowsToReturn) {
		this.maxRowsToReturn = maxRowsToReturn;
	}
}
