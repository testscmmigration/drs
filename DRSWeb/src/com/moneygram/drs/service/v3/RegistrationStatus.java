/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.service.v3;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author T007
 * 
 */
public class RegistrationStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Map codes = new HashMap();
	public static final String CODE_PENDING = "PEN";
	public static final String CODE_ACTIVE = "ACT";
	public static final String CODE_NOT_ACTIVE = "NAT";
	public static final RegistrationStatus PENDING = new RegistrationStatus(CODE_PENDING);
	public static final RegistrationStatus ACTIVE = new RegistrationStatus(CODE_ACTIVE);
	public static final RegistrationStatus NOT_ACTIVE = new RegistrationStatus(CODE_NOT_ACTIVE);
	private String code;

	private RegistrationStatus(String code) {
		this.code = code;
		codes.put(code, this);
	}

	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}
		RegistrationStatus target = (RegistrationStatus) obj;
		return target.code.equals(this.code);
	}

	public int hashCode() {
		return this.code.hashCode() * 7;
	}

	public static RegistrationStatus fromValue(String code) {
		return (RegistrationStatus) codes.get(code);
	}

	public static RegistrationStatus fromString(String code) {
		return (RegistrationStatus) codes.get(code);
	}

	public String getValue() {
		return this.code;
	}

	public String toString() {
		return this.code;
	}
}
