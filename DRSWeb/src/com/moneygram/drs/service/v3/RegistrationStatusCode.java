package com.moneygram.drs.service.v3;

import java.io.Serializable;

import com.moneygram.common.mgEnum.Enum;

public class RegistrationStatusCode extends Enum implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final RegistrationStatusCode PENDING = new RegistrationStatusCode("PEN");
	public static final RegistrationStatusCode ACTIVE = new RegistrationStatusCode("ACT");
	public static final RegistrationStatusCode NOT_ACTIVE = new RegistrationStatusCode("NAT");
//	public static final RegistrationStatusCode UNK = new RegistrationStatusCode("UNK");

	private RegistrationStatusCode(String code) {
		super(code);
	}
}
