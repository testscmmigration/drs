/**
 * QueryRegistrationByCustomerReceiveNumberRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

public class QueryRegistrationByCustomerReceiveNumberRequest extends
		com.moneygram.drs.service.v1.Request implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String mgCustomerReceiveNumber;
	private boolean superUser;
	private java.lang.String language;

	public QueryRegistrationByCustomerReceiveNumberRequest() {
	}

	public QueryRegistrationByCustomerReceiveNumberRequest(java.lang.String language,
			java.lang.String mgCustomerReceiveNumber, boolean superUser) {
		super(language);
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		this.superUser = superUser;
	}

	/**
	 * Gets the mgCustomerReceiveNumber value for this
	 * QueryRegistrationByCustomerReceiveNumberRequest.
	 * 
	 * @return mgCustomerReceiveNumber
	 */
	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * Sets the mgCustomerReceiveNumber value for this
	 * QueryRegistrationByCustomerReceiveNumberRequest.
	 * 
	 * @param mgCustomerReceiveNumber
	 */
	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
