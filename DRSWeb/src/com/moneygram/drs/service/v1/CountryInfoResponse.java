/**
 * GetFQDOByCustomerReceiveNumberResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

public class CountryInfoResponse extends com.moneygram.drs.service.v1.Response implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	CountryInfo[] conutryInfoArray;

	public CountryInfoResponse() {
	}

	public CountryInfo[] getConutryInfoArray() {
		return conutryInfoArray;
	}

	public void setConutryInfoArray(CountryInfo[] conutryInfoArray) {
		this.conutryInfoArray = conutryInfoArray;
	}
}
