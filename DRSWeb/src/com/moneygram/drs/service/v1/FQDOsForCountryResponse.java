/**
 * FQDOsForCountryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

public class FQDOsForCountryResponse extends com.moneygram.drs.service.v1.Response implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private com.moneygram.drs.service.v1.FQDOInfo[] fqdoInfo;

	public FQDOsForCountryResponse() {
	}

	public FQDOsForCountryResponse(com.moneygram.drs.service.v1.FQDOInfo[] fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	/**
	 * Gets the fqdoInfo value for this FQDOsForCountryResponse.
	 * 
	 * @return fqdoInfo
	 */
	public com.moneygram.drs.service.v1.FQDOInfo[] getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * Sets the fqdoInfo value for this FQDOsForCountryResponse.
	 * 
	 * @param fqdoInfo
	 */
	public void setFqdoInfo(com.moneygram.drs.service.v1.FQDOInfo[] fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	public com.moneygram.drs.service.v1.FQDOInfo getFqdoInfo(int i) {
		return this.fqdoInfo[i];
	}

	public void setFqdoInfo(int i, com.moneygram.drs.service.v1.FQDOInfo _value) {
		this.fqdoInfo[i] = _value;
	}
}
