/**
 * GetFQDOByCustomerReceiveNumberResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

public class GetFQDOByCustomerReceiveNumberResponse extends com.moneygram.drs.service.v1.Response
		implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private FQDOInfo fqdoInfo = new FQDOInfo();
	private RegistrationStatus registrationStatusCode;

	public GetFQDOByCustomerReceiveNumberResponse() {
	}

	public GetFQDOByCustomerReceiveNumberResponse(FQDOInfo fqdoInfo,
			RegistrationStatus registrationStatusCode) {
		this.fqdoInfo = fqdoInfo;
		this.registrationStatusCode = registrationStatusCode;
	}

	/**
	 * Gets the fqdoInfo value for this GetFQDOByCustomerReceiveNumberResponse.
	 * 
	 * @return fqdoInfo
	 */
	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * Sets the fqdoInfo value for this GetFQDOByCustomerReceiveNumberResponse.
	 * 
	 * @param fqdoInfo
	 */
	public void setFqdoInfo(FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	/**
	 * Gets the registrationStatusCode value for this
	 * GetFQDOByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationStatusCode
	 */
	public RegistrationStatus getRegistrationStatusCode() {
		return registrationStatusCode;
	}

	/**
	 * Sets the registrationStatusCode value for this
	 * GetFQDOByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationStatusCode
	 */
	public void setRegistrationStatusCode(RegistrationStatus registrationStatusCode) {
		this.registrationStatusCode = registrationStatusCode;
	}
}
