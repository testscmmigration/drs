/*
 * Created on Feb 11, 2005
 *
 */
package com.moneygram.drs.service.v1;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author T007
 * 
 */
public class DataTypeCodeEnum implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean isMinMaxCheckable;
	private static Map codes = new HashMap();
	public static final String CODE_BOOLEAN = "boolean";
	public static final String CODE_STRING = "string";
	public static final String CODE_INT = "int";
	public static final String CODE_DECIMAL = "decimal";
	public static final String CODE_DATE = "date";
	public static final String CODE_TIME = "time";
	public static final String CODE_TEXT = "text";
	public static final String CODE_COUNTRY_CODE = "cntrycode";
	public static final String CODE_ENUM = "enum";
	public static final String CODE_STRINGBOOL = "stringbool";
	public static final String CODE_DATETIME = "datetime";
	public static final DataTypeCodeEnum BOOLEAN = new DataTypeCodeEnum(CODE_BOOLEAN);
	public static final DataTypeCodeEnum STRING = new DataTypeCodeEnum(CODE_STRING);
	public static final DataTypeCodeEnum INT = new DataTypeCodeEnum(CODE_INT);
	public static final DataTypeCodeEnum DECIMAL = new DataTypeCodeEnum(CODE_DECIMAL);
	public static final DataTypeCodeEnum DATE = new DataTypeCodeEnum(CODE_DATE);
	public static final DataTypeCodeEnum DATETIME = new DataTypeCodeEnum(CODE_DATETIME);
	public static final DataTypeCodeEnum TIME = new DataTypeCodeEnum(CODE_TIME);
	public static final DataTypeCodeEnum TEXT = new DataTypeCodeEnum(CODE_TEXT);
	public static final DataTypeCodeEnum COUNTRY_CODE = new DataTypeCodeEnum(CODE_COUNTRY_CODE);
	public static final DataTypeCodeEnum ENUM = new DataTypeCodeEnum(CODE_ENUM);
	public static final DataTypeCodeEnum STRINGBOOL = new DataTypeCodeEnum(CODE_STRINGBOOL);
	private String code;

	private DataTypeCodeEnum(String code) {
		this.code = code;
		codes.put(code, this);
	}

	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}
		DataTypeCodeEnum target = (DataTypeCodeEnum) obj;
		return target.code.equals(this.code);
	}

	public int hashCode() {
		return this.code.hashCode() * 7;
	}

	public static DataTypeCodeEnum fromValue(String code) {
		return (DataTypeCodeEnum) codes.get(code);
	}

	public static DataTypeCodeEnum fromString(String code) {
		return (DataTypeCodeEnum) codes.get(code);
	}

	public String getValue() {
		return this.code;
	}

	public String toString() {
		return this.code;
	}
}
