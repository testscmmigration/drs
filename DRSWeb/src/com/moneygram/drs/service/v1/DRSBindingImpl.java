/**
 * DRSBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

import java.math.BigInteger;
import java.util.Calendar;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.ejb.DRSLocal;
import com.moneygram.drs.ejb.DRSLocalHome;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v1.mappers.DRSMapper;
import com.moneygram.drs.service.v1.mappers.DRSMapperFactory;

public class DRSBindingImpl {
	private static Logger log = LogFactory.getInstance().getLogger(DRSBindingImpl.class);

	public com.moneygram.drs.service.v1.DirectedSendRegistrationFieldsResponse directedSendRegistrationFields(
			com.moneygram.drs.service.v1.DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (DirectedSendRegistrationFieldsResponse) satisfyRequest(directedSendRegistrationFieldsRequest);
	}

	public com.moneygram.drs.service.v1.FQDOsForCountryResponse getFQDOsForCountry(
			com.moneygram.drs.service.v1.FQDOsForCountryRequest FQDOsForCountryRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (FQDOsForCountryResponse) satisfyRequest(FQDOsForCountryRequest);
	}

	public com.moneygram.drs.service.v1.GetFQDOByCustomerReceiveNumberResponse getFQDOByCustomerReceiveNumber(
			com.moneygram.drs.service.v1.GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (GetFQDOByCustomerReceiveNumberResponse) satisfyRequest(getFQDOByCustomerReceiveNumberRequest);
	}

	public com.moneygram.drs.service.v1.SimplifiedQueryRegistrationResponse simplifiedQueryRegistration(
			com.moneygram.drs.service.v1.SimplifiedQueryRegistrationRequest simplifiedQueryRegistrationRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (SimplifiedQueryRegistrationResponse) satisfyRequest(simplifiedQueryRegistrationRequest);
	}

	public com.moneygram.drs.service.v1.SaveRegistrationResponse saveRegistration(
			com.moneygram.drs.service.v1.SaveRegistrationRequest saveRegistrationRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (SaveRegistrationResponse) satisfyRequest(saveRegistrationRequest);
	}

	public com.moneygram.drs.service.v1.QueryRegistrationByCustomerReceiveNumberResponse queryRegistrationByCustomerReceiveNumber(
			com.moneygram.drs.service.v1.QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (QueryRegistrationByCustomerReceiveNumberResponse) satisfyRequest(queryRegistrationByCustomerReceiveNumberRequest);
	}

	public com.moneygram.drs.service.v1.QueryRegistrationsResponse queryRegistrations(
			com.moneygram.drs.service.v1.QueryRegistrationsRequest queryRegistrationsRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (QueryRegistrationsResponse) satisfyRequest(queryRegistrationsRequest);
	}

	public CountryInfoResponse getCountryInfo(CountryInfoRequest countryInfoRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (CountryInfoResponse) satisfyRequest(countryInfoRequest);
	}

	public CustomerProfileResponse getCustomerProfile(
			CustomerProfileRequest customerProfileRequest)
			throws java.rmi.RemoteException, com.moneygram.drs.service.v1.Error {
		return (CustomerProfileResponse) satisfyRequest(customerProfileRequest);
	}
	
	
	private Response satisfyRequest(Request2 request) throws com.moneygram.drs.service.v1.Error {
		Response response = null;
		try {
			DRSMapper mapper = DRSMapperFactory.getMapper(request);
			CommandRequest commandRequest = mapper.mapToCommandRequest(request);
			DRSLocal drsLocal = createDRSBean();
			Object result = drsLocal.execute(commandRequest);
			response = mapper.mapToServiceResponse(result);
		} catch (DRSException drs) {
			buildAndThrowException(drs);
		} catch (Exception e) {
			log.error("Error during satisfyRequest (requestType: " + request.getClass().getName()
					+ "): ", e);
			throw new RuntimeException(e);
		}
		return response;
	}

	private void buildAndThrowException(DRSException be) throws IllegalArgumentException, Error {
		Error exception = new Error();
		exception.setDetailString(be.getMessage());
		exception.setOffendingField(be.getOffendingField());
		exception.setErrorString(be.getErrorString());
		exception.setTimeStamp(Calendar.getInstance());
		exception.setFaultString(be.getDebugString());
		exception.setLanguage("ENG");
		exception.setFaultActor(be.getFaultCode());
		exception.setErrorCode(new BigInteger(new Integer(be.getErrorCode()).toString()));
		throw exception;
	}

	private DRSLocal createDRSBean() throws Exception {
		DRSLocalHome drsHome = null;
		DRSLocal drsLocal = null;
		Context ctx = new InitialContext();
		Object o = ctx.lookup("java:comp/env/ejb/DRSHome");
		if (o instanceof String)
			ctx.unbind("java:comp/env/ejb/DRSHome");
		drsHome = (DRSLocalHome) PortableRemoteObject.narrow(o, DRSLocalHome.class);
		drsLocal = drsHome.create();
		return drsLocal;
	}
}
