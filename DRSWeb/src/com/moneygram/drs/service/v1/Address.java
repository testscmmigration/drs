package com.moneygram.drs.service.v1;

import java.io.Serializable;

/**
 * @author
 * 
 */
public class Address implements Serializable {
	private String addrLine1;
	private String addrLine2;
	private String addrLine3;
	private String city;
	private String state;
	private String postalCode;
	private String cntryCode;
	private String deliveryInstructions1;
	private String deliveryInstructions2;
	private String deliveryInstructions3;

	/**
	 * @return
	 */
	public String getAddrLine1() {
		return addrLine1;
	}

	/**
	 * @return
	 */
	public String getAddrLine2() {
		return addrLine2;
	}

	/**
	 * @return
	 */
	public String getAddrLine3() {
		return addrLine3;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public String getCntryCode() {
		return cntryCode;
	}

	/**
	 * @return
	 */
	public String getDeliveryInstructions1() {
		return deliveryInstructions1;
	}

	/**
	 * @return
	 */
	public String getDeliveryInstructions2() {
		return deliveryInstructions2;
	}

	/**
	 * @return
	 */
	public String getDeliveryInstructions3() {
		return deliveryInstructions3;
	}

	/**
	 * @return
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @return
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param string
	 */
	public void setAddrLine1(String string) {
		addrLine1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddrLine2(String string) {
		addrLine2 = string;
	}

	/**
	 * @param string
	 */
	public void setAddrLine3(String string) {
		addrLine3 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param string
	 */
	public void setCntryCode(String string) {
		cntryCode = string;
	}

	/**
	 * @param string
	 */
	public void setDeliveryInstructions1(String string) {
		deliveryInstructions1 = string;
	}

	/**
	 * @param string
	 */
	public void setDeliveryInstructions2(String string) {
		deliveryInstructions2 = string;
	}

	/**
	 * @param string
	 */
	public void setDeliveryInstructions3(String string) {
		deliveryInstructions3 = string;
	}

	/**
	 * @param string
	 */
	public void setPostalCode(String string) {
		postalCode = string;
	}

	/**
	 * @param string
	 */
	public void setState(String string) {
		state = string;
	}

	public String toString() {
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("addrLine1            " + addrLine1 + "\n");
		strbuf.append("addrLine2   " + addrLine2 + "\n");
		strbuf.append("addrLine3  " + addrLine3 + "\n");
		strbuf.append("city        " + city + "\n");
		strbuf.append("state " + state + "\n");
		strbuf.append("country " + cntryCode + "\n");
		return strbuf.toString();
	}
}
