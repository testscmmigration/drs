package com.moneygram.drs.service.v1.mappers;

import java.util.Iterator;
import java.util.List;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v1.FQDOsForCountryRequest;
import com.moneygram.drs.service.v1.Request2;
import com.moneygram.drs.service.v1.Response;
import com.moneygram.drs.util.ObjectUtils;

public class FQDOsForCountryrMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		FQDOsForCountryRequest fqdosForCountryRequest = (FQDOsForCountryRequest) serviceRequest;
		com.moneygram.drs.request.FQDOsForCountryRequest fqdosForCountryRequestEJB = new com.moneygram.drs.request.FQDOsForCountryRequest();
		fqdosForCountryRequestEJB.setReceiveCountry(fqdosForCountryRequest.getReceiveCountry());
		fqdosForCountryRequestEJB.setLanguage(fqdosForCountryRequest.getLanguage());
		return fqdosForCountryRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		List fqdosForCountryResponse = (List) commandResponse;
		com.moneygram.drs.service.v1.FQDOsForCountryResponse fqdosForCountryResponseWeb = new com.moneygram.drs.service.v1.FQDOsForCountryResponse();
		com.moneygram.drs.service.v1.FQDOInfo[] fqdoInfoWebArray = new com.moneygram.drs.service.v1.FQDOInfo[fqdosForCountryResponse
				.size()];
		int i = 0;
		for (Iterator iter = fqdosForCountryResponse.iterator(); iter.hasNext();) {
			com.moneygram.drs.service.v1.FQDOInfo fqdoInfoWeb = new com.moneygram.drs.service.v1.FQDOInfo();
			ObjectUtils.copyProperties(fqdoInfoWeb, iter.next());
			fqdoInfoWebArray[i++] = fqdoInfoWeb;
		}
		fqdosForCountryResponseWeb.setFqdoInfo(fqdoInfoWebArray);
		return fqdosForCountryResponseWeb;
	}
}
