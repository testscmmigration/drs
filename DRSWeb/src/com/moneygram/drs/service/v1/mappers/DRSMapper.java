package com.moneygram.drs.service.v1.mappers;

import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v1.Request2;
import com.moneygram.drs.service.v1.Response;

public interface DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest);

	public Response mapToServiceResponse(Object commandResponse);
}