package com.moneygram.drs.service.v1.mappers;

import java.util.Iterator;
import java.util.List;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v1.Request2;
import com.moneygram.drs.service.v1.Response;
import com.moneygram.drs.util.ObjectUtils;

public class CountryInfoMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		// CountryInfoRequest countryInfoRequest = (CountryInfoRequest)
		// serviceRequest;
		com.moneygram.drs.request.CountryInfoRequest countryInfoRequesttEJB = new com.moneygram.drs.request.CountryInfoRequest();
		return countryInfoRequesttEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		List countryInfoList = (List) commandResponse;
		com.moneygram.drs.service.v1.CountryInfoResponse countryInfoResponseWeb = new com.moneygram.drs.service.v1.CountryInfoResponse();
		com.moneygram.drs.service.v1.CountryInfo[] countryInfoWebArray = new com.moneygram.drs.service.v1.CountryInfo[countryInfoList
				.size()];
		int i = 0;
		for (Iterator iter = countryInfoList.iterator(); iter.hasNext();) {
			com.moneygram.drs.service.v1.CountryInfo countryInfoWeb = new com.moneygram.drs.service.v1.CountryInfo();
			ObjectUtils.copyProperties(countryInfoWeb, iter.next());
			countryInfoWebArray[i++] = countryInfoWeb;
		}
		countryInfoResponseWeb.setConutryInfoArray(countryInfoWebArray);
		return countryInfoResponseWeb;
	}
}
