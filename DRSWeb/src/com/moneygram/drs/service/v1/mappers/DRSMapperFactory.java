package com.moneygram.drs.service.v1.mappers;

import java.util.HashMap;
import java.util.Map;
import com.moneygram.drs.service.v1.CountryInfoRequest;
import com.moneygram.drs.service.v1.CustomerProfileRequest;
import com.moneygram.drs.service.v1.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.service.v1.FQDOsForCountryRequest;
import com.moneygram.drs.service.v1.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v1.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v1.SimplifiedQueryRegistrationRequest;
import com.moneygram.drs.service.v1.QueryRegistrationsRequest;
import com.moneygram.drs.service.v1.Request2;
import com.moneygram.drs.service.v1.SaveRegistrationRequest;

public class DRSMapperFactory {
	private static Map<Class, DRSMapper> mapOfMappers = new HashMap<Class, DRSMapper>();
	static {
		mapOfMappers.put(DirectedSendRegistrationFieldsRequest.class,
				new DirectedSendRegistrationFieldsMapper());
		mapOfMappers.put(FQDOsForCountryRequest.class, new FQDOsForCountryrMapper());
		mapOfMappers.put(GetFQDOByCustomerReceiveNumberRequest.class,
				new GetFQDOByCustomerReceiveNumberMapper());
		mapOfMappers
				.put(SimplifiedQueryRegistrationRequest.class, new SimplifiedQueryRegistrationMapper());
		mapOfMappers.put(SaveRegistrationRequest.class, new SaveRegistrationMapper());
		mapOfMappers.put(QueryRegistrationByCustomerReceiveNumberRequest.class,
				new QueryRegistrationByCustomerReceiveNumberMapper());
		mapOfMappers.put(QueryRegistrationsRequest.class, new QueryRegistrationMapper());
		mapOfMappers.put(CountryInfoRequest.class, new CountryInfoMapper());
		mapOfMappers.put(CustomerProfileRequest.class, new CustomerProfileMapper());
	}

	/**
	 * @param com.moneygram.consumerservice.webservice.nmf.v1.Request
	 * @return
	 */
	public static DRSMapper getMapper(Request2 request) {
		return (DRSMapper) mapOfMappers.get(request.getClass());
	}
}
