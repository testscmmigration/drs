package com.moneygram.drs.service.v1;

public class CustomerProfileResponse extends com.moneygram.drs.service.v1.Response implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private CustomerProfile customerProfile;
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
}
