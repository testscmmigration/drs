package com.moneygram.drs.service.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomerProfilePerson implements Serializable {
	private String BeneficiaryFlag;
	private String registrationProfileFlag;
	private String accountOwnerFlag;
	private int sequenceID;
	private String firstName;
	private String middleName;
	private String lastName;
	private String maternalName;
	private String phoneNumber;
	private Address[] addresses;
	
	public Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMaternalName() {
		return maternalName;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMaternalName(String string) {
		maternalName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}

	/**
	 * 
	 * @param string
	 */
	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	public int getSequenceID() {
		return sequenceID;
	}

	public void setSequenceID(int sequenceID) {
		this.sequenceID = sequenceID;
	}

	public String getBeneficiaryFlag() {
		return BeneficiaryFlag;
	}

	public void setBeneficiaryFlag(String beneficiaryFlag) {
		BeneficiaryFlag = beneficiaryFlag;
	}

	public String getRegistrationProfileFlag() {
		return registrationProfileFlag;
	}

	public void setRegistrationProfileFlag(String registrationProfileFlag) {
		this.registrationProfileFlag = registrationProfileFlag;
	}

	public String getAccountOwnerFlag() {
		return accountOwnerFlag;
	}

	public void setAccountOwnerFlag(String accountOwnerFlag) {
		this.accountOwnerFlag = accountOwnerFlag;
	}

	/**
	 * Checks to see if the name is different for this person.
	 * 
	 * @param existingObject
	 *            The input object is the existing object. If we are null, then
	 *            we assume no change, since the request doesn't have to fill in
	 *            all data. However, if the exisitng object is null and we are
	 *            not, then we assume that the name HAS changed.
	 * @return
	 */
	public boolean isNameDifferent(CustomerProfilePerson existingObject) {
		if (existingObject == null)
			return true;
		if (this.firstName != null) {
			if (existingObject.firstName != null) {
				if (!existingObject.firstName.equalsIgnoreCase(this.firstName))
					return true;
			} else {
				return true;
			}
		}
		if (this.middleName != null) {
			if (existingObject.middleName != null) {
				if (!existingObject.middleName.equalsIgnoreCase(this.middleName))
					return true;
			} else {
				return true;
			}
		}
		if (this.lastName != null) {
			if (existingObject.lastName != null) {
				if (!existingObject.lastName.equalsIgnoreCase(this.lastName))
					return true;
			} else {
				return true;
			}
		}
		if (this.maternalName != null) {
			if (existingObject.maternalName != null) {
				if (!existingObject.maternalName.equalsIgnoreCase(this.maternalName))
					return true;
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("CustomerProfilePerson[");
		buffer.append("sequenceID = ").append(sequenceID);
		buffer.append("firstName = ").append(firstName);
		buffer.append(", middleName = ").append(middleName);
		buffer.append(", lastName = ").append(lastName);
		buffer.append(", maternalName = ").append(maternalName);
		buffer.append(", phoneNumber = ").append(phoneNumber);
		buffer.append(", addresses = ").append(addresses);
		buffer.append("]");
		return buffer.toString();
	}
}
