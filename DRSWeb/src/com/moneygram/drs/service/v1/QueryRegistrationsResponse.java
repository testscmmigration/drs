/**
 * QueryRegistrationsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v1;

public class QueryRegistrationsResponse extends com.moneygram.drs.service.v1.Response implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private com.moneygram.drs.service.v1.RegistrationInfo[] registrationInfo;

	public QueryRegistrationsResponse() {
	}

	public QueryRegistrationsResponse(
			com.moneygram.drs.service.v1.RegistrationInfo[] registrationInfo) {
		this.registrationInfo = registrationInfo;
	}

	/**
	 * Gets the registrationInfo value for this QueryRegistrationsResponse.
	 * 
	 * @return registrationInfo
	 */
	public com.moneygram.drs.service.v1.RegistrationInfo[] getRegistrationInfo() {
		return registrationInfo;
	}

	/**
	 * Sets the registrationInfo value for this QueryRegistrationsResponse.
	 * 
	 * @param registrationInfo
	 */
	public void setRegistrationInfo(com.moneygram.drs.service.v1.RegistrationInfo[] registrationInfo) {
		this.registrationInfo = registrationInfo;
	}

	public com.moneygram.drs.service.v1.RegistrationInfo getRegistrationInfo(int i) {
		return this.registrationInfo[i];
	}

	public void setRegistrationInfo(int i, com.moneygram.drs.service.v1.RegistrationInfo _value) {
		this.registrationInfo[i] = _value;
	}
}
