/*
 * Created on Jan 28, 2005
 *
 */
package com.moneygram.drs.service.v2;

import java.io.Serializable;

/**
 * Represents FQDO data.
 * 
 * @author T007
 * 
 */
public class FQDOInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private String receiveCountry;
	private String deliveryOption;
	private String receiveAgentID;
	private String receiveCurrency;
	private String receiveAgentName;
	private String receiveAgentAbbreviation;
	private String deliveryOptionDisplayName;
	private String registrationAuthorizationText;
	private String speedOfDeliveryText;

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getDeliveryOptionDisplayName() {
		return deliveryOptionDisplayName;
	}

	public void setDeliveryOptionDisplayName(String deliveryOptionDisplayName) {
		this.deliveryOptionDisplayName = deliveryOptionDisplayName;
	}

	public String getReceiveAgentAbbreviation() {
		return receiveAgentAbbreviation;
	}

	public void setReceiveAgentAbbreviation(String receiveAgentAbbreviation) {
		this.receiveAgentAbbreviation = receiveAgentAbbreviation;
	}

	public String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public String getReceiveAgentName() {
		return receiveAgentName;
	}

	public void setReceiveAgentName(String receiveAgentName) {
		this.receiveAgentName = receiveAgentName;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public String getRegistrationAuthorizationText() {
		return registrationAuthorizationText;
	}

	public void setRegistrationAuthorizationText(String registrationAuthorizationText) {
		this.registrationAuthorizationText = registrationAuthorizationText;
	}

	public String getSpeedOfDeliveryText() {
		return speedOfDeliveryText;
	}

	public void setSpeedOfDeliveryText(String speedOfDeliveryText) {
		this.speedOfDeliveryText = speedOfDeliveryText;
	}
}
