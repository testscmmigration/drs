/**
 * SaveRegistrationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v2;

public class SaveRegistrationResponse extends com.moneygram.drs.service.v2.Response implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String mgCustomerReceiveNumber;
	private java.math.BigInteger mgCustomerReceiveNumberVersion;
	private RegistrationStatus registrationStatusCode;
	private boolean verificationRequiredForUse;
	private boolean ofacStatus;
	private java.lang.String ofacSourceID;
	private String accountNickname;
	private String accountNumberLastFour;
	private int receiverCustomerSequenceID;

	public SaveRegistrationResponse() {
	}

	public SaveRegistrationResponse(java.lang.String mgCustomerReceiveNumber,
			java.math.BigInteger mgCustomerReceiveNumberVersion,
			RegistrationStatus registrationStatusCode, boolean verificationRequiredForUse,
			boolean ofacStatus, java.lang.String ofacSourceID, String accountNickname,
			String accountNumberLastFour) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		this.mgCustomerReceiveNumberVersion = mgCustomerReceiveNumberVersion;
		this.registrationStatusCode = registrationStatusCode;
		this.verificationRequiredForUse = verificationRequiredForUse;
		this.ofacStatus = ofacStatus;
		this.ofacSourceID = ofacSourceID;
		this.accountNickname = accountNickname;
		this.accountNumberLastFour = accountNumberLastFour;
	}

	/**
	 * Gets the mgCustomerReceiveNumber value for this SaveRegistrationResponse.
	 * 
	 * @return mgCustomerReceiveNumber
	 */
	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * Sets the mgCustomerReceiveNumber value for this SaveRegistrationResponse.
	 * 
	 * @param mgCustomerReceiveNumber
	 */
	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	/**
	 * Gets the mgCustomerReceiveNumberVersion value for this
	 * SaveRegistrationResponse.
	 * 
	 * @return mgCustomerReceiveNumberVersion
	 */
	public java.math.BigInteger getMgCustomerReceiveNumberVersion() {
		return mgCustomerReceiveNumberVersion;
	}

	/**
	 * Sets the mgCustomerReceiveNumberVersion value for this
	 * SaveRegistrationResponse.
	 * 
	 * @param mgCustomerReceiveNumberVersion
	 */
	public void setMgCustomerReceiveNumberVersion(
			java.math.BigInteger mgCustomerReceiveNumberVersion) {
		this.mgCustomerReceiveNumberVersion = mgCustomerReceiveNumberVersion;
	}

	/**
	 * Gets the registrationStatusCode value for this SaveRegistrationResponse.
	 * 
	 * @return registrationStatusCode
	 */
	public RegistrationStatus getRegistrationStatusCode() {
		return registrationStatusCode;
	}

	/**
	 * Sets the registrationStatusCode value for this SaveRegistrationResponse.
	 * 
	 * @param registrationStatusCode
	 */
	public void setRegistrationStatusCode(RegistrationStatus registrationStatusCode) {
		this.registrationStatusCode = registrationStatusCode;
	}

	/**
	 * Gets the verificationRequiredForUse value for this
	 * SaveRegistrationResponse.
	 * 
	 * @return verificationRequiredForUse
	 */
	public boolean isVerificationRequiredForUse() {
		return verificationRequiredForUse;
	}

	/**
	 * Sets the verificationRequiredForUse value for this
	 * SaveRegistrationResponse.
	 * 
	 * @param verificationRequiredForUse
	 */
	public void setVerificationRequiredForUse(boolean verificationRequiredForUse) {
		this.verificationRequiredForUse = verificationRequiredForUse;
	}

	/**
	 * Gets the ofacStatus value for this SaveRegistrationResponse.
	 * 
	 * @return ofacStatus
	 */
	public boolean isOfacStatus() {
		return ofacStatus;
	}

	/**
	 * Sets the ofacStatus value for this SaveRegistrationResponse.
	 * 
	 * @param ofacStatus
	 */
	public void setOfacStatus(boolean ofacStatus) {
		this.ofacStatus = ofacStatus;
	}

	/**
	 * Gets the ofacSourceID value for this SaveRegistrationResponse.
	 * 
	 * @return ofacSourceID
	 */
	public java.lang.String getOfacSourceID() {
		return ofacSourceID;
	}

	/**
	 * Sets the ofacSourceID value for this SaveRegistrationResponse.
	 * 
	 * @param ofacSourceID
	 */
	public void setOfacSourceID(java.lang.String ofacSourceID) {
		this.ofacSourceID = ofacSourceID;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}

	public String getAccountNumberLastFour() {
		return accountNumberLastFour;
	}

	public void setAccountNumberLastFour(String accountNumberLastFour) {
		this.accountNumberLastFour = accountNumberLastFour;
	}

	public int getReceiverCustomerSequenceID() {
		return receiverCustomerSequenceID;
	}

	public void setReceiverCustomerSequenceID(int receiverCustomerSequenceID) {
		this.receiverCustomerSequenceID = receiverCustomerSequenceID;
	}
}
