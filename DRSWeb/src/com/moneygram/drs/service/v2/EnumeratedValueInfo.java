/**
 * EnumeratedValueInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v2;

public class EnumeratedValueInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String value;
	private java.lang.String label;

	public EnumeratedValueInfo() {
	}

	public EnumeratedValueInfo(java.lang.String value, java.lang.String label) {
		this.value = value;
		this.label = label;
	}

	/**
	 * Gets the value value for this EnumeratedValueInfo.
	 * 
	 * @return value
	 */
	public java.lang.String getValue() {
		return value;
	}

	/**
	 * Sets the value value for this EnumeratedValueInfo.
	 * 
	 * @param value
	 */
	public void setValue(java.lang.String value) {
		this.value = value;
	}

	/**
	 * Gets the label value for this EnumeratedValueInfo.
	 * 
	 * @return label
	 */
	public java.lang.String getLabel() {
		return label;
	}

	/**
	 * Sets the label value for this EnumeratedValueInfo.
	 * 
	 * @param label
	 */
	public void setLabel(java.lang.String label) {
		this.label = label;
	}
}
