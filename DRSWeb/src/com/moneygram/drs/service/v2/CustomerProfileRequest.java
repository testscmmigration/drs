package com.moneygram.drs.service.v2;

public class CustomerProfileRequest extends Request2 implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private String customerReceiverNumber;
	private int customerProfileVersionNumber;

	public String getCustomerReceiverNumber() {
		return customerReceiverNumber;
	}

	public void setCustomerReceiverNumber(String customerReceiverNumber) {
		this.customerReceiverNumber = customerReceiverNumber;
	}

	public int getCustomerProfileVersionNumber() {
		return customerProfileVersionNumber;
	}

	public void setCustomerProfileVersionNumber(int customerProfileVersionNumber) {
		this.customerProfileVersionNumber = customerProfileVersionNumber;
	}

}
