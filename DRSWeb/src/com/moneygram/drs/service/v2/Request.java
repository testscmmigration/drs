package com.moneygram.drs.service.v2;

public class Request extends Request2 {
	private static final long serialVersionUID = 1L;
	private java.lang.String language;

	public Request() {
		super();
	}

	public Request(java.lang.String language) {
		super();
		this.language = language;
	}

	/**
	 * Gets the language value for this Request.
	 * 
	 * @return language
	 */
	public java.lang.String getLanguage() {
		return language;
	}

	/**
	 * Sets the language value for this Request.
	 * 
	 * @param language
	 */
	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
