/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.service.v2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author T007
 * 
 */
public class RegistrationSubStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Map codes = new HashMap();
	public static final String CODE_INITIAL = "INI";
	public static final String CODE_QUEUED = "QUE";
	public static final String CODE_PENDING = "PEN";
	public static final String CODE_INACTIVATED = "IAT";
	public static final String CODE_REJECTED = "REJ";
	public static final String CODE_VALIDATED = "VAL";
	public static final String CODE_ACTIVE = "ACT";
	public static final RegistrationSubStatus INITIAL = new RegistrationSubStatus(CODE_INITIAL);
	public static final RegistrationSubStatus QUEUED = new RegistrationSubStatus(CODE_QUEUED);
	public static final RegistrationSubStatus INACTIVATED = new RegistrationSubStatus(
			CODE_INACTIVATED);
	public static final RegistrationSubStatus PENDING = new RegistrationSubStatus(CODE_PENDING);
	public static final RegistrationSubStatus ACTIVE = new RegistrationSubStatus(CODE_ACTIVE);
	public static final RegistrationSubStatus REJECTED = new RegistrationSubStatus(CODE_REJECTED);
	public static final RegistrationSubStatus VALIDATED = new RegistrationSubStatus(CODE_VALIDATED);
	private String code;

	private RegistrationSubStatus(String code) {
		this.code = code;
		codes.put(code, this);
	}

	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}
		RegistrationSubStatus target = (RegistrationSubStatus) obj;
		return target.code.equals(this.code);
	}

	public int hashCode() {
		return this.code.hashCode() * 7;
	}

	public static RegistrationSubStatus fromValue(String code) {
		return (RegistrationSubStatus) codes.get(code);
	}

	public static RegistrationSubStatus fromString(String code) {
		return (RegistrationSubStatus) codes.get(code);
	}

	public String getValue() {
		return this.code;
	}

	public String toString() {
		return this.code;
	}
}
