package com.moneygram.drs.service.v2;



public class DirectedSendFieldsRequest extends com.moneygram.drs.service.v2.Request
implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private String receiveCountry;
	private String deliveryOption;
	private String receiveAgentID;
	private String receiveCurrency;
	private String customerReceiverNumber;
	private boolean supportsComplianceFields;
    private boolean skipReceiverPhoneCountryCode;
	
	public DirectedSendFieldsRequest(){
		
	}


	public DirectedSendFieldsRequest(String lanugage,String receiveCountry,
			String deliveryOption, String receiveAgentID,
			String receiveCurrency, String customerReceiverNumber,boolean supportsComplianceFields,boolean skipReceiverPhoneCountryCode) {
		super(lanugage);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
		this.customerReceiverNumber = customerReceiverNumber;
		this.supportsComplianceFields = supportsComplianceFields;
		this.skipReceiverPhoneCountryCode = skipReceiverPhoneCountryCode;
	}


	public String getReceiveCountry() {
		return receiveCountry;
	}


	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}


	public String getDeliveryOption() {
		return deliveryOption;
	}


	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}


	public String getReceiveAgentID() {
		return receiveAgentID;
	}


	public void setReceiveAgentID(String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}


	public String getReceiveCurrency() {
		return receiveCurrency;
	}


	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}


	public String getCustomerReceiverNumber() {
		return customerReceiverNumber;
	}


	public void setCustomerReceiverNumber(String customerReceiverNumber) {
		this.customerReceiverNumber = customerReceiverNumber;
	}


	public boolean isSupportsComplianceFields() {
		return supportsComplianceFields;
	}


	public void setSupportsComplianceFields(boolean supportsComplianceFields) {
		this.supportsComplianceFields = supportsComplianceFields;
	}


	public boolean isSkipReceiverPhoneCountryCode() {
		return skipReceiverPhoneCountryCode;
	}


	public void setSkipReceiverPhoneCountryCode(boolean skipReceiverPhoneCountryCode) {
		this.skipReceiverPhoneCountryCode = skipReceiverPhoneCountryCode;
	}


}

