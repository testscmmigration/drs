/*
 * Created on Feb 15, 2005
 *
 */
package com.moneygram.drs.service.v2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author T007
 * 
 */
public class ExtendedRegistrationFieldInfo extends RegistrationFieldInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private int attrID;
	private String attrName;
	private TransactionAttributeType attrType;
	private boolean encrypt;
	private boolean confirmEntry;
	private String checkDigitAlgorithm;

	/**
	 * @return
	 */
	public int getAttrID() {
		return attrID;
	}

	/**
	 * @return
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @return
	 */
	public TransactionAttributeType getAttrType() {
		return attrType;
	}

	/**
	 * @return
	 */
	public boolean isEncrypt() {
		return encrypt;
	}

	/**
	 * @param i
	 */
	public void setAttrID(int i) {
		attrID = i;
	}

	/**
	 * @param string
	 */
	public void setAttrName(String string) {
		attrName = string;
	}

	/**
	 * @param type
	 */
	public void setAttrType(TransactionAttributeType type) {
		attrType = type;
	}

	/**
	 * @param b
	 */
	public void setEncrypt(boolean b) {
		encrypt = b;
	}

	public Object deepClone() throws Exception {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(b);
		out.writeObject(this);
		ByteArrayInputStream bIn = new ByteArrayInputStream(b.toByteArray());
		ObjectInputStream oi = new ObjectInputStream(bIn);
		return (oi.readObject());
	}

	/**
	 * @return
	 */
	public boolean isConfirmEntry() {
		return confirmEntry;
	}

	/**
	 * @param b
	 */
	public void setConfirmEntry(boolean b) {
		confirmEntry = b;
	}

	/**
	 * @return
	 */
	public String getCheckDigitAlgorithm() {
		return checkDigitAlgorithm;
	}

	/**
	 * @param string
	 */
	public void setCheckDigitAlgorithm(String string) {
		checkDigitAlgorithm = string;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("ExtendedRegistrationFieldInfo[");
		buffer.append("attrID = ").append(attrID);
		buffer.append(", attrName = ").append(attrName);
		buffer.append(", attrType = ").append(attrType);
		buffer.append(", encrypt = ").append(encrypt);
		buffer.append(", confirmEntry = ").append(confirmEntry);
		buffer.append(", Chk DGt ALG = ").append(checkDigitAlgorithm);
		buffer.append("]");
		return buffer.toString();
	}
}
