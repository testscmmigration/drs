/**
 * QueryRegistrationsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v2;

public class QueryRegistrationsRequest extends com.moneygram.drs.service.v2.Request implements
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiverFirstName;
	private java.lang.String receiverLastName;
	private java.lang.String receiverPhoneNumber;
	private java.lang.String accountNumberLast4;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String language;

	public QueryRegistrationsRequest() {
	}

	public QueryRegistrationsRequest(java.lang.String language, java.lang.String receiverFirstName,
			java.lang.String receiverLastName, java.lang.String receiverPhoneNumber,
			java.lang.String accountNumberLast4, java.lang.String receiveCountry,
			java.lang.String deliveryOption, java.lang.String receiveAgentID,
			java.lang.String receiveCurrency) {
		super(language);
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.receiverPhoneNumber = receiverPhoneNumber;
		this.accountNumberLast4 = accountNumberLast4;
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * Gets the receiverFirstName value for this QueryRegistrationsRequest.
	 * 
	 * @return receiverFirstName
	 */
	public java.lang.String getReceiverFirstName() {
		return receiverFirstName;
	}

	/**
	 * Sets the receiverFirstName value for this QueryRegistrationsRequest.
	 * 
	 * @param receiverFirstName
	 */
	public void setReceiverFirstName(java.lang.String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	/**
	 * Gets the receiverLastName value for this QueryRegistrationsRequest.
	 * 
	 * @return receiverLastName
	 */
	public java.lang.String getReceiverLastName() {
		return receiverLastName;
	}

	/**
	 * Sets the receiverLastName value for this QueryRegistrationsRequest.
	 * 
	 * @param receiverLastName
	 */
	public void setReceiverLastName(java.lang.String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	/**
	 * Gets the receiverPhoneNumber value for this QueryRegistrationsRequest.
	 * 
	 * @return receiverPhoneNumber
	 */
	public java.lang.String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	/**
	 * Sets the receiverPhoneNumber value for this QueryRegistrationsRequest.
	 * 
	 * @param receiverPhoneNumber
	 */
	public void setReceiverPhoneNumber(java.lang.String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	/**
	 * Gets the accountNumberLast4 value for this QueryRegistrationsRequest.
	 * 
	 * @return accountNumberLast4
	 */
	public java.lang.String getAccountNumberLast4() {
		return accountNumberLast4;
	}

	/**
	 * Sets the accountNumberLast4 value for this QueryRegistrationsRequest.
	 * 
	 * @param accountNumberLast4
	 */
	public void setAccountNumberLast4(java.lang.String accountNumberLast4) {
		this.accountNumberLast4 = accountNumberLast4;
	}

	/**
	 * Gets the receiveCountry value for this QueryRegistrationsRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this QueryRegistrationsRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the deliveryOption value for this QueryRegistrationsRequest.
	 * 
	 * @return deliveryOption
	 */
	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * Sets the deliveryOption value for this QueryRegistrationsRequest.
	 * 
	 * @param deliveryOption
	 */
	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * Gets the receiveAgentID value for this QueryRegistrationsRequest.
	 * 
	 * @return receiveAgentID
	 */
	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * Sets the receiveAgentID value for this QueryRegistrationsRequest.
	 * 
	 * @param receiveAgentID
	 */
	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	/**
	 * Gets the receiveCurrency value for this QueryRegistrationsRequest.
	 * 
	 * @return receiveCurrency
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * Sets the receiveCurrency value for this QueryRegistrationsRequest.
	 * 
	 * @param receiveCurrency
	 */
	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
