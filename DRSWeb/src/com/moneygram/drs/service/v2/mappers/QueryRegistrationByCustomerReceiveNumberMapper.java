package com.moneygram.drs.service.v2.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v2.DataTypeCodeEnum;
import com.moneygram.drs.service.v2.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v2.QueryRegistrationByCustomerReceiveNumberResponse;
import com.moneygram.drs.service.v2.RegistrationStatus;
import com.moneygram.drs.service.v2.RegistrationSubStatus;
import com.moneygram.drs.service.v2.Request2;
import com.moneygram.drs.service.v2.Response;
import com.moneygram.drs.util.ObjectUtils;

public class QueryRegistrationByCustomerReceiveNumberMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest = (QueryRegistrationByCustomerReceiveNumberRequest) serviceRequest;
		com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequestEJB = new com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest();
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setMgCustomerReceiveNumber(queryRegistrationByCustomerReceiveNumberRequest
						.getMgCustomerReceiveNumber());
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setSuperUser(queryRegistrationByCustomerReceiveNumberRequest.isSuperUser());
		queryRegistrationByCustomerReceiveNumberRequestEJB
				.setLanguage(queryRegistrationByCustomerReceiveNumberRequest.getLanguage());
		queryRegistrationByCustomerReceiveNumberRequestEJB
		.setDisplayFullAccountNumber(queryRegistrationByCustomerReceiveNumberRequest.isDisplayFullAccountNumber());
		return queryRegistrationByCustomerReceiveNumberRequestEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		Collection<com.moneygram.drs.service.v2.RegistrationFieldInfo> registrationFieldInfoWebList = new ArrayList<com.moneygram.drs.service.v2.RegistrationFieldInfo>();
		RegistrationData registrationData = (RegistrationData) commandResponse;
		QueryRegistrationByCustomerReceiveNumberResponse queryRegistrationByCustomerReceiveNumberResponse = new QueryRegistrationByCustomerReceiveNumberResponse();
		ObjectUtils.copyProperties(queryRegistrationByCustomerReceiveNumberResponse.getFqdoInfo(),
				registrationData.getFqdoInfo());
		queryRegistrationByCustomerReceiveNumberResponse.setAgentStatusCode(registrationData.getAgentStatusCode());
		queryRegistrationByCustomerReceiveNumberResponse.setOfacExceptionStateCode(registrationData
				.getOfacExceptionStateCode());
		queryRegistrationByCustomerReceiveNumberResponse.setRegistrationStatus(RegistrationStatus
				.fromValue(registrationData.getRegistrationStatus().getName()));
		queryRegistrationByCustomerReceiveNumberResponse
				.setRegistrationSubStatus(RegistrationSubStatus.fromValue(registrationData
						.getRegistrationSubStatus()));
		queryRegistrationByCustomerReceiveNumberResponse.setCustTranFreqCode(registrationData.getCustTranFreqCode());
		Collection fieldInfoListEJB = registrationData.getRegistrationFieldInfo();
		for (Iterator iter = fieldInfoListEJB.iterator(); iter.hasNext();) {
			List<RegistrationFieldInfo> elementList =  (List<RegistrationFieldInfo>) iter.next();
			for(RegistrationFieldInfo element:elementList){
			
			com.moneygram.drs.service.v2.RegistrationFieldInfo registrationFieldInfoWeb = new com.moneygram.drs.service.v2.RegistrationFieldInfo();
			registrationFieldInfoWeb.setDefaultValue(element.getDefaultValue());
			registrationFieldInfoWeb.setDisplayOrder(element.getDisplayOrder());
			registrationFieldInfoWeb.setEnumerated(element.isEnumerated());
			registrationFieldInfoWeb.setExampleFormat(element.getExampleFormat());
			//PRF 54351: To Add Extended Help Text in response
			registrationFieldInfoWeb.setExampleFormatID(element.getExampleFormatID());
			registrationFieldInfoWeb.setExtendedHelpTextID(element.getExtendedHelpTextID());
			registrationFieldInfoWeb.setFieldLabel(element.getFieldLabel());
			registrationFieldInfoWeb.setFieldMax(element.getFieldMax());
			registrationFieldInfoWeb.setFieldMin(element.getFieldMin());
			registrationFieldInfoWeb.setFieldScale(element.getFieldScale());
			registrationFieldInfoWeb.setFieldValue(element.getFieldValue());
			registrationFieldInfoWeb.setHidden(element.isHidden());
			registrationFieldInfoWeb.setReadOnly(element.isReadOnly());
			registrationFieldInfoWeb.setRequired(element.isRequired());
			registrationFieldInfoWeb.setValidationRegEx(element.getValidationRegEx());
			registrationFieldInfoWeb.setXmlTag(element.getXmlTag());
			registrationFieldInfoWeb.setDataTypeCodeEnum(DataTypeCodeEnum.fromString(element
					.getDataTypeCodeEnum().getName()));
			registrationFieldInfoWeb.setCategory(element.getCategory());
			Collection enumeratedValueListEJB = element.getEnumeratedValues();
			if (enumeratedValueListEJB != null) {
				Collection<com.moneygram.drs.service.v2.EnumeratedValueInfo> enumeratedValueWebList = new ArrayList<com.moneygram.drs.service.v2.EnumeratedValueInfo>();
				for (Iterator iterator = enumeratedValueListEJB.iterator(); iterator.hasNext();) {
					EnumeratedValueInfo enumeratedValueInfoEJB = (EnumeratedValueInfo) iterator
							.next();
					com.moneygram.drs.service.v2.EnumeratedValueInfo enumeratedValueInfoWeb = new com.moneygram.drs.service.v2.EnumeratedValueInfo();
					enumeratedValueInfoWeb.setLabel(enumeratedValueInfoEJB.getLabel());
					enumeratedValueInfoWeb.setValue(enumeratedValueInfoEJB.getValue());
					enumeratedValueWebList.add(enumeratedValueInfoWeb);
				}
				registrationFieldInfoWeb
						.setEnumeratedValues((com.moneygram.drs.service.v2.EnumeratedValueInfo[]) enumeratedValueWebList
								.toArray(new com.moneygram.drs.service.v2.EnumeratedValueInfo[enumeratedValueListEJB
										.size()]));
			}
			registrationFieldInfoWebList.add(registrationFieldInfoWeb);
			}
		}
		queryRegistrationByCustomerReceiveNumberResponse
				.setRegistrationFieldInfo((com.moneygram.drs.service.v2.RegistrationFieldInfo[]) registrationFieldInfoWebList
						.toArray(new com.moneygram.drs.service.v2.RegistrationFieldInfo[fieldInfoListEJB
								.size()]));
		return queryRegistrationByCustomerReceiveNumberResponse;
	}
}
