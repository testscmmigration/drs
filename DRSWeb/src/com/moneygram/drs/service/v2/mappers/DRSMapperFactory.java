package com.moneygram.drs.service.v2.mappers;

import java.util.HashMap;
import java.util.Map;

import com.moneygram.drs.service.v2.CountryInfoRequest;
import com.moneygram.drs.service.v2.CustomerProfileRequest;
import com.moneygram.drs.service.v2.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.service.v2.FQDOsForCountryRequest;
import com.moneygram.drs.service.v2.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v2.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.service.v2.QueryRegistrationsRequest;
import com.moneygram.drs.service.v2.Request2;
import com.moneygram.drs.service.v2.SaveRegistrationRequest;
import com.moneygram.drs.service.v2.SimplifiedQueryRegistrationRequest;
import com.moneygram.drs.service.v2.DirectedSendFieldsRequest;
import com.moneygram.drs.service.v2.mappers.DirectedSendFieldsMapper;

public class DRSMapperFactory {
	private static Map<Class, DRSMapper> mapOfMappers = new HashMap<Class, DRSMapper>();
	static {
		mapOfMappers.put(DirectedSendRegistrationFieldsRequest.class,
				new DirectedSendRegistrationFieldsMapper());
		mapOfMappers.put(FQDOsForCountryRequest.class, new FQDOsForCountryrMapper());
		mapOfMappers.put(GetFQDOByCustomerReceiveNumberRequest.class,
				new GetFQDOByCustomerReceiveNumberMapper());
		mapOfMappers
				.put(SimplifiedQueryRegistrationRequest.class, new SimplifiedQueryRegistrationMapper());
		mapOfMappers.put(SaveRegistrationRequest.class, new SaveRegistrationMapper());
		mapOfMappers.put(QueryRegistrationByCustomerReceiveNumberRequest.class,
				new QueryRegistrationByCustomerReceiveNumberMapper());
		mapOfMappers.put(QueryRegistrationsRequest.class, new QueryRegistrationMapper());
		mapOfMappers.put(CountryInfoRequest.class, new CountryInfoMapper());
		mapOfMappers.put(CustomerProfileRequest.class, new CustomerProfileMapper());
		mapOfMappers.put(DirectedSendFieldsRequest.class,
				new DirectedSendFieldsMapper());
	}

	/**
	 * @param com.moneygram.consumerservice.webservice.nmf.v1.Request
	 * @return
	 */
	public static DRSMapper getMapper(Request2 request) {
		return (DRSMapper) mapOfMappers.get(request.getClass());
	}
}
