package com.moneygram.drs.service.v2.mappers;

import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v2.Request2;
import com.moneygram.drs.service.v2.Response;

public interface DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest);

	public Response mapToServiceResponse(Object commandResponse);
}