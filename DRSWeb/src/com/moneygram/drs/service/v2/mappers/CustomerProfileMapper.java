package com.moneygram.drs.service.v2.mappers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.service.v2.CustomerProfile;
import com.moneygram.drs.service.v2.CustomerProfileRequest;
import com.moneygram.drs.service.v2.Request2;
import com.moneygram.drs.service.v2.Response;
import com.moneygram.drs.util.ObjectUtils;

public class CustomerProfileMapper implements DRSMapper {
	public CommandRequest mapToCommandRequest(Request2 serviceRequest) {
		com.moneygram.drs.request.CustomerProfileRequest2 customerProfileRequesttEJB = new com.moneygram.drs.request.CustomerProfileRequest2();
		customerProfileRequesttEJB.setMgCustomerReceiveNumber(((CustomerProfileRequest) serviceRequest).getCustomerReceiverNumber());
		customerProfileRequesttEJB.setCustomerProfileVersionNumber(((CustomerProfileRequest) serviceRequest).getCustomerProfileVersionNumber());
		return customerProfileRequesttEJB;
	}

	public Response mapToServiceResponse(Object commandResponse) {
		com.moneygram.drs.bo.CustomerProfile customerProfile = (com.moneygram.drs.bo.CustomerProfile) commandResponse;
		com.moneygram.drs.service.v2.CustomerProfileResponse customerProfleResponseWeb = new com.moneygram.drs.service.v2.CustomerProfileResponse();
			customerProfleResponseWeb.setCustomerProfile(getConvertedCustomerProfile(customerProfile));
		return customerProfleResponseWeb;
	}
	
	private CustomerProfile getConvertedCustomerProfile(com.moneygram.drs.bo.CustomerProfile customerProfile2) {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setCustomerReceiveNumberVersion(customerProfile2.getCustomerReceiveNumberVersion());
		customerProfile.setCustomerReceiveNumber(customerProfile2.getCustomerReceiveNumber());
		customerProfile.setAgentID(customerProfile2.getAgentID());
		customerProfile.setDeliveryOptionID(customerProfile2.getDeliveryOptionID());
		customerProfile.setAccountNickname(customerProfile2.getAccountNickname());
		//Commented Against Defect#821
		//customerProfile
		//		.setOfacStatus((customerProfile2.getOfacStatus() != null && customerProfile2
		//				.getOfacStatus().equals("P")) ? true : false);
		customerProfile.setOfacStatus(true);
		customerProfile.setPoeSourceSystem(customerProfile2.getPoeSourceSystem());
		customerProfile.setRcvAgentBankID(customerProfile2.getRcvAgentBankID());
		customerProfile.setRcvAgentCustAcctName(customerProfile2.getRcvAgentCustAcctName());
		if(customerProfile2.getRcvAgentCustAcctNbrPanFlag().equalsIgnoreCase("Y")){
			customerProfile.setRcvAgentCustAcctNbr(ObjectUtils.getMaskedString(customerProfile2
					.getRcvAgentCustAcctNbr()));
		}else{
			customerProfile.setRcvAgentCustAcctNbr(customerProfile2.getRcvAgentCustAcctNbr());
		}
		customerProfile.setRcvAgentCustAcctNbrMask(customerProfile2.getRcvAgentCustAcctNbrMask());
		customerProfile.setRcvAgentCustAcctNbrPanFlag(customerProfile2.getRcvAgentCustAcctNbrPanFlag());

		if (customerProfile2.getCreator() != null) {
			customerProfile.setCreator(getConvertedCustomerProfilePerson(customerProfile2.getCreator()));
		}
		
		if (customerProfile2.getReceiver() != null) {
			customerProfile.setReceiver(getConvertedCustomerProfilePerson(customerProfile2.getReceiver()));
		}
		
		customerProfile.setRegistrationStatus(getConvertedRegistrationStatusCode(customerProfile2.getRegistrationStatus()));
		
		ArrayList<com.moneygram.drs.service.v2.CustomerProfileCustomField> customerProfileCustomFields = new ArrayList<com.moneygram.drs.service.v2.CustomerProfileCustomField>();
		Iterator<com.moneygram.drs.bo.CustomerProfileCustomField> iterator = customerProfile2.getCustomFields().iterator();
		while (iterator.hasNext()) {
			com.moneygram.drs.bo.CustomerProfileCustomField customerProfileCustomField = iterator.next();
			com.moneygram.drs.service.v2.CustomerProfileCustomField  customerProfileCustomField2 = getConvertedCustomerProfileCustomField(customerProfileCustomField);
			customerProfileCustomFields.add(customerProfileCustomField2);
		}

		com.moneygram.drs.service.v2.CustomerProfileCustomField[] customerProfileCustomField2 = new com.moneygram.drs.service.v2.CustomerProfileCustomField[customerProfileCustomFields.size()];
		for (com.moneygram.drs.service.v2.CustomerProfileCustomField customerProfileCustomField: customerProfileCustomFields) {
			customerProfileCustomField2[customerProfileCustomFields.indexOf(customerProfileCustomField)] = customerProfileCustomField;
		}

		customerProfile.setCustomFields(customerProfileCustomField2);
		
		ArrayList<com.moneygram.drs.service.v2.CustomerProfilePerson> customerProfilePersons = new ArrayList<com.moneygram.drs.service.v2.CustomerProfilePerson>();
		Iterator<com.moneygram.drs.bo.CustomerProfilePerson> iterator2 = customerProfile2.getJointAccountHolders().iterator();
		while (iterator2.hasNext()) {
			com.moneygram.drs.bo.CustomerProfilePerson customerProfilePerson = iterator2.next();
			com.moneygram.drs.service.v2.CustomerProfilePerson  customerProfilePerson2 = getConvertedCustomerProfilePerson(customerProfilePerson);
			customerProfilePersons.add(customerProfilePerson2);
		}	
		
		com.moneygram.drs.service.v2.CustomerProfilePerson[] customerProfilePersons2 = new com.moneygram.drs.service.v2.CustomerProfilePerson[customerProfilePersons.size()];
		for (com.moneygram.drs.service.v2.CustomerProfilePerson customerProfilePerson2: customerProfilePersons) {
			customerProfilePersons2[customerProfilePersons.indexOf(customerProfilePerson2)] = customerProfilePerson2;
		}
		customerProfile.setJointAccountHolders(customerProfilePersons2);
		
		return customerProfile;
	}
	
	private com.moneygram.drs.service.v2.RegistrationStatusCode getConvertedRegistrationStatusCode(com.moneygram.drs.bo.RegistrationStatusCode registrationStatusCode) {
		if (registrationStatusCode.equals(com.moneygram.drs.bo.RegistrationStatusCode.ACTIVE)) {
			return com.moneygram.drs.service.v2.RegistrationStatusCode.ACTIVE;
		} else if (registrationStatusCode.equals(com.moneygram.drs.bo.RegistrationStatusCode.NOT_ACTIVE)) {
			return com.moneygram.drs.service.v2.RegistrationStatusCode.NOT_ACTIVE;
		} else if (registrationStatusCode.equals(com.moneygram.drs.bo.RegistrationStatusCode.PENDING)) {
			return com.moneygram.drs.service.v2.RegistrationStatusCode.PENDING;
		} else {
			return null;
		}
	}
	
	private com.moneygram.drs.service.v2.Address getConvertedAddress(com.moneygram.drs.bo.Address address) {
		com.moneygram.drs.service.v2.Address address2 = new com.moneygram.drs.service.v2.Address();
		address2.setAddrLine1(address.getAddrLine1());
		address2.setAddrLine2(address.getAddrLine2());
		address2.setAddrLine3(address.getAddrLine3());
		address2.setCity(address.getCity());
		address2.setCntryCode(address.getCntryCode());
		address2.setDeliveryInstructions1(address.getDeliveryInstructions1());
		address2.setDeliveryInstructions2(address.getDeliveryInstructions2());
		address2.setDeliveryInstructions3(address.getDeliveryInstructions3());
		address2.setPostalCode(address.getPostalCode());
		address2.setState(address.getState());
		return address2;
	}
	
	private com.moneygram.drs.service.v2.CustomerProfileCustomField getConvertedCustomerProfileCustomField (com.moneygram.drs.bo.CustomerProfileCustomField customerProfileCustomField) {
		com.moneygram.drs.service.v2.CustomerProfileCustomField customerProfileCustomField2 = new com.moneygram.drs.service.v2.CustomerProfileCustomField();
		customerProfileCustomField2.setValue(customerProfileCustomField.getValue());
		customerProfileCustomField2.setXmlTag(customerProfileCustomField.getXmlTag());
		return customerProfileCustomField2;
	}
	
	private com.moneygram.drs.service.v2.CustomerProfilePerson getConvertedCustomerProfilePerson(com.moneygram.drs.bo.CustomerProfilePerson customerProfilePerson) {
		com.moneygram.drs.service.v2.CustomerProfilePerson customerProfilePerson2 = new com.moneygram.drs.service.v2.CustomerProfilePerson ();
		customerProfilePerson2.setSequenceID(customerProfilePerson.getSequenceID());
		customerProfilePerson2.setFirstName(customerProfilePerson.getFirstName());
		customerProfilePerson2.setLastName(customerProfilePerson.getLastName());
		customerProfilePerson2.setMaternalName(customerProfilePerson.getMaternalName());
		customerProfilePerson2.setMiddleName(customerProfilePerson.getMiddleName());
		customerProfilePerson2.setPhoneNumber(customerProfilePerson.getPhoneNumber());
		customerProfilePerson2.setAccountOwnerFlag(customerProfilePerson.getAccountOwnerFlag());
		customerProfilePerson2.setBeneficiaryFlag(customerProfilePerson.getBeneficiaryFlag());
		customerProfilePerson2.setRegistrationProfileFlag(customerProfilePerson.getRegistrationProfileFlag());

		ArrayList<com.moneygram.drs.service.v2.Address>addresses = new ArrayList<com.moneygram.drs.service.v2.Address>();

		for (com.moneygram.drs.bo.Address address: (List<com.moneygram.drs.bo.Address>)customerProfilePerson.getAddresses()) {
			addresses.add(getConvertedAddress(address));
		}

		com.moneygram.drs.service.v2.Address[] addresses2 = new com.moneygram.drs.service.v2.Address[addresses.size()];
		for (com.moneygram.drs.service.v2.Address address: addresses) {
			addresses2[addresses.indexOf(address)] = address;
		}
		customerProfilePerson2.setAddresses(addresses2);
		
		return customerProfilePerson2;
	}
}
