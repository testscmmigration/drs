/**
 * RegistrationFieldInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.service.v2;

public class RegistrationFieldInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String xmlTag;
	private com.moneygram.drs.service.v2.DataTypeCodeEnum dataTypeCodeEnum;
	private boolean enumerated;
	private boolean hidden;
	private boolean required;
	private java.lang.String fieldLabel;
	private java.lang.Integer fieldMin;
	private java.lang.Integer fieldMax;
	private java.lang.Integer fieldScale;
	private java.lang.String defaultValue;
	private java.lang.String validationRegEx;
	private java.lang.Integer displayOrder;
	private java.lang.String exampleFormat;
	private boolean readOnly;
	private java.lang.String fieldValue;
	private com.moneygram.drs.service.v2.EnumeratedValueInfo[] enumeratedValues;
	private String category;
	private Boolean includeInInterTransaction;
	//PRF 54351: To Add extendedHelpTextID and exampleFormatID 
	private java.lang.Integer exampleFormatID;
	private java.lang.Integer extendedHelpTextID;

	public RegistrationFieldInfo() {
	}

	public RegistrationFieldInfo(java.lang.String xmlTag,
			com.moneygram.drs.service.v2.DataTypeCodeEnum dataTypeCodeEnum, boolean enumerated,
			boolean hidden, boolean required, java.lang.String fieldLabel,
			java.lang.Integer fieldMin, java.lang.Integer fieldMax, java.lang.Integer fieldScale,
			java.lang.String defaultValue, java.lang.String validationRegEx,
			java.lang.Integer displayOrder, java.lang.String exampleFormat, boolean readOnly,
			java.lang.String fieldValue,
			com.moneygram.drs.service.v2.EnumeratedValueInfo[] enumeratedValues,
			String category, Boolean includeInInterTransaction,java.lang.Integer exampleFormatID,java.lang.Integer extendedHelpTextID) {
		this.xmlTag = xmlTag;
		this.dataTypeCodeEnum = dataTypeCodeEnum;
		this.enumerated = enumerated;
		this.hidden = hidden;
		this.required = required;
		this.fieldLabel = fieldLabel;
		this.fieldMin = fieldMin;
		this.fieldMax = fieldMax;
		this.fieldScale = fieldScale;
		this.defaultValue = defaultValue;
		this.validationRegEx = validationRegEx;
		this.displayOrder = displayOrder;
		this.exampleFormat = exampleFormat;
		this.readOnly = readOnly;
		this.fieldValue = fieldValue;
		this.enumeratedValues = enumeratedValues;
		this.category = category;
		this.includeInInterTransaction = includeInInterTransaction;
		//PRF 54351:  To Add extendedHelpTextID and exampleFormatID,Also change in method Signature
		this.exampleFormatID = exampleFormatID;
		this.extendedHelpTextID = extendedHelpTextID;
	}

	/**
	 * Gets the xmlTag value for this RegistrationFieldInfo.
	 * 
	 * @return xmlTag
	 */
	public java.lang.String getXmlTag() {
		return xmlTag;
	}

	/**
	 * Sets the xmlTag value for this RegistrationFieldInfo.
	 * 
	 * @param xmlTag
	 */
	public void setXmlTag(java.lang.String xmlTag) {
		this.xmlTag = xmlTag;
	}

	/**
	 * Sets the dataTypeCode value for this RegistrationFieldInfo.
	 * 
	 * @param dataTypeCode
	 */
	public void setDataTypeCodeEnum(com.moneygram.drs.service.v2.DataTypeCodeEnum dataTypeCode) {
		this.dataTypeCodeEnum = dataTypeCode;
	}

	/**
	 * Gets the enumerated value for this RegistrationFieldInfo.
	 * 
	 * @return enumerated
	 */
	public boolean isEnumerated() {
		return enumerated;
	}

	/**
	 * Sets the enumerated value for this RegistrationFieldInfo.
	 * 
	 * @param enumerated
	 */
	public void setEnumerated(boolean enumerated) {
		this.enumerated = enumerated;
	}

	/**
	 * Gets the hidden value for this RegistrationFieldInfo.
	 * 
	 * @return hidden
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * Sets the hidden value for this RegistrationFieldInfo.
	 * 
	 * @param hidden
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * Gets the required value for this RegistrationFieldInfo.
	 * 
	 * @return required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Sets the required value for this RegistrationFieldInfo.
	 * 
	 * @param required
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * Gets the fieldLabel value for this RegistrationFieldInfo.
	 * 
	 * @return fieldLabel
	 */
	public java.lang.String getFieldLabel() {
		return fieldLabel;
	}

	/**
	 * Sets the fieldLabel value for this RegistrationFieldInfo.
	 * 
	 * @param fieldLabel
	 */
	public void setFieldLabel(java.lang.String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	/**
	 * Gets the fieldMin value for this RegistrationFieldInfo.
	 * 
	 * @return fieldMin
	 */
	public java.lang.Integer getFieldMin() {
		return fieldMin;
	}

	/**
	 * Sets the fieldMin value for this RegistrationFieldInfo.
	 * 
	 * @param fieldMin
	 */
	public void setFieldMin(java.lang.Integer fieldMin) {
		this.fieldMin = fieldMin;
	}

	/**
	 * Gets the fieldMax value for this RegistrationFieldInfo.
	 * 
	 * @return fieldMax
	 */
	public java.lang.Integer getFieldMax() {
		return fieldMax;
	}

	/**
	 * Sets the fieldMax value for this RegistrationFieldInfo.
	 * 
	 * @param fieldMax
	 */
	public void setFieldMax(java.lang.Integer fieldMax) {
		this.fieldMax = fieldMax;
	}

	/**
	 * Gets the fieldScale value for this RegistrationFieldInfo.
	 * 
	 * @return fieldScale
	 */
	public java.lang.Integer getFieldScale() {
		return fieldScale;
	}

	/**
	 * Sets the fieldScale value for this RegistrationFieldInfo.
	 * 
	 * @param fieldScale
	 */
	public void setFieldScale(java.lang.Integer fieldScale) {
		this.fieldScale = fieldScale;
	}

	/**
	 * Gets the defaultValue value for this RegistrationFieldInfo.
	 * 
	 * @return defaultValue
	 */
	public java.lang.String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets the defaultValue value for this RegistrationFieldInfo.
	 * 
	 * @param defaultValue
	 */
	public void setDefaultValue(java.lang.String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Gets the validationRegEx value for this RegistrationFieldInfo.
	 * 
	 * @return validationRegEx
	 */
	public java.lang.String getValidationRegEx() {
		return validationRegEx;
	}

	/**
	 * Sets the validationRegEx value for this RegistrationFieldInfo.
	 * 
	 * @param validationRegEx
	 */
	public void setValidationRegEx(java.lang.String validationRegEx) {
		this.validationRegEx = validationRegEx;
	}

	/**
	 * Gets the displayOrder value for this RegistrationFieldInfo.
	 * 
	 * @return displayOrder
	 */
	public java.lang.Integer getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * Sets the displayOrder value for this RegistrationFieldInfo.
	 * 
	 * @param displayOrder
	 */
	public void setDisplayOrder(java.lang.Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * Gets the exampleFormat value for this RegistrationFieldInfo.
	 * 
	 * @return exampleFormat
	 */
	public java.lang.String getExampleFormat() {
		return exampleFormat;
	}

	/**
	 * Sets the exampleFormat value for this RegistrationFieldInfo.
	 * 
	 * @param exampleFormat
	 */
	public void setExampleFormat(java.lang.String exampleFormat) {
		this.exampleFormat = exampleFormat;
	}

	/**
	 * Gets the readOnly value for this RegistrationFieldInfo.
	 * 
	 * @return readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * Sets the readOnly value for this RegistrationFieldInfo.
	 * 
	 * @param readOnly
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * Gets the fieldValue value for this RegistrationFieldInfo.
	 * 
	 * @return fieldValue
	 */
	public java.lang.String getFieldValue() {
		return fieldValue;
	}

	/**
	 * Sets the fieldValue value for this RegistrationFieldInfo.
	 * 
	 * @param fieldValue
	 */
	public void setFieldValue(java.lang.String fieldValue) {
		this.fieldValue = fieldValue;
	}

	/**
	 * Gets the enumeratedValues value for this RegistrationFieldInfo.
	 * 
	 * @return enumeratedValues
	 */
	public com.moneygram.drs.service.v2.EnumeratedValueInfo[] getEnumeratedValues() {
		return enumeratedValues;
	}

	/**
	 * Sets the enumeratedValues value for this RegistrationFieldInfo.
	 * 
	 * @param enumeratedValues
	 */
	public void setEnumeratedValues(
			com.moneygram.drs.service.v2.EnumeratedValueInfo[] enumeratedValues) {
		this.enumeratedValues = enumeratedValues;
	}

	public com.moneygram.drs.service.v2.DataTypeCodeEnum getDataTypeCodeEnum() {
		return dataTypeCodeEnum;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Boolean getIncludeInInterTransaction() {
		return includeInInterTransaction;
	}

	public void setIncludeInInterTransaction(Boolean includeInInterTransaction) {
		this.includeInInterTransaction = includeInInterTransaction;
	}

	/**
	 * @return the exampleFormatID
	 */
	public java.lang.Integer getExampleFormatID() {
		return exampleFormatID;
	}

	/**
	 * @param exampleFormatID the exampleFormatID to set
	 */
	public void setExampleFormatID(java.lang.Integer exampleFormatID) {
		this.exampleFormatID = exampleFormatID;
	}

	/**
	 * @return the extendedHelpTextID
	 */
	public java.lang.Integer getExtendedHelpTextID() {
		return extendedHelpTextID;
	}

	/**
	 * @param extendedHelpTextID the extendedHelpTextID to set
	 */
	public void setExtendedHelpTextID(java.lang.Integer extendedHelpTextID) {
		this.extendedHelpTextID = extendedHelpTextID;
	}

	
}
