package com.moneygram.drs.command;

import org.junit.Assert;
//import org.junit.Before;
import org.junit.Test;
import com.moneygram.drs.util.ObjectUtils;

public class SaveRegistrationCommandTestADP {
	@Test
	public void maskSringTestWithStringVaryingLength() {
		String tempString = "            1234567890987654321          ";
		Assert.assertTrue(ObjectUtils.getMaskedString(null).equals(""));
		Assert.assertTrue(ObjectUtils.getMaskedString("").equals(""));
		Assert.assertTrue(ObjectUtils.getMaskedString(" ").equals(""));
		Assert.assertTrue(ObjectUtils.getMaskedString("1").equals("1"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12").equals("*2"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123").equals("**3"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234").equals("***4"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12345").equals("12*45"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123456").equals("12**56"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234567").equals("12***67"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12345678").equals("12****78"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123456789").equals("1234**789"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234554321").equals("1234***321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12345654321").equals("1234****321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123456654321").equals("123456**4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234567654321").equals("123456***4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12345677654321").equals("123456****4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123456787654321").equals("123456*****4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234567887654321").equals("123456******4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("12345678987654321").equals("123456*******4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("123456789987654321").equals("123456********4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString("1234567890987654321").equals("123456*********4321"));
		Assert.assertTrue(ObjectUtils.getMaskedString(tempString).equals("123456*********4321"));
		
	}
}
