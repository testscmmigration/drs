package com.moneygram.drs.criteria;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.moneygram.drs.bo.ACVersionBO;
import com.moneygram.drs.request.SaveRegistrationRequest;

public class CalculateHandleSuppressReEnterFieldsTests {
	SaveRegistrationRequest request = new SaveRegistrationRequest();
	FieldsInfoCriteria fieldsInfoCriteria = new FieldsInfoCriteria();
	String apiVersionNot1611 = "1610";
	String apiVersion1611 = "1611";
	String apiVersionEmpty;

	@Test
	public void calculateHandleSuppressReEnterFields_not1808_notAC1611() {
		request.setApiVersion(apiVersionNot1611);
		request.setSupportsReenterFields(false);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertTrue(fieldsInfoCriteria.isSuppressReEnterFields());
	}

	@Test
	public void calculateHandleSuppressReEnterFields_1611_Not1808() {
		request.setApiVersion(apiVersion1611);
		request.setSupportsReenterFields(false);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertTrue(fieldsInfoCriteria.isSuppressReEnterFields());
	}

	@Test
	public void calculateHandleSuppressReEnterFields_Not1808_NoApiVersion() {
		request.setApiVersion(apiVersionEmpty);
		request.setSupportsReenterFields(false);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertTrue(fieldsInfoCriteria.isSuppressReEnterFields());
	}

	@Test
	public void calculateHandleSuppressReEnterFields_1808_NoApiVersion() {
		request.setApiVersion(apiVersionEmpty);
		request.setSupportsReenterFields(true);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertTrue(fieldsInfoCriteria.isSuppressReEnterFields());
	}

	@Test
	public void calculateHandleSuppressReEnterFields_1808_NotAC1611() {
		request.setApiVersion(apiVersionNot1611);
		request.setSupportsReenterFields(true);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertTrue(fieldsInfoCriteria.isSuppressReEnterFields());
	}

	@Test
	public void calculateHandleSuppressReEnterFields_1808_IsAC1611() {
		request.setApiVersion(apiVersion1611);
		request.setSupportsReenterFields(true);
		if (!StringUtils.isEmpty(request.getApiVersion())) {
			request.setAcVersionBO(ACVersionBO.create(request.getApiVersion()));
		} else {
			request.setAcVersionBO(ACVersionBO.create("0"));
		}
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		Assert.assertFalse(fieldsInfoCriteria.isSuppressReEnterFields());
	}
}
