package com.moneygram.drs;

import junit.framework.TestCase;
import com.moneygram.drs.util.MockPersistenceManager;

public abstract class MockPersistenceManagerTestCase extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}
}
