package com.moneygram.drs.xtravalidators.factory;

import com.moneygram.drs.xtra.validators.DCCRangeCheck;
import com.moneygram.drs.xtra.validators.RealTimeValidator;
import com.moneygram.drs.xtra.validators.XtraValidators;
import junit.framework.TestCase;

public class XtraValidatorsFactoryTest extends TestCase {
	public void testGetXtraValidatorReturnCRDRNG() throws Exception {
		XtraValidators validator = XtraValidatorsFactory
				.getXtraValidator(XtraValidatorsFactory.CRDRNG);
		assertEquals(DCCRangeCheck.class, validator.getClass());
	}

	public void testGetXtraValidatorReturnRLTVLD() throws Exception {
		XtraValidators validator = XtraValidatorsFactory
				.getXtraValidator(XtraValidatorsFactory.RLTVLD);
		assertEquals(RealTimeValidator.class, validator.getClass());
	}

	public void testGetXtraValidatorReturnNULL() throws Exception {
		XtraValidators validator = XtraValidatorsFactory.getXtraValidator("BOGUS");
		assertNull(validator);
	}

	public void testGetXtraValidatorOnNULL() throws Exception {
		XtraValidators validator = XtraValidatorsFactory.getXtraValidator(null);
		assertNull(validator);
	}

	public void testGetXtraValidatorReturnOnEmpty() throws Exception {
		XtraValidators validator = XtraValidatorsFactory.getXtraValidator("");
		assertNull(validator);
	}
}
