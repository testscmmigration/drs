package com.moneygram.drs.validators;

import junit.framework.TestCase;

public class LUHN10ValidatorTest extends TestCase {
	public void testLUHN10Number() {
		String clabeNumber = "155334854749623515";
		LUHN10Validator numberValidator = new LUHN10Validator();
		assertFalse(numberValidator.checkDigit(clabeNumber));
	}
}
