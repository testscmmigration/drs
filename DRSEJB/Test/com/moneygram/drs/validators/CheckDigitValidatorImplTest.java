package com.moneygram.drs.validators;

import junit.framework.TestCase;

public class CheckDigitValidatorImplTest extends TestCase {
	public void testCheckAlgorithmExists() throws Exception {
		String algorithmName = "CLABE";
		assertTrue(CheckDigitValidatorImpl.validateNumber(algorithmName, "211111111111111112"));
	}

	public void testCheckAlgorithmDoesNotExists() throws Exception {
		try {
			CheckDigitValidatorImpl.validateNumber("dummyAlgo", "155334854749623515");
			fail("Should have thrown exception");
		} catch (Exception e) {
			assertEquals("No validator found for algorithm: dummyAlgo", e.getMessage());
		}
	}
}
