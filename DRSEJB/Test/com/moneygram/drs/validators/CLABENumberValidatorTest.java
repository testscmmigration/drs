package com.moneygram.drs.validators;

import junit.framework.TestCase;

public class CLABENumberValidatorTest extends TestCase {
	public void testCLABENumber() {
		String clabeNumber = "155334854749623515";
		String notAClabeNumber = "12345678910112";
		CLABENumberValidator numberValidator = new CLABENumberValidator();
		assertTrue(numberValidator.checkDigit(clabeNumber));
		assertFalse(numberValidator.checkDigit(notAClabeNumber));
	}
}
