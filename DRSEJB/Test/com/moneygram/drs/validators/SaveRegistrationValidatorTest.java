package com.moneygram.drs.validators;

import java.util.ArrayList;
import junit.framework.TestCase;
import com.moneygram.drs.bo.DataTypeCodeEnum;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.util.ObjectMaster;

public class SaveRegistrationValidatorTest extends TestCase {
	public void testInvalidFieldLength() throws Exception {
		SaveRegistrationRequest request = createRequest();
		RegistrationFieldsCollection registrationFields = ObjectMaster
				.getRegistrationFieldCollection();
		SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
		RegistrationFieldInfo registrationFieldInfo = (RegistrationFieldInfo) registrationFields
				.getInfoEntryOnFullList("RECEIVERFIRSTNAME");
		registrationFieldInfo.setFieldMax(new Integer(3));
		registrationFieldInfo.setDataTypeCode(DataTypeCodeEnum.STRING);
		try {
			SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
			fail("Should have thrown max value field length failure");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC323_INVALID_FIELD_LENGTH_MAX, e.getErrorCode());
		}
	}

	private SaveRegistrationRequest createRequest() {
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setReceiveCountry("PHL");
		request.setDeliveryOption("CAMB_PLUS");
		request.setReceiveAgentID("12345678");
		request.setReceiveCurrency("USD");
		request.setLanguage("ENG");
		request.setRegistrationStatus(RegistrationStatusCode.ACTIVE.getName());
		request.addFieldValue(new KeyValuePair("RECEIVERFIRSTNAME", "Sven"));
		request.addFieldValue(new KeyValuePair("RECEIVERLASTNAME", "ODonnel"));
		request.addFieldValue(new KeyValuePair("PASSWORD", "barFoo"));
		request.addFieldValue(new KeyValuePair("PASSWORD_HIDDEN", "barFoo"));
		return request;
	}

	public void testRegexMatch() throws Exception {
		SaveRegistrationRequest request = createRequest();
		RegistrationFieldInfo registrationFieldInfo = null;
		RegistrationFieldsCollection registrationFields = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldInfo = (RegistrationFieldInfo) registrationFields
				.getInfoEntryOnFullList("TEST_REGEX");
		registrationFieldInfo.setValidationRegEx("test");
		request.addFieldValue(new KeyValuePair("TEST_REGEX", "121212"));
		registrationFieldInfo.setDataTypeCode(DataTypeCodeEnum.STRING);
		registrationFields = new RegistrationFieldsCollection(
				new ArrayList<ExtendedRegistrationFieldInfo>(),
				"DCC", false,false);
		try {
			SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
			fail("Should have thrown exception");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC328_REGEX_MATCH_FAILURE, e.getErrorCode());
		}
	}

	public void testFieldRequired() throws Exception {
		SaveRegistrationRequest request = createRequest();
		RegistrationFieldInfo registrationFieldInfo = null;
		RegistrationFieldsCollection registrationFields = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldInfo = (RegistrationFieldInfo) registrationFields
				.getInfoEntryOnFullList("JAH1PHONE");
		registrationFieldInfo.setRequired(true);
		registrationFieldInfo.setHidden(false);
		registrationFieldInfo.setDataTypeCode(DataTypeCodeEnum.STRING);
		registrationFields = new RegistrationFieldsCollection(
				new ArrayList<ExtendedRegistrationFieldInfo>(),
				"DCC", false,false);
		try {
			SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
			fail("Should have thrown exception");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC329_FIELD_REQUIRED, e.getErrorCode());
		}
	}

	public void xtestFirstNameLastNameReq() throws Exception {
		RegistrationFieldInfo registrationFieldInfo = null;
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setReceiveCountry("PHL");
		request.setDeliveryOption("CAMB_PLUS");
		request.setReceiveAgentID("12345678");
		request.setReceiveCurrency("USD");
		request.setLanguage("ENG");
		request.setRegistrationStatus(RegistrationStatusCode.ACTIVE.getName());
		request.addFieldValue(new KeyValuePair("RECEIVERLASTNAME", "ODonnel"));
		request.addFieldValue(new KeyValuePair("PASSWORD", "barFoo"));
		request.addFieldValue(new KeyValuePair("PASSWORD_HIDDEN", "barFoo"));
		RegistrationFieldsCollection registrationFields = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldInfo = (RegistrationFieldInfo) registrationFields
				.getInfoEntryOnFullList("RECEIVERFIRSTNAME");
		registrationFieldInfo.setRequired(false);
		registrationFieldInfo.setHidden(false);
		registrationFieldInfo.setDataTypeCode(DataTypeCodeEnum.STRING);
		registrationFields = new RegistrationFieldsCollection(
				new ArrayList<ExtendedRegistrationFieldInfo>(),
				"DCC", false,false);
		try {
			SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
			fail("Should have thrown exception");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC337_FIRST_NAME_LAST_NAME_NOT_PROVIDED, e
					.getErrorCode());
		}
	}

	public void testValidateCountry() throws Exception {
		RegistrationFieldInfo registrationFieldInfo = null;
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setDeliveryOption("CAMB_PLUS");
		request.setReceiveAgentID("12345678");
		request.setReceiveCurrency("USD");
		request.setLanguage("ENG");
		request.setRegistrationStatus(RegistrationStatusCode.ACTIVE.getName());
		request.addFieldValue(new KeyValuePair("RECEIVERFIRSTNAME", "Sven"));
		request.addFieldValue(new KeyValuePair("RECEIVERLASTNAME", "ODonnel"));
		request.addFieldValue(new KeyValuePair("PASSWORD", "barFoo"));
		request.addFieldValue(new KeyValuePair("PASSWORD_HIDDEN", "barFoo"));
		request.addFieldValue(new KeyValuePair("JAH1COUNTRY", "phla"));
		RegistrationFieldsCollection registrationFields = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldInfo = (RegistrationFieldInfo) registrationFields
				.getInfoEntryOnFullList("JAH1COUNTRY");
		registrationFieldInfo.setFieldMax(new Integer(3));
		registrationFieldInfo.setDataTypeCode(DataTypeCodeEnum.COUNTRY_CODE);
		registrationFields = new RegistrationFieldsCollection(
				new ArrayList<ExtendedRegistrationFieldInfo>(),
				"DCC", false,false);
		try {
			SaveRegistrationValidator.validateRegistrationRequest(request, registrationFields,false);
			fail("Should have thrown exception");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC305_INVALID_COUNTRY_CODE, e.getErrorCode());
		}
	}
}
