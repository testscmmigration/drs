package com.moneygram.drs.get_send;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.util.Data;

public class AlertRegistrationInfoBuilderTest {
	@Test
	public void convertToAgg3Structure_null() {
		AlertRegistrationInfo result = AlertRegistrationInfoBuilder
				.convertToAgg3Structure(null);
		assertEquals(0, result.getErrorData().size());
	}

	@Test
	public void convertToAgg3Structure() {
		Data data = new Data();
		RegistrationFieldsCollection fieldsCollection = data.create(2, 4, 5);
		AlertRegistrationInfo result = AlertRegistrationInfoBuilder
				.convertToAgg3Structure(fieldsCollection);
		assertEquals(3, result.getErrorData().size());
	}
}
