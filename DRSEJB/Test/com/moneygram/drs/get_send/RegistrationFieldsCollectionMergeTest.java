package com.moneygram.drs.get_send;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.util.Data;
import com.moneygram.drs.util.ObjectUtils;

public class RegistrationFieldsCollectionMergeTest {

	@Test
	public void regOnly() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection result = merge.merge(set1, null, null,
				false);
		assertSame(set1, result);
	}

	@Test
	public void addrOnly() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection result = merge.merge(null, set1, null,
				false);
		assertSame(set1, result);
	}

	@Test
	public void phoneOnly() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection result = merge.merge(null, null, set1,
				false);
		assertSame(set1, result);
	}

	@Test
	public void regAddrMerge() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection set2 = data.create(6, 10);
		RegistrationFieldsCollection result = merge.merge(set1, set2, null,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertEquals(10, result.getFullListCount());
		assertAllExist(10, null, result, set1, set2);
	}

	@Test
	public void regPhoneMerge() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection set2 = data.create(6, 10);
		RegistrationFieldsCollection result = merge.merge(set1, null, set2,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertEquals(10, result.getFullListCount());
		assertAllExist(10, null, result, set1, set2);
	}

	@Test
	public void regPhoneMerge_PhoneRequired() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection set2 = data.builder().add(2, true)
				.add(6, false).get();

		RegistrationFieldsCollection result = merge.merge(set1, null, set2,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertEquals(6, result.getFullListCount());
		assertAllExist(6, set("tag_2"), result, set1, set2);
	}

	@Test
	public void regPhoneMerge_PhoneRegRequired() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection set2 = data.builder().add(2, true)
				.add(6, false).get();

		RegistrationFieldsCollection result = merge.merge(set2, null, set1,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertEquals(6, result.getFullListCount());
		assertAllExist(6, set("tag_2"), result, set1, set2);
	}

	@Test
	public void regPhoneMerge_AddressRequired() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.builder().add(2, true)
				.add(6, true).add(7, false).add(9, true).add(10, false)
				.add(12, false).get();
		RegistrationFieldsCollection set2 = data.builder().add(2, true)
				.add(6, false).add(7, true).add(8, true).add(11, false)
				.add(12, false).get();
		// 2, 6, 7, 8, 9, 10, 11, 12
		RegistrationFieldsCollection result = merge.merge(set2, null, set1,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertAllExist(8, set("tag_2", "tag_6", "tag_7", "tag_8", "tag_9"),
				result, set1, set2);
	}

	@Test
	public void mergeAll() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.builder().add(2, true)
				.add(6, true).add(7, false).add(9, true).add(10, false).get();
		RegistrationFieldsCollection set2 = data.builder().add(2, true)
				.add(6, false).add(7, true).add(8, true).add(11, false).get();
		RegistrationFieldsCollection set3 = data.builder().add(7, false)
				.add(8, false).add(11, true).add(12, true).add(14, false).get();
		// 2, 6, 7, 8, 9, 10, 11, 12, 14
		RegistrationFieldsCollection result = merge.merge(set1, set2, set3,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);
		assertAllExist(
				9,
				set("tag_2", "tag_6", "tag_7", "tag_8", "tag_9", "tag_11",
						"tag_12"), result, set1, set2, set3);
	}

	@Test
	public void addrPhoneMerge() {
		RegistrationFieldsCollectionMerge merge = new RegistrationFieldsCollectionMerge();
		Data data = new Data();
		RegistrationFieldsCollection set1 = data.create(1, 5);
		RegistrationFieldsCollection set2 = data.create(6, 10);
		RegistrationFieldsCollection result = merge.merge(null, set1, set2,
				false);
		assertNotSame(set1, result);
		assertNotSame(set2, result);

		assertAllExist(10, null, result, set1, set2);
	}

	public Set<String> set(String... values) {
		return new HashSet<String>(Arrays.asList(values));
	}

	private void assertAllExist(int expectedMergedSize,
			Set<String> requiredFields, RegistrationFieldsCollection merged,
			RegistrationFieldsCollection... sources) {
		Set<ExtendedRegistrationFieldInfo> mergedSet = ObjectUtils
				.flattenToSet(merged.getMutableXmlTagMap().values());
		Set<ExtendedRegistrationFieldInfo> sourcesSet = new HashSet<ExtendedRegistrationFieldInfo>();
		for (RegistrationFieldsCollection source : sources) {
			Set<ExtendedRegistrationFieldInfo> ss = ObjectUtils
					.flattenToSet(source.getMutableXmlTagMap().values());
			sourcesSet.addAll(ss);
		}
		if (requiredFields != null) {
			Set<String> leftOverRequiredFields = new HashSet<String>(
					requiredFields);
			for (ExtendedRegistrationFieldInfo info : mergedSet) {
				String xmlTag = info.getXmlTag();
				boolean expectedRequired = requiredFields.contains(xmlTag);
				boolean actualRequired = info.isRequired();
				assertEquals(xmlTag, expectedRequired, actualRequired);
				if (actualRequired) {
					leftOverRequiredFields.remove(xmlTag);
				}
			}
			if (leftOverRequiredFields.size() > 0) {
				Assert.fail("The following fields were supposed to be required: "
						+ leftOverRequiredFields);
			}
		}
		int expectedUnmergedSize = sourcesSet.size() - mergedSet.size();

		assertTrue(sourcesSet.removeAll(mergedSet));

		// Does the size of the merge
		assertEquals(expectedMergedSize, mergedSet.size());
		assertEquals(expectedUnmergedSize, sourcesSet.size());
	}
}