package com.moneygram.drs.get_send;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public class UpdateRRNComplianceFieldsCollectionsFilterTest {

	UpdateRRNComplianceFieldsCollectionsFilter filter;

	FieldsCollectionRequest fieldsCollectionRequest = new FieldsCollectionRequest();

	CustomerProfileSimpleDAO dao;

	ComplianceFieldsCollections complianceFieldsCollections = new ComplianceFieldsCollections();

	RegistrationFieldsCollection addressFieldsCollection;

	RegistrationFieldsCollection phoneFieldsCollection;

	CustomerProfile customerProfile;

	CustomerProfilePerson receiver;

	Address address;

	@Before
	public void setup() {
		dao = Mockito.mock(CustomerProfileSimpleDAO.class);
		filter = new UpdateRRNComplianceFieldsCollectionsFilter(dao);
	}

	private void mockResponseFor_readCustomerProfile(
			CustomerProfile customerProfile) throws NotFoundException,
			DRSException {
		Mockito.when(
				dao.readCustomerProfile(Mockito
						.any(CustomerProfileCriteria.class))).thenReturn(
				customerProfile);
	}

	private void process() throws DRSException, NotFoundException {
		if (address != null) {
			if (receiver == null) {
				receiver = new CustomerProfilePerson();
			}
			receiver.addAddress(address);
		}
		if (receiver != null) {
			if (customerProfile == null) {
				customerProfile = new CustomerProfile();
			}
			customerProfile.setReceiver(receiver);
		}
		mockResponseFor_readCustomerProfile(customerProfile);

		complianceFieldsCollections
				.setAddressFieldsCollection(addressFieldsCollection);
		complianceFieldsCollections
				.setPhoneFieldsCollection(phoneFieldsCollection);

		String customerReceiverNumber = customerProfile != null ? "MG001"
				: null;
		fieldsCollectionRequest
				.setCustomerReceiveNumber(customerReceiverNumber);

		UpdateRRNComplianceFieldsContext context = new UpdateRRNComplianceFieldsContext(
				fieldsCollectionRequest, complianceFieldsCollections);
		filter.process(context);
	}

	@Test
	public void process_no_collections() throws DRSException, NotFoundException {
		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_no_profile() throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_no_person() throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		customerProfile = new CustomerProfile();

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());

	}

	@Test
	public void process_existingAddress_keepAlerts() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		address = new Address();
		address.setAddrLine1("abc");
		address.setCntryCode("def");

		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_existingAddress_removeAlerts() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		address = new Address();
		address.setAddrLine1("abc");
		address.setCntryCode("def");
		address.setCity("ghi");

		process();

		assertEquals(null,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_existingAddressValid_saveRegistrationInputIncludesAddress()
			throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		address = new Address();
		address.setAddrLine1("abc");
		address.setCntryCode("def");
		address.setCity("ghi");

		fieldsCollectionRequest.getComplianceObjectTypesForUpdate().add(
				FieldsCollectionRequest.RECEIVER_ADDRESS);

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_existingPhone_keepAlerts() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		receiver = new CustomerProfilePerson();
		receiver.setPhoneNumber("9998887777");

		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_existingPhone_removeAlerts_skipCountryCode()
			throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone_1305()
				.get();
		receiver = new CustomerProfilePerson();
		receiver.setPhoneNumber("9998887777");

		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(null,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_intraTran_skipCountryCode() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone_1305()
				.get();
		receiver = new CustomerProfilePerson();

		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
		assertEquals(1, phoneFieldsCollection.getFullListCount());
	}

	@Test
	public void process_existingPhone_removeAlerts() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		receiver = new CustomerProfilePerson();
		receiver.setPhoneNumber("9998887777");
		receiver.setPrimaryPhoneCountryCode("1234");

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(null,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_existingPhoneValid_saveRegistrationInputHasPhoneFields()
			throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addStandardPhone().get();
		receiver = new CustomerProfilePerson();
		receiver.setPhoneNumber("9998887777");
		receiver.setPrimaryPhoneCountryCode("1234");

		fieldsCollectionRequest.getComplianceObjectTypesForUpdate().add(
				FieldsCollectionRequest.RECEIVER_PHONE);

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}

	@Test
	public void process_intraTran_dynamic() throws DRSException,
			NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addDynamicPhone().get();
		receiver = new CustomerProfilePerson();

		process();
		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(phoneFieldsCollection,
				complianceFieldsCollections.getPhoneFieldsCollection());
		assertEquals(2, phoneFieldsCollection.getFullListCount());
	}

	@Test
	public void process_existingPhone_dynamicAttrs_removeAlerts()
			throws DRSException, NotFoundException {
		addressFieldsCollection = new FieldsBuilder().addStandardAddress()
				.get();
		phoneFieldsCollection = new FieldsBuilder().addDynamicPhone().get();

		customerProfile = new CustomerProfile();
		CustomerProfileCustomField field = new CustomerProfileCustomField(
				FieldsBuilder.MOBILEPHONENUMBER, "9998887777");
		customerProfile.addCustomFields(field, field.getXmlTag());

		receiver = new CustomerProfilePerson();
		receiver.setPrimaryPhoneCountryCode("1234");

		process();

		assertEquals(addressFieldsCollection,
				complianceFieldsCollections.getAddressFieldsCollection());
		assertEquals(null,
				complianceFieldsCollections.getPhoneFieldsCollection());
	}
}