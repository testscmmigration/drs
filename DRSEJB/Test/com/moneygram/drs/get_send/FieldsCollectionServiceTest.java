package com.moneygram.drs.get_send;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManager;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public class FieldsCollectionServiceTest {

	FieldsCollectionService service = new FieldsCollectionService();
	RegistrationFieldsCollectionMerge fieldsCollectionMerge;

	UpdateRRNComplianceFieldsCollectionsFilter updateRRNComplianceFieldsFilter;

	PersistenceManager persistenceManager;

	FieldsCollectionRequest request = new FieldsCollectionRequest();

	FieldsInfoCriteria fieldsInfoCriteria = new FieldsInfoCriteria();

	RegistrationFieldsCollection daoResponse;

	RegistrationFieldsCollection mergeResponse;

	@Before
	public void setup() {
		fieldsCollectionMerge = Mockito
				.mock(RegistrationFieldsCollectionMerge.class);
		updateRRNComplianceFieldsFilter = Mockito
				.mock(UpdateRRNComplianceFieldsCollectionsFilter.class);
		persistenceManager = Mockito.mock(PersistenceManager.class);
		service.fieldsCollectionMerge = fieldsCollectionMerge;
		service.updateRRNComplianceFieldsFilter = updateRRNComplianceFieldsFilter;
		service.persistenceManager = persistenceManager;

		request.setDaoCriteria(fieldsInfoCriteria);

		fieldsInfoCriteria.setLanguage("ENG");
		fieldsInfoCriteria.setDeliveryOption("BANK_DEPOSIT");
		fieldsInfoCriteria.setReceiveAgentId("12345678");
		fieldsInfoCriteria.setReceiveCountry("USA");
		fieldsInfoCriteria.setReceiveCurrency("USD");

		daoResponse = Mockito
				.mock(RegistrationFieldsCollection.class);
	}

	@Test
	public void noRegistrationFields() throws Exception {
		try {
			service.execute(request);
			fail();
		} catch (DRSException drsEx) {
			assertTrue(drsEx.toString(),
					drsEx.getMessage().contains("No registration fields found"));
		}
	}

	private void mockResponseFor_persistenceManager_read()
			throws NotFoundException, DRSException {
		Mockito.when(persistenceManager.read(Mockito.any(ObjectId.class)))
				.thenReturn(daoResponse);
	}

	private void mockResponseFor_FieldsCollectionMerge()
			throws NotFoundException, DRSException {
		Mockito.when(
				fieldsCollectionMerge.merge(
						Mockito.any(RegistrationFieldsCollection.class),
						Mockito.any(RegistrationFieldsCollection.class),
						Mockito.any(RegistrationFieldsCollection.class),
						Mockito.anyBoolean())).thenReturn(
				mergeResponse);
	}

	@Test
	public void notSupportsComplianceFields() throws Exception {
		mockResponseFor_persistenceManager_read();
		RegistrationFieldsCollection actual = service.execute(request);
		assertSame(daoResponse, actual);
	}

	@Test
	public void supportsComplianceFields() throws Exception {
		request.setSupportsComplianceFields(true);
		mockResponseFor_persistenceManager_read();
		mockResponseFor_FieldsCollectionMerge();
		
		RegistrationFieldsCollection actual = service.execute(request);
		assertSame(mergeResponse, actual);
	}
}
