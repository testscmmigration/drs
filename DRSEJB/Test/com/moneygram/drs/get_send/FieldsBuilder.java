package com.moneygram.drs.get_send;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.TransactionAttributeType;

public class FieldsBuilder {

	public static final String MOBILEPHONENUMBER = "MOBILEPHONENUMBER";

	List<ExtendedRegistrationFieldInfo> fields = new ArrayList<ExtendedRegistrationFieldInfo>();

	int index;

	public FieldsBuilder() {
		super();
	}

	public RegistrationFieldsCollection get() {
		return new RegistrationFieldsCollection(fields, "abc", true, false);
	}

	public FieldsBuilder addStandardAddress() {
		add(ESB_AddressTag.receiverAddress, true);
		add(ESB_AddressTag.receiverCity, true);
		add(ESB_AddressTag.receiverCountry, true);
		add(ESB_AddressTag.receiverPostalCode, false);
		add(ESB_AddressTag.receiverCountrySubdivisionCode, false);
		return this;
	}

	public FieldsBuilder addStandardPhone() {
		add(ESB_PersonTag.receiverPrimaryPhone, true);
		add(ESB_PersonTag.receiverPrimaryPhoneCountryCode, true);
		return this;
	}

	public FieldsBuilder addDynamicPhone() {
		addCustom(ESB_PersonTag.receiverPrimaryPhone, MOBILEPHONENUMBER, true);
		add(ESB_PersonTag.receiverPrimaryPhoneCountryCode, true);
		return this;
	}

	public FieldsBuilder addStandardPhone_1305() {
		add(ESB_PersonTag.receiverPrimaryPhone, true);
		return this;
	}

	public FieldsBuilder add(Enum<?> newXmlTag, boolean required) {
		add(newXmlTag.name(), required);
		return this;
	}
	
	public FieldsBuilder add(String newXmlTag, boolean required) {
		add(index++, newXmlTag, null, required, TransactionAttributeType.STANDARD_TYPE);
		return this;
	}

	public FieldsBuilder addCustom(Enum<?> e, String xmlTag, boolean required) {
		addCustom(e.name(), xmlTag, required);
		return this;
	}
	
	public FieldsBuilder addCustom(String newXmlTag, String xmlTag, boolean required) {
		add(index++, newXmlTag, xmlTag, required, TransactionAttributeType.NON_STANDARD_TYPE);
		return this;
	}

	private void add(int index, String newXmlTag, String xmlTag,
			boolean required, TransactionAttributeType attrType) {
		ExtendedRegistrationFieldInfo field = new ExtendedRegistrationFieldInfo();
		field.setAttrType(attrType);
		field.setAttrID(index);
		field.setXmlTag("tag_" + index);
		field.setNewXmlTag(newXmlTag);
		field.setXmlTag(xmlTag);
		field.setRequired(required);

		fields.add(field);
	}
}
