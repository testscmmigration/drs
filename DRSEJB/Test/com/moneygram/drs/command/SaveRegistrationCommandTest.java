package com.moneygram.drs.command;

import java.util.Calendar;
import junit.framework.TestCase;
import org.easymock.MockControl;
import com.moneygram.common.util.PanIdInfo;
import com.moneygram.drs.bo.AuthenticationData;
import com.moneygram.drs.bo.CustomerInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.OFACInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.bo.SaveRegistrationCommandResponse;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.AuthenticationCodeCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.pci.PciTranslator;
import com.moneygram.drs.pciservicemanager.PCIServiceManager;
import com.moneygram.drs.pciservicemanager.PCIServiceServiceFacade;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.drs.util.ObjectUtils;
import com.moneygram.drs.ws.pci.EncryptAccountRequest;
import com.moneygram.drs.ws.pci.EncryptAccountResponse;
import com.moneygram.drs.ws.pci.EncryptAccountResponseCode;
import com.moneygram.drs.ws.pci.SourceUsageCategoryCode;
import com.moneygram.drs.ws.pci.SourceUsageTypeCode;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapper;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapperFactory;
import com.moneygram.testutils.ObjectAccessorMatcher;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class SaveRegistrationCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testSaveRegistrationAllowed() throws Exception {
		ObjectId objectId = null;
		SaveRegistrationRequest request = createRequest();
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(request.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(request.getReceiveAgentID());
		dsrCriteria.setLanguage(request.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(request.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(request.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(request.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(request.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(request.getReceiveCurrency());
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		AuthenticationCodeCriteria authenticationCodeCriteria = new AuthenticationCodeCriteria();
		authenticationCodeCriteria.setAgentId(request.getReceiveAgentID());
		authenticationCodeCriteria.setDeliveryOption(request.getDeliveryOption());
		objectId = new ObjectId(AuthenticationData.class, authenticationCodeCriteria);
		AuthenticationData authenticationData = ObjectMaster.getAuthenticationData();
		MockPersistenceManager.mockRead(objectId, authenticationData, false);
		CustomerProfile existingProfile = ObjectMaster.getCustomerProfile();
		objectId = new ObjectId(CustomerProfile.class, request);
		MockPersistenceManager.mockRead(objectId, existingProfile, false);
		StandardFieldXMLTagMapper fieldXMLTagMapperImpl = StandardFieldXMLTagMapperFactory
				.getInstance();
		CustomerProfile customerProfile = fieldXMLTagMapperImpl.convertIntoCustomerProfile(request
				.getFieldValues());
		setCRNAndRegistrationStatus(true, request.getCustomerReceiveNumber(),
				RegistrationStatusCode.PENDING, customerProfile, "ENG");
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setCustomerProfile(customerProfile);
		customerInfo.setAgentId(request.getReceiveAgentID());
		customerInfo.setDeliveryOption(request.getDeliveryOption());
		MockPersistenceManager.mockSave(customerInfo);
		OFACInfo ofacInfo = new OFACInfo();
		ofacInfo.setCustomerReceiveNumber(customerProfile.getCustomerReceiveNumber());
		ofacInfo.setCustomerReceiveNumberVersion(customerProfile.getCustomerReceiveNumberVersion());
		ofacInfo.setOFACStatus("P");
		MockPersistenceManager.mockSave(ofacInfo, false);
		MockPersistenceManager.mockNotifyDSSOfRegistrationChange(customerProfile
				.getCustomerReceiveNumber());
		MockPersistenceManager.replayMocks();
		DRSBean drsBean = createDRSBean();
		SaveRegistrationCommandResponse saveRegistrationCommandResponse = (SaveRegistrationCommandResponse) drsBean
				.execute(request);
		assertNotNull(saveRegistrationCommandResponse);
	}

	private SaveRegistrationRequest createRequest() {
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setReceiveCountry("PHL");
		request.setDeliveryOption("CAMB_PLUS");
		request.setReceiveAgentID("12345678");
		request.setReceiveCurrency("USD");
		request.setLanguage("ENG");
		request.setRegistrationStatus(RegistrationStatusCode.ACTIVE.getName());
		request.addFieldValue(new KeyValuePair("RECEIVERFIRSTNAME", "Sven"));
		request.addFieldValue(new KeyValuePair("RECEIVERLASTNAME", "ODonnel"));
		request.addFieldValue(new KeyValuePair("PASSWORD", "barFoo"));
		request.addFieldValue(new KeyValuePair("PASSWORD_HIDDEN", "barFoo"));
		return request;
	}

	private void setCRNAndRegistrationStatus(boolean creating, String crn,
			RegistrationStatusCode regStatus, CustomerProfile cp, String language) throws Exception {
		// If we are creating, we need to set the inital registration state.
		if (creating) {
			cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
			cp.setRegistrationSubStatus("INI");
		} else {
			cp.setCustomerReceiveNumber(crn);
			// 4-16-05 : Set the registration status.
			if (regStatus != null) {
				// Check to see if an de-activation
				if (regStatus == RegistrationStatusCode.NOT_ACTIVE) {
					cp.setRegistrationStatus(RegistrationStatusCode.NOT_ACTIVE);
					cp.setRegistrationSubStatus("IAT");
				} else if (regStatus == RegistrationStatusCode.PENDING) {
					// Any registration change causes a transition to pending
					// ini
					// with the exception of of the NAT above
					cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
					cp.setRegistrationSubStatus("INI");
				} else {
					// No other state change is acceptable, throw....
					// tossRTSExceptionWithRollback(RTSExceptionMgr.createClientException(
					// RTSExceptionMgr.EC336_INVALID_REGISTRATION_STATUS,
					// "registrationStatus",
					// "Cannot set registration status to: "+regStatus,
					// language));
					// TODO do we need to throw an exception US
				}
			} else {
				// Any registration change causes a transition to pending ini
				// with the exception of of the NAT above
				cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
				cp.setRegistrationSubStatus("INI");
			}
		}
	}

	public void testEncryptCallOnPCI() throws Exception {
		PCIServiceManager manager;
		MockControl managerControl;
		managerControl = MockControl.createControl(PCIServiceManager.class);
		manager = (PCIServiceManager) managerControl.getMock();
		ObjectUtils.setPrivateField(PCIServiceServiceFacade.class, "pciServiceManager", manager);
		PanIdInfo panIDInfo = new PanIdInfo();
		panIDInfo.setPan(true);
		String accountNumber = "TEST";
		String rrn = "MG123456789";
		int version = 1;
		String SENS_ID = "TESTID";
		SaveRegistrationCommandResponse commandResponse = new SaveRegistrationCommandResponse();
		commandResponse.setMgCustomerReceiveNumber(rrn);
		commandResponse.setMgCustomerReceiveNumberVersion(version);
		EncryptAccountRequest expectedaccountRequest = new EncryptAccountRequest();
		expectedaccountRequest.setPrimaryAccountNumber(accountNumber);
		expectedaccountRequest.setSourceSystemID(commandResponse.getMgCustomerReceiveNumber() + "-"
				+ commandResponse.getMgCustomerReceiveNumberVersion());
		expectedaccountRequest.setSourceUsageType(SourceUsageTypeCode.RECEIVER_REGISTRATION_NUMBER);
		expectedaccountRequest.setSourceUsageCategory(SourceUsageCategoryCode.PROFILE);
		Calendar cal = Calendar.getInstance();
		expectedaccountRequest.setLastUsedDate(cal);
		EncryptAccountResponse accountResponse = new EncryptAccountResponse();
		accountResponse.setResult(EncryptAccountResponseCode.SUCCESS);
		accountResponse.setSensitiveDataValueID(SENS_ID);
		SensitiveIDInfo sensID = new SensitiveIDInfo();
		sensID.setCustomerReceiveNumber(commandResponse.getMgCustomerReceiveNumber());
		sensID.setCustomerReceiveNumberVersion(commandResponse.getMgCustomerReceiveNumberVersion());
		sensID.setSensitiveID(SENS_ID);
		manager.encryptAccount(expectedaccountRequest);
		managerControl.setMatcher(new ObjectAccessorMatcher());
		managerControl.setReturnValue(accountResponse);
		MockPersistenceManager.mockSave(sensID, true);
		MockPersistenceManager.replayMocks();
		managerControl.replay();
		SaveRegistrationCommand command = new SaveRegistrationCommand();
		PciTranslator pciTranslator = new PciTranslator();
		 sensID=pciTranslator.encryptCallOnPCI(panIDInfo.isPan(), accountNumber, commandResponse.getMgCustomerReceiveNumber(),commandResponse.getMgCustomerReceiveNumberVersion(), Calendar.getInstance());
		
		MockPersistenceManager.verifyMocks();
		managerControl.verify();
		MockPersistenceManager.resetMocks();
		ObjectUtils.setPrivateField(PCIServiceServiceFacade.class, "pciServiceManager", null);
	}

	public void testEncryptCallOnNonPCI() throws Exception {
		PanIdInfo panIDInfo = new PanIdInfo();
		panIDInfo.setPan(false);
		String accountNumber = "TEST";
		String rrn = "MG123456789";
		int version = 1;
		SaveRegistrationCommandResponse commandResponse = new SaveRegistrationCommandResponse();
		commandResponse.setMgCustomerReceiveNumber(rrn);
		commandResponse.setMgCustomerReceiveNumberVersion(version);
		Calendar cal = Calendar.getInstance();
		SaveRegistrationCommand command = new SaveRegistrationCommand();
		PciTranslator pciTranslator = new PciTranslator();
		SensitiveIDInfo sensID = pciTranslator.encryptCallOnPCI(panIDInfo.isPan(), accountNumber, commandResponse.getMgCustomerReceiveNumber(),commandResponse.getMgCustomerReceiveNumberVersion(), Calendar.getInstance());
		//command.encryptCallOnPCI(panIDInfo, accountNumber, commandResponse, cal);
	}

	public void testSaveRegistrationThrowsCardRageValidationError() throws Exception {
		ObjectId objectId = null;
		SaveRegistrationRequest request = createRequest();
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(request.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(request.getReceiveAgentID());
		dsrCriteria.setLanguage(request.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(request.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(request.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(request.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(request.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(request.getReceiveCurrency());
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		AuthenticationCodeCriteria authenticationCodeCriteria = new AuthenticationCodeCriteria();
		authenticationCodeCriteria.setAgentId(request.getReceiveAgentID());
		authenticationCodeCriteria.setDeliveryOption(request.getDeliveryOption());
		objectId = new ObjectId(AuthenticationData.class, authenticationCodeCriteria);
		AuthenticationData authenticationData = ObjectMaster.getAuthenticationData();
		MockPersistenceManager.mockRead(objectId, authenticationData, false);
		CustomerProfile existingProfile = ObjectMaster.getCustomerProfile();
		objectId = new ObjectId(CustomerProfile.class, request);
		MockPersistenceManager.mockRead(objectId, existingProfile, false);
		StandardFieldXMLTagMapper fieldXMLTagMapperImpl = StandardFieldXMLTagMapperFactory
				.getInstance();
		CustomerProfile customerProfile = fieldXMLTagMapperImpl.convertIntoCustomerProfile(request
				.getFieldValues());
		setCRNAndRegistrationStatus(true, request.getCustomerReceiveNumber(),
				RegistrationStatusCode.PENDING, customerProfile, "ENG");
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setCustomerProfile(customerProfile);
		customerInfo.setAgentId(request.getReceiveAgentID());
		customerInfo.setDeliveryOption(request.getDeliveryOption());
		MockPersistenceManager.mockSave(customerInfo);
		OFACInfo ofacInfo = new OFACInfo();
		ofacInfo.setCustomerReceiveNumber(customerProfile.getCustomerReceiveNumber());
		ofacInfo.setCustomerReceiveNumberVersion(customerProfile.getCustomerReceiveNumberVersion());
		ofacInfo.setOFACStatus("P");
		MockPersistenceManager.mockSave(ofacInfo, false);
		MockPersistenceManager.mockNotifyDSSOfRegistrationChange(customerProfile
				.getCustomerReceiveNumber());
		MockPersistenceManager.replayMocks();
		DRSBean drsBean = createDRSBean();
		try {
			drsBean.execute(request);
		} catch (Exception e) {
			fail("This is supposed to throw the Card Range validation error" + e);
		}
	}
}
