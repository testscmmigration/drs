package com.moneygram.drs.command;

import java.util.Collection;
import java.util.Iterator;
import junit.framework.TestCase;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class QueryResgistrationByCustomerReceiveNumberCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		FQDOCriteria fqdoCriteria = null;
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest = new QueryRegistrationByCustomerReceiveNumberRequest();
		queryRegistrationByCustomerReceiveNumberRequest.setMgCustomerReceiveNumber("12345");
		queryRegistrationByCustomerReceiveNumberRequest.setLanguage("Eng");
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(false);
		customerProfileCriteria.setCustomerReceiveNumber("12345");
		customerProfileCriteria.setProfileVersion(0);
		ObjectId objectId = new ObjectId(CustomerProfile.class, customerProfileCriteria);
		CustomerProfile customerProfileExpected = ObjectMaster.getCustomerProfile();
		MockPersistenceManager.mockRead(objectId, customerProfileExpected);
		// read fqdo info
		FQDOInfo infoExpected = ObjectMaster.getFQDOInfo();
		fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setCustomerReceiveNumber(queryRegistrationByCustomerReceiveNumberRequest
				.getMgCustomerReceiveNumber());
		fqdoCriteria.setLanguage(queryRegistrationByCustomerReceiveNumberRequest.getLanguage());
		fqdoCriteria.setFqdoEnum(FqdoEnum.FULLFQDO);
		objectId = new ObjectId(FQDOInfo.class, fqdoCriteria);
		MockPersistenceManager.mockRead(objectId, infoExpected, false);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(infoExpected.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(queryRegistrationByCustomerReceiveNumberRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(infoExpected.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(infoExpected.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(infoExpected.getReceiveCurrency());
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection, false);
		MockPersistenceManager.replayMocks();
		DRSBean drsBean = createDRSBean();
		RegistrationData registrationData = (RegistrationData) drsBean
				.execute(queryRegistrationByCustomerReceiveNumberRequest);
		Collection regFieldCollActual = registrationData.getRegistrationFieldInfo();
		Iterator i = regFieldCollActual.iterator();
		CustomerProfilePerson receiver = customerProfileExpected.getReceiver();
		Address recvAddr = (Address) receiver.getAddresses().get(0);
		CustomerProfilePerson jah1 = customerProfileExpected.getJointAccountHolderByIndex(1);
		Address jah1Addr = (Address) jah1.getAddresses().get(0);
		while (i.hasNext()) {
			RegistrationFieldInfo rfi = (RegistrationFieldInfo) i.next();
			if (rfi.getXmlTag().equalsIgnoreCase("receiverFirstName"))
				assertEquals(receiver.getFirstName(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("receiverLastName"))
				assertEquals(receiver.getLastName(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERPHONE"))
				assertEquals(receiver.getPhoneNumber(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERADDRESS1"))
				assertEquals(recvAddr.getAddrLine1(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERCITY"))
				assertEquals(recvAddr.getCity(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERSTATE"))
				assertEquals(recvAddr.getState(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERPOSTALCODE"))
				assertEquals(recvAddr.getPostalCode(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("RECEIVERCOUNTRY"))
				assertEquals(recvAddr.getCntryCode(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1PHONE"))
				assertEquals(jah1.getPhoneNumber(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1FIRSTNAME"))
				assertEquals(jah1.getFirstName(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1LASTNAME"))
				assertEquals(jah1.getLastName(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1ADDRESS1"))
				assertEquals(jah1Addr.getAddrLine1(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1CITY"))
				assertEquals(jah1Addr.getCity(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1STATE"))
				assertEquals(jah1Addr.getState(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1POSTALCODE"))
				assertEquals(jah1Addr.getPostalCode(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("JAH1COUNTRY"))
				assertEquals(jah1Addr.getCntryCode(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("TEST_INT"))
				assertEquals("42", rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("receiverAccountType"))
				assertEquals(customerProfileExpected.getRcvAgentCustAcctName(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("receiverAccountNumber"))
				assertEquals(customerProfileExpected.getRcvAgentCustAcctNbr(), rfi.getFieldValue());
			else if (rfi.getXmlTag().equalsIgnoreCase("receiverBankIdentifier"))
				assertEquals(customerProfileExpected.getRcvAgentBankID(), rfi.getFieldValue());
		}
		MockPersistenceManager.verifyMocks();
	}
}
