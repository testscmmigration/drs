package com.moneygram.drs.command;

import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.criteria.FQDOForCountryCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.request.FQDOsForCountryRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ObjectAccessorMatcher;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class FQDOSForCountryCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		List expectedFqdos = ObjectMaster.getFqdosForCountry();
		FQDOsForCountryRequest fqdosForCountryRequest = new FQDOsForCountryRequest();
		FQDOForCountryCriteria fqdoCountryCriteria = new FQDOForCountryCriteria();
		fqdoCountryCriteria.setReceiveCountry("USA");
		fqdoCountryCriteria.setLanguage("Eng");
		fqdosForCountryRequest.setReceiveCountry("USA");
		fqdosForCountryRequest.setLanguage("Eng");
		// Find mock using criteria
		MockPersistenceManager.mockFind(fqdoCountryCriteria, expectedFqdos);
		MockPersistenceManager.replayMocks();
		// Make call thru bean
		DRSBean drsBean = createDRSBean();
		List actualFqdos = (List) drsBean.execute(fqdosForCountryRequest);
		MockPersistenceManager.verifyMocks();
		ObjectAccessorMatcher.assertAccessorsEquals(expectedFqdos, actualFqdos);
	}
}
