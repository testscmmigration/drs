package com.moneygram.drs.command;

import java.util.Collection;
import junit.framework.TestCase;
import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.RegistrationFieldsAlertsRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class RegistrationFieldsAlertsCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest = createRequest();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(registrationFieldsAlertsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(registrationFieldsAlertsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(registrationFieldsAlertsRequest.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(registrationFieldsAlertsRequest
				.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(registrationFieldsAlertsRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(registrationFieldsAlertsRequest
				.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(registrationFieldsAlertsRequest
				.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(registrationFieldsAlertsRequest
				.getReceiveCurrency());
		//registrationFieldsInfoCriteria.setRegFldsAlrtsCrit(true);
		ObjectId objectId = new ObjectId(RegistrationFieldsCollection.class,
				registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		MockPersistenceManager.replayMocks();		
		DRSBean drsBean = createDRSBean();
		AlertRegistrationInfo alertRegistrationInfo = (AlertRegistrationInfo) drsBean
				.execute(registrationFieldsAlertsRequest);
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertRegistrationInfo.getAlertActionCode());
		Collection<AlertErrorInfo> alertErrorInfo = alertRegistrationInfo.getErrorData();
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertErrorInfo);
		
		//List<AlertErrorInfo> errorList = new ArrayList<AlertErrorInfo>();
		//errorList.addAll(alertErrorInfo);
		
	}

	public void testExecuteWithExternalReceiverProfile() throws Exception {
		
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest = createRequest();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(registrationFieldsAlertsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(registrationFieldsAlertsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(registrationFieldsAlertsRequest.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(registrationFieldsAlertsRequest
				.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(registrationFieldsAlertsRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(registrationFieldsAlertsRequest
				.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(registrationFieldsAlertsRequest
				.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(registrationFieldsAlertsRequest
				.getReceiveCurrency());
		registrationFieldsInfoCriteria.setExternalReceiverProfile(true);
		//registrationFieldsInfoCriteria.setRegFldsAlrtsCrit(true);
		ObjectId objectId = new ObjectId(RegistrationFieldsCollection.class,
				registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		MockPersistenceManager.replayMocks();		
		DRSBean drsBean = createDRSBean();
		AlertRegistrationInfo alertRegistrationInfo = (AlertRegistrationInfo) drsBean
				.execute(registrationFieldsAlertsRequest);
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertRegistrationInfo.getAlertActionCode());
		Collection<AlertErrorInfo> alertErrorInfo = alertRegistrationInfo.getErrorData();
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertErrorInfo);
		
		//List<AlertErrorInfo> errorList = new ArrayList<AlertErrorInfo>();
		//errorList.addAll(alertErrorInfo);
		
	}

	private RegistrationFieldsAlertsRequest createRequest() {
		RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest = new RegistrationFieldsAlertsRequest();
		registrationFieldsAlertsRequest.setDeliveryOption("BANK_DEPOSIT");
		registrationFieldsAlertsRequest.setLanguage("ENG");
		registrationFieldsAlertsRequest.setReceiveAgentID("48076876");
		registrationFieldsAlertsRequest.setReceiveCurrency("MXN");
		registrationFieldsAlertsRequest.setReceiveCountry("MEX");
		return registrationFieldsAlertsRequest;
	}

	private RegistrationFieldsAlertsRequest createRequestWithExternalRecProfTrue() {
		RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest = new RegistrationFieldsAlertsRequest();
		registrationFieldsAlertsRequest.setDeliveryOption("BANK_DEPOSIT");
		registrationFieldsAlertsRequest.setLanguage("ENG");
		registrationFieldsAlertsRequest.setReceiveAgentID("48076876");
		registrationFieldsAlertsRequest.setReceiveCurrency("MXN");
		registrationFieldsAlertsRequest.setReceiveCountry("MEX");
		return registrationFieldsAlertsRequest;
	}
	private RegistrationFieldsAlertsRequest createRequestWithExternalRecProfFalse() {
		RegistrationFieldsAlertsRequest registrationFieldsAlertsRequest = new RegistrationFieldsAlertsRequest();
		registrationFieldsAlertsRequest.setDeliveryOption("BANK_DEPOSIT");
		registrationFieldsAlertsRequest.setLanguage("ENG");
		registrationFieldsAlertsRequest.setReceiveAgentID("48076876");
		registrationFieldsAlertsRequest.setReceiveCurrency("MXN");
		registrationFieldsAlertsRequest.setReceiveCountry("MEX");
		return registrationFieldsAlertsRequest;
	}
}

