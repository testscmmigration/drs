package com.moneygram.drs.command;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ObjectAccessorMatcher;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class GetFQDOByCustomerReceiveNumberTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		FullFQDOWithRegistrationStatus fqdoByCustomerReceiveNumber = ObjectMaster
				.getFqdoByCustomerReceiveNumber();
		List<FullFQDOWithRegistrationStatus> expectedFqdos = new ArrayList<FullFQDOWithRegistrationStatus>();
		expectedFqdos.add(fqdoByCustomerReceiveNumber);
		GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest = new GetFQDOByCustomerReceiveNumberRequest();
		FQDOCriteria fqdoByCustomerReceiverNumberCriteria = new FQDOCriteria();
		fqdoByCustomerReceiverNumberCriteria.setCustomerReceiveNumber("123456789");
		fqdoByCustomerReceiverNumberCriteria.setLanguage("Eng");
		fqdoByCustomerReceiverNumberCriteria.setFqdoEnum(FqdoEnum.FULLFQDO_WITH_REGISTRATIONSTATUS);
		getFQDOByCustomerReceiveNumberRequest.setLanguage("Eng");
		getFQDOByCustomerReceiveNumberRequest.setMgCustomerReceiveNumber("123456789");
		// Read mock using criteria
		ObjectId objectId = new ObjectId(FullFQDOWithRegistrationStatus.class,
				fqdoByCustomerReceiverNumberCriteria);
		MockPersistenceManager.mockRead(objectId, fqdoByCustomerReceiveNumber);
		MockPersistenceManager.replayMocks();
		// Make call thru bean
		DRSBean drsBean = createDRSBean();
		FullFQDOWithRegistrationStatus actualFqdos = (FullFQDOWithRegistrationStatus) drsBean
				.execute(getFQDOByCustomerReceiveNumberRequest);
		MockPersistenceManager.verifyMocks();
		ObjectAccessorMatcher.assertAccessorsEquals(fqdoByCustomerReceiveNumber, actualFqdos);
	}
}
