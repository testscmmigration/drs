package com.moneygram.drs.command;

import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.criteria.QueryRegistrationsCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.request.QueryRegistrationsRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ObjectAccessorMatcher;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class QueryRegistrationsCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		List expectedRegistrationInfoList = ObjectMaster.getRegistrationInfo();
		QueryRegistrationsRequest queryRegistrationsRequest = createRequest();
		QueryRegistrationsCriteria queryRegistrationsCriteria = createCriteria(queryRegistrationsRequest);
		MockPersistenceManager.mockFind(queryRegistrationsCriteria, expectedRegistrationInfoList);
		MockPersistenceManager.replayMocks();
		// Make call thru bean
		DRSBean drsBean = createDRSBean();
		List actualRegistrationInfo = (List) drsBean.execute(queryRegistrationsRequest);
		MockPersistenceManager.verifyMocks();
		ObjectAccessorMatcher.assertAccessorsEquals(expectedRegistrationInfoList,
				actualRegistrationInfo);
	}

	private QueryRegistrationsCriteria createCriteria(
			QueryRegistrationsRequest queryRegistrationsRequest) {
		QueryRegistrationsCriteria queryRegistrationsCriteria = new QueryRegistrationsCriteria();
		queryRegistrationsCriteria.setAccountNumberLast4(queryRegistrationsRequest
				.getAccountNumberLast4());
		queryRegistrationsCriteria.setDeliveryOption(queryRegistrationsRequest.getDeliveryOption());
		queryRegistrationsCriteria.setReceiveAgentID(queryRegistrationsRequest.getReceiveAgentID());
		queryRegistrationsCriteria.setReceiveCountry(queryRegistrationsRequest.getReceiveCountry());
		queryRegistrationsCriteria.setReceiveCurrency(queryRegistrationsRequest
				.getReceiveCurrency());
		queryRegistrationsCriteria.setReceiverFirstName(queryRegistrationsRequest
				.getReceiverFirstName());
		queryRegistrationsCriteria.setReceiverLastName(queryRegistrationsRequest
				.getReceiverLastName());
		return queryRegistrationsCriteria;
	}

	private QueryRegistrationsRequest createRequest() {
		QueryRegistrationsRequest queryRegistrationsRequest = new QueryRegistrationsRequest();
		queryRegistrationsRequest.setAccountNumberLast4("1234");
		queryRegistrationsRequest.setDeliveryOption("Option1");
		queryRegistrationsRequest.setReceiveAgentID("1");
		queryRegistrationsRequest.setReceiveCountry("USA");
		queryRegistrationsRequest.setReceiveCurrency("USD");
		queryRegistrationsRequest.setReceiverFirstName("FirstName");
		queryRegistrationsRequest.setReceiverLastName("LastName");
		return queryRegistrationsRequest;
	}
}
