package com.moneygram.drs.command;

import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.criteria.CountryInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.request.CountryInfoRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class CountryInfoCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		List expectedCountryInfoList = ObjectMaster.getCountryInfoList();
		CountryInfoRequest countryInfoRequest = new CountryInfoRequest();
		CountryInfoCriteria criteria = new CountryInfoCriteria();
		// Find mock using criteria
		MockPersistenceManager.mockFind(criteria, expectedCountryInfoList);
		MockPersistenceManager.replayMocks();
		// Make call thru bean
		DRSBean drsBean = createDRSBean();
		List actualCountryInfoList = (List) drsBean.execute(countryInfoRequest);
		MockPersistenceManager.verifyMocks();
		assertEquals(expectedCountryInfoList, actualCountryInfoList);
	}
}
