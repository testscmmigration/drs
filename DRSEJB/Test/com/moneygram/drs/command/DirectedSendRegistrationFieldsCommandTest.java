package com.moneygram.drs.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import junit.framework.TestCase;
import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class DirectedSendRegistrationFieldsCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		FQDOCriteria fqdoCriteria = null;
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = createRequest();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(directedSendRegistrationFieldsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(directedSendRegistrationFieldsRequest.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		registrationFieldsInfoCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest
				.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(directedSendRegistrationFieldsRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(directedSendRegistrationFieldsRequest
				.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(directedSendRegistrationFieldsRequest
				.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(directedSendRegistrationFieldsRequest
				.getReceiveCurrency());
		ObjectId objectId = new ObjectId(RegistrationFieldsCollection.class,
				registrationFieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		FQDOInfo infoExpected = ObjectMaster.getFQDOInfo();
		// read fqdoinfo
		fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setReceiveCountry(directedSendRegistrationFieldsRequest.getReceiveCountry());
		fqdoCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest.getDeliveryOption());
		fqdoCriteria.setReceiveAgentID(directedSendRegistrationFieldsRequest.getReceiveAgentID());
		fqdoCriteria.setReceiveCurrency(directedSendRegistrationFieldsRequest.getReceiveCurrency());
		fqdoCriteria.setLanguage(directedSendRegistrationFieldsRequest.getLanguage());
		fqdoCriteria.setFqdoEnum(FqdoEnum.FQDO);
		objectId = new ObjectId(FQDOInfo.class, fqdoCriteria);
		MockPersistenceManager.mockRead(objectId, infoExpected, false);
		MockPersistenceManager.replayMocks();
		DRSBean drsBean = createDRSBean();
		DirectedSendRegistrationInfo directedSendRegistrationInfo = (DirectedSendRegistrationInfo) drsBean
				.execute(directedSendRegistrationFieldsRequest);
		assertNotNull(directedSendRegistrationInfo);
		Collection<ExtendedRegistrationFieldInfo> registrationFieldInfos = directedSendRegistrationInfo
				.getRegistrationFieldData();
		assertNotNull(registrationFieldInfos);
		List<ExtendedRegistrationFieldInfo> retList = new ArrayList<ExtendedRegistrationFieldInfo>();
		retList.addAll(registrationFieldInfos);
		RegistrationFieldsCollection retRfc = new RegistrationFieldsCollection(retList, "DCC", false,false);
		RegistrationFieldInfo expected = (RegistrationFieldInfo) retRfc
				.getInfoEntryOnFullList("RECEIVERFIRSTNAME").get(0);
		RegistrationFieldInfo actual = (RegistrationFieldInfo) expectedRegistrationFieldsCollection
				.getInfoEntryOnFullList("RECEIVERFIRSTNAME").get(0);
		assertEquals(expected.isRequired(),
				actual
						.isRequired());
	}

	private DirectedSendRegistrationFieldsRequest createRequest() {
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = new DirectedSendRegistrationFieldsRequest();
		directedSendRegistrationFieldsRequest.setDeliveryOption("CAMB_PLUS");
		directedSendRegistrationFieldsRequest.setLanguage("Eng");
		directedSendRegistrationFieldsRequest.setReceiveAgentID("12345678");
		directedSendRegistrationFieldsRequest.setReceiveCurrency("USD");
		directedSendRegistrationFieldsRequest.setReceiveCountry("USA");
		return directedSendRegistrationFieldsRequest;
	}
}
