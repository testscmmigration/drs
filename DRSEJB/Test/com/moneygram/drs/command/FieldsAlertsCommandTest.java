package com.moneygram.drs.command;

import java.util.Collection;

import junit.framework.TestCase;

import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.ejb.DRSBean;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.request.FieldsAlertsRequest;
import com.moneygram.drs.request.RegistrationFieldsAlertsRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;
import com.moneygram.testutils.ejb.TestEJBSessionContext;

public class FieldsAlertsCommandTest extends TestCase {
	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
	}

	private DRSBean createDRSBean() {
		DRSBean bean = new DRSBean();
		bean.setSessionContext(new TestEJBSessionContext());
		return bean;
	}

	public void testExecute() throws Exception {
		
		FieldsInfoCriteria fieldsInfoCriteria = new FieldsInfoCriteria();
		FieldsAlertsRequest fieldsAlertsRequest = createRequest();
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(fieldsAlertsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(fieldsAlertsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(fieldsAlertsRequest.getLanguage());
		MockPersistenceManager.mockAssertData(dsrCriteria, true);
		RegistrationFieldsCollection expectedRegistrationFieldsCollection = ObjectMaster
				.getRegistrationFieldCollection();
		fieldsInfoCriteria.setDeliveryOption(fieldsAlertsRequest
				.getDeliveryOption());
		fieldsInfoCriteria.setLanguage(fieldsAlertsRequest
				.getLanguage());
		fieldsInfoCriteria.setReceiveAgentId(fieldsAlertsRequest
				.getReceiveAgentID());
		fieldsInfoCriteria.setReceiveCountry(fieldsAlertsRequest
				.getReceiveCountry());
		fieldsInfoCriteria.setReceiveCurrency(fieldsAlertsRequest
				.getReceiveCurrency());
		fieldsInfoCriteria.setExternalReceiverProfile(fieldsAlertsRequest
				.isExternalReceiverProfile());
		//registrationFieldsInfoCriteria.setRegFldsAlrtsCrit(true);
		ObjectId objectId = new ObjectId(RegistrationFieldsCollection.class,
				fieldsInfoCriteria);
		MockPersistenceManager.mockRead(objectId, expectedRegistrationFieldsCollection);
		MockPersistenceManager.replayMocks();		
		DRSBean drsBean = createDRSBean();
		AlertRegistrationInfo alertRegistrationInfo = (AlertRegistrationInfo) drsBean
				.execute(fieldsAlertsRequest);
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertRegistrationInfo.getAlertActionCode());
		Collection<AlertErrorInfo> alertErrorInfo = alertRegistrationInfo.getErrorData();
		assertNotNull(alertRegistrationInfo);
		assertNotNull(alertErrorInfo);
		
		//List<AlertErrorInfo> errorList = new ArrayList<AlertErrorInfo>();
		//errorList.addAll(alertErrorInfo);
		
	}

	private FieldsAlertsRequest createRequest() {
		FieldsAlertsRequest fieldsAlertsRequest = new FieldsAlertsRequest();
		fieldsAlertsRequest.setDeliveryOption("BANK_DEPOSIT");
		fieldsAlertsRequest.setLanguage("ENG");
		fieldsAlertsRequest.setReceiveAgentID("48076876");
		fieldsAlertsRequest.setReceiveCurrency("MXN");
		fieldsAlertsRequest.setReceiveCountry("MEX");
		return fieldsAlertsRequest;
	}

}

