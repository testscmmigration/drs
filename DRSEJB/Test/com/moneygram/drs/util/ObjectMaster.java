package com.moneygram.drs.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.AuthenticationData;
import com.moneygram.drs.bo.CountryInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.DataTypeCodeEnum;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationInfo;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.xtra.validators.RealTimeValidator;

public class ObjectMaster {
	List registrationFields = new ArrayList();

	public static List<FQDOInfo> getFqdosForCountry() {
		List<FQDOInfo> fqdosForCountryList = new ArrayList<FQDOInfo>();
		FQDOInfo fqdoInfo = getFQDOInfo();
		fqdosForCountryList.add(fqdoInfo);
		return fqdosForCountryList;
	}

	public static FullFQDOWithRegistrationStatus getFqdoByCustomerReceiveNumber() {
		FullFQDOWithRegistrationStatus fqByCustomerReceiveNumber = new FullFQDOWithRegistrationStatus();
		FQDOInfo fqdoInfo = getFQDOInfo();
		fqByCustomerReceiveNumber.setFqdoInfo(fqdoInfo);
		fqByCustomerReceiveNumber.setRegistrationStatusCode("RegStatCode");
		return fqByCustomerReceiveNumber;
	}

	public static FQDOInfo getFQDOInfo() {
		FQDOInfo STD_FQDO = new FQDOInfo();
		STD_FQDO.setDeliveryOption("CAMB_PLUS");
		STD_FQDO.setDeliveryOptionDisplayName("Foo Display");
		STD_FQDO.setReceiveAgentID("12345678");
		STD_FQDO.setReceiveAgentAbbreviation("Rec Agent Abbr");
		STD_FQDO.setReceiveAgentName("Rec Agent Name");
		STD_FQDO.setReceiveCountry("PHL");
		STD_FQDO.setReceiveCurrency("USD");
		STD_FQDO.setRegistrationAuthorizationText("Enter authorization");
		STD_FQDO.setSpeedOfDeliveryText("Fast");
		return STD_FQDO;
	}

	public static List<RegistrationInfo> getRegistrationInfo() {
		List<RegistrationInfo> registrationInfoList = new ArrayList<RegistrationInfo>();
		RegistrationInfo registrationInfo = new RegistrationInfo();
		registrationInfo.setCreatorFirstName("FirstName");
		registrationInfo.setCreatorLastName("LastName");
		registrationInfo.setMgCustomerReceiveNumber("MgRRN");
		registrationInfo.setReceiverFirstName("ReceiverFN");
		registrationInfo.setReceiverLastName("ReceiverLastN");
		registrationInfo.setMgCustomerReceiveNumberVersion(new BigInteger("12345"));
		registrationInfo.setReceiverPhoneNumber("123456789");
		registrationInfo.setAccountNickname("Account Nickname");
		registrationInfo.setFqdoInfo(getFQDOInfo());
		registrationInfoList.add(registrationInfo);
		return registrationInfoList;
	}

	public static List<CountryInfo> getCountryInfoList() {
		List<CountryInfo> countryInfoList = new ArrayList<CountryInfo>();
		CountryInfo countryInfo = new CountryInfo();
		countryInfo.setCountryCode("TESTCOUNTRYCODE");
		countryInfo.setCountryLegacyCode("TESTCOUNTRYLEGACYCODE");
		countryInfo.setCountryName("TESTCOUNTRYNAME");
		countryInfo.setDirectedSendCountry(true);
		countryInfo.setMgDirectedSendCountry(true);
		countryInfo.setReceiveActive(true);
		countryInfo.setSendActive(true);
		countryInfoList.add(countryInfo);
		return countryInfoList;
	}

	public static CustomerProfile getCustomerProfile() {
		CustomerProfile cp = new CustomerProfile();
		Address receiverAddr = new Address();
		receiverAddr.setAddrLine1("100 wilson lane");
		receiverAddr.setCity("Mpls");
		receiverAddr.setState("MN");
		receiverAddr.setPostalCode("55443");
		receiverAddr.setCntryCode("USA");
		CustomerProfilePerson receiver = new CustomerProfilePerson();
		receiver.setFirstName("Fred");
		receiver.setLastName("Flintstone");
		receiver.setPhoneNumber("8881112222");
		receiver.addAddress(receiverAddr);
		CustomerProfilePerson jah1 = new CustomerProfilePerson();
		jah1.setFirstName("Wilma");
		jah1.setLastName("Flintstone");
		jah1.setPhoneNumber("888222111");
		jah1.addAddress(receiverAddr);
		cp.setReceiver(receiver);
		cp.addJointAccountHolder(jah1, 1);
		cp.setCustomerReceiveNumber("MG11119518");
		cp.setRcvAgentBankID("BankID");
		cp.setRcvAgentCustAcctName("Checking");
		cp.setRcvAgentCustAcctNbr("1232322");
		cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
		cp.setRegistrationSubStatus("INI");
		cp.setOfacStatus("P");
		cp.setCustomerReceiveNumberVersion(1);
		CustomerProfileCustomField cpcf = new CustomerProfileCustomField("TEST_INT", "42");
		cp.addCustomFields(cpcf, "TEST_INT");
		return cp;
	}

	public static RegistrationFieldsCollection getRegistrationFieldCollection() throws Exception {
		List<ExtendedRegistrationFieldInfo> registrationFields = new ArrayList<ExtendedRegistrationFieldInfo>();
		registrationFields.add(createRegFieldInfo("RECEIVERFIRSTNAME", "string", false, true, 2,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERLASTNAME", "string", false, true, 2, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVER2NDLASTNAME", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERPHONE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERADDRESS1", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERCITY", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERSTATE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERPOSTALCODE", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERCOUNTRY", "cntrycode", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("PASSWORD", "string", true, true, 0, 18, 0, null,
				false, true));
		registrationFields.add(createRegFieldInfo("PASSWORD_HIDDEN", "string", true, true, 0, 18,
				0, null, false, true));
		registrationFields.add(createRegFieldInfo("confirm", "string", false, false, 0, 18, 0,
				null, false, true));
		registrationFields.add(createRegFieldInfo("confirm_HIDDEN", "string", false, false, 0, 18,
				0, null, false, true));
		registrationFields.add(createRegFieldInfo("JAH1PHONE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1FIRSTNAME", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("JAH1LASTNAME", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1PHONE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1ADDRESS1", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1CITY", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1STATE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1POSTALCODE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("JAH1COUNTRY", "cntrycode", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("TEST_BOOLEAN", "boolean", false, false, 0, 18,
				0, null, false));
		// Regexp from:
		// http://www.javaworld.com/javaworld/jw-07-2001/jw-0713-regex.html
		registrationFields.add(createRegFieldInfo("TEST_REGEX", "string", false, false, 0, 18, 0,
				"[0-9]{3}\\-?[0-9]{2}\\-?[0-9]{4}", false));
		registrationFields.add(createRegFieldInfo("TEST_INT", "int", false, false, 1, 20, 0, null,
				false));
		registrationFields.add(createRegFieldInfo("TEST_DEC", "decimal", false, false, 1, 20, 2,
				null, false));
		registrationFields.add(createRegFieldInfo("TEST_CHK_DIG", "string", false, false, 0, 40, 0,
				null, false, null, false, "LUHNmod10"));
		return new RegistrationFieldsCollection(registrationFields, "DCC", false,false);
	}

	public static ExtendedRegistrationFieldInfo createRegFieldInfo(String xmlTag, String type,
			boolean hidden, boolean required, int min, int max, int scale, String regExp,
			boolean readOnly) {
		return createRegFieldInfo(xmlTag, type, hidden, required, min, max, scale, regExp,
				readOnly, null, false, null);
	}

	public static ExtendedRegistrationFieldInfo createRegFieldInfo(String xmlTag, String type,
			boolean hidden, boolean required, int min, int max, int scale, String regExp,
			boolean readOnly, boolean confirmField) {
		return createRegFieldInfo(xmlTag, type, hidden, required, min, max, scale, regExp,
				readOnly, null, confirmField, null);
	}

	public static ExtendedRegistrationFieldInfo createRegFieldInfo(String xmlTag, String type,
			boolean hidden, boolean required, int min, int max, int scale, String regExp,
			boolean readOnly, String value, boolean confirmField, String chkDigAlg) {
		ExtendedRegistrationFieldInfo rfi = new ExtendedRegistrationFieldInfo();
		rfi.setXmlTag(xmlTag);
		rfi.setDataTypeCode(DataTypeCodeEnum.BOOLEAN);
		rfi.setHidden(hidden);
		rfi.setRequired(required);
		rfi.setFieldMin(new Integer(min));
		rfi.setFieldMax(new Integer(max));
		rfi.setFieldScale(new Integer(scale));
		rfi.setFieldValue(value);
		rfi.setValidationRegEx(regExp);
		rfi.setConfirmEntry(confirmField);
		rfi.setCheckDigitAlgorithm(chkDigAlg);
		return rfi;
	}

	public static Object getRegistrationData() throws Exception {
		RegistrationData registrationData = new RegistrationData();
		registrationData.setRegistrationFieldInfo(getRegistrationfieldInfoList());
		registrationData.setFqdoInfo(getFQDOInfo());
		registrationData.setOfacExceptionStateCode("Pass");
		registrationData.setRegistrationStatus(RegistrationStatusCode.ACTIVE);
		registrationData.setRegistrationSubStatus("INI");
		return registrationData;
	}

	private static List<RegistrationFieldInfo> getRegistrationfieldInfoList() {
		List<RegistrationFieldInfo> registrationFields = new ArrayList<RegistrationFieldInfo>();
		registrationFields.add(createRegFieldInfo("receiverFirstName", "string", false, true, 2,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("receiverLastName", "string", false, true, 2, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVER2NDLASTNAME", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERPHONE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERADDRESS1", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERCITY", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERSTATE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERPOSTALCODE", "string", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("RECEIVERCOUNTRY", "cntrycode", false, false, 0,
				18, 0, null, false));
		registrationFields.add(createRegFieldInfo("password", "string", true, true, 0, 18, 0, null,
				false, true));
		registrationFields.add(createRegFieldInfo("password_HIDDEN", "string", true, true, 0, 18,
				0, null, false, true));
		registrationFields.add(createRegFieldInfo("confirm", "string", false, false, 0, 18, 0,
				null, false, true));
		registrationFields.add(createRegFieldInfo("confirm_HIDDEN", "string", false, false, 0, 18,
				0, null, false, true));
		registrationFields.add(createRegFieldInfo("JAH1PHONE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1FIRSTNAME", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("JAH1LASTNAME", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1PHONE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1ADDRESS1", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1CITY", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1STATE", "string", false, false, 0, 18, 0,
				null, false));
		registrationFields.add(createRegFieldInfo("JAH1POSTALCODE", "string", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("JAH1COUNTRY", "cntrycode", false, false, 0, 18,
				0, null, false));
		registrationFields.add(createRegFieldInfo("TEST_BOOLEAN", "boolean", false, false, 0, 18,
				0, null, false));
		// Regexp from:
		// http://www.javaworld.com/javaworld/jw-07-2001/jw-0713-regex.html
		registrationFields.add(createRegFieldInfo("TEST_REGEX", "string", false, false, 0, 18, 0,
				"[0-9]{3}\\-?[0-9]{2}\\-?[0-9]{4}", false));
		registrationFields.add(createRegFieldInfo("TEST_INT", "int", false, false, 1, 20, 0, null,
				false));
		registrationFields.add(createRegFieldInfo("TEST_DEC", "decimal", false, false, 1, 20, 2,
				null, false));
		registrationFields.add(createRegFieldInfo("TEST_CHK_DIG", "string", false, false, 0, 40, 0,
				null, false, null, false, "LUHNmod10"));
		return registrationFields;
	}

	public static AuthenticationData getAuthenticationData() {
		AuthenticationData authenticationData = new AuthenticationData();
		authenticationData.setAuthenticationCode("true");
		return authenticationData;
	}

	public static RealTimeValidationRequest getRealTimeValidationRequest() {
		RealTimeValidationRequest request = new RealTimeValidationRequest();
		HashMap<String, String> commMap = new HashMap<String, String>();
		commMap.put(RealTimeValidator.CLASS_NAME_KEY,
				"com.moneygram.drs.xtra.validators.RTDRSValidatorMock");
		request.setContext(commMap);
		return request;
	}
}
