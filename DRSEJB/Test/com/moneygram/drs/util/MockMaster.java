package com.moneygram.drs.util;

import org.easymock.MockControl;
import com.moneygram.drs.notification.NotificationManager;
import com.moneygram.drs.notification.NotificationManagerFactory;
import com.moneygram.drs.persistence.PersistenceManager;
import com.moneygram.drs.persistence.PersistenceManagerFactory;

public class MockMaster {
	public static PersistenceManager persistenceManager;
	public static MockControl persistenceManagerControl;
	public static NotificationManager notfManager;
	public static MockControl notfManagerControl;

	public static void setUpManagers() throws Exception {
		persistenceManagerControl = MockControl.createControl(PersistenceManager.class);
		persistenceManager = (PersistenceManager) persistenceManagerControl.getMock();
		ObjectUtils
				.setPrivateField(PersistenceManagerFactory.class, "instance", persistenceManager);
		notfManagerControl = MockControl.createControl(NotificationManager.class);
		notfManager = (NotificationManager) notfManagerControl.getMock();
		ObjectUtils.setPrivateField(NotificationManagerFactory.class, "instance", notfManager);
	}

	public static void tearDownManagers() throws Exception {
		ObjectUtils.setPrivateField(PersistenceManagerFactory.class, "instance", null);
		ObjectUtils.setPrivateField(NotificationManagerFactory.class, "instance", null);
	}
}
