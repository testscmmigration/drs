package com.moneygram.drs.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.easymock.MockControl;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.testutils.ObjectAccessorMatcher;

public class MockPersistenceManager {
	private static Set<String> controlMatcherSet = new HashSet<String>();

	public static void setUp() throws Exception {
		controlMatcherSet.clear();
		MockMaster.setUpManagers();
	}

	public static void tearDown() throws Exception {
		controlMatcherSet.clear();
		MockMaster.tearDownManagers();
	}

	public static void mockSave(BusinessObject businessObject) throws Exception {
		mockSave(businessObject, true);
	}

	public static void mockSave(BusinessObject businessObject, boolean setMatcher) throws Exception {
		MockMaster.persistenceManager.save(businessObject);
		if (setMatcher) {
			MockMaster.persistenceManagerControl.setMatcher(new ObjectAccessorMatcher());
		}
		MockMaster.persistenceManagerControl.setVoidCallable();
	}

	public static void mockRead(ObjectId objectId, BusinessObject businessObject) throws Exception {
		mockRead(objectId, businessObject, true);
	}

	public static void mockRead(ObjectId objectId, BusinessObject businessObject, boolean setmatcher)
			throws Exception {
		MockMaster.persistenceManager.read(objectId);
		if (setmatcher) {
			MockMaster.persistenceManagerControl.setMatcher(new ObjectAccessorMatcher());
		}
		MockMaster.persistenceManagerControl.setReturnValue(businessObject);
	}
	static final String mockFindMatcherToken = "mockFindMatcherToken";

	public static void mockFind(Criteria criteria, List list) throws Exception {
		MockMaster.persistenceManager.find(criteria);
		setMatcher(MockMaster.persistenceManagerControl, mockFindMatcherToken,
				new ObjectAccessorMatcher());
		MockMaster.persistenceManagerControl.setReturnValue(list);
	}

	public static void mockRead(ObjectId objectId, BusinessObject businessObject,
			ObjectId objectId2, BusinessObject businessObject2) throws Exception {
		MockMaster.persistenceManager.read(objectId);
		MockMaster.persistenceManagerControl.setMatcher(new ObjectAccessorMatcher());
		MockMaster.persistenceManagerControl.setReturnValue(businessObject);
		MockMaster.persistenceManager.read(objectId2);
		MockMaster.persistenceManagerControl.setReturnValue(businessObject2);
	}

	public static void replayMocks() {
		MockMaster.persistenceManagerControl.replay();
	}

	public static void verifyMocks() {
		MockMaster.persistenceManagerControl.verify();
	}

	public static void resetMocks() {
		controlMatcherSet.clear();
		MockMaster.persistenceManagerControl.reset();
	}

	public static void setMatcher(MockControl control, String matcherToken,
			ObjectAccessorMatcher matcher) {
		if (!controlMatcherSet.contains(matcherToken)) {
			controlMatcherSet.add(matcherToken);
			control.setMatcher(matcher);
		}
	}

	public static void mockRead(ObjectId objectId, NotFoundException exception)
			throws NotFoundException, DRSException {
		MockMaster.persistenceManager.read(objectId);
		MockMaster.persistenceManagerControl.setMatcher(new ObjectAccessorMatcher());
		MockMaster.persistenceManagerControl.setThrowable(exception);
	}

	public static void mockAssertData(Criteria criteria, boolean isAllowed) throws Exception {
		MockMaster.persistenceManager.assertData(criteria);
		MockMaster.persistenceManagerControl.setMatcher(new ObjectAccessorMatcher());
		MockMaster.persistenceManagerControl.setReturnValue(isAllowed);
	}

	public static void mockNotifyDSSOfRegistrationChange(String customerReceiveNumber)
			throws Exception {
		MockMaster.notfManager.notifyDSSOfRegistrationChange(customerReceiveNumber);
		MockMaster.notfManagerControl.setVoidCallable();
	}
}
