package com.moneygram.drs.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import com.moneygram.testutils.jdbc.TestDataSource;

public class TestDataHelper {
	private TestDataSource ds;

	public TestDataHelper(TestDataSource ds) {
		this.ds = ds;
	}

	public ResultSet executeQuery(String queryString) throws Exception {
		Connection conn = null;
		conn = ds.getConnection();
		System.out.println("EXECUTING TEST SQL: " + queryString);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(queryString);
		return rs;
	}

	public int executeUpdate(String queryString) throws Exception {
		Connection conn = ds.getConnection();
		System.out.println("EXECUTING TEST SQL: " + queryString);
		Statement stmt = conn.createStatement();
		return stmt.executeUpdate(queryString);
	}
}
