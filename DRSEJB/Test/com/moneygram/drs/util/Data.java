package com.moneygram.drs.util;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;

public class Data {
	public RegistrationFieldsCollection create(int min, int max) {
		List<ExtendedRegistrationFieldInfo> fields = new ArrayList<ExtendedRegistrationFieldInfo>();
		for (int i = min; i <= max; i++) {
			add(fields, i);
		}
		return new RegistrationFieldsCollection(fields, "abc", true, false);
	}

	public class Builder {
		List<ExtendedRegistrationFieldInfo> fields = new ArrayList<ExtendedRegistrationFieldInfo>();

		public Builder add(int index, boolean required) {
			Data.this.add(fields, index, required);
			return this;
		}

		public RegistrationFieldsCollection get() {
			return new RegistrationFieldsCollection(fields, "abc", true, false);
		}
	}
	
	public RegistrationFieldsCollection create(boolean saveRegistrationWithNewXmlTags, int... indices) {
		List<ExtendedRegistrationFieldInfo> fields = new ArrayList<ExtendedRegistrationFieldInfo>();
		for (int i : indices) {
			add(fields, i);
		}
		return new RegistrationFieldsCollection(fields, "abc", true, saveRegistrationWithNewXmlTags);
	}

	public RegistrationFieldsCollection create(int... indices) {
		return create(false, indices);
	}

	public Builder builder() {
		return new Builder();
	}

	private void add(List<ExtendedRegistrationFieldInfo> fields, int index) {
		add(fields, index, false);
	}

	private void add(List<ExtendedRegistrationFieldInfo> fields, int index,
			boolean required) {
		ExtendedRegistrationFieldInfo field = new ExtendedRegistrationFieldInfo();
		field.setAttrID(index);
		field.setXmlTag("tag_" + index);
		field.setNewXmlTag("new_tag_" + index);
		field.setRequired(required);
		
		fields.add(field);
	}
}
