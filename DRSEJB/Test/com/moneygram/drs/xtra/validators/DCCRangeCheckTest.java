package com.moneygram.drs.xtra.validators;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.SaveRegistrationRequest;

public class DCCRangeCheckTest extends DaoTestCase {
	public void testValidateCardRangeThrowException() throws Exception {
		try {
			CustomerProfile profile = new CustomerProfile();
			SaveRegistrationRequest req = new SaveRegistrationRequest();
			profile.setRcvAgentCustAcctNbr("001");
			req.setReceiveAgentID("86000286");
			req.setReceiveCurrency("USD");
			DCCRangeCheck check = new DCCRangeCheck();
			check.validate(req, profile, new CustomerProfile(), false);
			fail("Exception has to be thrown");
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC362_DCC_CARD_RNG_FAILURE, e.getErrorCode());
			// TODO need to change this number once it is determined -- US
		}
	}

	public void testValidateCardRange() throws Exception {
		try {
			CustomerProfile profile = new CustomerProfile();
			SaveRegistrationRequest req = new SaveRegistrationRequest();
			profile.setRcvAgentCustAcctNbr("4001571000000000");
			req.setReceiveAgentID("86000286");
			req.setReceiveCurrency("USD");
			DCCRangeCheck check = new DCCRangeCheck();
			check.validate(req, profile, new CustomerProfile(), false);
		} catch (DRSException e) {
			assertEquals(DRSExceptionCodes.EC362_DCC_CARD_RNG_FAILURE, e.getErrorCode());
			fail("Exception  thrown");
		}
	}
}
