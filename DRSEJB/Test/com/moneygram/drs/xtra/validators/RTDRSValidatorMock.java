package com.moneygram.drs.xtra.validators;

import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.bo.RealTimeValidationResponse;
import com.moneygram.drs.realtime.comm.RTDRSValidator;

public class RTDRSValidatorMock implements RTDRSValidator {
	public static final int ERROR_CODE = 0;
	public static final String FIELD_ACCOUNT_NUMBER = "AccountNumber";
	public static final String IN_VALID_NUMBER = "InValidNumber";

	public RealTimeValidationResponse validate(RealTimeValidationRequest request) throws Exception {
		RealTimeValidationResponse response = new RealTimeValidationResponse();
		if (request.getCustomerProfile() != null) {
			if (request.getCustomerProfile().getRcvAgentCustAcctNbr() == IN_VALID_NUMBER) {
				response.setResult(RealTimeValidationResponse.Result.FAIL);
				response.setErrorCode(ERROR_CODE);
				response.setOffendingField(FIELD_ACCOUNT_NUMBER);
				response.setErrorString("MockClass");
				return response;
			}
		}
		response.setResult(RealTimeValidationResponse.Result.PASS);
		return response;
	}
}
