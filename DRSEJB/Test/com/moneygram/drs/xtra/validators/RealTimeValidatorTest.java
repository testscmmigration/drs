package com.moneygram.drs.xtra.validators;

import junit.framework.TestCase;
import java.util.ArrayList;
import java.util.List;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.criteria.RealTimeInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.util.MockPersistenceManager;
import com.moneygram.drs.util.ObjectMaster;

public class RealTimeValidatorTest extends TestCase {
	RealTimeValidator validator;

	public void setUp() throws Exception {
		super.setUp();
		MockPersistenceManager.setUp();
		validator = new RealTimeValidator();
	}

	public void tearDown() throws Exception {
		super.tearDown();
		MockPersistenceManager.tearDown();
		validator = null;
	}

	public void testValidate() throws Exception {
		RealTimeValidationRequest expectedreq = ObjectMaster.getRealTimeValidationRequest();// Set
																							// IMPl
																							// class
																							// as
																							// the
																							// mock
																							// Class
		RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
		criteria.setAgentId("TestAgentID");
		ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
		// Find mock using criteria
		MockPersistenceManager.mockRead(objectID, expectedreq);
		MockPersistenceManager.replayMocks();
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setReceiveAgentID("TestAgentID");
		validator.validate(request, null, null, true);
	}

	public void testValidateThrowsValidationException() throws Exception {
		try {
			RealTimeValidationRequest expectedreq = ObjectMaster.getRealTimeValidationRequest();// Set
																								// IMPl
																								// class
																								// as
																								// the
																								// mock
																								// Class
			RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
			criteria.setAgentId("TestAgentID");
			ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
			// Find mock using criteria
			MockPersistenceManager.mockRead(objectID, expectedreq);
			MockPersistenceManager.replayMocks();
			SaveRegistrationRequest request = new SaveRegistrationRequest();
			request.setReceiveAgentID("TestAgentID");
			CustomerProfile cp = new CustomerProfile();
			cp.setRcvAgentCustAcctNbr(RTDRSValidatorMock.IN_VALID_NUMBER);// request
																			// mock
																			// class
																			// to
																			// return
																			// ERROR
			validator.validate(request, cp, null, true);
			fail();// Should have thrown Exception
		} catch (DRSException e) {
			assertTrue(RTDRSValidatorMock.ERROR_CODE == e.getErrorCode());
			assertEquals(RTDRSValidatorMock.FIELD_ACCOUNT_NUMBER, e.getOffendingField());
		}
	}
	public void testValidateSkipValidationException() throws Exception {
		RealTimeValidationRequest expectedreq = ObjectMaster.getRealTimeValidationRequest();// Set
																							// IMPl
																							// class
																							// as
																							// the
																							// mock
																							// Class
		RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
		criteria.setAgentId("TestAgentID");
		ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
		// Find mock using criteria
		MockPersistenceManager.mockRead(objectID, expectedreq);
		MockPersistenceManager.replayMocks();
		SaveRegistrationRequest request = new SaveRegistrationRequest();
		request.setReceiveAgentID("TestAgentID");
		List<String> complianceUpdates = new ArrayList<String>();
		complianceUpdates.add("RECEIVER_PHONE");
		request.setComplianceUpdates(complianceUpdates);
		CustomerProfile cp = new CustomerProfile();
		cp.setRcvAgentCustAcctNbr(RTDRSValidatorMock.IN_VALID_NUMBER);// request
																		// mock
																		// class
																		// to
																		// return
																		// ERROR
		validator.validate(request, cp, null, false);  // changing false to true for creating will make test fail.
}
	public void testValidateComplianceCreatingThrowsException() throws Exception {
		try {
			RealTimeValidationRequest expectedreq = ObjectMaster.getRealTimeValidationRequest();// Set
																								// IMPl
																								// class
																								// as
																								// the
																								// mock
																								// Class
			RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
			criteria.setAgentId("TestAgentID");
			ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
			// Find mock using criteria
			MockPersistenceManager.mockRead(objectID, expectedreq);
			MockPersistenceManager.replayMocks();
			SaveRegistrationRequest request = new SaveRegistrationRequest();
			request.setReceiveAgentID("TestAgentID");
			List<String> complianceUpdates = new ArrayList<String>();
			complianceUpdates.add("RECEIVER_PHONE");
			request.setComplianceUpdates(complianceUpdates);
			CustomerProfile cp = new CustomerProfile();
			cp.setRcvAgentCustAcctNbr(RTDRSValidatorMock.IN_VALID_NUMBER);// request
																			// mock
																			// class
																			// to
																			// return
																			// ERROR
			validator.validate(request, cp, null, true);  // if change creating to false test will fail as no Exception is thrown.
			fail();// Should have thrown Exception
		} catch (DRSException e) {
			assertTrue(RTDRSValidatorMock.ERROR_CODE == e.getErrorCode());
			assertEquals(RTDRSValidatorMock.FIELD_ACCOUNT_NUMBER, e.getOffendingField());
		}
	}
}
