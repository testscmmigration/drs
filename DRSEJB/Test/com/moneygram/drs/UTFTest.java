package com.moneygram.drs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import junit.framework.AssertionFailedError;

/**
 * Finds characters that our Linux systems won't like because they are not
 * available in the charset. These characters should be converted to unicode
 * references.
 * 
 * @author C50035
 * 
 */
public class UTFTest extends junit.framework.TestCase {
	private static final String UTF_8 = "UTF-8";
	private static final String ISO_8859_1 = "ISO-8859-1";

	public void testVerifyUTF8OnAllHtmlFiles() throws Exception {
		String classPath = "ejbModule";
		System.out.println("Classpath: " + classPath);
		StringTokenizer tokenizer = new StringTokenizer(classPath, File.pathSeparator);
		ArrayList<String> htmlFiles = new ArrayList<String>();
		while (tokenizer.hasMoreTokens()) {
			File file = new File(tokenizer.nextToken());
			if (file.isDirectory()) {
				htmlFiles.addAll(getHtmlFileNames(file));
			}
		}
		for (Iterator iter = htmlFiles.iterator(); iter.hasNext();) {
			assertUTF8((String) iter.next());
		}
	}

	public Collection<String> getHtmlFileNames(File directory) {
		String[] fileNames = directory.list();
		List<String> htmlFiles = new ArrayList<String>();
		for (int i = 0; i < fileNames.length; i++) {
			String fullPath = directory.getPath() + File.separator + fileNames[i];
			if (fullPath.endsWith(".java")) {
				htmlFiles.add(fullPath);
			} else {
				File otherFile = new File(fullPath);
				if (otherFile.isDirectory()) {
					htmlFiles.addAll(getHtmlFileNames(otherFile));
				}
			}
		}
		return htmlFiles;
	}

	public void assertUTF8(String fileName) throws IOException {
		System.out.println("VERIFYING:  " + fileName);
		Charset charSetUTF8 = Charset.forName(UTF_8);
		Charset charSet8859 = Charset.forName(ISO_8859_1);
		CharsetDecoder utf8Decoder = charSetUTF8.newDecoder();
		CharsetDecoder utf8859Decoder = charSet8859.newDecoder();
		FileInputStream stream = new FileInputStream(fileName);
		byte[] b = new byte[1];
		int total = stream.available();
		int i = stream.read(b);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while (i != -1) {
			baos.write(b);
			ByteBuffer byteBuffer = ByteBuffer.wrap(b);
			CharBuffer charBuffer;
			try {
				charBuffer = utf8Decoder.decode(byteBuffer);
			} catch (MalformedInputException e) {
				System.out.print(baos.toString());
				charBuffer = utf8859Decoder.decode(byteBuffer);
				throw new AssertionFailedError(charBuffer + " is invalidCharater, at "
						+ (total - stream.available() + " in " + fileName));
			}
			i = stream.read(b);
		}
	}
}
