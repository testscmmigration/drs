package com.moneygram.drs.cache;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.drs.DaoTestCase;

public class PhoneCountryCodeInfoCacheTest extends DaoTestCase {
	public void testFind() throws Exception {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContextEhCache.xml");
		PhoneCountryCodeCache phoneCountryCodeCache = (PhoneCountryCodeCache) appContext.getBean("getPhoneCountryCode");
		//assertEquals(null, phoneCountryCodeCache.getCountryToPhoneCountryCode("HND"));
		assertEquals("1",phoneCountryCodeCache.getCountryToPhoneCountryCode("USA"));
		assertEquals("39", phoneCountryCodeCache.getCountryToPhoneCountryCode("VAT"));
		assertEquals("290", phoneCountryCodeCache.getCountryToPhoneCountryCode("SHN"));
		assertEquals(null, phoneCountryCodeCache.getCountryToPhoneCountryCode("HMD"));
		
	}

}
