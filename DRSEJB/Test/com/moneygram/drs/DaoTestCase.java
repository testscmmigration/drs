package com.moneygram.drs;

import junit.framework.TestCase;
import com.moneygram.drs.util.TestDataHelper;
import com.moneygram.testutils.jdbc.TestDataSource;
import com.moneygram.testutils.naming.TestContext;
import com.moneygram.testutils.naming.TestInitialContextFactory;

public abstract class DaoTestCase extends TestCase {
	//protected TestDataSource dataSource = new TestDataSource("oracle.jdbc.OracleDriver","jdbc:oracle:thin:@tped1.ad.moneygram.com:1521:tped1", "objects", "objects");

//  D1			
//	protected TestDataSource dataSource = new TestDataSource("oracle.jdbc.OracleDriver",
//			"jdbc:oracle:thin:@(description=(ADDRESS_LIST=(address=(protocol=tcp)(host=dmnalx4208.ad.moneygram.com)(port=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=tped1s1.ad.moneygram.com)))", "objects", "objects");
	
//  D5
	protected TestDataSource dataSource = new TestDataSource("oracle.jdbc.OracleDriver",
			"jdbc:oracle:thin:@(description=(ADDRESS_LIST=(address=(protocol=tcp)(host=dmnalx4208.ad.moneygram.com)(port=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=tped5s1.ad.moneygram.com)))", "objects", "objects");

	protected TestDataHelper dataHelper;

	protected void setUp() throws Exception {
		super.setUp();
		dataHelper = new TestDataHelper(dataSource);
		TestContext.objectMap.put("java:comp/env/jdbc/TPEDataSource", dataSource);
		TestInitialContextFactory.setupFactory();
	}

	protected void tearDown() throws Exception {
		dataSource.releaseConnections();
		super.tearDown();
	}
}
