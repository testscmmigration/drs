package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;

public class RegistrationFieldsAlertsDAOTest extends DaoTestCase {
	public void testReadRegistrationFieldsAlertsCollection() throws Exception {
		RegistrationFieldsInfoCriteria registrationFieldsAlertsInfoCriteria = new RegistrationFieldsInfoCriteria();
		registrationFieldsAlertsInfoCriteria.setReceiveCountry("MEX");
		registrationFieldsAlertsInfoCriteria.setDeliveryOption("BANK_DEPOSIT");
		//registrationFieldsInfoCriteria.setReceiveAgentId("44087720");
		registrationFieldsAlertsInfoCriteria.setReceiveAgentId("48076876");
		registrationFieldsAlertsInfoCriteria.setReceiveCurrency("MXN");
		registrationFieldsAlertsInfoCriteria.setLanguage("ENG");
		RegistrationFieldsAlertsDAO registrationFieldsDAO = new RegistrationFieldsAlertsDAO();
		RegistrationFieldsCollection registrationFieldsCollection = (RegistrationFieldsCollection) registrationFieldsDAO
				.read(new ObjectId(RegistrationFieldsCollection.class,
						registrationFieldsAlertsInfoCriteria));
		assertNotNull(registrationFieldsCollection);
	}
}
