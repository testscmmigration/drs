package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public class CustomerProfileDaoTest extends DaoTestCase {
	public void testReadCustomerProfile() throws NotFoundException, DRSException {
		CustomerProfileDAO customerProfileDAO = new CustomerProfileDAO();
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(false);
		customerProfileCriteria.setCustomerReceiveNumber("MG14200380");
		customerProfileCriteria.setProfileVersion(1);
		ObjectId objectId = new ObjectId(CustomerProfile.class, customerProfileCriteria);
		CustomerProfile customerProfile = (CustomerProfile) customerProfileDAO.read(objectId);
		assertEquals("P", customerProfile.getOfacStatus());
		assertEquals("4321", customerProfile.getRcvAgentCustAcctNbr());
		assertEquals("First", customerProfile.getCreator().getFirstName());
		assertEquals("Last", customerProfile.getCreator().getLastName());
		assertTrue(customerProfile.getJointAccountHolders() != null);
	}

	public void testReadAbbrCustomerProfile() throws NotFoundException, DRSException {
		CustomerProfileDAO customerProfileDAO = new CustomerProfileDAO();
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(false);
		customerProfileCriteria.setCustomerReceiveNumber("MG14200380");
		customerProfileCriteria.setProfileVersion(1);
		customerProfileCriteria.setAbbrCustomerProfile(true);
		ObjectId objectId = new ObjectId(CustomerProfile.class, customerProfileCriteria);
		CustomerProfile customerProfile = (CustomerProfile) customerProfileDAO.read(objectId);
		assertEquals("P", customerProfile.getOfacStatus());
		assertEquals("4321", customerProfile.getRcvAgentCustAcctNbr());
		assertTrue(customerProfile.getCreator() == null);
		assertTrue(customerProfile.getJointAccountHolders().isEmpty());
	}
}
