package com.moneygram.drs.persistence;

import java.sql.ResultSet;
import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.SensitiveIDInfo;

public class SaveSensitiveIDDAOTest extends DaoTestCase {
	public void testUpdate() throws Exception {
		SensitiveIDInfo info = new SensitiveIDInfo();
		info.setCustomerReceiveNumber("MG11119518");
		info.setCustomerReceiveNumberVersion(1);
		info.setSensitiveID("1234");
		SaveSensitiveIDDAO dao = new SaveSensitiveIDDAO();
		dao.save(info);
		String sql = "Select cp.sens_data_val_id from cust_profile cp  where cp.cust_rcv_nbr = 'MG11119518' and cp.cust_prfl_ver_nbr = 1";
		ResultSet rs = dataHelper.executeQuery(sql);
		assertNotNull(rs);
		assertTrue(rs.next());
		assertEquals("1234", rs.getString(1));
	}
}
