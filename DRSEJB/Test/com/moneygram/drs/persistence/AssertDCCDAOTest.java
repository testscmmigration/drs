package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.criteria.AssertDCCCriteria;

public class AssertDCCDAOTest extends DaoTestCase {
	public void testAsserDataReturnFalse() throws Exception {
		AssertDCCDAO dao = new AssertDCCDAO();
		AssertDCCCriteria criteria = new AssertDCCCriteria();
		criteria.setAgentId("43706167");
		criteria.setAccountNumber("0000001111111111");
		criteria.setCurrency("INR");
		assertFalse(dao.assertData(criteria));
	}

	public void xtestAsserDataReturnTrue() throws Exception {
		AssertDCCDAO dao = new AssertDCCDAO();
		AssertDCCCriteria criteria = new AssertDCCCriteria();
		criteria.setAgentId("43706167");
		criteria.setAccountNumber("1234567777777777");
		criteria.setCurrency("USD");
		assertFalse(dao.assertData(criteria));
	}
}
