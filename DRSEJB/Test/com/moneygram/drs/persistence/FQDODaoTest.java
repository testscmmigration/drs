package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public class FQDODaoTest extends DaoTestCase {
	public void testReadFQDOForFqdo() throws NotFoundException, DRSException {
		FQDOCriteria fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setDeliveryOption("Bank_Deposit");
		fqdoCriteria.setLanguage("ENG");
		fqdoCriteria.setReceiveAgentID("44087733");
		fqdoCriteria.setReceiveCountry("MEX");
		fqdoCriteria.setReceiveCurrency("MXP");
		fqdoCriteria.setFqdoEnum(FqdoEnum.FQDO);
		fqdoCriteria.setCustomerReceiveNumber("MG11174463");
		FQDODao fqDao = new FQDODao();
		FQDOInfo fqdoInfo = (FQDOInfo) fqDao.read(new ObjectId(FQDODao.class, fqdoCriteria));
		System.out.println(fqdoInfo);
		// TODO need to fix this
		// assertTrue(fqdoInfo.getDeliveryOption().equals("BANK_DEPOSIT"));
		// assertTrue(fqdoInfo.getDeliveryOptionDisplayName().equals("Account
		// Deposit"));
		// assertTrue(fqdoInfo.getReceiveCountry().equals("MEX"));
		// assertTrue(fqdoInfo.getReceiveCurrency().equals("MXN"));
		// assertTrue(fqdoInfo.getReceiveAgentName().equals("WELLS FARGO MX BANK
		// ACCT DEP"));
	}
}
