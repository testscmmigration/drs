package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;

public class RegistrationFieldsDAOTest extends DaoTestCase {
	public void testReadRegistrationFieldCollection() throws Exception {
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		registrationFieldsInfoCriteria.setReceiveCountry("MEX");
		registrationFieldsInfoCriteria.setDeliveryOption("BANK_DEPOSIT");
		//registrationFieldsInfoCriteria.setReceiveAgentId("44087720");
		registrationFieldsInfoCriteria.setReceiveAgentId("48076876");
		registrationFieldsInfoCriteria.setReceiveCurrency("MXN");
		registrationFieldsInfoCriteria.setLanguage("ENG");
		RegistrationFieldsDAO registrationFieldsDAO = new RegistrationFieldsDAO();
		RegistrationFieldsCollection registrationFieldsCollection = (RegistrationFieldsCollection) registrationFieldsDAO
				.read(new ObjectId(RegistrationFieldsCollection.class,
						registrationFieldsInfoCriteria));
		assertNotNull(registrationFieldsCollection);
	}
}
