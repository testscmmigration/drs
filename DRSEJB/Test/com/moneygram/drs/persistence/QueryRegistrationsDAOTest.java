package com.moneygram.drs.persistence;

import java.util.List;
import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.criteria.QueryRegistrationsCriteria;

public class QueryRegistrationsDAOTest extends DaoTestCase {
	public void testFindRegistrationData() throws Exception {
		QueryRegistrationsDAO queryRegistrationsDAO = new QueryRegistrationsDAO();
		QueryRegistrationsCriteria queryRegistrationsCriteria = new QueryRegistrationsCriteria();
		queryRegistrationsCriteria.setAccountNumberLast4("8A89");
		queryRegistrationsCriteria.setDeliveryOption("BANK_DEPOSIT");
		queryRegistrationsCriteria.setReceiveAgentID("42059323");
		queryRegistrationsCriteria.setLanguage("ENG");
		queryRegistrationsCriteria.setReceiveCountry("USA");
		queryRegistrationsCriteria.setReceiveCurrency("USD");
		queryRegistrationsCriteria.setReceiverFirstName("Dan");
		queryRegistrationsCriteria.setReceiverLastName("DePolis");
		queryRegistrationsCriteria.setReceiverPhoneNumber("12345678");
		List registrationData = queryRegistrationsDAO.find(queryRegistrationsCriteria);
		assertNotNull(registrationData);
	}

	public void testFindRegistrationDataByNames() throws Exception {
		QueryRegistrationsDAO queryRegistrationsDAO = new QueryRegistrationsDAO();
		QueryRegistrationsCriteria queryRegistrationsCriteria = new QueryRegistrationsCriteria();
		queryRegistrationsCriteria.setDeliveryOption("BANK_DEPOSIT");
		queryRegistrationsCriteria.setReceiveAgentID("40809452");
		queryRegistrationsCriteria.setLanguage("ENG");
		queryRegistrationsCriteria.setReceiveCountry("USA");
		queryRegistrationsCriteria.setReceiveCurrency("USD");
		queryRegistrationsCriteria.setReceiverFirstName("First");
		queryRegistrationsCriteria.setReceiverLastName("Last");
		queryRegistrationsCriteria.setReceiverPhoneNumber("1234567890");
		List registrationData = queryRegistrationsDAO.find(queryRegistrationsCriteria);
		assertNotNull(registrationData);
	}
}
