package com.moneygram.drs.persistence;

import java.util.List;
import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.CountryInfo;
import com.moneygram.drs.criteria.CountryInfoCriteria;

public class CountryInfoDAOTest extends DaoTestCase {
	public void testFind() throws Exception {
		CountryInfoCriteria criteria = new CountryInfoCriteria();
		CountryInfoDAO countryInfoDao = new CountryInfoDAO();
		List countryInfoList = countryInfoDao.find(criteria);
		assertNotNull(countryInfoList);
		CountryInfo countryInfo = (CountryInfo) countryInfoList.get(0);
		assertEquals("AFGHANISTAN", countryInfo.getCountryName());
		assertEquals("AFG", countryInfo.getCountryCode());
		assertEquals("AF", countryInfo.getCountryLegacyCode());
		assertFalse(countryInfo.isMgDirectedSendCountry());
	}
}
