package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;

public class AssertRegistrationDAOTest extends DaoTestCase {
	public void testAsserDataReturnFalse() throws Exception {
		AssertRegistrationDAO dao = new AssertRegistrationDAO();
		AssertRegistrationCriteria criteria = new AssertRegistrationCriteria();
		criteria.setDeliveryOption("CARD_DEPOSIT");
		criteria.setReceiveAgentID("43633683");
		assertFalse(dao.assertData(criteria));
	}

	public void testAsserDataReturnTrue() throws Exception {
		AssertRegistrationDAO dao = new AssertRegistrationDAO();
		AssertRegistrationCriteria criteria = new AssertRegistrationCriteria();
		criteria.setDeliveryOption("CARD_DEPOSIT");
		criteria.setReceiveAgentID("40291303");
		assertTrue(dao.assertData(criteria));
	}
}
