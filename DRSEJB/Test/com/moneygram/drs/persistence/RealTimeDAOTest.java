package com.moneygram.drs.persistence;

import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.criteria.RealTimeInfoCriteria;

public class RealTimeDAOTest extends DaoTestCase {
	public void testReadDoesNotBlow() throws Exception {
		RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
		criteria.setAgentId("12345678");
		ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
		RealTimeDAO dao = new RealTimeDAO();
		dao.read(objectID);
	}
}
