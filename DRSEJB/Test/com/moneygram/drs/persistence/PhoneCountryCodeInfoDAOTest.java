package com.moneygram.drs.persistence;

import java.util.Map;

import com.moneygram.drs.DaoTestCase;

public class PhoneCountryCodeInfoDAOTest extends DaoTestCase {
	public void testFind() throws Exception {
		PhoneCountryCodeInfoDAO phoneCountryCodeInfoDao = new PhoneCountryCodeInfoDAO();
		Map<String, String> countryInfoMap = phoneCountryCodeInfoDao.loadCountryToPhoneCountryCode();
		assertNotNull(countryInfoMap);
		System.out.println(countryInfoMap);
		assertEquals("1", countryInfoMap.get("USA"));
		assertEquals(null, countryInfoMap.get("BLM"));
		assertTrue(countryInfoMap.containsKey("BLM"));
		assertEquals(null, countryInfoMap.get("___"));
		// check for duplicated countries return 2nd value in the list?
		assertEquals("39", countryInfoMap.get("VAT"));
		assertEquals("290", countryInfoMap.get("SHN"));
	}
}
