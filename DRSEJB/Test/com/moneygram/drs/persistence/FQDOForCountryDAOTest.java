package com.moneygram.drs.persistence;

import java.util.Iterator;
import java.util.List;
import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.criteria.FQDOForCountryCriteria;

public class FQDOForCountryDAOTest extends DaoTestCase {
	public void testFind() throws Exception {
		FQDOForCountryCriteria criteria = new FQDOForCountryCriteria();
		criteria.setReceiveCountry("PHL");
		FQDOForCountryDAO dao = new FQDOForCountryDAO();
		List fqdoList = dao.find(criteria);
		assertNotNull(fqdoList);
		for (Iterator iter = fqdoList.iterator(); iter.hasNext();) {
			iter.next();
		}
		// TODO need to fix this
		// assertTrue(fqdoList.size() == 3);
		// FQDOInfo fqdoInfo = (FQDOInfo) fqdoList.get(0);
		// assertEquals("HOME_DELIVERY", fqdoInfo.getDeliveryOption());
		// assertEquals("Home Delivery",
		// fqdoInfo.getDeliveryOptionDisplayName());
		// assertEquals("PHP", fqdoInfo.getReceiveCurrency());
		// assertEquals("PHL", fqdoInfo.getReceiveCountry());
		// assertEquals("LBC HOME", fqdoInfo.getReceiveAgentName());
	}
}
