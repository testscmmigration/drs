package com.moneygram.drs.persistence;

import java.sql.ResultSet;
import java.util.Iterator;
import com.moneygram.drs.DaoTestCase;
import com.moneygram.drs.bo.CustomerInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.util.ObjectMaster;

public class SaveRegistrationDAOTest extends DaoTestCase {
	public void testSaveRegistration() throws Exception {
		CustomerInfo customerInfo = new CustomerInfo();
		CustomerProfile customerProfile = ObjectMaster.getCustomerProfile();
		customerProfile.setCustomerReceiveNumber(null);
		customerInfo.setCustomerProfile(customerProfile);
		customerInfo.setAgentId("40017118");
		customerInfo.setDeliveryOption("BANK_DEPOSIT");
		SaveRegistrationDAO saveRegistrationDAO = new SaveRegistrationDAO();
		saveRegistrationDAO.save(customerInfo);
		String sql = "Select * from cust_profile cp  where cp.cust_rcv_nbr = '"
				+ customerProfile.getCustomerReceiveNumber() + "'";
		ResultSet rs = dataHelper.executeQuery(sql);
		assertNotNull(rs);
		assertTrue(rs.next());
		assertEquals("40017118", rs.getString(3));
		assertEquals("10", rs.getString(4));
		assertEquals("PEN", rs.getString(5));
		assertEquals("INI", rs.getString(6));
		assertEquals("P", rs.getString(7));
		assertEquals("2322", rs.getString(9));
		rs.close();
		sql = "select * from cust_profile_person t where t.cust_rcv_nbr  = '"
				+ customerProfile.getCustomerReceiveNumber() + "'";
		rs = dataHelper.executeQuery(sql);
		assertNotNull(rs);
		assertTrue(rs.next());
	}

	public void testUpdateRegistration() throws Exception {
		CustomerInfo customerInfo = new CustomerInfo();
		CustomerProfile customerProfile = ObjectMaster.getCustomerProfile();
		customerProfile.setRegistrationStatus(RegistrationStatusCode.PENDING);
		customerProfile.setCustomerReceiveNumber("MG11119518");
		customerProfile.setRegistrationSubStatus("INI");
		customerProfile.setOfacStatus("F");
		customerInfo.setCustomerProfile(customerProfile);
		customerInfo.setAgentId("44087733");
		customerInfo.setDeliveryOption("BANK_DEPOSIT");
		SaveRegistrationDAO saveRegistrationDAO = new SaveRegistrationDAO();
		saveRegistrationDAO.save(customerInfo);
		CustomerProfileDAO customerProfileDAO = new CustomerProfileDAO();
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(false);
		customerProfileCriteria.setCustomerReceiveNumber("MG11119518");
		customerProfileCriteria.setProfileVersion(0);
		customerProfileCriteria.setAdminUser(true);
		ObjectId objectId = new ObjectId(CustomerProfile.class, customerProfileCriteria);
		customerProfile = (CustomerProfile) customerProfileDAO.read(objectId);
		assertEquals("PEN", customerProfile.getRegistrationStatus().getName());
		assertEquals("INI", customerProfile.getRegistrationSubStatus());
		assertEquals("F", customerProfile.getOfacStatus());
		assertEquals("1232322", customerProfile.getRcvAgentCustAcctNbr());
		if (customerProfile.getJointAccountHolders() != null) {
			for (Iterator iter = customerProfile.getJointAccountHolders().iterator(); iter
					.hasNext();) {
				CustomerProfilePerson element = (CustomerProfilePerson) iter.next();
				assertEquals("Wilma", element.getFirstName());
				assertEquals("Flintstone", element.getLastName());
			}
		}
		assertEquals("Fred", customerProfile.getReceiver().getFirstName());
		assertEquals("Flintstone", customerProfile.getReceiver().getLastName());
	}

	public void testInsertCustPrflPerAddr() throws Exception {
		CustomerProfilePerson person = new CustomerProfilePerson();
		person.setFirstName("TEST");
		SaveRegistrationDAO saveRegistrationDAO = new SaveRegistrationDAO();
		saveRegistrationDAO.insertCustPrflPerAddr(person, "MG14060388", 3, 3,
				SaveRegistrationDAO.JOINTACCHLDR);
		String sql = "select cp.cust_frst_name from cust_profile_person cp where cp.cust_rcv_nbr ='MG14060388' and cp.cust_prfl_ver_nbr =  3 and cp.cust_seq_id =3";
		ResultSet rs = dataHelper.executeQuery(sql);
		assertNotNull(rs);
		assertTrue(rs.next());
		assertEquals("TEST", rs.getString(1));
	}
}
