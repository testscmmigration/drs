package com.moneygram.drs.compliance;

import java.util.ArrayList;
import junit.framework.TestCase;
import com.moneygram.drs.compliance.domain.OFAC;
import com.moneygram.drs.compliance.domain.OFACCustomer;
import com.moneygram.drs.compliance.domain.OFACDescriptorInfo;
import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;

public class OFACQueryManagerTest extends TestCase {
	private String failFirstName = "MICHAEL";
	private String failLastName = "DOOLEY";
	private String failMiddleInitial = "P";

	// TODO The test cases which are failing now are to be tested once the MF
	// env is
	// set up properly to return ofac status. Currently the build is failing so
	// it is commented
	// /US or VJ
	/**
	 * @see com.moneygram.compliance.ComplianceTestBase#doSetUp()
	 */
	protected void doSetUp() throws Exception {
	}

	/**
	 * @see com.moneygram.compliance.ComplianceTestBase#doTearDown()
	 */
	protected void doTearDown() throws Exception {
	}

	public void xtestTransceiverHit() throws Exception {
		RTSTransceiver xmitter = RTSTransceiverImpl.getInstance();
		OFACQueryRequest request = new OFACQueryRequest();
		request.setFunction("M");
		request.setWhere("5");
		request.setTranType("7");
		request.setSqlProductCd("CR");
		request.setSqlExceptionSource("CRN");
		request.setPosKey("0006110001");
		OFACDescriptorInfo info = new OFACDescriptorInfo();
		info.setDescriptorType("14");
		info.setFirstName(failFirstName);
		info.setLastName(failLastName);
		info.setMiddleInitial(failMiddleInitial);
		ArrayList<OFACDescriptorInfo> list = new ArrayList<OFACDescriptorInfo>();
		list.add(info);
		request.setDescriptorList(list);
		OFACQueryResponse response = new OFACQueryResponse();
		response = (OFACQueryResponse) xmitter.send(request, response);
	}

	public void testPass() throws Exception {
		OFAC ofac = new OFAC();
		OFACCustomer customer = new OFACCustomer();
		customer.setFirstName("John");
		customer.setLastName("Doe");
		ofac.setCustomer(customer);
		ofac = OFACQueryManager.getInstance()
				.queryOFAC("123456", ofac, OFACQueryManager.SOURCE_DSS);
		assertEquals("P", ofac.getStatus());
	}

	public void xtestHit() throws Exception {
		OFAC ofac = new OFAC();
		OFACCustomer customer = new OFACCustomer();
		customer.setFirstName(failFirstName);
		customer.setLastName(failLastName);
		customer.setMiddleInitial(failMiddleInitial);
		ofac.setCustomer(customer);
		ofac = OFACQueryManager.getInstance()
				.queryOFAC("123456", ofac, OFACQueryManager.SOURCE_DSS);
		assertEquals("F", ofac.getStatus());
	}

	public void xtestThirdPartyHit() throws Exception {
		OFAC ofac = new OFAC();
		OFACCustomer customer = new OFACCustomer();
		customer.setFirstName("John");
		customer.setLastName("Doe");
		ofac.setCustomer(customer);
		customer = new OFACCustomer();
		customer.setFirstName(failFirstName);
		customer.setLastName(failLastName);
		customer.setMiddleInitial(failMiddleInitial);
		ofac.setThirdParty1(customer);
		ofac = OFACQueryManager.getInstance()
				.queryOFAC("123456", ofac, OFACQueryManager.SOURCE_DSS);
		assertEquals("F", ofac.getStatus());
	}

	public void testThirdPartyPass() throws Exception {
		OFAC ofac = new OFAC();
		OFACCustomer customer = new OFACCustomer();
		customer.setFirstName("John");
		customer.setLastName("Doe");
		ofac.setCustomer(customer);
		customer = new OFACCustomer();
		customer.setFirstName("Jim");
		customer.setLastName("Smith");
		ofac.setThirdParty1(customer);
		ofac = OFACQueryManager.getInstance()
				.queryOFAC("123456", ofac, OFACQueryManager.SOURCE_DSS);
		assertEquals("P", ofac.getStatus());
	}

	public void testPOSId() throws Exception {
		OFAC ofac = new OFAC();
		OFACCustomer customer = new OFACCustomer();
		customer.setFirstName("John");
		customer.setLastName("Doe");
		ofac.setCustomer(customer);
		ofac = OFACQueryManager.getInstance().queryOFAC("123", ofac, OFACQueryManager.SOURCE_DSS);
		String sourceId = ofac.getSourceId();
		assertTrue("verify proper pos id positioning", sourceId.startsWith("123"));
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
