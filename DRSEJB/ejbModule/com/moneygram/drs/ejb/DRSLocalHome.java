package com.moneygram.drs.ejb;

/**
 * Local Home interface for Enterprise Bean: DRS
 */
public interface DRSLocalHome extends javax.ejb.EJBLocalHome {
	/**
	 * Creates a default instance of Session Bean: DRS
	 */
	public com.moneygram.drs.ejb.DRSLocal create() throws javax.ejb.CreateException;
}
