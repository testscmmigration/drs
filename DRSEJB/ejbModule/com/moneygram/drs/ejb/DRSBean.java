package com.moneygram.drs.ejb;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.command.AbstractCommand;
import com.moneygram.drs.command.CountryInfoCommand;
import com.moneygram.drs.command.DirectedSendFieldsCommand;
import com.moneygram.drs.command.DirectedSendRegistrationFieldsCommand;
import com.moneygram.drs.command.FQDOSForCountryCommand;
import com.moneygram.drs.command.FieldsAlertsCommand;
import com.moneygram.drs.command.GetCustomerProfileCommand;
import com.moneygram.drs.command.GetFQDOByCustomerReceiveNumberCommand;
import com.moneygram.drs.command.QueryRegistrationsCommand;
import com.moneygram.drs.command.QueryResgistrationByCustomerReceiveNumberCommand;
import com.moneygram.drs.command.RegistrationFieldsAlertsCommand;
import com.moneygram.drs.command.SaveRegistrationCommand;
import com.moneygram.drs.command.SimplifiedQueryRegistrationCommand;
import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.request.CountryInfoRequest;
import com.moneygram.drs.request.CustomerProfileRequest2;
import com.moneygram.drs.request.DirectedSendFieldsRequest;
import com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest;
import com.moneygram.drs.request.FQDOsForCountryRequest;
import com.moneygram.drs.request.FieldsAlertsRequest;
import com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.request.QueryRegistrationsRequest;
import com.moneygram.drs.request.RegistrationFieldsAlertsRequest;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.request.SimplifiedQueryRegistrationRequest;

/**
 * Bean implementation class for Enterprise Bean: DRS
 */
public class DRSBean implements javax.ejb.SessionBean {
	static final long serialVersionUID = 3206093459760846163L;
	private javax.ejb.SessionContext mySessionCtx;
	private static Logger log = LogFactory.getInstance().getLogger(DRSBean.class);
	private static Map<Class, Class> commandMap = new HashMap<Class, Class>();
	static {
		commandMap.put(CountryInfoRequest.class, CountryInfoCommand.class);
		commandMap.put(FQDOsForCountryRequest.class, FQDOSForCountryCommand.class);
		commandMap.put(GetFQDOByCustomerReceiveNumberRequest.class,
				GetFQDOByCustomerReceiveNumberCommand.class);
		commandMap
				.put(SimplifiedQueryRegistrationRequest.class, SimplifiedQueryRegistrationCommand.class);
		commandMap.put(QueryRegistrationsRequest.class, QueryRegistrationsCommand.class);
		commandMap.put(QueryRegistrationByCustomerReceiveNumberRequest.class,
				QueryResgistrationByCustomerReceiveNumberCommand.class);
		commandMap.put(DirectedSendRegistrationFieldsRequest.class,
				DirectedSendRegistrationFieldsCommand.class);
		commandMap.put(DirectedSendFieldsRequest.class,
				DirectedSendFieldsCommand.class);		
		commandMap.put(SaveRegistrationRequest.class, SaveRegistrationCommand.class);
		commandMap.put(CustomerProfileRequest2.class, GetCustomerProfileCommand.class);
		commandMap.put(RegistrationFieldsAlertsRequest.class, RegistrationFieldsAlertsCommand.class);
		commandMap.put(FieldsAlertsRequest.class, FieldsAlertsCommand.class);
	}

	/**
	 * getSessionContext
	 */
	public javax.ejb.SessionContext getSessionContext() {
		return mySessionCtx;
	}

	/**
	 * setSessionContext
	 */
	public void setSessionContext(javax.ejb.SessionContext ctx) {
		mySessionCtx = ctx;
	}

	/**
	 * ejbCreate
	 */
	public void ejbCreate() throws javax.ejb.CreateException {
	}

	/**
	 * ejbActivate
	 */
	public void ejbActivate() {
	}

	/**
	 * ejbPassivate
	 */
	public void ejbPassivate() {
	}

	/**
	 * ejbRemove
	 */
	public void ejbRemove() {
	}

	public Object execute(CommandRequest request) throws Exception {
		try {
			AbstractCommand command = buildCommand(request);
			command.execute();
			return command.getResult();
		} catch (Exception e) {
			log.error("Exception error when processing request [" + request.getClass().getName()
					+ "]. ", e);
			getSessionContext().setRollbackOnly();
			throw e;
		}
	}

	private AbstractCommand buildCommand(CommandRequest request) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		Class commandClass = (Class) commandMap.get(request.getClass());
		Constructor constructor = commandClass.getConstructor((Class[]) null);
		AbstractCommand command = (AbstractCommand) constructor.newInstance((Object[]) null);
		command.initialize(request);
		return command;
	}
}
