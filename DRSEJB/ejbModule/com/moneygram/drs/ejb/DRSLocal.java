package com.moneygram.drs.ejb;

import com.moneygram.drs.request.CommandRequest;

/**
 * Local interface for Enterprise Bean: DRS
 */
public interface DRSLocal extends javax.ejb.EJBLocalObject {
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Object execute(CommandRequest request) throws Exception;
}
