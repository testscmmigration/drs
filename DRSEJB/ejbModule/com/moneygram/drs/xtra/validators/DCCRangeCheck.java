package com.moneygram.drs.xtra.validators;

import java.util.Date;

import org.apache.axis.utils.StringUtils;

import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.criteria.AssertDCCCriteria;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.xmlTagMapper.StandardFieldTags;

public class DCCRangeCheck implements XtraValidators {
	public void validate(SaveRegistrationRequest req, CustomerProfile cp,
			CustomerProfile existingProfile, boolean creating) throws Exception {
		String accNumm = cp.getRcvAgentCustAcctNbr() == null ? existingProfile
				.getRcvAgentCustAcctNbr() : cp.getRcvAgentCustAcctNbr();
		if (accNumm == null) {
			// Nothing to check Return;
		}
		//Added this check to not range check if Account number is not available in CP request. 
		//Discussed with Chris. changes for get@sends project to fix the existing bug. 
		if(StringUtils.isEmpty(cp.getRcvAgentCustAcctNbr())){
			return;
		}
		boolean passed = false;
		AssertDCCCriteria asrCriteria = new AssertDCCCriteria();
		asrCriteria.setAccountNumber(cp.getRcvAgentCustAcctNbr());
		asrCriteria.setAgentId(req.getReceiveAgentID());
		asrCriteria.setCurrency(req.getReceiveCurrency());
		passed = PersistenceManagerFactory.getInstance().assertData(asrCriteria);
		if (!passed) {
			throw new DRSException(DRSExceptionCodes.EC362_DCC_CARD_RNG_FAILURE, "Value",
					"receiverAccountNumber", new Date(), "DCC Card Range Match Failure",
					StandardFieldTags.CLIENT);
		}
	}
}
