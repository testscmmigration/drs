package com.moneygram.drs.xtra.validators;

import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.request.SaveRegistrationRequest;

public interface XtraValidators {
	public void validate(SaveRegistrationRequest req, CustomerProfile cp,
			CustomerProfile existingProfile, boolean creating) throws DRSException, Exception;
}
