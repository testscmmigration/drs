package com.moneygram.drs.xtra.validators;

import java.util.Date;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.bo.RealTimeValidationResponse;
import com.moneygram.drs.criteria.RealTimeInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.realtime.comm.RTDRSValidator;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.xmlTagMapper.StandardFieldTags;

public class RealTimeValidator implements XtraValidators {
	public static final String CLASS_NAME_KEY = "COMM_CLAZZ_NAME";

	public void validate(SaveRegistrationRequest req, CustomerProfile cp,
			CustomerProfile existingProfile, boolean creating) throws DRSException, Exception {
		if (req.hasComplianceUpdates() && !creating) return;
		RealTimeInfoCriteria criteria = new RealTimeInfoCriteria();
		criteria.setAgentId(req.getReceiveAgentID());
		ObjectId objectID = new ObjectId(RealTimeValidationRequest.class, criteria);
		RealTimeValidationRequest realTimeValidationReq = (RealTimeValidationRequest) PersistenceManagerFactory
				.getInstance().read(objectID);
		realTimeValidationReq.setCreating(creating);
		realTimeValidationReq.setExistingProfile(existingProfile);
		realTimeValidationReq.setCustomerProfile(cp);
		realTimeValidationReq.setSaveRegistrationRequest(req);
		String CommModuleImplClazzName = realTimeValidationReq.getContext().get(CLASS_NAME_KEY);
		RTDRSValidator commModule = (RTDRSValidator) Class.forName(CommModuleImplClazzName)
				.newInstance();
		RealTimeValidationResponse response = commModule.validate(realTimeValidationReq);
		if (RealTimeValidationResponse.Result.PASS == response.getResult()) {
			return;
		}
		// IF we want to handle errors put logic here
		throw new DRSException(response.getErrorCode(), response.getErrorString(), response
				.getOffendingField(), new Date(), response.getErrorString(),
				StandardFieldTags.CLIENT);
	}
}
