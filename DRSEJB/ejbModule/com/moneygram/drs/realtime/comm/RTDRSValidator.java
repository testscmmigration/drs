package com.moneygram.drs.realtime.comm;

import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.bo.RealTimeValidationResponse;

public interface RTDRSValidator {
	public RealTimeValidationResponse validate(RealTimeValidationRequest request) throws Exception;
}
