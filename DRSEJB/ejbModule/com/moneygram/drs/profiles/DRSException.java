/**
 * (c) 2001 Travelers Express
 *
 * RTSException.java
 *
 * Exception that encapsulates exception information for problems arising
 * during RTS processing.  This information is needed to send back an 
 * appropriate fault information to the client app.
 *
 * $Workfile:   RTSException.java  $
 * $Revision: 1.5 $
 * $Author: a210 $
 * @author Phil Mork
 * $Log: DRSException.java,v $
 * Revision 1.5  2018/12/24 16:34:24  a210
 * changes to alter the check digit for new cust_rcv_nbr values and make sure we do not overlay pre-existing profiles should a number get re-used that already existed for a different person.
 *
 * Revision 1.4  2008/10/23 21:50:33  a118
 * US: integration fixes
 *
 * Revision 1.3  2008/10/22 19:35:42  a235
 * refactoring - vj
 *
 * Revision 1.2  2008/09/26 20:23:47  a235
 * Initial project addition -VJ
 *
 * Revision 1.1  2008/09/19 16:55:43  a118
 * US: API FQDOSForCountry() flow
 *
 * Revision 1.7  2008/03/19 19:32:33  cruisecontrol
 * merged from AC_OCT07_041807 by a700
 *
 * Revision 1.5.20.2.4.1  2007/06/18 20:24:38  t007
 * Merge from AC72_JUL_07_FC_5
 *
 * Revision 1.5.20.3  2007/05/24 14:04:41  t007
 * Updates for TSCOMM2
 *
 * Revision 1.5.20.2  2007/04/06 18:17:38  t007
 * Make output consistent.
 *
 * Revision 1.5.20.1  2007/03/30 20:52:06  t007
 * Merge from France AML DEV 9
 *
 * Revision 1.5.16.1  2007/03/30 20:01:23  t007
 * Update from France AML DEV_9
 *
 * Revision 1.6  2007/03/30 19:32:14  t007
 * Updates based on feedback from Alex and Piyush
 *
 * Revision 1.5  2006/01/12 17:18:13  t007
 * Organized the imports.
 *
 * Revision 1.4  2005/09/26 19:23:05  t007
 * SCR #113 - Fixed problem with MF lookups on errors.
 *
 * Revision 1.3  2005/09/23 19:41:32  t007
 * AC10.4 - More error corrections.
 *
 * Revision 1.2  2005/09/23 18:54:54  t007
 * AC10.4 - Enhanced error handling.
 *
 * Revision 1.1  2005/05/01 19:31:50  t007
 * Initial load of files.
 *
 * 
 *    Rev 1.5   Mar 21 2005 21:35:04   T359
 * Revised RTSException creation (using the error lookup cache) and logging (using English "EN" as the default language)
 * 
 *    Rev 1.4   Jul 15 2004 09:45:52   W172
 * Added language for RTSException and getting the error message from the database.
 * 
 *    Rev 1.3   May 27 2004 17:37:52   T358
 * removed nested stack trace printing (now handled by Log4JLogger in LoggingClient
 * 
 *    Rev 1.2   Apr 21 2004 14:31:22   W170
 * Renamed all com.travelersexpress packages to com.moneygram
 * 
 *    Rev 1.1   Feb 10 2004 19:57:14   W170
 * Added new logging and organized imports
 * 
 *    Rev 1.0   Nov 06 2003 12:14:02   A209
 * Initial revision.
 * 
 *    Rev 1.3   Sep 24 2003 12:28:50   A215
 * Updates for subErrorInfo.
 * 
 *    Rev 1.2   May 30 2003 08:29:56   A209
 * Gets subErrorCode from the mainframe exception rather than parsing out the string.
 * 
 *    Rev 1.1   May 16 2003 15:49:20   A209
 * Add subErrorCode() and populate it from mainframe detail, if available.
 * 
 *    Rev 1.0   Feb 26 2002 11:42:34   T267
 * Initial revision.
 * 
 *    Rev 1.8   Mar 14 2001 17:22:50   T273
 * Get only the MG description message for the MG errorString instead of the hostLevel: , ID:, ...
 * 
 *    Rev 1.7   Mar 13 2001 18:32:04   T273
 * Strip illegal XML chars out of debugString which may come from mainframe.
 * 
 *    Rev 1.6   Mar 13 2001 14:23:42   T273
 * Added the debugstring to errorString for MoneyGram errors (600 - 799) because we want client to see more specific message.
 * 
 *    Rev 1.5   Mar 12 2001 12:34:30   T273
 * Added code to ensure the root exception, if any, has its stackTrace printed out.
 * 
 *    Rev 1.4   Mar 09 2001 12:34:44   T273
 * Added printing of nested exception if non-null
 * 
 *    Rev 1.3   Mar 06 2001 15:25:22   T267
 * Don't extend RemoteException
 * 
 *    Rev 1.2   Mar 05 2001 17:22:32   T273
 * Added \r in front of \n for newlines
 * 
 *    Rev 1.1   Feb 22 2001 19:51:56   T273
 * Overrode toString() for more info.
 * 
 *    Rev 1.0   Feb 20 2001 12:41:50   T273
 * Initial revision.
 *
 */
package com.moneygram.drs.profiles;

import com.moneygram.common.guid.LoggingKeys;

public class DRSException extends Exception implements LoggingKeys {
	private static final long serialVersionUID = 1L;
	/**
	 * This represents the general class of the error by the first digit, and
	 * the more specific type of error by the next two digits similar to HTTP
	 * return codes See ExceptionMgr for detailed list of the error codes, but
	 * for example
	 * 
	 * errorCode 900 indicates a Catastrophic error, specific instances of 900
	 * errors: 901 - Database failure 902 - Can't send to MoneyGram ...
	 */
	private int errorCode;
	/** This should be a short human readable description of the error */
	private String errorString;
	/**
	 * Which field caused the error, if applicable - applies to Validation
	 * exceptions
	 */
	private String offendingField;
	/** timeStamp of when the error occured */
	private java.util.Date timeStamp;
	/**
	 * Used to pass in any information that should be logged for debug purposes,
	 * but should not be sent back to the client
	 */
	private String debugString;
	/** Used for SOAP faultCode - should be either Client or Server */
	private String faultCode;
	private Throwable detail;
	private int subErrorCode;
	public static final String SERVER = "server";
	public static final String CLIENT = "client";

	public DRSException(String pErrorString) {
		super(pErrorString);
	}

	public DRSException(int errorCode, String errorString, String offendingField,
			java.util.Date timeStamp, String debugString, String faultCode) {
		this(errorString);
		this.errorCode = errorCode;
		this.errorString = errorString;
		this.offendingField = offendingField != null ? offendingField : "";
		this.timeStamp = timeStamp;
		this.debugString = debugString;
		this.faultCode = faultCode;
	}

	public DRSException(int errorCode, String errorString, String offendingField,
			java.util.Date timeStamp, String debugString, String faultCode, int subErrorCode) {
		this(errorCode, errorString, offendingField, timeStamp, debugString, faultCode);
		this.subErrorCode = subErrorCode;
	}

	public DRSException(int errorCode, String errorString, String offendingField,
			java.util.Date timeStamp, String debugString, String faultCode, Throwable t) {
		this(errorCode, errorString, offendingField, timeStamp, debugString, faultCode);
		this.detail = t;
	}

	public DRSException(int errorCode, String errorString, String offendingField,
			java.util.Date timeStamp, String debugString, String faultCode, Throwable t,
			int subErrorCode) {
		this(errorCode, errorString, offendingField, timeStamp, debugString, faultCode,
				subErrorCode);
		this.detail = t;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int pErrorCode) {
		this.errorCode = pErrorCode;
	}

	public int getSubErrorCode() {
		return subErrorCode;
	}

	public void setSubErrorCode(int pErrorCode) {
		this.subErrorCode = pErrorCode;
	}

	public String getErrorString() {
		return errorString;
	}

	public void setErrorString(String pErrorString) {
		this.errorString = pErrorString;
	}

	public String getOffendingField() {
		return offendingField;
	}

	public void setOffendingField(String pField) {
		this.offendingField = pField;
	}

	public String getDebugString() {
		return debugString;
	}

	public void setDebugString(String pDebug) {
		this.debugString = pDebug;
	}

	public java.util.Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(java.util.Date pDate) {
		this.timeStamp = pDate;
	}

	public String getFaultCode() {
		return faultCode;
	}

	public void setFaultCode(String pCode) {
		this.faultCode = pCode;
	}

	public Throwable getDetail() {
		return detail;
	}

	public void setDetail(Throwable pDetail) {
		this.detail = pDetail;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(RTSEX_RTS_ERROR_CODE).append(this.getErrorCode());
		sb.append(" ").append(RTSEX_SUB_ERROR_CODE).append(this.getSubErrorCode());
		sb.append(" ").append(RTSEX_EXCEPTION_MSG).append("\"").append(getMessage()).append("\"");
		sb.append(" ").append(RTSEX_ERROR_STRING).append("\"").append(this.getErrorString())
				.append("\"");
		sb.append(" ").append(RTSEX_OFFENDING_FIELD).append(this.getOffendingField());
		if (getTimeStamp() != null)
			sb.append(" ").append(TIMESTAMP).append(DATE_FMT_LOG.format(this.getTimeStamp()));
		sb.append(" ").append(RTSEX_DEBUG_STRING).append("\"").append(this.getDebugString())
				.append("\"");
		sb.append(" ").append(RTSEX_SOAP_FAULT_CODE).append(this.getFaultCode());
		if (this.getDetail() != null) {
			Throwable t = getDetail();
			sb.append(" Nested exception: " + t);
		}
		return sb.toString();
	}
}
