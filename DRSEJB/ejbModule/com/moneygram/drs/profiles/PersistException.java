package com.moneygram.drs.profiles;

/**
 * Representes an exception associated with loading or storing from persistent
 * storage.
 */
public class PersistException extends Exception {
	private static final long serialVersionUID = 1L;

	public PersistException() {
		super();
	} // end-method

	public PersistException(String msg) {
		super(msg);
	} // end-method

	/**
	 * @param x
	 */
	public PersistException(Throwable x) {
		super(x);
	}

	/**
	 * @param msg
	 * @param e
	 */
	public PersistException(String msg, Throwable e) {
		super(msg, e);
	}

	public PersistException(Exception x) {
		super(x);
	} // end-method
} // end-class
