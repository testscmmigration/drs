/*
 * Created on Sep 25, 2005
 *
 */
package com.moneygram.drs.profiles;

/**
 * <P>
 * This represents the general class of the error by the first digit, and the
 * more specific type of error by the next two digits similar to HTTP return
 * codes.
 * 
 * <P>
 * The static labels for all the error codes - the prefix indicates the general
 * class of errorCode that the particluar code is in so that the users of this
 * file can easily tell what the groupings of errors. (ie., all 9xx codes labels
 * below will be prefixed with EC9xx followed by the specific error message
 * type. The general error Code classification labels end in _ERRORS - whereas
 * instances of that general errorCode class have singular labels (i.e. end in
 * _ERROR, _FAILURE, etc.).
 * <P>
 * NOTE: check the RTS_ERROR_MESSAGE table in the database before adding new
 * entries. The error number may already be used.
 * 
 * @author T007
 */
public interface DRSExceptionCodes {
	public static final int EC305_INVALID_COUNTRY_CODE = 305;
	public static final int EC316_CUSTOMER_NOT_FOUND = 316;
	public static final int EC322_INVALID_FIELD_LENGTH_MIN = 322;
	public static final int EC323_INVALID_FIELD_LENGTH_MAX = 323;
	public static final int EC324_MIN_VALUE_CHECK_FAILURE = 324;
	public static final int EC325_MAX_VALUE_CHECK_FAILURE = 325;
	public static final int EC326_MAX_SCALE_CHECK_FAILURE = 326;
	// not being used. public static final int EC327_HIDDEN_FIELD_MISMATCH = 327;
	public static final int EC328_REGEX_MATCH_FAILURE = 328;
	public static final int EC329_FIELD_REQUIRED = 329;
	public static final int EC330_NO_TYPE_FOUND_ON_FIELD = 330;
	public static final int EC331_UNKNOWN_FIELD = 331;
	public static final int EC332_INVALID_ENUM_VALUE = 332;
	public static final int EC333_INVALID_DELIVERY_OPTION = 333;
	public static final int EC334_INVALID_DEL_OPT_FOR_COUNTRY = 334;
	public static final int EC335_READ_ONLY_FIELD = 335;
	public static final int EC336_INVALID_REGISTRATION_STATUS = 336;
	public static final int EC337_FIRST_NAME_LAST_NAME_NOT_PROVIDED = 337;
	public static final int EC338_CANNOT_DELETE_WITHOUT_FIRST_LAST_NAME_EMPTY = 338;
	public static final int EC339_CANNOT_DELETE_RECEIVER_CREATOR = 339;
	public static final int EC340_CHECK_DIGIT_FAILED = 340;
	public static final int EC355_INVALID_DATA = 355;
	public static final int EC362_DCC_CARD_RNG_FAILURE = 362;
	public static final int EC384_NAME_ON_CRD_NT_VLD = 384;
	public static final int EC385_CRD_NMBR_NT_VLD_PLS_CNFRM = 385;
	public static final int EC515_NO_FIELDS_FOUND_FOR_REGISTRATION = 515;
	public static final int EC625_MG_CUSTOMER_RCV_NUMBER_NOT_FOUND = 625;
	public static final int EC626_TOO_MANY_RECORDS_FOUND = 626;
	public static final int EC627_NO_RECORDS_FOUND = 627;
	public static final int EC628_WILDCARDS_NOT_AT_END_OF_STRINGS = 628;
	public static final int EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM = 629;
	public static final int EC700_MONEYGRAM_SERVER_ERRORS = 700;
	public static final int EC701_EMPTY_RESPONSE_RECEIVED = 701;
	public static final int EC702_ERROR_RECEIVED_FROM_MONEYGRAM = 702;
	public static final int EC901_DATABASE_FAILURE = 901;
	public static final int EC902_CANT_SEND_TO_MG_ERROR = 902;
	public static final int EC905_VALIDATION_PROCESSING_ERROR = 905;
	public static final int EC906_MESSAGE_TRANSLATION_ERROR = 906;
	public static final int EC910_MONEYGRAM_COMMUNICATION_ERROR = 910;
	public static final int EC912_CANNOT_SEND_MQ_MESSAGE = 912;
	public static final int EC923_PCI_CALL_FAILURE = 923;
	public static final int EC924_PCI_DECRYPT_CALL_FAILURE = 924;
	public static final int EC931_SYS_UNAVLB_TRY_LATER = 931;
	public static final int EC1700_ACCOUNT_NICKNAME_PAN_ID_CHECK_FAILED = 1700;
	public static final int EC3022_IFSC_VALIDATION_FAILED = 3022;
	public static final int EC9657_VERIFICATION_FIELD_MATCHING_FAILED = 9657;
	
}
