package com.moneygram.drs.profiles;

import java.util.ArrayList;
import java.util.Date;

public class DRSExceptionWithErrors extends DRSException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private ArrayList<DRSException> exceptionList = new ArrayList<DRSException>();


	public DRSExceptionWithErrors(int errorCode, String errorString,
			String offendingField, Date timeStamp, String debugString,
			String faultCode,String hiddenOffendingField) {
		super(errorString);
		DRSException drsException = new DRSException(errorCode, errorString, offendingField, timeStamp, debugString, faultCode); 
		exceptionList.add(drsException);
		drsException = new DRSException(errorCode, errorString, hiddenOffendingField, timeStamp, debugString, faultCode); 
		exceptionList.add(drsException);	
	
	}


	public ArrayList<DRSException> getExceptionList() {
		return exceptionList;
	}
	

}
