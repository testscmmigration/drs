package com.moneygram.drs.notification;

import java.util.GregorianCalendar;
import java.util.TimeZone;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class NotificationManagerImpl implements NotificationManager {
	private final TimeZone TZ_UTC = TimeZone.getTimeZone("UTC");
	private final TimeZone TZ_DEFAULT = TimeZone.getDefault();
	private static String XML_PREFIX = "<tns:notification xmlns:tns=\"http://www.moneygram.com/Notify10\">\n"
			+ "<tns:type>ValidateProfile</tns:type>\n" + "<tns:context>RTS</tns:context>\n";
	private static String DATA_TAG = "tns:data";
	private static String DATE_TIME_TAG = "tns:dateTime";
	private static String END_NOTIFICATION_TAG = "</tns:notification>";
	JMSMessageWriter writer = new JMSMessageWriter();
	private DatatypeFactory factory;
	public static final int TYPE_GYEAR = 1;
	public static final int TYPE_GMONTH = 2;
	public static final int TYPE_GDAY = 4;
	public static final int TYPE_DATE = 7;
	public static final int TYPE_DATETIME = 63;
	public static final int TYPE_TIME = 56;
	public static final int TYPE_GYEARMONTH = 3;
	public static final int TYPE_GMONTHDAY = 6;

	public NotificationManagerImpl() {
		try {
			factory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException("failed to initialize translator" + e);
		}
	}

	public void notifyDSSOfRegistrationChange(String customerReceiveNumber) throws Exception {
		StringBuffer fullMsg = new StringBuffer(XML_PREFIX);
		fullMsg.append("<" + DATA_TAG + ">" + customerReceiveNumber + "</" + DATA_TAG + ">\n");
		String currentTime = deriveTimestamp();
		fullMsg.append("<" + DATE_TIME_TAG + ">" + currentTime + "</" + DATE_TIME_TAG + ">\n");
		fullMsg.append(END_NOTIFICATION_TAG);
		writer.writeToQueue(fullMsg.toString().getBytes());
	}

	private String deriveTimestamp() {
		GregorianCalendar cal = new GregorianCalendar();
		if (cal.getTimeZone().equals(TZ_DEFAULT)) {
			cal.getTime();
			cal.setTimeZone(TZ_UTC);
		}
		XMLGregorianCalendar gcal = factory.newXMLGregorianCalendar(cal);
		return gcal.toXMLFormat();
	}
}
