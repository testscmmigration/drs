package com.moneygram.drs.notification;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

public class JMSMessageWriter {
	private static final String qcfBindingName = "java:comp/env/jms/DSS.NOTIFY.REQ.QCF";
	private static final String queueBindingName = "java:comp/env/jms/DSS.NOTIFY.REQ";

	public void writeToQueue(byte[] bytes) throws NamingException, JMSException {
		Logger log = LogFactory.getInstance().getLogger(getClass());
		QueueConnectionFactory queueConnectionFactory;
		QueueConnection queueConnection = null;
		QueueSession queueSession = null;
		QueueSender queueSender = null;
		log.debug("getting InitialContext");
		Context jndi;
		jndi = new InitialContext();
		log.debug("looking up qcf: " + qcfBindingName);
		Object o = jndi.lookup(qcfBindingName);
		log.debug("got qcf: " + o);
		queueConnectionFactory = (QueueConnectionFactory) o;
		try {
			log.debug("creating QueueConnection");
			queueConnection = queueConnectionFactory.createQueueConnection();
			log.debug("creating non-transactional QueueSession in AUTO_ACKNOWLEDGE mode.");
			queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			log.debug("looking up queue: " + queueBindingName);
			Queue queue = (Queue) jndi.lookup(queueBindingName);
			log.debug("creating QueueSender");
			queueSender = queueSession.createSender(queue);
			log.debug("starting QueueConnection");
			queueConnection.start();
			TextMessage jmsMessage = queueSession.createTextMessage();
			jmsMessage.setText(new String(bytes));
			// BytesMessage jmsMessage = queueSession.createBytesMessage();
			// jmsMessage.writeBytes(bytes);
			queueSender.send(jmsMessage);
			// in mq, we have to set msg.format = MQC.MQFMT_STRING;
			// jmsMessage.setStringProperty("mq_format", "mqfmt_string");
		} finally {
			try {
				queueSender.close();
			} catch (Exception e) {
			}
			try {
				jndi.close();
			} catch (Exception e) {
			}
			try {
				queueConnection.close();
			} catch (Exception e) {
			}
		}
	}
}
