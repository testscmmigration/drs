package com.moneygram.drs.notification;

public interface NotificationManager {
	public void notifyDSSOfRegistrationChange(String customReceiveNumber) throws Exception;
}
