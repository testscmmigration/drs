package com.moneygram.drs.notification;

public class NotificationManagerFactory {
	private static NotificationManager instance;

	// private static Logger log =
	// LogFactory.getInstance().getLogger(NotificationManagerFactory.class);
	NotificationManagerFactory() throws Exception {
	}

	public static NotificationManager getInstance() throws Exception {
		if (instance == null) {
			instance = new NotificationManagerImpl();
		}
		return instance;
	}
}
