package com.moneygram.drs.xtravalidators.factory;

import java.util.HashMap;
import java.util.Map;
import com.moneygram.drs.xtra.validators.DCCRangeCheck;
import com.moneygram.drs.xtra.validators.RealTimeValidator;
import com.moneygram.drs.xtra.validators.XtraValidators;

public class XtraValidatorsFactory {
	public static final String CRDRNG = "CRDRNG";
	public static final String RLTVLD = "RLTVLD";
	private static Map<String, XtraValidators> mapOfMappers = new HashMap<String, XtraValidators>();
	static {
		mapOfMappers.put(CRDRNG, new DCCRangeCheck());
		mapOfMappers.put(RLTVLD, new RealTimeValidator());
	}

	/**
	 * @param com.moneygram.consumerservice.webservice.nmf.v1.Request
	 * @return
	 */
	public static XtraValidators getXtraValidator(String string) {
		return mapOfMappers.get(string);
	}
}
