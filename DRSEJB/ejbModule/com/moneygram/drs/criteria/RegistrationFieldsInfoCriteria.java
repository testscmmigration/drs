package com.moneygram.drs.criteria;

public class RegistrationFieldsInfoCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String receiveCountry;
	private String deliveryOption;
	private boolean externalReceiverProfile;
	private String receiveAgentId;
	private String receiveCurrency;
	private String language;
	private boolean saveRegistrationWithNewXmlTags;
	private boolean suppressReEnterFields;


	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getReceiveAgentId() {
		return receiveAgentId;
	}

	public void setReceiveAgentId(String receiveAgentId) {
		this.receiveAgentId = receiveAgentId;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * @return the saveRegistrationWithNewXmlTags
	 */
	public boolean isSaveRegistrationWithNewXmlTags() {
		return saveRegistrationWithNewXmlTags;
	}

	/**
	 * @param saveRegistrationWithNewXmlTags the saveRegistrationWithNewXmlTags to set
	 */
	public void setSaveRegistrationWithNewXmlTags(
			boolean saveRegistrationWithNewXmlTags) {
		this.saveRegistrationWithNewXmlTags = saveRegistrationWithNewXmlTags;
	}

	public boolean isSuppressReEnterFields() {
		return suppressReEnterFields;
	}

	public void setSuppressReEnterFields(boolean suppressReEnterFields) {
		this.suppressReEnterFields = suppressReEnterFields;
	}
	
	public boolean isExternalReceiverProfile() {
		return externalReceiverProfile;
	}

	public void setExternalReceiverProfile(boolean externalReceiverProfile) {
		this.externalReceiverProfile = externalReceiverProfile;
	}


}
