package com.moneygram.drs.criteria;

public class QueryRegistrationsCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiverFirstName;
	private java.lang.String receiverLastName;
	private java.lang.String receiverPhoneNumber;
	private java.lang.String accountNumberLast4;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String language;

	public java.lang.String getAccountNumberLast4() {
		return accountNumberLast4;
	}

	public void setAccountNumberLast4(java.lang.String accountNumberLast4) {
		this.accountNumberLast4 = accountNumberLast4;
	}

	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public java.lang.String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(java.lang.String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public java.lang.String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(java.lang.String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public java.lang.String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	public void setReceiverPhoneNumber(java.lang.String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
