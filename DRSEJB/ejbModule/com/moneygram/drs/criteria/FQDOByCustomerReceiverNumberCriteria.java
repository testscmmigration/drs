package com.moneygram.drs.criteria;

public class FQDOByCustomerReceiverNumberCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String mgCustomerReceiveNumber;
	private String language;

	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
