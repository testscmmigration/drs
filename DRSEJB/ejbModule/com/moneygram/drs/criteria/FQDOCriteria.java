package com.moneygram.drs.criteria;

import com.moneygram.drs.bo.FqdoEnum;

public class FQDOCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String language;
	private java.lang.String customerReceiveNumber;
	private FqdoEnum fqdoEnum;

	public java.lang.String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(java.lang.String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public FqdoEnum getFqdoEnum() {
		return fqdoEnum;
	}

	public void setFqdoEnum(FqdoEnum fqdoEnum) {
		this.fqdoEnum = fqdoEnum;
	}
}
