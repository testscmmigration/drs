package com.moneygram.drs.criteria;

public class RealTimeInfoCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String agentId;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
}
