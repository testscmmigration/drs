package com.moneygram.drs.criteria;

public class FieldsInfoCriteria extends RegistrationFieldsInfoCriteria {

	private static final long serialVersionUID = 1L;

	private boolean skipReceiverPhoneCountryCode;

	public FieldsInfoCriteria() {
		super();
	}

	public boolean isSkipReceiverPhoneCountryCode() {
		return skipReceiverPhoneCountryCode;
	}

	public void setSkipReceiverPhoneCountryCode(
			boolean skipReceiverPhoneCountryCode) {
		this.skipReceiverPhoneCountryCode = skipReceiverPhoneCountryCode;
	}
}
