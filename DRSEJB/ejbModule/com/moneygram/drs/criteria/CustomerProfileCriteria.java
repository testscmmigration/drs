package com.moneygram.drs.criteria;

public class CustomerProfileCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String customerReceiveNumber;
	private int profileVersion;
	private boolean adminUser;
	private boolean abbrCustomerProfile = false;
	private boolean displayFullAccountNumber;

	public boolean isAdminUser() {
		return adminUser;
	}

	public void setAdminUser(boolean adminUser) {
		this.adminUser = adminUser;
	}

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public int getProfileVersion() {
		return profileVersion;
	}

	public void setProfileVersion(int profileVersion) {
		this.profileVersion = profileVersion;
	}

	public boolean isAbbrCustomerProfile() {
		return abbrCustomerProfile;
	}

	public void setAbbrCustomerProfile(boolean abbrCustomerProfile) {
		this.abbrCustomerProfile = abbrCustomerProfile;
	}

	public boolean isDisplayFullAccountNumber() {
		return displayFullAccountNumber;
	}

	public void setDisplayFullAccountNumber(boolean displayFullAccountNumber) {
		this.displayFullAccountNumber = displayFullAccountNumber;
	}

	
}
