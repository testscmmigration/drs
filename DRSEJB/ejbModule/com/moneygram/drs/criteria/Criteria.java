package com.moneygram.drs.criteria;

import java.io.Serializable;
import com.moneygram.drs.util.Reflector;

public class Criteria implements Serializable {
	private static final long serialVersionUID = 1L;

	public String toString() {
		return Reflector.getInstance(this.getClass()).printBean(this);
	}
}
