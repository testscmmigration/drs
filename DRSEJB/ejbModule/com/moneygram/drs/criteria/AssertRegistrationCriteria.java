package com.moneygram.drs.criteria;

public class AssertRegistrationCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String language;

	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
