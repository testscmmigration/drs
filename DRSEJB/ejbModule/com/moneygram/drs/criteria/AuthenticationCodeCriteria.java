package com.moneygram.drs.criteria;

public class AuthenticationCodeCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String agentId;
	private String deliveryOption;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}
}
