package com.moneygram.drs.criteria;

public class SimplifiedQueryRegistrationCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String receiverFirstName;
	private java.lang.String receiverLastName;
	private java.lang.String receiverPhoneNumber;
	private java.lang.String registrationCreatorFirstName;
	private java.lang.String registrationCreatorLastName;
	private java.lang.String registrationCreatorPhoneNumber;
	private java.lang.String language;
	private java.lang.String mgCustomerReceiveNumber;
	private Boolean activeRecordsOnly;
	private Integer maxRowsToReturn;

	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public java.lang.String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(java.lang.String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public java.lang.String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(java.lang.String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public java.lang.String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	public void setReceiverPhoneNumber(java.lang.String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	public java.lang.String getRegistrationCreatorFirstName() {
		return registrationCreatorFirstName;
	}

	public void setRegistrationCreatorFirstName(java.lang.String registrationCreatorFirstName) {
		this.registrationCreatorFirstName = registrationCreatorFirstName;
	}

	public java.lang.String getRegistrationCreatorLastName() {
		return registrationCreatorLastName;
	}

	public void setRegistrationCreatorLastName(java.lang.String registrationCreatorLastName) {
		this.registrationCreatorLastName = registrationCreatorLastName;
	}

	public java.lang.String getRegistrationCreatorPhoneNumber() {
		return registrationCreatorPhoneNumber;
	}

	public void setRegistrationCreatorPhoneNumber(java.lang.String registrationCreatorPhoneNumber) {
		this.registrationCreatorPhoneNumber = registrationCreatorPhoneNumber;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public Boolean getActiveRecordsOnly() {
		return activeRecordsOnly;
	}

	public void setActiveRecordsOnly(Boolean activeRecordsOnly) {
		this.activeRecordsOnly = activeRecordsOnly;
	}

	public Integer getMaxRowsToReturn() {
		return maxRowsToReturn;
	}

	public void setMaxRowsToReturn(Integer maxRowsToReturn) {
		this.maxRowsToReturn = maxRowsToReturn;
	}
}
