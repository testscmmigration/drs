package com.moneygram.drs.criteria;

public class FQDOForCountryCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String receiveCountry;
	private String language;

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
