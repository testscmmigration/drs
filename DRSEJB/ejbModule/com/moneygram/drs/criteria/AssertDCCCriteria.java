package com.moneygram.drs.criteria;

public class AssertDCCCriteria extends Criteria {
	private static final long serialVersionUID = 1L;
	private String accountNumber;
	private String agentId;
	private String currency;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
