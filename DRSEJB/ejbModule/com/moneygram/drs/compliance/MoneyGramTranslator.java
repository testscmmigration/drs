/**
 * (c) 2001 Travelers Express
 * 
 * MoneyGramTranslator.java
 * 
 * Abstract super-class of all Money Gram message translators. Contains helper
 * methods to aid in the conversion from Java classes to byte arrays, and vice
 * versa.
 * 
 * $Workfile: MoneyGramTranslator.java $ $Revision: 1.4 $ $Author: a235 $ $Log:
 * MoneyGramTranslator.java,v $ Revision 1.11 2006/01/12 17:18:16 t007 Organized
 * the imports.
 * 
 * 
 */
package com.moneygram.drs.compliance;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public abstract class MoneyGramTranslator {
	protected static final SimpleDateFormat longFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
	protected static final SimpleDateFormat shortFormatter = new SimpleDateFormat("MMddyy");
	protected static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HHmmss");
	protected static final SimpleDateFormat longDateFormatterYearFirst = new SimpleDateFormat(
			"yyyyMMdd");
	// used in writing
	protected OutputStreamWriter writer = null;
	// used in reading
	protected int offset;
	private static Map countries2to3 = null;
	private static Map countries3to2 = null;
	private boolean tscommUsed = false;
	private static boolean isMock = false;
	protected static Logger log = LogFactory.getInstance().getLogger(MoneyGramTranslator.class);
	private static final String ASCII_ENC = "US-ASCII";

	private static String getCharEncoding() {
		// return RTSPropertyMgr.isTSCOMM2() ? UTF_8_ENC : ASCII_ENC ;
		return ASCII_ENC;
	}

	public MoneyGramTranslator() {
	}

	/**
	 * Abstract method that returns the 4 character message type code.
	 * Subclasses must redefine to provide the correct code.
	 */
	protected abstract String getRequestMessageType();

	/**
	 * Abstract method for the serialization of a Request. Subclasses are
	 * responsible for casting msg to the appropriate type.
	 */
	public abstract byte[] serialize(OFACQueryRequest in) throws DRSException;

	/**
	 * Abstract method for the deserialization of a MoneyGram response into a
	 * Response object. Subclasses are responsible for casting msg to the
	 * appropriate type.
	 */
	public abstract void deserialize(byte[] response, OFACQueryResponse out) throws DRSException;

	/**
	 * Initializes the output writer.
	 */
	protected void initializeWriter(OutputStream stream) throws DRSException {
		try {
			writer = new OutputStreamWriter(stream, getCharEncoding());
		} catch (UnsupportedEncodingException e) {
			throw new DRSException(DRSExceptionCodes.EC906_MESSAGE_TRANSLATION_ERROR, "Bad "
					+ getCharEncoding() + " encoding in serialize().", "", Calendar.getInstance()
					.getTime(), "", "", e);
		}
	}

	/**
	 * Read a date in ccyyMMdd format (e.g 20020501 is February 5, 2001. The
	 * offset is advanced 8 characters.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The date. If the date is not parsable null is returned
	 * @exception -
	 *                UnsupportedEncodingException if ASCII is not supported.
	 */
	protected Date readLongDate(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 8) {
			offset += 8;
			return null;
		}
		String strValue = new String(msg, offset, 8, getCharEncoding());
		offset += 8;
		if ("00000000".equals(strValue.trim())) {
			return null;
		}
		// Mainframe is actually returning " 0"
		if ("0".equals(strValue.trim())) {
			return null;
		}
		// Let's check for nothing too
		if (strValue.trim().length() == 0) {
			return null;
		}
		try {
			return longDateFormatterYearFirst.parse(strValue);
		} catch (ParseException pe) {
			return null;
		}
	}

	/**
	 * Read a date in CCMMddyy format (e.g 02052001 is February 5, 2001. The
	 * offset is advanced 6 characters.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The date. If the date is not parsable null is returned
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported.
	 */
	protected Date readShortDate(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 6) {
			offset += 6;
			return null;
		}
		String strValue = new String(msg, offset, 6, getCharEncoding());
		offset += 6;
		if ("000000".equals(strValue.trim())) {
			return null;
		}
		try {
			return shortFormatter.parse(strValue);
		} catch (ParseException pe) {
			return null;
		}
	}

	/**
	 * Read a two-digit country code and translate it to three digits. The
	 * offset is advanced 2 characters.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The three-digit country code.
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported.
	 */
	protected String readCountry(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 2) {
			offset += 2;
			return "";
		}
		String country2 = new String(msg, offset, 2, getCharEncoding());
		offset += 2;
		if (country2 == null || country2.trim().length() == 0)
			return null;
		return (String) countries2to3.get(country2.toUpperCase());
	}

	protected String readCountry(byte[] msg, int length) throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return "";
		}
		String country2 = new String(msg, offset, length, getCharEncoding());
		offset += length;
		if (country2 == null)
			return null;
		String countryTrim = country2.trim();
		if (countryTrim.length() == 0)
			return null;
		return (String) countries2to3.get(countryTrim.toUpperCase());
	}

	/**
	 * Takes in a three-digit country code and translates it to two digits
	 * before writing it out.
	 * 
	 * @param country3 -
	 *            The three-digit country code
	 * @exception -
	 *                IOException on write error
	 */
	protected void writeCountry(String country3) throws IOException {
		String country2;
		if (country3 != null) {
			country2 = (String) countries3to2.get(country3.toUpperCase());
		} else {
			country2 = "  ";
		}
		write(country2, 2);
	}

	/**
	 * Read a time value in HHmmss format (e.g 143225 is 2:32:25 pm. The offset
	 * is advanced 6 characters. The returned date instance uses the base date
	 * of 1/1/1970 and whatever time is read in from the message.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The date 1/1/1970 with the parsed time. If the time is not
	 *         parsable null is returned.
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported.
	 */
	protected Date readTime(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 6) {
			offset += 6;
			return null;
		}
		StringBuffer sb = new StringBuffer();
		sb.toString();
		String strValue = new String(msg, offset, 6, getCharEncoding());
		offset += 6;
		try {
			return timeFormatter.parse(strValue);
		} catch (ParseException pe) {
			return null;
		}
	}

	/**
	 * Read a timestamp in yyMMddHHmm format (e.g 0004040830 is April 4, 2000 at
	 * 08:30 a.m. The offset is advanced 10 characters.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The date. If the date is not parsable the current date is
	 *         returned
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported. Like that will happen.
	 */
	protected Date readTimestamp(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 10) {
			offset += 10;
			return null;
		}
		String strValue = new String(msg, offset, 10, getCharEncoding());
		offset += 10;
		DateFormat formatter = getDateFormat("yyMMddHHmm");
		try {
			return formatter.parse(strValue);
		} catch (ParseException pe) {
			// do nothing, used to print stack trace
		}
		return new Date();
	}

	/**
	 * Reads date value in the format specified by <code>String
	 * strFormat</code>
	 * and returns a <code>GregorianCalendar</code> instance.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @param formatStr -
	 *            The message as bytes
	 * @return - GregorianCalendar instance of the specified date in the msg. If
	 *         the time is not, parsable null is returned.
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported.
	 */
	protected GregorianCalendar readCalendar(byte[] msg, String strFormat)
			throws UnsupportedEncodingException {
		int len = strFormat.length();
		if (msg.length < offset + len) {
			offset += len;
			return null;
		}
		String strValue = new String(msg, offset, len, getCharEncoding());
		DateFormat format = getDateFormat(strFormat);
		offset += len;
		try {
			// tracker 284 if mainframe is 0 return a null
			if (Integer.parseInt(strValue.trim()) == 0)
				return null;
			Date date = format.parse(strValue);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException pe) {
			return null;
		}
	}

	/**
	 * Read a boolean . The offset is advanced by 1.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The boolean.
	 */
	protected boolean readBoolean(byte[] msg) {
		if (msg.length < offset + 1) {
			offset += 1;
			return false;
		}
		char booleanValue = (char) msg[offset];
		offset += 1;
		if (booleanValue == '0' || booleanValue == 'F' || booleanValue == 'N')
			return false;
		else
			return true;
	}

	/**
	 * Read a String from the buffer of a given length. The String is trimmed to
	 * have no trailing spaces. The offset is incremented by the length.
	 */
	protected String readString(byte[] msg, int length) throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return "";
		}
		String returnValue = new String(msg, offset, length, getCharEncoding());
		offset += length;
		return returnValue.trim();
	}

	/**
	 * Read a String from the buffer of a given length. This method does not
	 * trim any spaces from the value. The offset is incremented by the length.
	 */
	protected String readStringAsIs(byte[] msg, int length) throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return "";
		}
		String returnValue = new String(msg, offset, length, getCharEncoding());
		offset += length;
		return returnValue;
	}

	/**
	 * Read an int from the buffer of a given length
	 * 
	 * @param msg
	 *            The buffer to read from
	 * @param length
	 *            The length to read
	 * @return The converted int
	 * @throws UnsupportedEncodingException
	 */
	protected int readInt(byte[] msg, int length) throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return 0;
		}
		String intString = readString(msg, length);
		if (null != intString && intString.length() > 0)
			return Integer.parseInt(intString);
		else
			return 0;
	}

	/**
	 * Read the currency precision of a given length. Defaults to 3 if the read
	 * integer is invalid.
	 * 
	 * @param msg
	 * @param length
	 * @param errorString
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	protected int readCurrencyPrecision(byte[] msg, int length, String errorString)
			throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return 3;
		}
		int retPrec = 3; // 3 is default
		try {
			retPrec = readInt(msg, length);
		} catch (NumberFormatException nfex) {
			log.warn("Error getting currency precision: " + errorString, nfex);
		}
		return retPrec;
	}

	/**
	 * Read a String from the buffer of a given length. The String is converted
	 * to an BigDecimal and given decimalPlaces decimal places.
	 */
	protected BigDecimal readMoney(byte[] msg, int length, int decimalPlaces)
			throws UnsupportedEncodingException, NumberFormatException {
		if (msg.length < offset + length) {
			offset += length;
			return null;
		}
		String moneyValue = new String(msg, offset, length, getCharEncoding()).trim();
		if (moneyValue.length() == 0) {
			offset += length;
			return null;
		}
		BigDecimal money = new BigDecimal(moneyValue);
		money = money.movePointLeft(decimalPlaces);
		offset += length;
		return money;
	}

	protected BigDecimal getScaledAmount(BigDecimal amt, int scale, String errorString) {
		BigDecimal newBD;
		try {
			newBD = amt.setScale(scale, BigDecimal.ROUND_UNNECESSARY);
		} catch (ArithmeticException e) {
			// Log the fact that the mainframe didn't follow the precision
			// rules, round and continue.
			log.warn("Error setting scale on BigDecimal: " + errorString + ", desired scale: "
					+ scale, e);
			newBD = amt.setScale(scale, BigDecimal.ROUND_HALF_UP);
		}
		return newBD;
	}

	/**
	 * Assumes that the data is a money value and left pads with zeros If a
	 * decimal is present, it is stripped out. The result is written to the
	 * output stream
	 */
	protected void writeMoney(BigDecimal amount, int length) throws IOException {
		// Assume 2 decimal places
		writeMoney(amount, length, 2);
	}

	/**
	 * Assumes that the data is a number and left pads with zeros
	 */
	protected void writeNumber(int value, int length) throws IOException {
		StringBuffer buf = new StringBuffer();
		buf.append(Integer.toString(value));
		while (buf.length() < length)
			buf.insert(0, 0);
		writer.write(buf.toString());
		writer.flush();
	}

	/**
	 * 24,58,99 Read a check number and parse it according to the rules
	 */
	protected String readCheckNumber(byte[] msg, int length) throws UnsupportedEncodingException {
		if (msg.length < offset + length) {
			offset += length;
			return "";
		}
		String checkNumber = readString(msg, length);
		if (checkNumber == null) {
			return null;
		}
		try {
			// Eat the first digit from the front of the check number
			String tmp = checkNumber.substring(0, 1);
			// If the first digit was not a 1, eat the next digit too.
			if (!tmp.equals("1")) {
				return checkNumber.substring(2);
			} else {
				return checkNumber.substring(1);
			}
		} catch (IndexOutOfBoundsException iob) {
			return checkNumber;
		}
	}

	/**
	 * Write a String to the buffer of a given length. The String is converted
	 * to an BigDecimal and given decimalPlaces decimal places.
	 */
	protected void writeMoney(BigDecimal amount, int length, int decimalPlaces) throws IOException {
		// Make sure we at least have a number.
		if (amount == null)
			amount = new BigDecimal(0);
		amount = amount.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		BigInteger unscaledAmount = amount.unscaledValue();
		StringBuffer paddedAmount = new StringBuffer("" + unscaledAmount);
		// If it's too short, pad it with zeros
		while (paddedAmount.length() < length) {
			paddedAmount.insert(0, 0);
		}
		writer.write(paddedAmount.substring(0, length));
		writer.flush();
	}

	/**
	 * Takes the given string and pads it (front or back) with the given
	 * character until the length equals the given length. The result is written
	 * to the output stream.
	 * 
	 * @param data
	 *            The data to be padded and written out
	 * @param padChar
	 *            The character to pad with
	 * @param length
	 *            The final length of the string to be written out
	 * @param padFront
	 *            If true, pads the front of the string; otherwise pads the end.
	 */
	protected void writePaddedString(String data, char padChar, int length, boolean padFront)
			throws IOException {
		StringBuffer paddedString = new StringBuffer();
		if (data != null) {
			paddedString = new StringBuffer(data);
		}
		// If it's too short, pad it
		while (paddedString.length() < length) {
			if (padFront) {
				paddedString.insert(0, padChar);
			} else {
				paddedString.append(padChar);
			}
		}
		// Make sure we don't write out more than we are supposed to
		writer.write(paddedString.substring(0, length));
		writer.flush();
	}

	/**
	 * Returns true if any one string is non-null and non-empty else returns
	 * false.
	 */
	protected boolean hasData(String[] strs) {
		for (int i = 0; i < strs.length; i++) {
			if (strs[i] != null && strs[i].length() > 0) {
				return true;
			} // end-if
		} // end-for
		return false;
	} // end-method

	/**
	 * Read a timestamp in yyyyMMddHHmmss format (e.g 20000404083022 is April 4,
	 * 2000 at 08:30:22 a.m. The offset is advanced 14 characters.
	 * 
	 * @param msg -
	 *            The message as bytes
	 * @return - The date. If the date is not parsable the current date is
	 *         returned
	 * @exception -
	 *                UnsupportedEncodingException if character set is not
	 *                supported. Like that will happen.
	 */
	protected Date readTimestampCentury(byte[] msg) throws UnsupportedEncodingException {
		if (msg.length < offset + 14) {
			offset += 14;
			return null;
		}
		String strValue = new String(msg, offset, 14, getCharEncoding());
		offset += 14;
		try {
			return longFormatter.parse(strValue);
		} catch (ParseException pe) {
			// do nothing, used to print stack trace
		}
		return new Date();
	}

	/**
	 * Writes the header of the message. Usually this is the a start of text
	 * character followed by the message type code.
	 * 
	 * @exception IOException
	 *                on write error
	 */
	protected void writeHeader() throws IOException {
		writer.write(getRequestMessageType());
	}

	/**
	 * Writes the given date in the long format yyyyMMddHHmmss, ex.
	 * 20010206074312 represents Feb 6, 2001 7:43:12.
	 * 
	 * @exception IOException
	 *                on write error
	 */
	protected void writeLongDate(Date date) throws IOException {
		writer.write(longFormatter.format(date));
	}

	/**
	 * Writes the given date in the short format MMddyy, ex. 020601 represents
	 * Feb 6, 2001.
	 * 
	 * @exception IOException
	 *                on write error
	 */
	protected void writeShortDate(Date date) throws IOException {
		writer.write(shortFormatter.format(date));
	}

	protected void writeShortDate(Date date, int length) throws IOException {
		write(shortFormatter.format(date), length);
	}

	/**
	 * Writes the footer of the message. Usually this is the a CR character
	 * 
	 * @exception IOException
	 *                on write error
	 */
	protected void writeFooter() throws IOException {
		writer.write(0x0004);
		writer.write(0x0003);
		writer.flush();
	}

	/**
	 * Converts the string to uppercase, pads with spaces to the proper length
	 * if needed, and writes it to the output stream
	 */
	protected void write(String str, int length) throws IOException {
		String pad;
		// Convert international chars first
		if (str != null) {
			pad = padString(convertToNonInternational(str).toUpperCase(), length);
		} else {
			pad = padString(str, length);
		}
		writer.write(pad);
		writer.flush();
	}

	// Wanted to do this in the CharSet.java class.
	// However, that converts the characters after the CRC calculation
	// has already been performed. Would need to rearrange the guts of
	// CICS transceiver to make it work there.
	private String convertToNonInternational(String str) {
		String s = null;
		if (str == null)
			return str;
		s = str.replace('\u00C0', 'A');
		s = s.replace('\u00C1', 'A');
		s = s.replace('\u00C2', 'A');
		s = s.replace('\u00C3', 'A');
		s = s.replace('\u00C4', 'A');
		s = s.replace('\u00C5', 'A');
		s = s.replace('\u00C7', 'C');
		s = s.replace('\u00C8', 'E');
		s = s.replace('\u00C9', 'E');
		s = s.replace('\u00CA', 'E');
		s = s.replace('\u00CB', 'E');
		s = s.replace('\u00CC', 'I');
		s = s.replace('\u00CD', 'I');
		s = s.replace('\u00CE', 'I');
		s = s.replace('\u00CF', 'I');
		s = s.replace('\u00D1', 'N');
		s = s.replace('\u00D2', 'O');
		s = s.replace('\u00D3', 'O');
		s = s.replace('\u00D4', 'O');
		s = s.replace('\u00D5', 'O');
		s = s.replace('\u00D6', 'O');
		s = s.replace('\u00D8', 'O');
		s = s.replace('\u00D9', 'U');
		s = s.replace('\u00DA', 'U');
		s = s.replace('\u00DB', 'U');
		s = s.replace('\u00DC', 'U');
		s = s.replace('\u00DD', 'Y');
		s = s.replace('\u0178', 'Y');
		s = s.replace('\u00E0', 'a');
		s = s.replace('\u00E1', 'a');
		s = s.replace('\u00E2', 'a');
		s = s.replace('\u00E3', 'a');
		s = s.replace('\u00E4', 'a');
		s = s.replace('\u00E5', 'a');
		s = s.replace('\u00E7', 'c');
		s = s.replace('\u00E8', 'e');
		s = s.replace('\u00E9', 'e');
		s = s.replace('\u00EA', 'e');
		s = s.replace('\u00EB', 'e');
		s = s.replace('\u00EC', 'i');
		s = s.replace('\u00ED', 'i');
		s = s.replace('\u00EE', 'i');
		s = s.replace('\u00EF', 'i');
		s = s.replace('\u00F1', 'n');
		s = s.replace('\u00F2', 'o');
		s = s.replace('\u00F3', 'o');
		s = s.replace('\u00F4', 'o');
		s = s.replace('\u00F5', 'o');
		s = s.replace('\u00F6', 'o');
		s = s.replace('\u00F8', 'o');
		s = s.replace('\u00F9', 'u');
		s = s.replace('\u00FA', 'u');
		s = s.replace('\u00FB', 'u');
		s = s.replace('\u00FC', 'u');
		s = s.replace('\u00FD', 'y');
		s = s.replace('\u00FF', 'y');
		// TODO added to support Turkish letters, story not yet written.
		/*
		 * s = s.replace('\u011E', 'G'); s = s.replace('\u011F', 'g'); s =
		 * s.replace('\u015E', 'S'); s = s.replace('\u015F', 's'); s =
		 * s.replace('\u011E', 'G'); s = s.replace('\u0130', 'I');
		 */
		// Translation of Unicode 0100-017F
		s = s.replace('\u0100', 'A');
		s = s.replace('\u0101', 'a');
		s = s.replace('\u0102', 'A');
		s = s.replace('\u0103', 'a');
		s = s.replace('\u0104', 'A');
		s = s.replace('\u0105', 'a');
		s = s.replace('\u0106', 'C');
		s = s.replace('\u0107', 'c');
		s = s.replace('\u0108', 'C');
		s = s.replace('\u0109', 'c');
		s = s.replace('\u010A', 'C');
		s = s.replace('\u010B', 'c');
		s = s.replace('\u010C', 'C');
		s = s.replace('\u010D', 'c');
		s = s.replace('\u010E', 'D');
		s = s.replace('\u010F', 'd');
		s = s.replace('\u0110', 'D');
		s = s.replace('\u0111', 'd');
		s = s.replace('\u0112', 'E');
		s = s.replace('\u0113', 'e');
		s = s.replace('\u0114', 'E');
		s = s.replace('\u0115', 'e');
		s = s.replace('\u0116', 'E');
		s = s.replace('\u0117', 'e');
		s = s.replace('\u0118', 'E');
		s = s.replace('\u0119', 'e');
		s = s.replace('\u011A', 'E');
		s = s.replace('\u011B', 'e');
		s = s.replace('\u011C', 'G');
		s = s.replace('\u011D', 'g');
		s = s.replace('\u011E', 'G');
		s = s.replace('\u011F', 'g');
		s = s.replace('\u0120', 'G');
		s = s.replace('\u0121', 'g');
		s = s.replace('\u0122', 'G');
		s = s.replace('\u0123', 'g');
		s = s.replace('\u0124', 'H');
		s = s.replace('\u0125', 'h');
		s = s.replace('\u0126', 'H');
		s = s.replace('\u0127', 'h');
		s = s.replace('\u0128', 'I');
		s = s.replace('\u0129', 'i');
		s = s.replace('\u012A', 'I');
		s = s.replace('\u012B', 'i');
		s = s.replace('\u012C', 'I');
		s = s.replace('\u012D', 'i');
		s = s.replace('\u012E', 'I');
		s = s.replace('\u012F', 'i');
		s = s.replace('\u0130', 'I');
		s = s.replace('\u0131', 'i');
		s = s.replace('\u0134', 'J');
		s = s.replace('\u0135', 'j');
		s = s.replace('\u0136', 'K');
		s = s.replace('\u0137', 'k');
		s = s.replace('\u0139', 'L');
		s = s.replace('\u013A', 'l');
		s = s.replace('\u013B', 'L');
		s = s.replace('\u013C', 'l');
		s = s.replace('\u013D', 'L');
		s = s.replace('\u013E', 'l');
		s = s.replace('\u013F', 'L');
		s = s.replace('\u0140', 'l');
		s = s.replace('\u0141', 'L');
		s = s.replace('\u0142', 'l');
		s = s.replace('\u0143', 'N');
		s = s.replace('\u0144', 'n');
		s = s.replace('\u0145', 'N');
		s = s.replace('\u0146', 'n');
		s = s.replace('\u0147', 'N');
		s = s.replace('\u0148', 'n');
		s = s.replace('\u014C', 'O');
		s = s.replace('\u014D', 'o');
		s = s.replace('\u014E', 'O');
		s = s.replace('\u014F', 'o');
		s = s.replace('\u0150', 'O');
		s = s.replace('\u0151', 'o');
		s = s.replace('\u0154', 'R');
		s = s.replace('\u0155', 'r');
		s = s.replace('\u0156', 'R');
		s = s.replace('\u0157', 'r');
		s = s.replace('\u0158', 'R');
		s = s.replace('\u0159', 'r');
		s = s.replace('\u015A', 'S');
		s = s.replace('\u015B', 's');
		s = s.replace('\u015C', 'S');
		s = s.replace('\u015D', 's');
		s = s.replace('\u015E', 'S');
		s = s.replace('\u015F', 's');
		s = s.replace('\u0160', 'S');
		s = s.replace('\u0161', 's');
		s = s.replace('\u0162', 'T');
		s = s.replace('\u0163', 't');
		s = s.replace('\u0164', 'T');
		s = s.replace('\u0165', 't');
		s = s.replace('\u0166', 'T');
		s = s.replace('\u0167', 't');
		s = s.replace('\u0168', 'U');
		s = s.replace('\u0169', 'u');
		s = s.replace('\u016A', 'U');
		s = s.replace('\u016B', 'u');
		s = s.replace('\u016C', 'U');
		s = s.replace('\u016D', 'u');
		s = s.replace('\u016E', 'U');
		s = s.replace('\u016F', 'u');
		s = s.replace('\u0170', 'U');
		s = s.replace('\u0171', 'u');
		s = s.replace('\u0172', 'U');
		s = s.replace('\u0173', 'u');
		s = s.replace('\u0174', 'W');
		s = s.replace('\u0175', 'w');
		s = s.replace('\u0176', 'Y');
		s = s.replace('\u0177', 'y');
		s = s.replace('\u0178', 'Y');
		s = s.replace('\u0179', 'Z');
		s = s.replace('\u017A', 'z');
		s = s.replace('\u017B', 'Z');
		s = s.replace('\u017C', 'z');
		s = s.replace('\u017D', 'Z');
		s = s.replace('\u017E', 'z');
		s = s.replace('\u017F', 's');
		// not supported characters
		// s = s.replace('\u0149', "'n");
		// s = s.replace('\u014A', "ENG");
		// s = s.replace('\u014B', "eng");
		// s = s.replace('\u0152', "OE");
		// s = s.replace('\u0153', "oe");
		// s = s.replace('\u0132', "IJ");
		// s = s.replace('\u0133', "ij");
		// s = s.replace('\u0138', "kra");
		return s;
	}

	/**
	 * Pads a String to the specified number of characters Done in the message
	 * since we don't want to save the space padded data.
	 * 
	 * @param str -
	 *            String to pad. If null a string with all spaces is returned.
	 * @param length -
	 *            number of characters. Trims the string if the string is
	 *            currently too long.
	 * @return the padded string.
	 */
	protected String padString(String str, int length) {
		if (str == null) {
			char[] padChars = new char[length];
			for (int i = 0; i < length; i++) {
				padChars[i] = ' ';
			}
			return new String(padChars);
		} else if (str.length() == length) {
			return str;
		} else if (str.length() > length) {
			return str.substring(0, length);
		} else {
			char[] strChars = str.toCharArray();
			char[] padChars = new char[length];
			System.arraycopy(strChars, 0, padChars, 0, strChars.length);
			for (int i = strChars.length; i < length; i++) {
				padChars[i] = ' ';
			}
			return new String(padChars);
		}
	}

	protected void writeCheckType(boolean useOurPaper, String checkNum, String checkType,
			String agentCountry) throws IOException {
		if (!useOurPaper) {
			write("99", 2);
		} else {
			// SLS 03-03-06 - If there is no check number specified then put
			// spaces in for check type.
			if (checkNum == null || checkNum.length() == 0 || isRepeatChar(checkNum, '0')) {
				write("  ", 2); // Two spaces
			} else if ("DG".equalsIgnoreCase(checkType) || "MT".equalsIgnoreCase(checkType)
					|| "MTC".equalsIgnoreCase(checkType)) {
				write(checkType, 2);
			} else {
				write("CA", 2); // cash
			}
		}
	}

	/**
	 * Indicates if the given string is solely composed of the given character
	 * repeated.
	 */
	protected boolean isRepeatChar(String s, char c) {
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c) {
				return false;
			}
		}
		return true;
	}
	private static Map<String, DateFormat> dateFormatCache = new HashMap<String, DateFormat>();

	private DateFormat getDateFormat(String strFormat) {
		DateFormat dateFormat = (DateFormat) dateFormatCache.get(strFormat);
		if (dateFormat == null) {
			dateFormat = new SimpleDateFormat(strFormat);
			dateFormatCache.put(strFormat, dateFormat);
		}
		return dateFormat;
	}

	// mainframe uses MXP vs. MXN
	protected String fixCurrency(String currency) {
		if (currency == null) {
			return currency;
		}
		if ("MXN".equals(currency)) {
			return "MXP";
		}
		if ("MXP".equals(currency)) {
			return "MXN";
		} else
			return currency;
	}

	/**
	 * @return Returns the mgcommUsed.
	 */
	public boolean isMgcommUsed() {
		return !tscommUsed;
	}

	/**
	 * @return Returns the mgcommUsed.
	 */
	public boolean isTSCommUsed() {
		return tscommUsed;
	}

	/**
	 * @param mgcommUsed
	 *            The mgcommUsed to set.
	 */
	public void setTSCommUsed(boolean tscommUsed) {
		this.tscommUsed = tscommUsed;
	}

	/**
	 * @return Returns the isMock.
	 */
	public boolean isMock() {
		return isMock;
	}

	/**
	 * @param isMock
	 *            The isMock to set.
	 */
	public static void setMock(boolean isMockIn) {
		isMock = isMockIn;
	}
}