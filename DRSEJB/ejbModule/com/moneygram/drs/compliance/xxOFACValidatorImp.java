/*
 * Created on Feb 21, 2005
 *
 */
package com.moneygram.drs.compliance;

import java.util.Date;
import java.util.List;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.compliance.domain.OFAC;
import com.moneygram.drs.compliance.domain.OFACCustomer;
import com.moneygram.drs.compliance.exception.OFACException;
import com.moneygram.drs.profiles.DRSException;

/**
 * Validates DSS registrations via OFAC
 * 
 * @author Stu Schrader
 * 
 */
public class xxOFACValidatorImp implements xxOFACValidator {
	private static final Logger log = LogFactory.getInstance().getLogger(xxOFACValidator.class);
	private static xxOFACValidator instance;

	public static void setMock(xxOFACValidator mock) {
		instance = mock;
	}

	public static xxOFACValidator getInstance() {
		if (instance == null) {
			instance = new xxOFACValidatorImp();
		}
		return instance;
	}

	public String validateOFAC(CustomerProfile newProfile, CustomerProfile existingProfile,
			boolean create, String language) throws DRSException {
		boolean checkOFAC = false;
		// If edit, get the existing customer profile
		if (!create && existingProfile != null) {
			// Check for name changes...
			if (newProfile.areNonCreatorNamesDifferent(existingProfile))
				checkOFAC = true;
		} else
			checkOFAC = true;
		if (checkOFAC) {
			try {
				OFAC retOfac = OFACQueryManager.getInstance().queryOFAC(
						newProfile.getCustomerReceiveNumber(),
						buildOFACFromCustomerProfile(newProfile, existingProfile),
						OFACQueryManager.SOURCE_DSS);
				return retOfac.getStatus();
			} catch (OFACException ofacEx) {
				log.error("Unable to retrieve OFAC status from MF", ofacEx);
				throw new DRSException(0, "Value", "OFAC", new Date(),
						"Unable to retrieve OFAC status from MF", null);
			}
		}
		return null;
	}

	private static OFAC buildOFACFromCustomerProfile(CustomerProfile cp,
			CustomerProfile existingProfile) {
		OFAC ofac = new OFAC();
		OFACCustomer ofacPerson = createCustomer(cp.getReceiver(), existingProfile == null ? null
				: existingProfile.getReceiver());
		if (ofacPerson != null)
			ofac.setCustomer(ofacPerson);
		ofacPerson = createCustomer(cp.getJointAccountHolderByIndex(1),
				existingProfile == null ? null : existingProfile.getJointAccountHolderByIndex(1));
		if (ofacPerson != null)
			ofac.setThirdParty1(ofacPerson);
		ofacPerson = createCustomer(cp.getJointAccountHolderByIndex(2),
				existingProfile == null ? null : existingProfile.getJointAccountHolderByIndex(1));
		if (ofacPerson != null)
			ofac.setThirdParty2(ofacPerson);
		ofacPerson = createCustomer(cp.getJointAccountHolderByIndex(3),
				existingProfile == null ? null : existingProfile.getJointAccountHolderByIndex(1));
		if (ofacPerson != null)
			ofac.setThirdParty3(ofacPerson);
		return ofac;
	}

	/**
	 * Copies from our customer profile to the ofac customer. Currently our
	 * profile has no date of birth, legal identification, organization so we
	 * don't copy.
	 * 
	 * @param cpp
	 * @param existingCpp
	 * @return
	 */
	private static OFACCustomer createCustomer(CustomerProfilePerson cpp,
			CustomerProfilePerson existingCpp) {
		if (cpp == null)
			return null;
		OFACCustomer ofacPerson = new OFACCustomer();
		ofacPerson.setFirstName(getActualValue(cpp.getFirstName(), existingCpp == null ? null
				: existingCpp.getFirstName()));
		ofacPerson.setMiddleInitial(getActualValue(cpp.getMiddleName(), existingCpp == null ? null
				: existingCpp.getMiddleName()));
		ofacPerson.setLastName(getActualValue(cpp.getLastName(), existingCpp == null ? null
				: existingCpp.getLastName()));
		ofacPerson.setMaternalName(getActualValue(cpp.getMaternalName(), existingCpp == null ? null
				: existingCpp.getMaternalName()));
		ofacPerson.setPhoneNumber(cpp.getPhoneNumber());
		List l = cpp.getAddresses();
		if (l != null && l.size() >= 1) {
			ofacPerson.setAddress(createAddress((Address) l.get(0)));
		}
		return ofacPerson;
	}

	private static String getActualValue(String changed, String existing) {
		return changed == null ? existing : changed;
	}

	private static com.moneygram.drs.compliance.domain.Address createAddress(Address profileAddress) {
		if (profileAddress == null)
			return null;
		com.moneygram.drs.compliance.domain.Address ofacAddress = new com.moneygram.drs.compliance.domain.Address();
		ofacAddress.setAddress1(profileAddress.getAddrLine1());
		ofacAddress.setAddress2(profileAddress.getAddrLine2());
		ofacAddress.setAddress3(profileAddress.getAddrLine3());
		ofacAddress.setCity(profileAddress.getCity());
		ofacAddress.setCountry(profileAddress.getCntryCode());
		ofacAddress.setPostalCode(profileAddress.getPostalCode());
		ofacAddress.setStateOrProvince(profileAddress.getState());
		return ofacAddress;
	}
}
