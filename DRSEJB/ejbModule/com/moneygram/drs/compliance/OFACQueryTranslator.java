/*
 * Created on Nov 6, 2003
 */
package com.moneygram.drs.compliance;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.compliance.domain.OFACDescriptorInfo;
import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.rts.transceiver.TranslationException;

/**
 * Translates to and fro for the OF03 OFAC check mainframe message.
 * 
 * @author Erik Pearson & Stu Schrader
 */
public class OFACQueryTranslator extends MoneyGramTranslator {
	private Logger log = LogFactory.getInstance().getLogger(OFACQueryTranslator.class);
	private static final String REQUEST_MSG_TYPE = "OF03";
	private static final String TRAN_VERSION = "1302";
	private static final int AVAIL_LINES = 4;
	private static final int DESCRIPTOR_SIZE = 2 + 60 + 40 + 40 + 40 + 40 + 30 + 30 + 30 + 20 + 2
			+ 2 + 9 + 14 + 20 + 1 + 20 + 1 + 2 + 2 + 6 + 2;

	public OFACQueryTranslator() {
	}

	protected String getRequestMessageType() {
		return REQUEST_MSG_TYPE;
	}

	public byte[] serialize(OFACQueryRequest in) throws DRSException {
		ByteArrayOutputStream outbs = new ByteArrayOutputStream();
		byte[] outBytes;
		try {
			writer = new OutputStreamWriter(outbs, "ASCII");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("unsupported encoding in OF03Template.toByteArray(): " + e);
		}
		OFACQueryRequest msg = (OFACQueryRequest) in;
		try {
			writeHeader();
			write(null, 1); // sf data flag
			write(null, 4); // resp display
			write(null, 14); // timestamp
			write(TRAN_VERSION, 4);
			write(msg.getFunction(), 1);
			write(msg.getWhere(), 1);
			write(msg.getTranType(), 1);
			write(msg.getSqlProductCd(), 2);
			write(msg.getSqlExceptionSource(), 4);
			write(msg.getPosKey(), 23);
			write(null, 23); // ofacKey
			int lines = msg.getDescriptorList().size();
			DecimalFormat df = new DecimalFormat("00");
			write(df.format(lines), 2);
			Iterator it = msg.getDescriptorList().iterator();
			OFACDescriptorInfo des = null;
			while (it.hasNext()) {
				des = (OFACDescriptorInfo) it.next();
				write(des.getDescriptorType(), 2);
				write(des.getOrganization(), 60);
				write(des.getLastName(), 40);
				write(des.getFirstName(), 40);
				write(des.getMiddleInitial(), 40);
				write(des.getMateralName(), 40);
				write(des.getAddress1(), 30);
				write(des.getAddress2(), 30);
				write(des.getAddress3(), 30);
				write(des.getCity(), 20);
				write(des.getState(), 2);
				write(des.getCountry(), 2);
				write(des.getPostalCode(), 9);
				write(des.getPhone(), 14);
				write(des.getLegalId(), 20);
				write(des.getLegalIdType(), 1);
				write(des.getPhotoId(), 20);
				write(des.getPhotoIdType(), 1);
				write(des.getPhotoIdState(), 2);
				write(des.getPhotoIdCountry(), 2);
				write(des.getDateOfBirth(), 6);
				write(null, 2); // line hit status
			}
			// pad for blank descriptors
			if (lines > AVAIL_LINES) {
				throw new TranslationException(
						"OFACQueryMsg cannot have more than 4 descriptor lines");
			}
			int pad = (AVAIL_LINES - lines) * DESCRIPTOR_SIZE;
			write(null, pad);
			write(null, 2); // hit status
			write(null, 2); // line status
			write(null, 34); // error message
			write(null, 133); // filler
			write("0123456789", 10); // more filler (debug code)
			writeFooter();
			outBytes = outbs.toByteArray();
			log.debug("request:  " + new String(outBytes));
			StringBuffer sb = new StringBuffer("(hex):    ");
			sb.append("" + 0 + ":" + Integer.toHexString(((int) outBytes[0]) & 0xff));
			for (int i = 1; i < outBytes.length; i++) {
				sb.append("," + i + ":" + Integer.toHexString(((int) outBytes[i]) & 0xff));
			}
			log.debug(sb.toString());
			return outBytes;
		} catch (IOException io) {
			throw new DRSException(DRSExceptionCodes.EC906_MESSAGE_TRANSLATION_ERROR,
					"IOException during serialize().", "", Calendar.getInstance().getTime(), "",
					"", io);
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
			try {
				outbs.close();
			} catch (Exception e) {
			}
		}
	}

	public void deserialize(byte[] buf, OFACQueryResponse out) throws DRSException {
		log.debug("response: " + new String(buf));
		StringBuffer sb = new StringBuffer("(hex):    ");
		sb.append("" + 0 + ":" + Integer.toHexString(((int) buf[0]) & 0xff));
		for (int i = 1; i < buf.length; i++) {
			sb.append("," + i + ":" + Integer.toHexString(((int) buf[i]) & 0xff));
		}
		log.debug(sb.toString());
		OFACQueryResponse response = (OFACQueryResponse) out;
		try {
			offset = 4; // skip tranid
			offset += 1; // skip success (already checked)
			offset += 4; // resp display -- what is this?
			offset += 14; // timestamp -- do we care about it?
			response.setMessageVersion(readString(buf, 4));
			response.setFunction(readString(buf, 1));
			response.setWhere(readString(buf, 1));
			response.setTranType(readString(buf, 1));
			response.setSqlProductCd(readString(buf, 2));
			response.setSqlExceptionSource(readString(buf, 4));
			response.setPosKey(readString(buf, 23));
			response.setOfacKey(readString(buf, 23));
			int count = Integer.parseInt(readString(buf, 2));
			List<OFACDescriptorInfo> list = new ArrayList<OFACDescriptorInfo>();
			OFACDescriptorInfo des = null;
			for (int i = 0; i < count; i++) {
				des = new OFACDescriptorInfo();
				des.setDescriptorType(readString(buf, 2));
				des.setOrganization(readString(buf, 60));
				des.setLastName(readString(buf, 40));
				des.setFirstName(readString(buf, 40));
				des.setMiddleInitial(readString(buf, 40));
				des.setMateralName(readString(buf, 40));
				des.setAddress1(readString(buf, 30));
				des.setAddress2(readString(buf, 30));
				des.setAddress3(readString(buf, 30));
				des.setCity(readString(buf, 20));
				des.setState(readString(buf, 2));
				des.setCountry(readString(buf, 2));
				des.setPostalCode(readString(buf, 9));
				des.setPhone(readString(buf, 14));
				des.setLegalId(readString(buf, 20));
				des.setLegalIdType(readString(buf, 1));
				des.setPhotoId(readString(buf, 20));
				des.setPhotoIdType(readString(buf, 1));
				des.setPhotoIdState(readString(buf, 2));
				des.setPhotoIdCountry(readString(buf, 2));
				des.setDateOfBirth(readString(buf, 6));
				des.setHitStatus(readString(buf, 2));
				list.add(des);
			}
			for (; count < AVAIL_LINES; count++) {
				offset += DESCRIPTOR_SIZE;
			}
			response.setDescriptorList(list);
			response.setHitStatus(readString(buf, 2));
			response.setCallStatus(readString(buf, 2));
			response.setErrorMessage(readString(buf, 34));
		} catch (UnsupportedEncodingException uee) {
			throw new DRSException(DRSExceptionCodes.EC906_MESSAGE_TRANSLATION_ERROR,
					"Bad character encoding in deserialize().", "", Calendar.getInstance()
							.getTime(), "", "", uee);
		}
	}
}
