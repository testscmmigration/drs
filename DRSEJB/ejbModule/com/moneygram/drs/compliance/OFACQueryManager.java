/*
 * Created on Nov 10, 2003
 */
package com.moneygram.drs.compliance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.moneygram.drs.compliance.domain.Address;
import com.moneygram.drs.compliance.domain.Identification;
import com.moneygram.drs.compliance.domain.OFAC;
import com.moneygram.drs.compliance.domain.OFACCustomer;
import com.moneygram.drs.compliance.domain.OFACDescriptorInfo;
import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;
import com.moneygram.drs.compliance.exception.OFACException;

/**
 * @author Erik Pearson
 */
public class OFACQueryManager {
	RTSTransceiver transceiver = null;
	private static OFACQueryManager INSTANCE;
	public static final int SOURCE_DSS = 2;

	private OFACQueryManager() throws OFACException {
		try {
			transceiver = RTSTransceiverImpl.getInstance();
		} catch (Exception e) {
			throw new OFACException("IOException while creating CICSTransceiver", e);
		}
	}

	public synchronized static OFACQueryManager getInstance() throws OFACException {
		if (INSTANCE == null)
			INSTANCE = new OFACQueryManager();
		return INSTANCE;
	}

	/**
	 * Build a descriptor for a given person
	 * 
	 * @param person -
	 *            The person to glean info from
	 * @param descriptorType
	 *            The type of descriptor.
	 * @return
	 */
	private OFACDescriptorInfo buildDescriptorInfo(OFACCustomer person, String descriptorType) {
		if (person == null)
			return null;
		OFACDescriptorInfo info = new OFACDescriptorInfo();
		info.setDescriptorType(descriptorType);
		info.setFirstName(person.getFirstName());
		info.setMiddleInitial(person.getMiddleInitial());
		info.setLastName(person.getLastName());
		Address address = person.getAddress();
		if (address != null) {
			info.setAddress1(address.getAddress1());
			info.setAddress2(address.getAddress2());
			info.setAddress3(address.getAddress3());
			info.setCity(address.getCity());
			info.setState(address.getStateOrProvince());
			info.setCountry(address.getCountry());
			info.setPostalCode(address.getPostalCode());
		}
		info.setPhone(person.getPhoneNumber());
		info.setOrganization(person.getOrganization());
		Date d = person.getDateOfBirth();
		if (d != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
			info.setDateOfBirth(sdf.format(d));
		}
		info.setMateralName(person.getMaternalName()); // no map for middle
		// initial
		Identification id = person.getLegalId();
		if (id != null) {
			info.setLegalId(id.getNumber());
			info.setLegalIdType(id.getType());
		}
		id = person.getPhotoId();
		if (id != null) {
			info.setPhotoId(id.getNumber());
			info.setPhotoIdCountry(id.getCountry());
			info.setPhotoIdState(id.getState());
			info.setPhotoIdType(id.getType());
		}
		return info;
	}

	public OFAC queryOFAC(String sourceID, OFAC ofac, int source) throws OFACException {
		if (source != SOURCE_DSS)
			throw new OFACException("Unkown source type: " + source);
		// build mainframe message
		OFACQueryRequest request = new OFACQueryRequest();
		// M = descriptor match
		// other values are E (entity List), L (match and list)
		request.setFunction("M");
		// 5 = mainframe passthrough
		// other values are 1 (voice), 2 (mws), 3 (batch), 4 (stargram)
		request.setWhere("5");
		request.setTranType("7");
		request.setSqlProductCd("CR");
		// Customer recieve number is sufficient for our source id.
		request.setPosKey(sourceID);
		ofac.setSourceId(sourceID);
		request.setSqlExceptionSource("CRN");
		// set up descriptor
		List<OFACDescriptorInfo> list = new ArrayList<OFACDescriptorInfo>();
		if (ofac.getCustomer() != null)
			list.add(buildDescriptorInfo(ofac.getCustomer(), "14"));
		if (ofac.getThirdParty1() != null)
			list.add(buildDescriptorInfo(ofac.getThirdParty1(), "15"));
		if (ofac.getThirdParty2() != null)
			list.add(buildDescriptorInfo(ofac.getThirdParty2(), "16"));
		if (ofac.getThirdParty3() != null)
			list.add(buildDescriptorInfo(ofac.getThirdParty3(), "17"));
		request.setDescriptorList(list);
		OFACQueryResponse response = new OFACQueryResponse();
		try {
			response = (OFACQueryResponse) transceiver.send(request, response);
		} catch (Exception e) {
			throw new OFACException("error during OFAC query", e);
		}
		if (response.getHitStatus().equals("H")) {
			ofac.setSourceId(response.getPosKey());
			ofac.setStatus("F");
		} else
			ofac.setStatus("P");
		return ofac;
	}
}
