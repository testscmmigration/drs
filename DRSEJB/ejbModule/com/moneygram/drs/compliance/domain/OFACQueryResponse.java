package com.moneygram.drs.compliance.domain;

import java.io.Serializable;
import java.util.List;

/**
 * This is the message for an OF03/OF04 mainframe message pair.
 * 
 * @see com.travelersexpress.deltaworks.message.info.OFACDescriptorInfo
 * @author Erik Pearson
 */
public class OFACQueryResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private String messageVersion;
	private String function;
	private String where;
	private String tranType;
	private String sqlProductCd;
	private String sqlExceptionSource;
	private String posKey;
	private String ofacKey;
	private List descriptorList;
	private String hitStatus;
	private String callStatus;
	private String errorMessage;

	public String getCallStatus() {
		return callStatus;
	}

	public List getDescriptorList() {
		return descriptorList;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getSqlExceptionSource() {
		return sqlExceptionSource;
	}

	public String getFunction() {
		return function;
	}

	public String getHitStatus() {
		return hitStatus;
	}

	public String getMessageVersion() {
		return messageVersion;
	}

	public String getSqlProductCd() {
		return sqlProductCd;
	}

	public String getTranType() {
		return tranType;
	}

	public String getWhere() {
		return where;
	}

	public void setCallStatus(String string) {
		callStatus = string;
	}

	public void setDescriptorList(List list) {
		descriptorList = list;
	}

	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	public void setSqlExceptionSource(String string) {
		sqlExceptionSource = string;
	}

	public void setFunction(String string) {
		function = string;
	}

	public void setHitStatus(String string) {
		hitStatus = string;
	}

	public void setMessageVersion(String string) {
		messageVersion = string;
	}

	public void setSqlProductCd(String string) {
		sqlProductCd = string;
	}

	public void setTranType(String string) {
		tranType = string;
	}

	public void setWhere(String string) {
		where = string;
	}

	public String getPosKey() {
		return posKey;
	}

	public void setPosKey(String string) {
		posKey = string;
	}

	public String getOfacKey() {
		return ofacKey;
	}

	public void setOfacKey(String string) {
		ofacKey = string;
	}
}
