package com.moneygram.drs.compliance.domain;

import java.io.Serializable;

public class Address implements Serializable {
	private static final long serialVersionUID = 1L;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String stateOrProvince;
	private String country;
	private String postalCode;

	/**
	 * @return
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @return
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @return
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @return
	 */
	public String getStateOrProvince() {
		return stateOrProvince;
	}

	/**
	 * @param string
	 */
	public void setAddress1(String string) {
		address1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress2(String string) {
		address2 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress3(String string) {
		address3 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * @param string
	 */
	public void setPostalCode(String string) {
		postalCode = string;
	}

	/**
	 * @param string
	 */
	public void setStateOrProvince(String string) {
		stateOrProvince = string;
	}
}
