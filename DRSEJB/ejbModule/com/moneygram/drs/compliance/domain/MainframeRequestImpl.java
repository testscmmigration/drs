/*
 * Created on Jul 17, 2007
 *
 */
package com.moneygram.drs.compliance.domain;

import com.moneygram.common.message.RequestInterface;
import com.moneygram.message.MainframeCallType;
import com.moneygram.message.MainframeRequest;
import com.moneygram.message.MainframeValueAdapter;

/**
 * @author t007
 * 
 */
public class MainframeRequestImpl implements MainframeRequest {
	OFACQueryRequest request;

	/**
	 * 
	 */
	public MainframeRequestImpl(OFACQueryRequest r) {
		super();
		request = r;
	}

	/**
	 * @see com.moneygram.message.MainframeRequest#getRequestInterface()
	 */
	public RequestInterface getRequestInterface() {
		return request;
	}

	/**
	 * @see com.moneygram.message.MainframeRequest#getMainframeCallType()
	 */
	public MainframeCallType getMainframeCallType() {
		return MainframeCallType.MAINFRAME_OFAC;
	}

	/**
	 * @see com.moneygram.message.MainframeRequest#getMainframeValueAdapter()
	 */
	public MainframeValueAdapter getMainframeValueAdapter() {
		// return MoneyGramCodeTranslator.getInstance(); //TODO Rob Morgan if
		return null;
	}
}
