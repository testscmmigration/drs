package com.moneygram.drs.compliance.domain;

public class Identification {
	private String number;
	private String type;
	private String state;
	private String country;

	public String getCountry() {
		return country;
	}

	public String getNumber() {
		return number;
	}

	public String getState() {
		return state;
	}

	public String getType() {
		return type;
	}

	public void setCountry(String string) {
		country = string;
	}

	public void setNumber(String string) {
		number = string;
	}

	public void setState(String string) {
		state = string;
	}

	public void setType(String type) {
		this.type = type;
	}
}
