package com.moneygram.drs.compliance.domain;

import java.io.Serializable;
import java.util.List;
import com.moneygram.common.guid.RequestGUID;
import com.moneygram.common.message.RequestInterface;
import com.moneygram.message.MainframeCallType;

/**
 * This is the message for an OF03/OF04 mainframe message pair.
 * 
 * @see com.travelersexpress.deltaworks.message.info.OFACDescriptorInfo
 * @author Erik Pearson
 */
public class OFACQueryRequest implements Serializable, RequestInterface {
	private static final long serialVersionUID = 1L;
	private String function;
	private String where;
	private String tranType;
	private String sqlProductCd;
	private String sqlExceptionSource;
	private String posKey;
	private String ofacKey;
	private List descriptorList;
	private String hitStatus;
	private String callStatus;
	private String errorMessage;
	private RequestGUID requestGUID;

	public String getCallStatus() {
		return callStatus;
	}

	public List getDescriptorList() {
		return descriptorList;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getSqlExceptionSource() {
		return sqlExceptionSource;
	}

	public String getFunction() {
		return function;
	}

	public String getHitStatus() {
		return hitStatus;
	}

	public String getSqlProductCd() {
		return sqlProductCd;
	}

	public String getTranType() {
		return tranType;
	}

	public String getWhere() {
		return where;
	}

	public void setCallStatus(String string) {
		callStatus = string;
	}

	public void setDescriptorList(List list) {
		descriptorList = list;
	}

	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	public void setSqlExceptionSource(String string) {
		sqlExceptionSource = string;
	}

	public void setFunction(String string) {
		function = string;
	}

	public void setHitStatus(String string) {
		hitStatus = string;
	}

	public void setSqlProductCd(String string) {
		sqlProductCd = string;
	}

	public void setTranType(String string) {
		tranType = string;
	}

	public void setWhere(String string) {
		where = string;
	}

	public String getPosKey() {
		return posKey;
	}

	public void setPosKey(String string) {
		posKey = string;
	}

	public String getOfacKey() {
		return ofacKey;
	}

	public void setOfacKey(String string) {
		ofacKey = string;
	}

	public MainframeCallType getMainframeCallType() {
		return MainframeCallType.MAINFRAME_OFAC;
	}

	/**
	 * This method should never return null!
	 * 
	 * @return
	 */
	public RequestGUID getRequestGUID() {
		if (requestGUID == null)
			establishGUID();
		return requestGUID;
	}

	protected RequestGUID setRequestGUID(RequestGUID reqGUID) {
		RequestGUID oldReqGuid = this.requestGUID;
		if (reqGUID != null)
			this.requestGUID = reqGUID;
		return oldReqGuid;
	}

	public RequestGUID establishGUID() {
		this.requestGUID = RequestGUID.createRequestGUID(getAPIMethodName(), null);
		return requestGUID;
	}

	public String getAPIMethodName() {
		StringBuffer methodNameBuf = new StringBuffer(this.getClass().getName());
		methodNameBuf.setLength(methodNameBuf.length() - "Request".length());
		int index = methodNameBuf.length() - 1;
		// Catch package name or last inner class
		while (index > 0 && methodNameBuf.charAt(index) != '.'
				&& methodNameBuf.charAt(index) != '$') {
			--index;
		}
		if (index > 0) {
			methodNameBuf.delete(0, index + 1);
		}
		methodNameBuf.setCharAt(0, Character.toLowerCase(methodNameBuf.charAt(0)));
		return methodNameBuf.toString();
	}
}
