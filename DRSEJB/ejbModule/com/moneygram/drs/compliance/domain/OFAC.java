package com.moneygram.drs.compliance.domain;

import java.util.Date;

public class OFAC {
	/** PK of the corresponding row in the OFAC table (if any) */
	private Long id;
	private String sourceId;
	private String status;
	private Date lastActivityDate;
	private OFACCustomer customer;
	private OFACCustomer thirdParty1;
	private OFACCustomer thirdParty2;
	private OFACCustomer thirdParty3;

	public OFACCustomer getCustomer() {
		return customer;
	}

	public Long getId() {
		return id;
	}

	public Date getLastActivityDate() {
		return lastActivityDate;
	}

	public String getSourceId() {
		return sourceId;
	}

	public String getStatus() {
		return status;
	}

	public OFACCustomer getThirdParty1() {
		return thirdParty1;
	}

	public void setCustomer(OFACCustomer customer) {
		this.customer = customer;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastActivityDate(Date date) {
		lastActivityDate = date;
	}

	public void setSourceId(String string) {
		sourceId = string;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setThirdParty1(OFACCustomer customer) {
		thirdParty1 = customer;
	}

	/**
	 * @return
	 */
	public OFACCustomer getThirdParty2() {
		return thirdParty2;
	}

	/**
	 * @return
	 */
	public OFACCustomer getThirdParty3() {
		return thirdParty3;
	}

	/**
	 * @param customer
	 */
	public void setThirdParty2(OFACCustomer customer) {
		thirdParty2 = customer;
	}

	/**
	 * @param customer
	 */
	public void setThirdParty3(OFACCustomer customer) {
		thirdParty3 = customer;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("OFAC[");
		buffer.append("id = ").append(id);
		buffer.append(", sourceId = ").append(sourceId);
		buffer.append(", status = ").append(status);
		buffer.append(", lastActivityDate = ").append(lastActivityDate);
		buffer.append(", customer = ").append(customer);
		buffer.append(", thirdParty1 = ").append(thirdParty1);
		buffer.append(", thirdParty2 = ").append(thirdParty2);
		buffer.append(", thirdParty3 = ").append(thirdParty3);
		buffer.append("]");
		return buffer.toString();
	}
}
