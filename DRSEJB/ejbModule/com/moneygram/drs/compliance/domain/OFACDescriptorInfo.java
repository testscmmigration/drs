/*
 * Created on Nov 6, 2003
 */
package com.moneygram.drs.compliance.domain;

import java.io.Serializable;

/**
 * This class represents a descriptor line returned on an OF04 (OFAC Query
 * Response).
 * 
 * @see com.travelersexpress.deltaworks.message.OFACQueryMsg
 * @author Erik Pearson
 */
public class OFACDescriptorInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private String descriptorType;
	private String organization;
	private String lastName;
	private String firstName;
	private String middleInitial;
	private String materalName;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private String phone;
	private String legalId;
	private String legalIdType;
	private String photoId;
	private String photoIdType;
	private String photoIdState;
	private String photoIdCountry;
	private String dateOfBirth;
	private String hitStatus;

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getAddress3() {
		return address3;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getDescriptorType() {
		return descriptorType;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getHitStatus() {
		return hitStatus;
	}

	public String getLastName() {
		return lastName;
	}

	public String getLegalId() {
		return legalId;
	}

	public String getLegalIdType() {
		return legalIdType;
	}

	public String getMateralName() {
		return materalName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public String getOrganization() {
		return organization;
	}

	public String getPhone() {
		return phone;
	}

	public String getPhotoId() {
		return photoId;
	}

	public String getPhotoIdCountry() {
		return photoIdCountry;
	}

	public String getPhotoIdState() {
		return photoIdState;
	}

	public String getPhotoIdType() {
		return photoIdType;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getState() {
		return state;
	}

	public void setAddress1(String string) {
		address1 = string;
	}

	public void setAddress2(String string) {
		address2 = string;
	}

	public void setAddress3(String string) {
		address3 = string;
	}

	public void setCity(String string) {
		city = string;
	}

	public void setCountry(String string) {
		country = string;
	}

	public void setDescriptorType(String string) {
		descriptorType = string;
	}

	public void setFirstName(String string) {
		firstName = string;
	}

	public void setHitStatus(String string) {
		hitStatus = string;
	}

	public void setLastName(String string) {
		lastName = string;
	}

	public void setLegalId(String string) {
		legalId = string;
	}

	public void setLegalIdType(String string) {
		legalIdType = string;
	}

	public void setMateralName(String string) {
		materalName = string;
	}

	public void setMiddleInitial(String string) {
		middleInitial = string;
	}

	public void setOrganization(String string) {
		organization = string;
	}

	public void setPhone(String string) {
		phone = string;
	}

	public void setPhotoId(String string) {
		photoId = string;
	}

	public void setPhotoIdCountry(String string) {
		photoIdCountry = string;
	}

	public void setPhotoIdState(String string) {
		photoIdState = string;
	}

	public void setPhotoIdType(String string) {
		photoIdType = string;
	}

	public void setPostalCode(String string) {
		postalCode = string;
	}

	public void setState(String string) {
		state = string;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String string) {
		dateOfBirth = string;
	}
}
