package com.moneygram.drs.compliance.domain;

import java.io.Serializable;
import java.util.Date;

public class OFACCustomer implements Serializable {
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String maternalName;
	private Address address;
	private String phoneNumber;
	private String organization;
	private Identification photoId;
	private Identification legalId;
	private Date dateOfBirth;

	public Address getAddress() {
		return address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Identification getLegalId() {
		return legalId;
	}

	public String getMaternalName() {
		return maternalName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public String getOrganization() {
		return organization;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Identification getPhotoId() {
		return photoId;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setDateOfBirth(Date date) {
		dateOfBirth = date;
	}

	public void setFirstName(String string) {
		firstName = string;
	}

	public void setLastName(String string) {
		lastName = string;
	}

	public void setLegalId(Identification identification) {
		legalId = identification;
	}

	public void setMaternalName(String string) {
		maternalName = string;
	}

	public void setMiddleInitial(String string) {
		middleInitial = string;
	}

	public void setOrganization(String string) {
		organization = string;
	}

	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	public void setPhotoId(Identification identification) {
		photoId = identification;
	}
}
