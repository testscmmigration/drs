package com.moneygram.drs.compliance;

import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;
import com.moneygram.drs.profiles.DRSException;

public interface RTSTransceiver {
	public static final String UTF8_ENC = "UTF-8";

	/**
	 * Sends the message to the MoneyGram mainframe, taking care of translations
	 * each way.
	 * 
	 * @param in
	 *            The request message.
	 * @param out
	 *            The response message.
	 * @return The response is also returned.
	 * @exception RTSException
	 *                Thrown if any problems are encountered. May include a
	 *                nested exception.
	 */
	public abstract OFACQueryResponse send(OFACQueryRequest in, OFACQueryResponse out)
			throws DRSException;
}