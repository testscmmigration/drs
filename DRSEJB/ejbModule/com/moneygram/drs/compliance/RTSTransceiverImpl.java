package com.moneygram.drs.compliance;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import javax.naming.Context;
import javax.naming.InitialContext;
import com.moneygram.common.guid.RequestGUID;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.perfmon.PerformanceMonitor;
import com.moneygram.common.perfmon.PerformanceMonitorLoggingKeys;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.compliance.domain.MainframeRequestImpl;
import com.moneygram.drs.compliance.domain.OFACQueryRequest;
import com.moneygram.drs.compliance.domain.OFACQueryResponse;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.ree.lib.Config;
import com.moneygram.rts.moneygram.MoneyGramException;
import com.moneygram.rts.transceiver.cics.ICICSTransceiver;
import com.moneygram.tscomm.TSCOMMInvokerImpl;

public class RTSTransceiverImpl implements RTSTransceiver, PerformanceMonitorLoggingKeys {
	private ICICSTransceiver mgMainframeTransceiver;
	private static RTSTransceiver mock = null;
	private static boolean _STARTUP_MESSAGE = false;
	private static Logger log = LogFactory.getInstance().getLogger(RTSTransceiverImpl.class);
	private int mfTimeout = 30000;

	public static void setMock(RTSTransceiver mock) {
		RTSTransceiverImpl.mock = mock;
	}

	public static RTSTransceiver getInstance() throws Exception {
		if (mock != null) {
			return mock;
		}
		return new RTSTransceiverImpl();
	}

	// Default constructor
	private RTSTransceiverImpl() throws Exception {
		try {
			Context ctx = new InitialContext();
			Object object = ctx.lookup("java:comp/env/rep/ResourceReference");
			Config config = (Config) object;
			String timeoutStr = (String) config.getAttribute("mfTimeout");
			mfTimeout = Integer.parseInt(timeoutStr);
		} catch (Exception e) {
			log.warn("unable to set MF timeout defaulting to 30000", e);
		}
	}

	protected OFACQueryResponse processMoneyGramMessage(OFACQueryRequest in, OFACQueryResponse out,
			MoneyGramTranslator translator) throws DRSException {
		byte[] sendBytes = translator.serialize(in);
		String[] inAndOut = new String[2];
		try {
			inAndOut[0] = StringUtility.getStringOfBytesToLogAsString(MF_SEND_PREFIX, SIZE_KEY,
					DATA_KEY, UTF8_ENC, sendBytes)[StringUtility.FULL_STRING];
			byte[] response = sendToMoneyGram(sendBytes, in);
			String[] responseStrArr = StringUtility.getStringOfBytesToLogAsString(MF_RECV_PREFIX,
					SIZE_KEY, DATA_KEY, UTF8_ENC, response);
			inAndOut[1] = responseStrArr[StringUtility.FULL_STRING];
			checkForErrors(response, in);
			translator.setTSCommUsed(true);
			translator.deserialize(response, out);
			return out;
		} catch (UnsupportedEncodingException uex) {
			throw new DRSException(DRSExceptionCodes.EC910_MONEYGRAM_COMMUNICATION_ERROR,
					"Requested encoding: " + UTF8_ENC
							+ " is unsupported for translation of mainframe message", "", Calendar
							.getInstance().getTime(), "", "", uex);
		}
	}

	/*
	 * Helper method that checks the MoneyGram response for errors. Throws an
	 * RTSException if errors are found.
	 */
	private void checkForErrors(byte[] response, OFACQueryRequest in) throws DRSException {
		if (response == null) {
			throw new DRSException(DRSExceptionCodes.EC701_EMPTY_RESPONSE_RECEIVED,
					"EMPTY_RESPONSE_RECEIVED from Mainframe", "", Calendar.getInstance().getTime(),
					"", "");
		} else if ((response[4] == (byte) 'F')
				|| (response[0] == (byte) 'M' && response[1] == (byte) 'V' && response[8] == (byte) 'F')) {
			StringBuffer responseBuffer = new StringBuffer(new String(response));
			String transactionId = responseBuffer.substring(0, 4);
			int offset = 0;
			if (transactionId.startsWith("MV")) {
				offset = 4; // Everything is shifted 4 spaced because of the
				// Version ID
			}
			String failureLevel = responseBuffer.substring(13 + offset, 14 + offset);
			String fieldID = responseBuffer.substring(14 + offset, 18 + offset);
			String failureReason = responseBuffer.substring(18 + offset, 22 + offset);
			String description = responseBuffer.substring(22 + offset, 182 + offset);
			// Create a MoneyGramException to hold MG-specific error
			// details and wrap it in an RTSException.
			MoneyGramException me = new MoneyGramException(failureLevel, fieldID, failureReason,
					description, transactionId);
			// set an empty stack trace to prevent extra log file output. Stack
			// trace originating here is irrelevant to the MG error.
			me.setStackTrace(new StackTraceElement[0]);
			throw new DRSException(DRSExceptionCodes.EC702_ERROR_RECEIVED_FROM_MONEYGRAM,
					"Error recived from Mainframe " + "Level: " + failureLevel + " ID: " + fieldID
							+ " Reason: " + failureReason + " Desc: " + description, "", Calendar
							.getInstance().getTime(), "", "", me);
		}
	}

	public OFACQueryResponse send(OFACQueryRequest in, OFACQueryResponse out) throws DRSException {
		OFACQueryTranslator xlator;
		xlator = new OFACQueryTranslator();
		return (OFACQueryResponse) processMoneyGramMessage(in, out, xlator);
	}

	/*
	 * Sends the message to the MoneyGram mainframe via the
	 * mgMainframeTransceiver.
	 */
	private byte[] sendToMoneyGram(byte[] message, OFACQueryRequest in) throws DRSException {
		if (message == null || message.length == 0) {
			throw new DRSException(DRSExceptionCodes.EC906_MESSAGE_TRANSLATION_ERROR,
					"OFACRequest message is empty!", "", Calendar.getInstance().getTime(), "", "");
		}
		PerformanceMonitor performanceMonitor = PerformanceMonitor.getInstance();
		RequestGUID reqGuid = in.getRequestGUID();
		try {
			performanceMonitor.addCall(reqGuid, SEND_BYTES_CALL);
			return getMGConnection().sendBytes(message, new MainframeRequestImpl(in));
		} catch (Exception e) {
			throw new DRSException(DRSExceptionCodes.EC902_CANT_SEND_TO_MG_ERROR,
					"Error sending to MoneyGram!", "", Calendar.getInstance().getTime(), "", "", e);
		} finally {
			performanceMonitor.finishCall(reqGuid, SEND_BYTES_CALL);
			releaseMGConnection();
		}
	}

	private ICICSTransceiver getMGConnection() throws Exception {
		if (mgMainframeTransceiver == null) {
			mgMainframeTransceiver = (ICICSTransceiver) TSCOMMInvokerImpl.getInstance();
			// mgMainframeTransceiver = (ICICSTransceiver)new MGWS2Tranceiver();
			if (!_STARTUP_MESSAGE) {
				log.info("Using CICS Webservice (TSCOMM2) connection to MG Mainframe.");
				_STARTUP_MESSAGE = true;
			}
			mgMainframeTransceiver.setTimeout(mfTimeout);
		}
		return mgMainframeTransceiver;
	}

	private void releaseMGConnection() {
		if (mgMainframeTransceiver != null) {
			try {
				mgMainframeTransceiver.close();
			} catch (IOException io) {
				log.error("RTSTransceiver.releaseMGConnection failed", io);
			} finally {
				mgMainframeTransceiver = null;
			}
		}
	}
}
