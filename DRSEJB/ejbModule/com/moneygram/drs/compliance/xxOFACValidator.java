/*
 * Created on Feb 21, 2005
 *
 */
package com.moneygram.drs.compliance;

import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.profiles.DRSException;

/**
 * Validates DSS registrations via OFAC
 * 
 * @author Stu Schrader
 * 
 */
public interface xxOFACValidator {
	public String validateOFAC(CustomerProfile newProfile, CustomerProfile existingProfile,
			boolean create, String language) throws DRSException;
}
