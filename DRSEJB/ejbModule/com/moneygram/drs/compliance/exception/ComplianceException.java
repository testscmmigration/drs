package com.moneygram.drs.compliance.exception;

public class ComplianceException extends Exception {
	private static final long serialVersionUID = 1L;

	public ComplianceException(String message) {
		super(message);
	}

	public ComplianceException(Throwable cause) {
		super(cause);
	}

	public ComplianceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ComplianceException() {
		super();
	}
}
