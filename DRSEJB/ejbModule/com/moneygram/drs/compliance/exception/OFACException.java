/*
 * Created on Nov 10, 2003
 */
package com.moneygram.drs.compliance.exception;

/**
 * @author Erik Pearson
 */
public class OFACException extends ComplianceException {
	private static final long serialVersionUID = 1L;

	public OFACException() {
		super();
	}

	public OFACException(String msg, Throwable t) {
		super(msg, t);
	}

	public OFACException(Throwable t) {
		super(t);
	}

	public OFACException(String arg0) {
		super(arg0);
	}
}
