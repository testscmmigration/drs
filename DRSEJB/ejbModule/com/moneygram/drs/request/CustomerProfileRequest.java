package com.moneygram.drs.request;

public class CustomerProfileRequest extends Request implements CommandRequest, java.io.Serializable {
		private static final long serialVersionUID = 1L;
		private java.lang.String mgCustomerReceiveNumber;
		private boolean superUser;
		
		public CustomerProfileRequest() {
			super();
			// TODO Auto-generated constructor stub
		}

		public java.lang.String getMgCustomerReceiveNumber() {
			return mgCustomerReceiveNumber;
		}

		public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
			this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		}

		public boolean isSuperUser() {
			return superUser;
		}

		public void setSuperUser(boolean superUser) {
			this.superUser = superUser;
		}
		
		
}
