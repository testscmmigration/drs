/**
 * QueryRegistrationByCustomerReceiveNumberRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.request;

public class QueryRegistrationByCustomerReceiveNumberRequest extends Request implements
		java.io.Serializable, CommandRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private java.lang.String mgCustomerReceiveNumber;
	private boolean superUser;
	private boolean displayFullAccountNumber;

	public QueryRegistrationByCustomerReceiveNumberRequest() {
	}

	public QueryRegistrationByCustomerReceiveNumberRequest(java.lang.String language,
			java.lang.String mgCustomerReceiveNumber) {
		super(language);
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	/**
	 * Gets the mgCustomerReceiveNumber value for this
	 * QueryRegistrationByCustomerReceiveNumberRequest.
	 * 
	 * @return mgCustomerReceiveNumber
	 */
	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * Sets the mgCustomerReceiveNumber value for this
	 * QueryRegistrationByCustomerReceiveNumberRequest.
	 * 
	 * @param mgCustomerReceiveNumber
	 */
	public void setMgCustomerReceiveNumber(java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public boolean isDisplayFullAccountNumber() {
		return displayFullAccountNumber;
	}

	public void setDisplayFullAccountNumber(boolean displayFullAccountNumber) {
		this.displayFullAccountNumber = displayFullAccountNumber;
	}

}
