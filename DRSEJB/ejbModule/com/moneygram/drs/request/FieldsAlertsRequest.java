package com.moneygram.drs.request;

import java.io.Serializable;

public class FieldsAlertsRequest extends Request implements Serializable,
		CommandRequest {

	private static final long serialVersionUID = 1L;
	private String receiveCountry;
	private String deliveryOption;
	private	String receiveAgentID;
	private String receiveCurrency;
	private String customerReceiveNumber;
	private boolean externalReceiverProfile;
	

	public FieldsAlertsRequest() {

	}

	public FieldsAlertsRequest(String lanugage, String receiveCountry,
			String deliveryOption, String receiveAgentID,
			String receiveCurrency, String customerReceiveNumber,boolean externalReceiverProfile) {
		super(lanugage);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
		this.customerReceiveNumber = customerReceiveNumber;
		this.externalReceiverProfile = externalReceiverProfile;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getReceiveAgentID() {
		return receiveAgentID;
	}

	public void setReceiveAgentID(String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public boolean isExternalReceiverProfile() {
		return externalReceiverProfile;
	}

	public void setExternalReceiverProfile(boolean externalReceiverProfile) {
		this.externalReceiverProfile = externalReceiverProfile;
	}
	

}
