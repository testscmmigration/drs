package com.moneygram.drs.request;

public class CustomerProfileRequest2 extends Request2 implements CommandRequest,
		java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String mgCustomerReceiveNumber;
	private int customerProfileVersionNumber;
	private boolean displayFullAccountNumber;

	public CustomerProfileRequest2() {
		super();
	}

	public java.lang.String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(
			java.lang.String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public int getCustomerProfileVersionNumber() {
		return customerProfileVersionNumber;
	}

	public void setCustomerProfileVersionNumber(int customerProfileVersionNumber) {
		this.customerProfileVersionNumber = customerProfileVersionNumber;
	}
	public boolean isDisplayFullAccountNumber() {
		return displayFullAccountNumber;
	}

	public void setDisplayFullAccountNumber(boolean displayFullAccountNumber) {
		this.displayFullAccountNumber = displayFullAccountNumber;
	}
}