/**
 * Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.request;

public class Request extends Request2 {
	private static final long serialVersionUID = 1L;
	private java.lang.String language;

	public Request() {
		super();
	}

	public Request(java.lang.String language) {
		super();
		this.language = language;
	}

	/**
	 * Gets the language value for this Request.
	 * 
	 * @return language
	 */
	public java.lang.String getLanguage() {
		return language;
	}

	/**
	 * Sets the language value for this Request.
	 * 
	 * @param language
	 */
	public void setLanguage(java.lang.String language) {
		this.language = language;
	}
}
