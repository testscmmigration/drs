/**
 * DirectedSendRegistrationFieldsRequest.java

 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.request;

public class DirectedSendRegistrationFieldsRequest extends Request implements java.io.Serializable,
		CommandRequest {
	private static final long serialVersionUID = 1L;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;

	public DirectedSendRegistrationFieldsRequest() {
	}

	public DirectedSendRegistrationFieldsRequest(java.lang.String language,
			java.lang.String receiveCountry, java.lang.String deliveryOption,
			java.lang.String receiveAgentID, java.lang.String receiveCurrency) {
		super(language);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * Gets the receiveCountry value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the deliveryOption value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @return deliveryOption
	 */
	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * Sets the deliveryOption value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @param deliveryOption
	 */
	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * Gets the receiveAgentID value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @return receiveAgentID
	 */
	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * Sets the receiveAgentID value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @param receiveAgentID
	 */
	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	/**
	 * Gets the receiveCurrency value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @return receiveCurrency
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * Sets the receiveCurrency value for this
	 * DirectedSendRegistrationFieldsRequest.
	 * 
	 * @param receiveCurrency
	 */
	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
}
