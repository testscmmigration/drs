/**
 * SaveRegistrationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.request;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.moneygram.drs.bo.ACVersionBO;
import com.moneygram.drs.bo.AgentProfileInfo;
import com.moneygram.drs.bo.KeyValuePair;


public class SaveRegistrationRequest extends Request implements java.io.Serializable,
		CommandRequest {
	private static final long serialVersionUID = 1L;
	private boolean superUser;
	private java.lang.String receiveCountry;
	private java.lang.String deliveryOption;
	private java.lang.String receiveAgentID;
	private java.lang.String receiveCurrency;
	private java.lang.String customerReceiveNumber;
	private java.lang.String registrationStatus;
	private Collection<KeyValuePair> fieldValues = new ArrayList<KeyValuePair>();
	private int poeSourceSystem;
	private BigDecimal receiveAmount;
	private AgentProfileInfo sendAgentInfo;
	private boolean intraTransaction;
	private java.lang.String apiVersion;
	// Used by client to indicate the update is compliance related and validation should occur
	// Only compliance related fields are supported during this kind of update
	private List<String> complianceUpdates;
	// Used by client to indicate that registration also supports compliance attributes
	private boolean supportsComplianceFields;
	// Used by client to indicate that pho
	private boolean skipReceiverPhoneCountryCode;
	private boolean supportsReenterFields;
	private boolean supportsMultipleErrors;
	private ACVersionBO acVersionBO;
	private String externalReceiverProfileId;
	private boolean accountNumberIsToken;
	
	//ADP-1535
	private String externalReceiverProfileType;

	public SaveRegistrationRequest() {
	}

	public SaveRegistrationRequest(java.lang.String language, java.lang.String receiveCountry,
			java.lang.String deliveryOption, java.lang.String receiveAgentID,
			java.lang.String receiveCurrency, java.lang.String customerReceiveNumber,
			java.lang.String registrationStatus, Collection<KeyValuePair> fieldValues,
			int poeSourceSystem, BigDecimal receiveAmount, AgentProfileInfo sendAgentInfo,boolean intraTransaction,java.lang.String apiVersion,String externalReceiverProfileId, String externalReceiverProfileType) {
		super(language);
		this.receiveCountry = receiveCountry;
		this.deliveryOption = deliveryOption;
		this.receiveAgentID = receiveAgentID;
		this.receiveCurrency = receiveCurrency;
		this.customerReceiveNumber = customerReceiveNumber;
		this.registrationStatus = registrationStatus;
		this.fieldValues = fieldValues;
		this.poeSourceSystem = poeSourceSystem;
		this.receiveAmount = receiveAmount;
		this.sendAgentInfo = sendAgentInfo;
		this.intraTransaction = intraTransaction;
		this.apiVersion = apiVersion;
		this.externalReceiverProfileId = externalReceiverProfileId;
		this.externalReceiverProfileType = externalReceiverProfileType;
	}

	/**
	 * Gets the receiveCountry value for this SaveRegistrationRequest.
	 * 
	 * @return receiveCountry
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * Sets the receiveCountry value for this SaveRegistrationRequest.
	 * 
	 * @param receiveCountry
	 */
	public void setReceiveCountry(java.lang.String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	/**
	 * Gets the deliveryOption value for this SaveRegistrationRequest.
	 * 
	 * @return deliveryOption
	 */
	public java.lang.String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * Sets the deliveryOption value for this SaveRegistrationRequest.
	 * 
	 * @param deliveryOption
	 */
	public void setDeliveryOption(java.lang.String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * Gets the receiveAgentID value for this SaveRegistrationRequest.
	 * 
	 * @return receiveAgentID
	 */
	public java.lang.String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * Sets the receiveAgentID value for this SaveRegistrationRequest.
	 * 
	 * @param receiveAgentID
	 */
	public void setReceiveAgentID(java.lang.String receiveAgentID) {
		this.receiveAgentID = receiveAgentID;
	}

	/**
	 * Gets the receiveCurrency value for this SaveRegistrationRequest.
	 * 
	 * @return receiveCurrency
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * Sets the receiveCurrency value for this SaveRegistrationRequest.
	 * 
	 * @param receiveCurrency
	 */
	public void setReceiveCurrency(java.lang.String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	/**
	 * Gets the customerReceiveNumber value for this SaveRegistrationRequest.
	 * 
	 * @return customerReceiveNumber
	 */
	public java.lang.String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	/**
	 * Sets the customerReceiveNumber value for this SaveRegistrationRequest.
	 * 
	 * @param customerReceiveNumber
	 */
	public void setCustomerReceiveNumber(java.lang.String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	/**
	 * Gets the registrationStatus value for this SaveRegistrationRequest.
	 * 
	 * @return registrationStatus
	 */
	public java.lang.String getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * Sets the registrationStatus value for this SaveRegistrationRequest.
	 * 
	 * @param registrationStatus
	 */
	public void setRegistrationStatus(java.lang.String registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	/**
	 * Gets the fieldValues value for this SaveRegistrationRequest.
	 * 
	 * @return fieldValues
	 */
	public Collection<KeyValuePair> getFieldValues() {
		return fieldValues;
	}

	/**
	 * Sets the fieldValues value for this SaveRegistrationRequest.
	 * 
	 * @param fieldValues
	 */
	public void setFieldValues(Collection<KeyValuePair> fieldValues) {
		this.fieldValues = fieldValues;
	}

	public void addFieldValue(KeyValuePair object) {
		this.fieldValues.add(object);
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public int getPoeSourceSystem() {
		return poeSourceSystem;
	}

	public void setPoeSourceSystem(int poeSourceSystem) {
		this.poeSourceSystem = poeSourceSystem;
	}

	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public AgentProfileInfo getSendAgentInfo() {
		return sendAgentInfo;
	}

	public void setSendAgentInfo(AgentProfileInfo sendAgentInfo) {
		this.sendAgentInfo = sendAgentInfo;
	}

	public boolean isIntraTransaction() {
		return intraTransaction;
	}

	public void setIntraTransaction(boolean intraTransaction) {
		this.intraTransaction = intraTransaction;
	}

	/**
	 * @return the apiVersion
	 */
	public java.lang.String getApiVersion() {
		return apiVersion;
	}

	/**
	 * @param apiVersion the apiVersion to set
	 */
	public void setApiVersion(java.lang.String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public List<String> getComplianceUpdates() {
		return complianceUpdates;
	}

	public void setComplianceUpdates(List<String> complianceUpdate) {
		this.complianceUpdates = complianceUpdate;
	}

	public boolean isSupportsComplianceFields() {
		return supportsComplianceFields;
	}

	public void setSupportsComplianceFields(boolean supportsComplianceFields) {
		this.supportsComplianceFields = supportsComplianceFields;
	}

	public boolean isSkipReceiverPhoneCountryCode() {
		return skipReceiverPhoneCountryCode;
	}

	public void setSkipReceiverPhoneCountryCode(boolean skipReceiverPhoneCountryCode) {
		this.skipReceiverPhoneCountryCode = skipReceiverPhoneCountryCode;
	}
	
	public boolean hasComplianceUpdates() {
		return getComplianceUpdates() != null && getComplianceUpdates().size() > 0;
	}

	public boolean isCustomerReceiveNumberAvailable() {
		return StringUtils.isNotBlank(customerReceiveNumber);
	}

	public boolean isSupportsReenterFields() {
		return supportsReenterFields;
	}

	public void setSupportsReenterFields(boolean supportsReenterFields) {
		this.supportsReenterFields = supportsReenterFields;
	}

	public boolean isAccountNumberIsToken() {
		return accountNumberIsToken;
	}

	public void setAccountNumberIsToken(boolean accountNumberIsToken) {
		this.accountNumberIsToken = accountNumberIsToken;
	}

	public boolean isSupportsMultipleErrors() {
		return supportsMultipleErrors;
	}

	public void setSupportsMultipleErrors(boolean isSupportsMultipleErrors) {
		this.supportsMultipleErrors = isSupportsMultipleErrors;
	}

	public ACVersionBO getAcVersionBO() {
		return acVersionBO;
	}

	public void setAcVersionBO(ACVersionBO acVersionBO) {
		this.acVersionBO = acVersionBO;
	}

	public String getExternalReceiverProfileId() {
		return externalReceiverProfileId;
	}

	public void setExternalReceiverProfileId(String externalReceiverProfileId) {
		this.externalReceiverProfileId = externalReceiverProfileId;
	}

	/**
	 * @param externalReceiverProfileType the externalReceiverProfileType to set
	 */
	public void setExternalReceiverProfileType(
			String externalReceiverProfileType) {
		this.externalReceiverProfileType = externalReceiverProfileType;
	}

	/**
	 * @return the externalReceiverProfileType
	 */
	public String getExternalReceiverProfileType() {
		return externalReceiverProfileType;
	}
}
