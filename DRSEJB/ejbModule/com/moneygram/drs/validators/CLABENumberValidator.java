/*
 * Created on Apr 13, 2006
 *
 */
package com.moneygram.drs.validators;

/**
 * @author T025
 * 
 */
public class CLABENumberValidator implements CheckDigitValidationInterface {
	public static final String CLABE_VAL = "CLABE";

	private static StringBuffer getDigitsOnly(String s) {
		StringBuffer digitsOnly = new StringBuffer();
		char c;
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (Character.isDigit(c)) {
				digitsOnly.append(c);
			}
		}
		return digitsOnly;
	}

	public boolean checkDigit(String accountNumber) {
		StringBuffer digitsOnly = getDigitsOnly(accountNumber);
		int digit;
		int n = 2;
		int sum = 0, rem = 0;
		int controlDigit = Integer.parseInt(digitsOnly.substring(digitsOnly.length() - 1,
				digitsOnly.length()));
		for (int i = 0; i <= digitsOnly.length() - 2; i++) {
			digit = Integer.parseInt(digitsOnly.substring(i, i + 1));
			sum += (digit * getMultilpier(n)) % 10;
			n += 1;
			if (n == 4)
				n = 1;
		}
		rem = sum % 10;
		int result = 10 - rem;
		if (result == 10)
			result = 0;
		if (result == controlDigit)
			return true;
		return false;
	}

	private int getMultilpier(int n) {
		return (int) Math.pow(2, n) - 1;
	}

	public String getAlgorithmName() {
		return CLABE_VAL;
	}
}
