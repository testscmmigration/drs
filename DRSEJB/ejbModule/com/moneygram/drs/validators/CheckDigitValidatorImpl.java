/*
 * Created on Feb 9, 2005
 *
 */
package com.moneygram.drs.validators;

import java.util.HashMap;

/**
 * @author T007
 * 
 */
public class CheckDigitValidatorImpl {
	private HashMap<String, CheckDigitValidationInterface> algorithms = new HashMap<String, CheckDigitValidationInterface>();
	private static CheckDigitValidatorImpl instance;

	private CheckDigitValidatorImpl() throws Exception {
		try {
			// //Assume that we will be implementing algorithms in the same
			// package as this class.
			// List l =
			// RuntimeClassInformation.find(getClass().getPackage().getName(),
			// CheckDigitValidationInterface.class);
			//			
			// if (l == null) {
			// throw new CheckDigitValidatorException("Find of implementers
			// failed. List returned as null");
			// }
			//			
			// Iterator i = l.iterator();
			// while(i.hasNext()){
			// Class interfaceClass = (Class)i.next();
			// CheckDigitValidationInterface cvi =
			// (CheckDigitValidationInterface)interfaceClass.newInstance();
			// algorithms.put(cvi.getAlgorithmName(), cvi);
			// }
			// TODO: SCR 521- RuntimeClassInformation seems to have trouble with
			// deployed WAS,
			// and is commented out above. Thus we will manually instantiate all
			// known
			// validators for now.
			CheckDigitValidationInterface cvi = new LUHN10Validator();
			algorithms.put(cvi.getAlgorithmName(), cvi);
			CheckDigitValidationInterface clabe = new CLABENumberValidator();
			algorithms.put(clabe.getAlgorithmName(), clabe);
		} catch (Exception ciex) {
			throw new Exception("Find of implementers failed", ciex);
		}
	}

	private CheckDigitValidationInterface getValidator(String algName) {
		return (CheckDigitValidationInterface) algorithms.get(algName);
	}

	private static CheckDigitValidatorImpl getInstance() throws Exception {
		if (instance != null)
			return instance;
		return new CheckDigitValidatorImpl();
	}

	/**
	 * Check the number for validity
	 * 
	 * @param algorithmName
	 *            The algorithm key to use
	 * @param number
	 *            The number to check. If will be stripped of charecters
	 * @return True if it passes, false if it fails
	 * @throws CheckDigitValidatorException
	 *             If the algorithm cannot be found.
	 */
	public static boolean validateNumber(String algorithmName, String number) throws Exception {
		CheckDigitValidationInterface cdvi = getInstance().getValidator(algorithmName);
		if (cdvi != null) {
			return cdvi.checkDigit(number);
		}
		throw new Exception("No validator found for algorithm: " + algorithmName);
	}
}
