/*
 * Created on Feb 9, 2005
 *
 */
package com.moneygram.drs.validators;

/**
 * @author T007
 * 
 */
public interface CheckDigitValidationInterface {
	String getAlgorithmName();

	boolean checkDigit(String accountNumber);
}
