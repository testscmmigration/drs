/*
 * Created on Feb 11, 2005
 *
 */
package com.moneygram.drs.validators;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.DataTypeCodeEnum;
import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.profiles.DRSExceptionWithErrors;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.xmlTagMapper.StandardFieldTags;

/**
 * Coordinates the validation of save registration requests.
 * 
 * @author T007
 * 
 */
public class SaveRegistrationValidator implements StandardFieldTags {
	private static final PatternCompiler compiler = new Perl5Compiler();
	private static final PatternMatcher matcher = new Perl5Matcher();
	Map fieldValues = null;
	private static final Logger log = LogFactory.getInstance().getLogger(
			SaveRegistrationValidator.class);

	/**
	 * 
	 */
	protected SaveRegistrationValidator() {
		super();
	}

	/**
	 * Coordinates the validation of a save registration request.
	 * 
	 * @param request
	 * @param registrationFields
	 * @throws RTSException
	 */
	public static void validateRegistrationRequest(SaveRegistrationRequest request,RegistrationFieldsCollection registrationFields,boolean  saveRegistrationWithNewXmlTags) throws Exception {
		boolean creating = request.getCustomerReceiveNumber() == null;
		// Only creation requires field existence.
		// 5-3-05: Check to make sure first and last names are matched on
		// create.
		Map<String, KeyValuePair> fieldValues = new HashMap<String, KeyValuePair>();
		Collection keyValuePair = request.getFieldValues();
		for (Iterator iter = keyValuePair.iterator(); iter.hasNext();) {
			KeyValuePair element = (KeyValuePair) iter.next();
			fieldValues.put(element.getKey(), element);
		}
		if (creating) {
			validateRequiredFields(request,
					registrationFields.getRequiredFields(), fieldValues,
					saveRegistrationWithNewXmlTags);
			// validateFirstLastNames(request, fieldValues);
		} else {
			validatePersonDeleting(request, registrationFields, fieldValues);
		}
		if(request.hasComplianceUpdates()) {
			validateRequiredFields(request,
					registrationFields.getRequiredFields(), fieldValues,
					saveRegistrationWithNewXmlTags);
		}
		validateReadOnlyFields(request, registrationFields);
		validateConfirmFields(request, registrationFields, fieldValues);
		validateRegExp(request, registrationFields);
		validateCheckDigits(request, registrationFields);
		validateEnumerations(request, registrationFields);
		validateMinMaxAndScale(request, registrationFields);
		// Validating just for country names less than 3 charactes.
		validateCountryNames(request, registrationFields);
	}

	/**
	 * This method verfies the rules regarding deleting customer profile
	 * persons. <BR>
	 * 1) A creator or a receiver cannot be deleted <BR>
	 * 2) In order to delete a person, both last name and first name must be
	 * blank (NOT NULL). All spaces is considered a delete situation
	 * 
	 * @param request
	 * @param registrationFields
	 * @param fieldValues
	 */
	private static void validatePersonDeleting(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields, Map fieldValues) throws Exception {
		Collection c = request.getFieldValues();
		int rfcFNPLen = FIRST_NAME_POSTFIX.length();
		int rfcLNPLen = LAST_NAME_POSTFIX.length();
		for (Iterator iter = c.iterator(); iter.hasNext();) {
			KeyValuePair keyValuePair = (KeyValuePair) iter.next();
			String key = keyValuePair.getKey();
			String value = keyValuePair.getValue();
			// Check if they request is trying to delete (ie blank string)
			if (value == null || !value.trim().equals(""))
				continue;
			if (key.endsWith(FIRST_NAME_POSTFIX)) {
				String prefix = key.substring(0, key.length() - rfcFNPLen);
				KeyValuePair lastName = (KeyValuePair) fieldValues.get(prefix + LAST_NAME_POSTFIX);
				// Make sure last name is empty (for indicating delete)
				if (lastName == null || lastName.getValue() == null
						|| !lastName.getValue().trim().equals("")) {
					// VJ As per 5/17/10 we can have only first name or Last
					// Name
					// throw new DRSException(
					// DRSExceptionCodes.EC338_CANNOT_DELETE_WITHOUT_FIRST_LAST_NAME_EMPTY,
					// "Tag",
					// key,
					// new Date(),
					// "In order to delete a profile person, both last name and
					// first name must be empty: ",
					// CLIENT);
					continue;
				}
				// Check the type of person. If create or receiver, then don't
				// allow delete.
				if (prefix.startsWith(CREATOR_PREFIX_VAL) || prefix.startsWith(RECEIVER_PREFIX_VAL)) {
					throw new DRSException(DRSExceptionCodes.EC339_CANNOT_DELETE_RECEIVER_CREATOR,
							"Tag", key, new Date(),
							"The creator and receiver profiles cannot be deleted: ", CLIENT);
				}
			} else if (key.endsWith(LAST_NAME_POSTFIX)) {
				String prefix = key.substring(0, key.length() - rfcLNPLen);
				// 5-4-05: Don't include 2nd last names.
				if (!prefix.endsWith(SECOND_POSTFIX)) {
					KeyValuePair firstName = (KeyValuePair) fieldValues.get(prefix
							+ FIRST_NAME_POSTFIX);
					// Make sure first name is empty (for indicating delete)
					// VJ As per 5/17/10 we can have only first name or Last
					// Name
					if (firstName == null || firstName.getValue() == null
							|| !firstName.getValue().trim().equals("")) {
						// throw new DRSException(
						// DRSExceptionCodes.EC338_CANNOT_DELETE_WITHOUT_FIRST_LAST_NAME_EMPTY,
						// "Tag",
						// key,
						// new Date(),
						// "In order to delete a profile person, both last name
						// and first name must be empty",
						// CLIENT);
						continue;
					}
					// Check the type of person. If create or receiver, then
					// don't
					// allow delete.
					if (prefix.startsWith(CREATOR_PREFIX_VAL)
							|| prefix.startsWith(RECEIVER_PREFIX_VAL)) {
						throw new DRSException(
								DRSExceptionCodes.EC339_CANNOT_DELETE_RECEIVER_CREATOR, "Tag", key,
								new Date(), "The creator and receiver profiles cannot be deleted",
								CLIENT);
					}
				}
			}
		}
	}

	/**
	 * Check country and state entries for validity vs code tables
	 * 
	 * @param request
	 */
	/*
	 * private static void validateCountryNames( SaveRegistrationRequest
	 * request, RegistrationFieldsCollection registrationFields) throws
	 * Exception { Collection c = request.getFieldValues(); CodeTables ct =
	 * CodeTablesLoader.getInstance();
	 * 
	 * for (Iterator iter = c.iterator(); iter.hasNext();) { KeyValuePair
	 * keyValuePair = (KeyValuePair) iter.next(); String key =
	 * keyValuePair.getKey(); String value = keyValuePair.getValue();
	 * 
	 * RegistrationFieldInfo rfi =
	 * registrationFields.getInfoEntryOnFullList(key);
	 * 
	 * //Don't check on the no field entry, null or empty case if (rfi == null ||
	 * value == null || value.length() == 0) continue;
	 * 
	 * //If a country code then we check. if (rfi.getDataTypeCode() ==
	 * DataTypeCodeEnum.COUNTRY_CODE) {
	 * 
	 * //SCR 328: Change all country codes to upper case. value =
	 * value.toUpperCase(); keyValuePair.setValue(value);
	 * 
	 * if (!ct.containsCountry(value, false)) // throw
	 * Exception.createClientException( // Exception.EC305_INVALID_COUNTRY_CODE, //
	 * key, // "Country supplied: " + value + " is invalid"); } } }
	 */
	/**
	 * Check to see that all first names coorespond with a last name entry
	 * Should be called only on create.
	 * 
	 * @param request
	 * @param registrationFields
	 */
	protected static void validateFirstLastNames(SaveRegistrationRequest request, Map fieldValues)
			throws Exception {
		Collection c = request.getFieldValues();
		int rfcFNPLen = FIRST_NAME_POSTFIX.length();
		int rfcLNPLen = LAST_NAME_POSTFIX.length();
		for (Iterator iter = c.iterator(); iter.hasNext();) {
			KeyValuePair keyValuePair = (KeyValuePair) iter.next();
			String key = keyValuePair.getKey();
			if (key.endsWith(FIRST_NAME_POSTFIX)) {
				String prefix = key.substring(0, key.length() - rfcFNPLen);
				if ((KeyValuePair) fieldValues.get(prefix + LAST_NAME_POSTFIX) == null) {
					throw new DRSException(
							DRSExceptionCodes.EC337_FIRST_NAME_LAST_NAME_NOT_PROVIDED, "Tag", key,
							new Date(), "Cannot have a first name without a last name", CLIENT);
				}
			} else if (key.endsWith(LAST_NAME_POSTFIX)) {
				String prefix = key.substring(0, key.length() - rfcLNPLen);
				// 5-4-05: Don't include 2nd last names.
				if (!prefix.endsWith(SECOND_POSTFIX)) {
					if ((KeyValuePair) fieldValues.get(prefix + FIRST_NAME_POSTFIX) == null) {
						throw new DRSException(
								DRSExceptionCodes.EC337_FIRST_NAME_LAST_NAME_NOT_PROVIDED, "Tag",
								key, new Date(), "Cannot have a last name without a first name ",
								CLIENT);
					}
				}
			}
		}
	}

	/**
	 * Make sure that the user is not setting read only fields.
	 * 
	 * @param request
	 * @param registrationFields
	 */
	private static void validateReadOnlyFields(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator i = request.getFieldValues().iterator();
		while (i.hasNext()) {
			KeyValuePair field = (KeyValuePair) i.next();
			if (registrationFields.isEntryReadOnly(field.getKey())) {
				throw new DRSException(DRSExceptionCodes.EC335_READ_ONLY_FIELD, "Tag", field
						.getKey(), new Date(), "is read only and cannot be set ", CLIENT);
			}
		}
	}

	/**
	 * Validates min, max, and scale setting for returned data.
	 * 
	 * @param request
	 * @param list
	 */
	private static void validateMinMaxAndScale(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator i = request.getFieldValues().iterator();
		while (i.hasNext()) {
			// Modified for 56737 GR project - July-2016
			// To handle multiple input language
			// To avoid overriding
			KeyValuePair field = (KeyValuePair) i.next();
			List<ExtendedRegistrationFieldInfo> rfiList = registrationFields.getInfoEntryOnFullList(field.getKey());
			for(RegistrationFieldInfo rfi : rfiList){
			if (rfi == null) {
				throw new DRSException(DRSExceptionCodes.EC331_UNKNOWN_FIELD, "Unknown field: ",
						field.getKey(), new Date(), "Unknown field: " + field.getKey()
								+ " encountered", CLIENT);
			}
			DataTypeCodeEnum dtce = rfi.getDataTypeCodeEnum();
			if (dtce == null) {
				throw new DRSException(DRSExceptionCodes.EC330_NO_TYPE_FOUND_ON_FIELD, "", field
						.getKey(), new Date(), " No field type found on", CLIENT);
			}
			String inputDataValue = field.getValue();
			// Only check if actually a length/value verifiable entry
			if (dtce.isMinMaxCheckable() && inputDataValue != null) {
				if (dtce == DataTypeCodeEnum.INT) {
					int readValue = Integer.parseInt(inputDataValue);
					if (readValue < rfi.getFieldMin().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC324_MIN_VALUE_CHECK_FAILURE,
								" ", field.getKey(), new Date(), "Value: " + inputDataValue
										+ " is less than allowed minimum: "
										+ ((int) rfi.getFieldMin().intValue()), CLIENT);
					}
					if (readValue > rfi.getFieldMax().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC325_MAX_VALUE_CHECK_FAILURE, "",
								field.getKey(), new Date(), "Value: " + inputDataValue
										+ " is greater than allowed maximum: "
										+ ((int) rfi.getFieldMax().intValue()), CLIENT);
					}
				}
				if (dtce == DataTypeCodeEnum.DECIMAL) {
					double readValue = Double.parseDouble(inputDataValue);
					if (readValue < rfi.getFieldMin().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC324_MIN_VALUE_CHECK_FAILURE, "",
								field.getKey(), new Date(),
								"Value: " + inputDataValue + " is less than allowed minimum: "
										+ rfi.getDefaultValue(), CLIENT);
					}
					if (readValue > rfi.getFieldMax().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC325_MAX_VALUE_CHECK_FAILURE, "",
								field.getKey(), new Date(), "Value: " + inputDataValue
										+ " is greater than allowed maximum: "
										+ ((int) rfi.getFieldMax().intValue()), CLIENT);
					}
					// Check precisions on decimal
					int decimalPoint = inputDataValue.lastIndexOf('.');
					if (decimalPoint != -1) {
						int numDecPlaces = inputDataValue.substring(decimalPoint + 1).length();
						if (numDecPlaces > rfi.getFieldScale().intValue()) {
							throw new DRSException(DRSExceptionCodes.EC326_MAX_SCALE_CHECK_FAILURE,
									"Value", field.getKey(), new Date(), "Value: " + inputDataValue
											+ " has more decimal places than allowed: ", CLIENT);
						}
					}
				}
				if (dtce == DataTypeCodeEnum.STRING || dtce == DataTypeCodeEnum.TEXT) {
					if (inputDataValue.trim().length() < rfi.getFieldMin().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC322_INVALID_FIELD_LENGTH_MIN,
								"", field.getKey(), new Date(), "Value: " + inputDataValue
										+ " is shorter allowed minimum: "
										+ ((int) rfi.getFieldMin().intValue()), CLIENT);
					}
					if (inputDataValue.length() > rfi.getFieldMax().intValue()) {
						throw new DRSException(DRSExceptionCodes.EC323_INVALID_FIELD_LENGTH_MAX,
								"", field.getKey(), new Date(), "Value: " + inputDataValue
										+ " is longer allowed maximum: "
										+ ((int) rfi.getFieldMax().intValue()), CLIENT);
					}
				}
			}
		}
		}
	}

	/**
	 * Validate any entries that are part of an enumeration
	 * 
	 * @param request
	 * @param list
	 */
	private static void validateEnumerations(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator fieldIterator = request.getFieldValues().iterator();
		while (fieldIterator.hasNext()) {
			KeyValuePair field = (KeyValuePair) fieldIterator.next();
			Collection allowedEnumValues = registrationFields
					.getEnueratedValuesList(field.getKey());
			// If this field is not enumerated, let pass..
			if (allowedEnumValues == null) {
				continue;
			}
			// Make sure the value is in the collection
			Iterator enumIterator = allowedEnumValues.iterator();
			boolean found = false;
			while (enumIterator.hasNext()) {
				EnumeratedValueInfo evi = (EnumeratedValueInfo) enumIterator.next();
				if (evi.getValue().equals(field.getValue())) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new DRSException(DRSExceptionCodes.EC332_INVALID_ENUM_VALUE, "Value", field
						.getKey(), new Date(), "Unknown enumeration value: " + field.getValue()
						+ " encountered", CLIENT);
			}
		}
	}

	/**
	 * Validates any entries with check digit algorithms.
	 * 
	 * @param request
	 * @param registrationFields
	 */
	private static void validateCheckDigits(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator i = request.getFieldValues().iterator();
		while (i.hasNext()) {
			KeyValuePair field = (KeyValuePair) i.next();
			String chkDigitAlgStr = registrationFields.getCheckDigitAlg(field.getKey());
			if (chkDigitAlgStr == null)
				continue;
			boolean cdOK = CheckDigitValidatorImpl.validateNumber(chkDigitAlgStr, field.getValue());
			if (cdOK == false) {
				throw new DRSException(DRSExceptionCodes.EC340_CHECK_DIGIT_FAILED, "Value", field
						.getKey(), new Date(), field.getValue()
						+ " fails checkdigit with algorithm " + chkDigitAlgStr, CLIENT);
			}
		}
	}

	/**
	 * Validate that any field having a cooresponding regular expression.
	 * 
	 * @param request
	 * @param list
	 */
	private static void validateRegExp(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator i = request.getFieldValues().iterator();
		while (i.hasNext()) {
			KeyValuePair field = (KeyValuePair) i.next();
			String regExp = registrationFields.getRegExpString(field.getKey());
			// If there is a regular expression for this field, check it,
			// otherwise pass.
			if (regExp != null) {
				Pattern p = null;
				try {
					p = compiler.compile(regExp);
					if (!matcher.matches(field.getValue(), p)) {
						throw new DRSException(DRSExceptionCodes.EC328_REGEX_MATCH_FAILURE,
								"Value", field.getKey(), new Date(),
								"Regular expression check fails ", CLIENT);
					}
				} catch (MalformedPatternException mpex) {
					throw new DRSException(DRSExceptionCodes.EC905_VALIDATION_PROCESSING_ERROR,
							"Value", field.getKey(), new Date(),
							"Failed to process regular expression string", CLIENT);
				}
			}
		}
	}

	/**
	 * Validate the matching input of fields with confirmation.
	 * 
	 * @param request
	 * @param fieldValuesMap
	 * @param hiddenList
	 * @throws RTSException
	 */
	private static void validateConfirmFields(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields, Map fieldValuesMap) throws Exception {
		Collection fieldValues = request.getFieldValues();
		Iterator fieldIter = fieldValues.iterator();
		while (fieldIter.hasNext()) {
			KeyValuePair hiddenKVP = (KeyValuePair) fieldIter.next();
			// Check to see if this field is supposed to have a confirm entry
			if (registrationFields.isConfirmEntry(hiddenKVP.getKey())) {
				// Elminate any _HIDDEN fields from contention since they will
				// be
				// paired up later.
				if (!hiddenKVP.getKey().endsWith(CONFIRM_FIELD_POSTFIX)) {
					String nonHiddenKey = hiddenKVP.getKey();
					String hiddenKey = nonHiddenKey + CONFIRM_FIELD_POSTFIX;
					// Get the hidden field from the request.
					KeyValuePair confirmKVP = (KeyValuePair) fieldValuesMap.get(hiddenKey);
					// Check to see that the values jive.
					if (confirmKVP == null || !hiddenKVP.getValue().equals(confirmKVP.getValue())) {
						if(request.isSupportsMultipleErrors()){
							throw new DRSExceptionWithErrors(DRSExceptionCodes.EC9657_VERIFICATION_FIELD_MATCHING_FAILED,
									"Value", ((confirmKVP == null) ? "unknown" : confirmKVP.getKey()), new Date(), "Hidden field mismatch",
									CLIENT,hiddenKVP.getKey());
						}
						throw new DRSException(DRSExceptionCodes.EC905_VALIDATION_PROCESSING_ERROR,
								"Value", hiddenKVP.getKey(), new Date(), "Hidden field mismatch",
								CLIENT);
					}
				} else {
					// 4-28-05: There has been a request to validate if only the
					// hidden field changes. Therefore we will handle
					// that case here.
					// See if there is a cooresponding normal entry
					String hiddenKey = hiddenKVP.getKey();
					String nonHiddenKey = hiddenKey.substring(0, hiddenKey.length()
							- CONFIRM_FIELD_POSTFIX.length());
					// Get the non-hidden field from the request
					KeyValuePair confirmKVP = (KeyValuePair) fieldValuesMap.get(nonHiddenKey);
					// Check to see if the values jive. In this case, since we
					// are dealing with the "HIDDEN" entry, there had better
					// be a non-HIDDEN entry...
					if (confirmKVP == null || !hiddenKVP.getValue().equals(confirmKVP.getValue())) {
						if(request.isSupportsMultipleErrors()){
							throw new DRSExceptionWithErrors(DRSExceptionCodes.EC9657_VERIFICATION_FIELD_MATCHING_FAILED,
									"Value", (confirmKVP == null) ? "unknown" : confirmKVP.getKey(), new Date(), "Hidden field mismatch",
									CLIENT,hiddenKVP.getKey());
						}
						throw new DRSException(DRSExceptionCodes.EC905_VALIDATION_PROCESSING_ERROR,
								"Value", hiddenKVP.getKey(), new Date(), "Hidden field mismatch",
								CLIENT);
					}
				}
			}
		}
	}

	/**
	 * Validate that required fields are all present in request.
	 * 
	 * @param request
	 * @param fieldValues
	 * @throws RTSException
	 */
	private static void validateRequiredFields(SaveRegistrationRequest request,
			List requiredFieldList, Map fieldValues, boolean saveRegistrationWithNewXmlTags) throws Exception {
		Iterator i = requiredFieldList.iterator();
		while (i.hasNext()) {
			RegistrationFieldInfo stdField = (RegistrationFieldInfo) i.next();
			// Check to make sure that a maxLength jives with required in
			// strings
			if (stdField.getDataTypeCodeEnum() == DataTypeCodeEnum.STRING
					&& stdField.getFieldMax().intValue() == 0) {
				log.warn("Improperly configured registration field (" + stdField.getXmlTag()
						+ ").  It is a string and required but maxLength == 0.");
				return;
			}
			String tagName = null;
			if (saveRegistrationWithNewXmlTags) {
				tagName = stdField.getNewXmlTag();
			}else {
				tagName = stdField.getXmlTag();
			}
			KeyValuePair fieldValue = (KeyValuePair) fieldValues.get(tagName);
			if (fieldValue == null || fieldValue.getValue() == null
					|| fieldValue.getValue().trim().length() == 0) {// Added for
				throw new DRSException(DRSExceptionCodes.EC329_FIELD_REQUIRED, "Standard field: ",
						tagName, new Date(), "Standard field required", CLIENT);
			}
		}
	}

	private static void validateCountryNames(SaveRegistrationRequest request,
			RegistrationFieldsCollection registrationFields) throws Exception {
		Iterator i = request.getFieldValues().iterator();
		while (i.hasNext()) {
			KeyValuePair field = (KeyValuePair) i.next();
			//// Modified  for 56737 GR project - July-2016
			for(ExtendedRegistrationFieldInfo infoEntryOnFullList: registrationFields.getInfoEntryOnFullList(field.getKey())){
			if (infoEntryOnFullList.getDataTypeCodeEnum()
					.getName() == DataTypeCodeEnum.COUNTRY_CODE.getName()) {
				if (!(field.getValue().length() <= 3))
					throw new DRSException(DRSExceptionCodes.EC305_INVALID_COUNTRY_CODE, "Tag",
							field.getKey(), new Date(), "Country supplied: " + field.getValue()
									+ "is invalid", CLIENT);
			}
		}
		}
	}
}
