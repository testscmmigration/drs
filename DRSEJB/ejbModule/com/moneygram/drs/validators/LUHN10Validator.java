/*
 * Created on Feb 9, 2005
 *
 */
package com.moneygram.drs.validators;

/**
 * @author T007
 */
public class LUHN10Validator implements CheckDigitValidationInterface {
	public static final String LHUN10_VAL = "LUHNmod10";

	/**
	 * Remove non digit charecters.
	 * 
	 * @param s
	 * @return
	 */
	private static StringBuffer getDigitsOnly(String s) {
		StringBuffer digitsOnly = new StringBuffer();
		char c;
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (Character.isDigit(c)) {
				digitsOnly.append(c);
			}
		}
		return digitsOnly;
	}

	/**
	 * Perform a LUHN10 check.
	 * 
	 * @param accountNumber
	 * @return
	 * @see com.moneygram.rts.validation.checkdigit.CheckDigitValidationInterface#checkDigit()
	 */
	public boolean checkDigit(String accountNumber) {
		StringBuffer digitsOnly = getDigitsOnly(accountNumber);
		int sum = 0;
		int digit = 0;
		int addend = 0;
		boolean timesTwo = false;
		for (int i = digitsOnly.length() - 1; i >= 0; i--) {
			digit = Integer.parseInt(digitsOnly.substring(i, i + 1));
			if (timesTwo) {
				addend = digit << 1;
				if (addend > 9) {
					addend -= 9;
				}
			} else {
				addend = digit;
			}
			sum += addend;
			timesTwo = !timesTwo;
		}
		int modulus = sum % 10;
		return modulus == 0;
	}

	/**
	 * @see com.moneygram.rts.validation.checkdigit.CheckDigitValidationInterface#getAlgorithmName()
	 */
	public String getAlgorithmName() {
		return LHUN10_VAL;
	}
}
