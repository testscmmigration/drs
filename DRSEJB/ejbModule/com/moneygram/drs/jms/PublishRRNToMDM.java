package com.moneygram.drs.jms;

import java.io.IOException;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

@EnableAsync
@Component
public class PublishRRNToMDM {
	
	private static Logger log = LogFactory.getInstance().getLogger(
			PublishRRNToMDM.class);
	
	@Autowired
	@Qualifier("esbJMSQueueTemplate")
	private JmsTemplate esbJMSQueueTemplate;	
	
	@Autowired
	@Qualifier("esbDestination")
	private Destination esbDestination;
	
	@SuppressWarnings("unchecked")
	
	@Async
	public void publishRRN(String customerReceiveNumber,String ucpId) throws IOException, NamingException, JMSException{		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("receiverUcpId", ucpId);
		jsonObject.put("rrn", customerReceiveNumber);
		final String jsonString = jsonObject.toJSONString();
		log.info("WRITING MESSAGE TO THE TOPIC : "+ esbDestination);
		log.info("MESSAGE : "+ jsonString);
		esbJMSQueueTemplate.send(esbDestination, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(jsonString);
			}
		});
			
			//TODO: Handle Tibco timeouts and failures
			//TODO: Handle event delivery in DB and publish the failed events with scheduler?
			//TODO: Maintain UCP implementation
	}
}
