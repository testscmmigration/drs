/*
 * Created on Feb 24, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.moneygram.drs.util;

import java.util.Comparator;

/**
 * @author T958
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IntegerCompare<T> implements Comparator<T> {
	public int compare(T obj1, T obj2) {
		int i1 = ((Integer) obj1).intValue();
		int i2 = ((Integer) obj2).intValue();
		return Math.abs(i1) - Math.abs(i2);
	}
}
