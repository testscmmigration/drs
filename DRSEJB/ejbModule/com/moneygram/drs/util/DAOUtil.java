package com.moneygram.drs.util;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DAOUtil {
	private static long oneDay = 86400000;

	public static void setParameter(PreparedStatement stmt, String value, int paramIndex)
			throws SQLException {
		if (value != null) {
			stmt.setString(paramIndex, value);
		} else {
			stmt.setNull(paramIndex, Types.VARCHAR);
		}
	}

	public static void setParameter(PreparedStatement stmt, long value, int paramIndex)
			throws SQLException {
		stmt.setLong(paramIndex, value);
	}

	public static void setParameter(PreparedStatement stmt, int value, int paramIndex)
			throws SQLException {
		stmt.setInt(paramIndex, value);
	}

	public static void setParameter(PreparedStatement stmt, float value, int paramIndex)
			throws SQLException {
		stmt.setFloat(paramIndex, value);
	}

	public static void setDate(PreparedStatement stmt, int paramIndex, long dateTimeMillis)
			throws SQLException {
		stmt.setDate(paramIndex, new Date(dateTimeMillis));
	}

	public static void setDistantFutureDate(PreparedStatement stmt, int paramIndex)
			throws SQLException {
		setDate(stmt, paramIndex, Long.MAX_VALUE);
	}

	public static void setCurrentDate(PreparedStatement stmt, int paramIndex) throws SQLException {
		setDate(stmt, paramIndex, System.currentTimeMillis());
	}

	public static void setString(CallableStatement stmt, int position, String obj,
			boolean convertEmpty) throws SQLException {
		if (obj == null) {
			stmt.setNull(position, OracleTypes.VARCHAR);
		} else {
			if (convertEmpty) {
				if (obj.equalsIgnoreCase("")) {
					obj = "NULL";
				}
			}
			stmt.setString(position, (String) obj);
		}
	}

	public static void setDateRange(PreparedStatement stmt, int effParamIndex, int thruParamIndex,
			boolean expired) throws SQLException {
		long effectiveDate = System.currentTimeMillis() - (oneDay * 60);
		DAOUtil.setDate(stmt, effParamIndex, effectiveDate);
		DAOUtil.setDate(stmt, thruParamIndex, expired ? effectiveDate + (oneDay * 30)
				: effectiveDate + (oneDay * 90));
	}
	
	public static void setBoolean(CallableStatement stmt, int paramIndex, boolean value) throws SQLException {
		stmt.setBoolean(paramIndex, value);
	}
}
