package com.moneygram.drs.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;


public class JAXBUtil {
	
	private static JAXBContextFactory jaxbContextFactory = JAXBContextFactory.getInstance();
	
	public static <T> String convertToXml(T target, Class<T> targetClass) throws Exception{		
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext = jaxbContextFactory.createJAXBContext(targetClass);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		marshaller.marshal(target, sw);           
		return sw.toString();
	}
	
	public static <T> T convertFromXml(String targetString, Class<T> targetClass) throws Exception{
		JAXBElement<T> target = null;
		XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(targetString));
		JAXBContext jaxbContext = jaxbContextFactory.createJAXBContext(targetClass);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		target = unmarshaller.unmarshal(xmlStreamReader,targetClass);
		if (target != null) {
			return target.getValue();
		} else {
			return null;
		}		
	}
}
