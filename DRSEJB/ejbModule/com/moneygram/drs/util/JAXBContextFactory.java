package com.moneygram.drs.util;

import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
 
public class JAXBContextFactory {	
    private static JAXBContextFactory instance = new JAXBContextFactory();
    @SuppressWarnings("rawtypes")
	private ConcurrentHashMap<Class, JAXBContext> jaxbContextMap = new ConcurrentHashMap<Class, JAXBContext>();
 
    private JAXBContextFactory() {
    } 
        /**
     * Returns an existing JAXBContext if one for the particular class exists,
     * else it creates an instance adds it to a internal map.
     * @param contextPath the context path
     * @throws JAXBException exception in creating context
     * @return a created JAXBContext
     */
    public <T> JAXBContext createJAXBContext(final Class<T> clazz) throws JAXBException {        
    	JAXBContext jaxbContext = jaxbContextMap.get(clazz);
    	 
        if (jaxbContext == null) {        	
        	JAXBContext newJaxbContext = JAXBContext.newInstance(clazz);
        	jaxbContext = jaxbContextMap.putIfAbsent(clazz, newJaxbContext);
        	if (jaxbContext == null) {
        		jaxbContext = newJaxbContext;
			}
        }
        return jaxbContext;
    }
 
    /**
     * Get instnace.
     * @return Instance of this factory
     */
    public static JAXBContextFactory getInstance() {
        return instance;
    }
}