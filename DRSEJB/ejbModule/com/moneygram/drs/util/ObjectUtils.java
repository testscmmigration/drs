package com.moneygram.drs.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;

public class ObjectUtils {
	private static Logger log = LogFactory.getInstance().getLogger(ObjectUtils.class);

	private ObjectUtils() {
		super();
	}

	public static void copyProperties(Object destination, Object source) {
		try {
			PropertyUtils.copyProperties(destination, source);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public static void setDefaultSpaceOnAllFields(Object object) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		if (object == null) {
			return;
		}
		Set setterMethods = getSetterMethods(object);
		for (Iterator iter = setterMethods.iterator(); iter.hasNext();) {
			Method method = (Method) iter.next();
			if (method.getParameterTypes().length == 1
					&& method.getParameterTypes()[0] == String.class) {
				Object args[] = { " " };
				method.invoke(object, args);
			}
		}
	}

	private static Set getSetterMethods(Object object) {
		Set<Method> setterMethods = new HashSet<Method>();
		Method[] allMethods = object.getClass().getMethods();
		for (int i = 0; i < allMethods.length; i++) {
			Method method = allMethods[i];
			if (method.getName().startsWith("set")) {
				setterMethods.add(method);
			}
		}
		return setterMethods;
	}

	public static void trimAllStrings(Object object) {
		Class objectLocal = null;
		Field[] fields = null;
		objectLocal = object.getClass();
		// Loop thru all the superclasses to get all the inherited fields to
		// trim
		while (objectLocal != null) {
			fields = objectLocal.getDeclaredFields();
			trimFields(object, fields);
			objectLocal = objectLocal.getSuperclass();
		}
	}

	private static void trimFields(Object object, Field[] fields) {
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			field.setAccessible(true);
			try {
				Object value = field.get(object);
				if (value instanceof String) {
					String strValue = (String) value;
					if (value != null) {
						field.set(object, strValue.trim());
					}
				}
			} catch (IllegalArgumentException e) {
				// If we cant Trim then we cant trim no big deal???
				log.warn("Unable to Trim Field " + field + ",Moving along", e);
			} catch (IllegalAccessException e) {
				// If we cant Trim then we cant trim no big deal???
				log.warn("Unable to Trim Field " + field + ",Moving along", e);
			}
		}
	}

	public static Object getPrivateField(Object object, String fieldName) throws Exception {
		Field field = object.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		return field.get(object);
	}

	public static void setPrivateField(Object object, String fieldName, Object value)
			throws Exception {
		Field field = null;
		if (object instanceof Class) {
			field = ((Class) object).getDeclaredField(fieldName);
		} else {
			field = object.getClass().getDeclaredField(fieldName);
		}
		field.setAccessible(true);
		field.set(object, value);
	}

	public static String trimZeroBeforeInt(String value) {
		int i = 0;
		if (!StringUtility.isNullOrEmpty(value)) {
			while (i < value.length() && value.charAt(i) == '0')
				i++;
			value = value.substring(i);
		}
		return value;
	}

	public static String getMaskedString(String string) {
		StringBuilder maskString = new StringBuilder(string == null ? "" : string.trim());
		int begin = maskString.length() > 11 ? 6 :
			maskString.length() > 8 ? 4 : maskString.length() > 4 ? 2 : 0; 
		int end = maskString.length() > 11 ? maskString.length()-4 :
			maskString.length() > 8 ? maskString.length()-3 :
				maskString.length() > 4 ? maskString.length()-2 :
					maskString.length() - 1;
		for(int i=begin; i < end; maskString.replace(i, ++i, "*"));
		//for(int i = 6; i < maskString.length()-4; maskString.replace(i, ++i, "*"));
		return maskString.toString();
	}

	/**
	 * Returns if requested version is greater than or equal
	 * @param request
	 * @param version
	 * @return
	 */
	public static boolean isRequestGreaterThanOrEqualToVersion(String apiVersion,int version) {
		int apiVersionInt = Integer.parseInt(apiVersion.trim());
		return apiVersionInt >= version;
	}

	public static <V> void addEntryToList(
			Map<String, List<V>> map, String key,
			V value) {
		List<V> list = map.get(key);
		if(list == null) {
			list = new ArrayList<V>();
			map.put(key, list);
		}
		list.add(value);
	}
	
	public static <V> List<V> flatten(
			Collection<List<V>> collectionOfLists) {
		List<V> result = new ArrayList<V>();
		for (List<V> list : collectionOfLists) {
			result.addAll(list);
		}
		return result;
	}
	
	public static <V> Set<V> flattenToSet(
			Collection<List<V>> collectionOfLists) {
		Set<V> result = new HashSet<V>();
		for (List<V> list : collectionOfLists) {
			result.addAll(list);
		}
		return result;
	}
}