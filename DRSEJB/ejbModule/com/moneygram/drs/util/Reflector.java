package com.moneygram.drs.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * This class prints the state of a javabean to a String, or a specified
 * StringBuffer. This class is intended primarily for use in logging and
 * debugging.
 * 
 * 
 * @see #printBean
 * @see #toString
 */
public class Reflector {
	private static Logger LOGGER = LogFactory.getInstance().getLogger(Reflector.class);
	private static final String indent = "    ";
	private static final String lineSep = System.getProperty("line.separator");
	private static HashMap<Class, Reflector> instanceMap = new HashMap<Class, Reflector>();

	interface TypeHandler {
		String toString(Object object);
	}
	private static HashMap<Class, TypeHandler> typeHandlerMap = new HashMap<Class, TypeHandler>();
	static {
		typeHandlerMap.put(Calendar.class, new TypeHandler() {
			public String toString(Object object) {
				Calendar c = (Calendar) object;
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a z");
				format.setCalendar(c);
				return format.format(c.getTime());
			}
		});
	}

	public synchronized static Reflector getInstance(Class clazz) {
		return getInstance(clazz, null, null);
	}

	public synchronized static Reflector getInstance(Class clazz, String[] maskProps) {
		return getInstance(clazz, maskProps, null);
	}

	public synchronized static Reflector getInstance(Class clazz, String[] maskProps,
			String[] hideProps) {
		Reflector reflector = (Reflector) instanceMap.get(clazz);
		if (null == reflector) {
			reflector = new Reflector(clazz, maskProps, hideProps);
			instanceMap.put(clazz, reflector);
		}
		return reflector;
	}
	private Class clazz;
	private PropertyDescriptor[] propertyDescriptors;
	private Set<String> maskPropSet;
	private Set<String> hidePropSet;

	// private Reflector(Class clazz) {
	// this(clazz, null, null);
	// }
	//
	// private Reflector(Class clazz, String[] maskProps) {
	// this(clazz, maskProps, null);
	// }
	private Reflector(Class clazz, String[] maskProps, String[] hideProps) {
		this.clazz = clazz;
		hidePropSet = new HashSet<String>();
		hidePropSet.add("class");
		if (null != hideProps) {
			hidePropSet.addAll(Arrays.asList(hideProps));
		}
		maskPropSet = new HashSet<String>();
		if (null != maskProps) {
			maskPropSet.addAll(Arrays.asList(maskProps));
		}
		try {
			BeanInfo bi = Introspector.getBeanInfo(clazz);
			propertyDescriptors = bi.getPropertyDescriptors();
			Arrays.sort(propertyDescriptors, new Comparator<PropertyDescriptor>() {
				public int compare(PropertyDescriptor o1, PropertyDescriptor o2) {
					PropertyDescriptor p1 = (PropertyDescriptor) o1;
					PropertyDescriptor p2 = (PropertyDescriptor) o2;
					return p1.getName().compareTo(p2.getName());
				}
			});
		} catch (IntrospectionException e) {
			LOGGER.error(getClass().getName() + ": unable to reflect class", e);
			throw new RuntimeException("unable to reflect class: " + clazz);
		}
	}

	public String printBean(Object bean) {
		StringBuffer sb = new StringBuffer();
		try {
			printBean(bean, sb, "");
			// return indentNewLines(sb.toString(), "");
		} catch (Exception e) {
			sb.append("(error...)");
		}
		return sb.toString();
	}

	/**
	 * Prints the bean to the stringbuffer, adding the prefix before the start
	 * of each line
	 * 
	 * @param bean
	 *            the bean whose state to print
	 * @param buffer
	 *            the stringbuffer to print the bean state to
	 * @param prefix
	 *            the prefix for each line
	 */
	private void printBean(Object bean, StringBuffer buffer, String prefix) {
		buffer.append("(");
		buffer.append(bean.getClass().getName());
		buffer.append(")");
		buffer.append(lineSep);
		String newprefix = prefix + indent;
		try {
			for (int i = 0; i < propertyDescriptors.length; i++) {
				PropertyDescriptor propertyDescriptor = propertyDescriptors[i];
				String propName = propertyDescriptor.getName();
				if (hidePropSet.contains(propName)) {
					continue;
				}
				Class propClazz = propertyDescriptor.getPropertyType();
				Method read = propertyDescriptor.getReadMethod();
				if (read == null) {
					continue;
				}
				Object prop = read.invoke(bean, (Object[]) null);
				buffer.append(newprefix);
				buffer.append(propName);
				buffer.append(": ");
				if (maskPropSet.contains(propName)) {
					buffer.append("(masked)\n");
				} else if (null == prop) {
					buffer.append("(null)\n");
				} else if (null == propClazz || propClazz.isArray()) {
					printArray(prop, propClazz, buffer, newprefix);
				} else if (Map.class.isAssignableFrom(propClazz)) {
					printMap((Map) prop, buffer, newprefix);
				} else if (Collection.class.isAssignableFrom(propClazz)) {
					printCollection((Collection) prop, buffer, newprefix);
				} else {
					buffer.append(indentNewLines(toString(prop), newprefix));
				}
			}
		} catch (RuntimeException e) {
			LOGGER.error(getClass().getName() + ": unable to reflect class", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error(getClass().getName() + ": unable to reflect class", e);
			throw new RuntimeException("unable to reflect class: " + clazz);
		}
	}

	private String toString(Object prop) {
		Class clazz = prop.getClass();
		TypeHandler handler = (TypeHandler) typeHandlerMap.get(clazz);
		while (null == handler && null != clazz) {
			clazz = clazz.getSuperclass();
			handler = (TypeHandler) typeHandlerMap.get(clazz);
		}
		if (null == handler) {
			return prop.toString();
		} else {
			return handler.toString(prop);
		}
	}

	private void printMap(Map map, StringBuffer buffer, String prefix) {
		buffer.append("(");
		buffer.append(map.getClass().getName());
		buffer.append(")");
		buffer.append(lineSep);
		String newprefix = prefix + indent;
		Iterator mi = map.entrySet().iterator();
		while (mi.hasNext()) {
			Map.Entry me = (Map.Entry) mi.next();
			buffer.append(newprefix);
			buffer.append(me.getKey());
			buffer.append(": ");
			Object val = me.getValue();
			String vstr = (null == val) ? "(null)" : val.toString();
			buffer.append(indentNewLines(vstr, newprefix));
		}
	}

	private void printArray(Object array, Class clazz, StringBuffer buffer, String prefix) {
		buffer.append("(array");
		if (null != clazz) {
			buffer.append(" of ");
			buffer.append(clazz.getName());
		}
		buffer.append("):");
		buffer.append(lineSep);
		int length = Array.getLength(array);
		String newprefix = prefix + indent;
		for (int i = 0; i < length; i++) {
			buffer.append(newprefix);
			buffer.append("#");
			buffer.append(i);
			buffer.append(": ");
			buffer.append(indentNewLines(Array.get(array, i).toString(), newprefix));
		}
	}

	private void printCollection(Collection coll, StringBuffer buffer, String prefix) {
		buffer.append("(");
		buffer.append(coll.getClass().getName());
		buffer.append(")");
		buffer.append(lineSep);
		String newprefix = prefix + indent;
		int counter = 0;
		for (Iterator ci = coll.iterator(); ci.hasNext(); counter++) {
			Object co = (Object) ci.next();
			buffer.append(newprefix);
			buffer.append("#");
			buffer.append(counter);
			buffer.append(": ");
			if (null == co) {
				buffer.append(" (null)\n");
			} else {
				buffer.append(indentNewLines(co.toString(), newprefix));
			}
		}
	}

	private String indentNewLines(String string, String prefix) {
		StringBuffer sb = new StringBuffer(string);
		boolean newline = false;
		// boolean found = false;
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == '\n' || sb.charAt(i) == '\r') {
				newline = true;
				// found = true;
			} else if (newline) {
				sb.insert(i, prefix);
				i += prefix.length() - 1;
				newline = false;
			}
		}
		// if (found) { sb.insert(0, lineSep + prefix); }
		if (!newline) {
			sb.append(lineSep);
		}
		return sb.toString();
	}
}
