/**
 * 
 */
package com.moneygram.drs.constants;

/**
 * Hard coded values to return in RegsitrationFieldsAlerts service consumed by ESB
 * @author Prasad
 *
 */
public interface DRSConstants {
	
	public static final String ERROR_CODE = "100";
	public static final String ERROR_SOURCE = "DRS";
	public static final String ERROR_CATEGORY_CODE = "UserError";
	public static final boolean PERSISTENCE_FLAG = true;
	public static final String ALERT_ACTION_CODE = "COLLECTDATA";
	public static final String ALERT_CATEGORY_CODE = "RCVG_PTNR";
	public static final String ALERT_SUB_CATEGORY_CODE = "RCVG_ACCT_RULE";
	public static final String ALERT_REASON_CODE = "MISSINGDATA";
	public static final int API_VERSION_1611 = 1611;
	public static final String ADDR_COMP_ATTR_PRC = "prc_get_addr_reg_info(?,?,?,?,?,?,?,?,?,?)";
	public static final String PHONE_COMP_ATTR_PRC = "prc_get_phn_reg_info(?,?,?,?,?,?,?,?,?,?)";
	public static final String RECEIVER_PHONE_COUNTRY_CODE = "RECEIVERPHONECOUNTRYCODE";
	public static final String RECEIVER_PRIMARY_PHONE_COUNTRY_CODE = "receiverPrimaryPhoneCountryCode";
	public static final String AC_API_VERSION_1305 = "1305";

}

