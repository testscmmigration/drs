/**
 * PCIEncryptService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Oct 03, 2009 (03:44:44 CDT) WSDL2Java emitter.
 */

package com.moneygram.drs.ws.pci;

public class PCIEncryptService_ServiceLocator extends org.apache.axis.client.Service implements com.moneygram.drs.ws.pci.PCIEncryptService_Service {

    public PCIEncryptService_ServiceLocator() {
    }


    public PCIEncryptService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PCIEncryptService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PCIEncryptServiceSOAP_v3
    private java.lang.String PCIEncryptServiceSOAP_v3_address = "https://devwsintapps.moneygram.com/PCIEncryptService/services/PCIEncryptServiceSOAP_v3";

    public java.lang.String getPCIEncryptServiceSOAP_v3Address() {
        return PCIEncryptServiceSOAP_v3_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PCIEncryptServiceSOAP_v3WSDDServiceName = "PCIEncryptServiceSOAP_v3";

    public java.lang.String getPCIEncryptServiceSOAP_v3WSDDServiceName() {
        return PCIEncryptServiceSOAP_v3WSDDServiceName;
    }

    public void setPCIEncryptServiceSOAP_v3WSDDServiceName(java.lang.String name) {
        PCIEncryptServiceSOAP_v3WSDDServiceName = name;
    }

    public com.moneygram.drs.ws.pci.PCIEncryptService_PortType getPCIEncryptServiceSOAP_v3() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PCIEncryptServiceSOAP_v3_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPCIEncryptServiceSOAP_v3(endpoint);
    }

    public com.moneygram.drs.ws.pci.PCIEncryptService_PortType getPCIEncryptServiceSOAP_v3(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.moneygram.drs.ws.pci.PCIEncryptServiceSOAP_v3Stub _stub = new com.moneygram.drs.ws.pci.PCIEncryptServiceSOAP_v3Stub(portAddress, this);
            _stub.setPortName(getPCIEncryptServiceSOAP_v3WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPCIEncryptServiceSOAP_v3EndpointAddress(java.lang.String address) {
        PCIEncryptServiceSOAP_v3_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.moneygram.drs.ws.pci.PCIEncryptService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.moneygram.drs.ws.pci.PCIEncryptServiceSOAP_v3Stub _stub = new com.moneygram.drs.ws.pci.PCIEncryptServiceSOAP_v3Stub(new java.net.URL(PCIEncryptServiceSOAP_v3_address), this);
                _stub.setPortName(getPCIEncryptServiceSOAP_v3WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PCIEncryptServiceSOAP_v3".equals(inputPortName)) {
            return getPCIEncryptServiceSOAP_v3();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "PCIEncryptService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "PCIEncryptServiceSOAP_v3"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PCIEncryptServiceSOAP_v3".equals(portName)) {
            setPCIEncryptServiceSOAP_v3EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
