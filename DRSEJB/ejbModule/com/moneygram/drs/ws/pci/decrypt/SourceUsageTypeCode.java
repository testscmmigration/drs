/**
 * SourceUsageTypeCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public class SourceUsageTypeCode implements java.io.Serializable {
	private java.lang.String _value_;
	private static java.util.HashMap _table_ = new java.util.HashMap();

	// Constructor
	protected SourceUsageTypeCode(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	}
	public static final java.lang.String _FORM_FREE = "FORM_FREE";
	public static final java.lang.String _EXPRESS_PAYMENT = "EXPRESS_PAYMENT";
	public static final java.lang.String _PAY_BY_SUITE = "PAY_BY_SUITE";
	public static final java.lang.String _EMONEY_TRANSFER = "EMONEY_TRANSFER";
	public static final java.lang.String _PROPERTY_BRIDGE = "PROPERTY_BRIDGE";
	public static final java.lang.String _RECEIVER_REGISTRATION_NUMBER = "RECEIVER_REGISTRATION_NUMBER";
	public static final SourceUsageTypeCode FORM_FREE = new SourceUsageTypeCode(_FORM_FREE);
	public static final SourceUsageTypeCode EXPRESS_PAYMENT = new SourceUsageTypeCode(
			_EXPRESS_PAYMENT);
	public static final SourceUsageTypeCode PAY_BY_SUITE = new SourceUsageTypeCode(_PAY_BY_SUITE);
	public static final SourceUsageTypeCode EMONEY_TRANSFER = new SourceUsageTypeCode(
			_EMONEY_TRANSFER);
	public static final SourceUsageTypeCode PROPERTY_BRIDGE = new SourceUsageTypeCode(
			_PROPERTY_BRIDGE);
	public static final SourceUsageTypeCode RECEIVER_REGISTRATION_NUMBER = new SourceUsageTypeCode(
			_RECEIVER_REGISTRATION_NUMBER);

	public java.lang.String getValue() {
		return _value_;
	}

	public static SourceUsageTypeCode fromValue(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		SourceUsageTypeCode enumeration = (SourceUsageTypeCode) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static SourceUsageTypeCode fromString(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

	public java.lang.Object readResolve() throws java.io.ObjectStreamException {
		return fromValue(_value_);
	}

	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.EnumSerializer(_javaType, _xmlType);
	}

	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.EnumDeserializer(_javaType, _xmlType);
	}
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			SourceUsageTypeCode.class);
	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", "SourceUsageTypeCode"));
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}
}
