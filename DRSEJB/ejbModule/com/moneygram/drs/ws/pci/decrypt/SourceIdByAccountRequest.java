/**
 * SourceIdByAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public class SourceIdByAccountRequest implements java.io.Serializable {
	private java.lang.String primaryAccountNumber;
	private com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory;
	private com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType;
	private java.lang.String userID;
	private int maxRows;

	public SourceIdByAccountRequest() {
	}

	public SourceIdByAccountRequest(java.lang.String primaryAccountNumber,
			com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory,
			com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType,
			java.lang.String userID, int maxRows) {
		this.primaryAccountNumber = primaryAccountNumber;
		this.sourceUsageCategory = sourceUsageCategory;
		this.sourceUsageType = sourceUsageType;
		this.userID = userID;
		this.maxRows = maxRows;
	}

	/**
	 * Gets the primaryAccountNumber value for this SourceIdByAccountRequest.
	 * 
	 * @return primaryAccountNumber
	 */
	public java.lang.String getPrimaryAccountNumber() {
		return primaryAccountNumber;
	}

	/**
	 * Sets the primaryAccountNumber value for this SourceIdByAccountRequest.
	 * 
	 * @param primaryAccountNumber
	 */
	public void setPrimaryAccountNumber(java.lang.String primaryAccountNumber) {
		this.primaryAccountNumber = primaryAccountNumber;
	}

	/**
	 * Gets the sourceUsageCategory value for this SourceIdByAccountRequest.
	 * 
	 * @return sourceUsageCategory
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode getSourceUsageCategory() {
		return sourceUsageCategory;
	}

	/**
	 * Sets the sourceUsageCategory value for this SourceIdByAccountRequest.
	 * 
	 * @param sourceUsageCategory
	 */
	public void setSourceUsageCategory(
			com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory) {
		this.sourceUsageCategory = sourceUsageCategory;
	}

	/**
	 * Gets the sourceUsageType value for this SourceIdByAccountRequest.
	 * 
	 * @return sourceUsageType
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode getSourceUsageType() {
		return sourceUsageType;
	}

	/**
	 * Sets the sourceUsageType value for this SourceIdByAccountRequest.
	 * 
	 * @param sourceUsageType
	 */
	public void setSourceUsageType(
			com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType) {
		this.sourceUsageType = sourceUsageType;
	}

	/**
	 * Gets the userID value for this SourceIdByAccountRequest.
	 * 
	 * @return userID
	 */
	public java.lang.String getUserID() {
		return userID;
	}

	/**
	 * Sets the userID value for this SourceIdByAccountRequest.
	 * 
	 * @param userID
	 */
	public void setUserID(java.lang.String userID) {
		this.userID = userID;
	}

	/**
	 * Gets the maxRows value for this SourceIdByAccountRequest.
	 * 
	 * @return maxRows
	 */
	public int getMaxRows() {
		return maxRows;
	}

	/**
	 * Sets the maxRows value for this SourceIdByAccountRequest.
	 * 
	 * @param maxRows
	 */
	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof SourceIdByAccountRequest))
			return false;
		SourceIdByAccountRequest other = (SourceIdByAccountRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.primaryAccountNumber == null && other.getPrimaryAccountNumber() == null) || (this.primaryAccountNumber != null && this.primaryAccountNumber
						.equals(other.getPrimaryAccountNumber())))
				&& ((this.sourceUsageCategory == null && other.getSourceUsageCategory() == null) || (this.sourceUsageCategory != null && this.sourceUsageCategory
						.equals(other.getSourceUsageCategory())))
				&& ((this.sourceUsageType == null && other.getSourceUsageType() == null) || (this.sourceUsageType != null && this.sourceUsageType
						.equals(other.getSourceUsageType())))
				&& ((this.userID == null && other.getUserID() == null) || (this.userID != null && this.userID
						.equals(other.getUserID()))) && this.maxRows == other.getMaxRows();
		__equalsCalc = null;
		return _equals;
	}
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getPrimaryAccountNumber() != null) {
			_hashCode += getPrimaryAccountNumber().hashCode();
		}
		if (getSourceUsageCategory() != null) {
			_hashCode += getSourceUsageCategory().hashCode();
		}
		if (getSourceUsageType() != null) {
			_hashCode += getSourceUsageType().hashCode();
		}
		if (getUserID() != null) {
			_hashCode += getUserID().hashCode();
		}
		_hashCode += getMaxRows();
		__hashCodeCalc = false;
		return _hashCode;
	}
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			SourceIdByAccountRequest.class, true);
	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", ">SourceIdByAccountRequest"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("primaryAccountNumber");
		elemField.setXmlName(new javax.xml.namespace.QName("", "primaryAccountNumber"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sourceUsageCategory");
		elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageCategory"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", "SourceUsageCategoryCode"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sourceUsageType");
		elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageType"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", "SourceUsageTypeCode"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("userID");
		elemField.setXmlName(new javax.xml.namespace.QName("", "userID"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("maxRows");
		elemField.setXmlName(new javax.xml.namespace.QName("", "maxRows"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"int"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}
}
