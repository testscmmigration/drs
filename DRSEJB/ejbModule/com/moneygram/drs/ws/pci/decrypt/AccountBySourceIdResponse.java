/**
 * AccountBySourceIdResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public class AccountBySourceIdResponse implements java.io.Serializable {
	private java.lang.String primaryAccountNumber;

	public AccountBySourceIdResponse() {
	}

	public AccountBySourceIdResponse(java.lang.String primaryAccountNumber) {
		this.primaryAccountNumber = primaryAccountNumber;
	}

	/**
	 * Gets the primaryAccountNumber value for this AccountBySourceIdResponse.
	 * 
	 * @return primaryAccountNumber
	 */
	public java.lang.String getPrimaryAccountNumber() {
		return primaryAccountNumber;
	}

	/**
	 * Sets the primaryAccountNumber value for this AccountBySourceIdResponse.
	 * 
	 * @param primaryAccountNumber
	 */
	public void setPrimaryAccountNumber(java.lang.String primaryAccountNumber) {
		this.primaryAccountNumber = primaryAccountNumber;
	}
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof AccountBySourceIdResponse))
			return false;
		AccountBySourceIdResponse other = (AccountBySourceIdResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.primaryAccountNumber == null && other.getPrimaryAccountNumber() == null) || (this.primaryAccountNumber != null && this.primaryAccountNumber
				.equals(other.getPrimaryAccountNumber())));
		__equalsCalc = null;
		return _equals;
	}
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getPrimaryAccountNumber() != null) {
			_hashCode += getPrimaryAccountNumber().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			AccountBySourceIdResponse.class, true);
	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", ">AccountBySourceIdResponse"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("primaryAccountNumber");
		elemField.setXmlName(new javax.xml.namespace.QName("", "primaryAccountNumber"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}
}
