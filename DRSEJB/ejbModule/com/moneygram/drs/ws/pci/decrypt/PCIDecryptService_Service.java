/**
 * PCIDecryptService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public interface PCIDecryptService_Service extends javax.xml.rpc.Service {
	public java.lang.String getPCIDecryptServiceSOAPAddress();

	public com.moneygram.drs.ws.pci.decrypt.PCIDecryptService_PortType getPCIDecryptServiceSOAP()
			throws javax.xml.rpc.ServiceException;

	public com.moneygram.drs.ws.pci.decrypt.PCIDecryptService_PortType getPCIDecryptServiceSOAP(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
