/**
 * PCIDecryptService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public interface PCIDecryptService_PortType extends java.rmi.Remote {
	/**
	 * This operation returns a primary account number for a unique
	 * SourceSystemID based on the usage/product type. The SourceSystemId may be
	 * stored as a concatenated value in the PCI database for certain
	 * aplications. If an account number is not found, an AccountNotFound SOAP
	 * fault is returned to the caller
	 */
	public com.moneygram.drs.ws.pci.decrypt.AccountBySourceIdResponse getAccountBySourceID(
			com.moneygram.drs.ws.pci.decrypt.AccountBySourceIdRequest requestParams)
			throws java.rmi.RemoteException,
			com.moneygram.drs.ws.pci.decrypt.AccountNotFoundException;

	/**
	 * This operation returns a sourceSystemID for a specific account number,
	 * multiple source records may be returned if the usage category is of type
	 * "TRANSACTION'. If a source ID is not found, a SourceIDNotFoundException
	 * SOAP fault is returned to the caller
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceIdRecord[] getSourceIdByAccount(
			com.moneygram.drs.ws.pci.decrypt.SourceIdByAccountRequest requestParams)
			throws java.rmi.RemoteException,
			com.moneygram.drs.ws.pci.decrypt.SourceIdNotFoundException;

	/**
	 * This operation returns all sourceSystemID for a specific account number
	 * If a source ID is not found, a SourceIDNotFoundException SOAP fault is
	 * returned to the caller
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceIdRecord[] getAllSourceIdByAccount(
			com.moneygram.drs.ws.pci.decrypt.AllSourceIdByAccountRequest requestParams)
			throws java.rmi.RemoteException,
			com.moneygram.drs.ws.pci.decrypt.SourceIdNotFoundException;
}
