/**
 * AccountBySourceIdRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.ws.pci.decrypt;

public class AccountBySourceIdRequest implements java.io.Serializable {
	private java.lang.String sourceSystemID;
	private com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory;
	private com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType;
	private java.util.Calendar lastUsedDate;
	private java.lang.String userID;

	public AccountBySourceIdRequest() {
	}

	public AccountBySourceIdRequest(java.lang.String sourceSystemID,
			com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory,
			com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType,
			java.util.Calendar lastUsedDate, java.lang.String userID) {
		this.sourceSystemID = sourceSystemID;
		this.sourceUsageCategory = sourceUsageCategory;
		this.sourceUsageType = sourceUsageType;
		this.lastUsedDate = lastUsedDate;
		this.userID = userID;
	}

	/**
	 * Gets the sourceSystemID value for this AccountBySourceIdRequest.
	 * 
	 * @return sourceSystemID
	 */
	public java.lang.String getSourceSystemID() {
		return sourceSystemID;
	}

	/**
	 * Sets the sourceSystemID value for this AccountBySourceIdRequest.
	 * 
	 * @param sourceSystemID
	 */
	public void setSourceSystemID(java.lang.String sourceSystemID) {
		this.sourceSystemID = sourceSystemID;
	}

	/**
	 * Gets the sourceUsageCategory value for this AccountBySourceIdRequest.
	 * 
	 * @return sourceUsageCategory
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode getSourceUsageCategory() {
		return sourceUsageCategory;
	}

	/**
	 * Sets the sourceUsageCategory value for this AccountBySourceIdRequest.
	 * 
	 * @param sourceUsageCategory
	 */
	public void setSourceUsageCategory(
			com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode sourceUsageCategory) {
		this.sourceUsageCategory = sourceUsageCategory;
	}

	/**
	 * Gets the sourceUsageType value for this AccountBySourceIdRequest.
	 * 
	 * @return sourceUsageType
	 */
	public com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode getSourceUsageType() {
		return sourceUsageType;
	}

	/**
	 * Sets the sourceUsageType value for this AccountBySourceIdRequest.
	 * 
	 * @param sourceUsageType
	 */
	public void setSourceUsageType(
			com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode sourceUsageType) {
		this.sourceUsageType = sourceUsageType;
	}

	/**
	 * Gets the lastUsedDate value for this AccountBySourceIdRequest.
	 * 
	 * @return lastUsedDate
	 */
	public java.util.Calendar getLastUsedDate() {
		return lastUsedDate;
	}

	/**
	 * Sets the lastUsedDate value for this AccountBySourceIdRequest.
	 * 
	 * @param lastUsedDate
	 */
	public void setLastUsedDate(java.util.Calendar lastUsedDate) {
		this.lastUsedDate = lastUsedDate;
	}

	/**
	 * Gets the userID value for this AccountBySourceIdRequest.
	 * 
	 * @return userID
	 */
	public java.lang.String getUserID() {
		return userID;
	}

	/**
	 * Sets the userID value for this AccountBySourceIdRequest.
	 * 
	 * @param userID
	 */
	public void setUserID(java.lang.String userID) {
		this.userID = userID;
	}
	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof AccountBySourceIdRequest))
			return false;
		AccountBySourceIdRequest other = (AccountBySourceIdRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.sourceSystemID == null && other.getSourceSystemID() == null) || (this.sourceSystemID != null && this.sourceSystemID
						.equals(other.getSourceSystemID())))
				&& ((this.sourceUsageCategory == null && other.getSourceUsageCategory() == null) || (this.sourceUsageCategory != null && this.sourceUsageCategory
						.equals(other.getSourceUsageCategory())))
				&& ((this.sourceUsageType == null && other.getSourceUsageType() == null) || (this.sourceUsageType != null && this.sourceUsageType
						.equals(other.getSourceUsageType())))
				&& ((this.lastUsedDate == null && other.getLastUsedDate() == null) || (this.lastUsedDate != null && this.lastUsedDate
						.equals(other.getLastUsedDate())))
				&& ((this.userID == null && other.getUserID() == null) || (this.userID != null && this.userID
						.equals(other.getUserID())));
		__equalsCalc = null;
		return _equals;
	}
	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getSourceSystemID() != null) {
			_hashCode += getSourceSystemID().hashCode();
		}
		if (getSourceUsageCategory() != null) {
			_hashCode += getSourceUsageCategory().hashCode();
		}
		if (getSourceUsageType() != null) {
			_hashCode += getSourceUsageType().hashCode();
		}
		if (getLastUsedDate() != null) {
			_hashCode += getLastUsedDate().hashCode();
		}
		if (getUserID() != null) {
			_hashCode += getUserID().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			AccountBySourceIdRequest.class, true);
	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", ">AccountBySourceIdRequest"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sourceSystemID");
		elemField.setXmlName(new javax.xml.namespace.QName("", "sourceSystemID"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sourceUsageCategory");
		elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageCategory"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", "SourceUsageCategoryCode"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sourceUsageType");
		elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageType"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.moneygram.com/PCIDecryptService", "SourceUsageTypeCode"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("lastUsedDate");
		elemField.setXmlName(new javax.xml.namespace.QName("", "lastUsedDate"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"dateTime"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("userID");
		elemField.setXmlName(new javax.xml.namespace.QName("", "userID"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema",
				"string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}
}
