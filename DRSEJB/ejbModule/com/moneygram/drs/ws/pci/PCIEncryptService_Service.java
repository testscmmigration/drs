/**
 * PCIEncryptService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Oct 03, 2009 (03:44:44 CDT) WSDL2Java emitter.
 */

package com.moneygram.drs.ws.pci;

public interface PCIEncryptService_Service extends javax.xml.rpc.Service {
    public java.lang.String getPCIEncryptServiceSOAP_v3Address();

    public com.moneygram.drs.ws.pci.PCIEncryptService_PortType getPCIEncryptServiceSOAP_v3() throws javax.xml.rpc.ServiceException;

    public com.moneygram.drs.ws.pci.PCIEncryptService_PortType getPCIEncryptServiceSOAP_v3(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
