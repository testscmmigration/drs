/**
 * EncryptAccountResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Oct 03, 2009 (03:44:44 CDT) WSDL2Java emitter.
 */

package com.moneygram.drs.ws.pci;

public class EncryptAccountResponse  implements java.io.Serializable {
    private com.moneygram.drs.ws.pci.EncryptAccountResponseCode result;

    private java.lang.String sensitiveDataValueID;

    private int accountNumberHashCode;

    public EncryptAccountResponse() {
    }

    public EncryptAccountResponse(
           com.moneygram.drs.ws.pci.EncryptAccountResponseCode result,
           java.lang.String sensitiveDataValueID,
           int accountNumberHashCode) {
           this.result = result;
           this.sensitiveDataValueID = sensitiveDataValueID;
           this.accountNumberHashCode = accountNumberHashCode;
    }


    /**
     * Gets the result value for this EncryptAccountResponse.
     * 
     * @return result
     */
    public com.moneygram.drs.ws.pci.EncryptAccountResponseCode getResult() {
        return result;
    }


    /**
     * Sets the result value for this EncryptAccountResponse.
     * 
     * @param result
     */
    public void setResult(com.moneygram.drs.ws.pci.EncryptAccountResponseCode result) {
        this.result = result;
    }


    /**
     * Gets the sensitiveDataValueID value for this EncryptAccountResponse.
     * 
     * @return sensitiveDataValueID
     */
    public java.lang.String getSensitiveDataValueID() {
        return sensitiveDataValueID;
    }


    /**
     * Sets the sensitiveDataValueID value for this EncryptAccountResponse.
     * 
     * @param sensitiveDataValueID
     */
    public void setSensitiveDataValueID(java.lang.String sensitiveDataValueID) {
        this.sensitiveDataValueID = sensitiveDataValueID;
    }


    /**
     * Gets the accountNumberHashCode value for this EncryptAccountResponse.
     * 
     * @return accountNumberHashCode
     */
    public int getAccountNumberHashCode() {
        return accountNumberHashCode;
    }


    /**
     * Sets the accountNumberHashCode value for this EncryptAccountResponse.
     * 
     * @param accountNumberHashCode
     */
    public void setAccountNumberHashCode(int accountNumberHashCode) {
        this.accountNumberHashCode = accountNumberHashCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EncryptAccountResponse)) return false;
        EncryptAccountResponse other = (EncryptAccountResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.sensitiveDataValueID==null && other.getSensitiveDataValueID()==null) || 
             (this.sensitiveDataValueID!=null &&
              this.sensitiveDataValueID.equals(other.getSensitiveDataValueID()))) &&
            this.accountNumberHashCode == other.getAccountNumberHashCode();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getSensitiveDataValueID() != null) {
            _hashCode += getSensitiveDataValueID().hashCode();
        }
        _hashCode += getAccountNumberHashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EncryptAccountResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", ">EncryptAccountResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "EncryptAccountResponseCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sensitiveDataValueID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sensitiveDataValueID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumberHashCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountNumberHashCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
