/**
 * PCIEncryptService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Oct 03, 2009 (03:44:44 CDT) WSDL2Java emitter.
 */

package com.moneygram.drs.ws.pci;

public interface PCIEncryptService_PortType extends java.rmi.Remote {
    public com.moneygram.drs.ws.pci.EncryptAccountResponse encryptAccount(com.moneygram.drs.ws.pci.EncryptAccountRequest parametersRequest) throws java.rmi.RemoteException;
}
