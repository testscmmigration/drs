/**
 * EncryptAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Oct 03, 2009 (03:44:44 CDT) WSDL2Java emitter.
 */

package com.moneygram.drs.ws.pci;

public class EncryptAccountRequest  implements java.io.Serializable {
    private java.lang.String sourceSystemID;

    private com.moneygram.drs.ws.pci.SourceUsageCategoryCode sourceUsageCategory;

    private com.moneygram.drs.ws.pci.SourceUsageTypeCode sourceUsageType;

    private java.lang.String primaryAccountNumber;

    private com.moneygram.drs.ws.pci.CardType cardType;

    private java.util.Calendar lastUsedDate;

    private java.lang.String userID;

    public EncryptAccountRequest() {
    }

    public EncryptAccountRequest(
           java.lang.String sourceSystemID,
           com.moneygram.drs.ws.pci.SourceUsageCategoryCode sourceUsageCategory,
           com.moneygram.drs.ws.pci.SourceUsageTypeCode sourceUsageType,
           java.lang.String primaryAccountNumber,
           com.moneygram.drs.ws.pci.CardType cardType,
           java.util.Calendar lastUsedDate,
           java.lang.String userID) {
           this.sourceSystemID = sourceSystemID;
           this.sourceUsageCategory = sourceUsageCategory;
           this.sourceUsageType = sourceUsageType;
           this.primaryAccountNumber = primaryAccountNumber;
           this.cardType = cardType;
           this.lastUsedDate = lastUsedDate;
           this.userID = userID;
    }


    /**
     * Gets the sourceSystemID value for this EncryptAccountRequest.
     * 
     * @return sourceSystemID
     */
    public java.lang.String getSourceSystemID() {
        return sourceSystemID;
    }


    /**
     * Sets the sourceSystemID value for this EncryptAccountRequest.
     * 
     * @param sourceSystemID
     */
    public void setSourceSystemID(java.lang.String sourceSystemID) {
        this.sourceSystemID = sourceSystemID;
    }


    /**
     * Gets the sourceUsageCategory value for this EncryptAccountRequest.
     * 
     * @return sourceUsageCategory
     */
    public com.moneygram.drs.ws.pci.SourceUsageCategoryCode getSourceUsageCategory() {
        return sourceUsageCategory;
    }


    /**
     * Sets the sourceUsageCategory value for this EncryptAccountRequest.
     * 
     * @param sourceUsageCategory
     */
    public void setSourceUsageCategory(com.moneygram.drs.ws.pci.SourceUsageCategoryCode sourceUsageCategory) {
        this.sourceUsageCategory = sourceUsageCategory;
    }


    /**
     * Gets the sourceUsageType value for this EncryptAccountRequest.
     * 
     * @return sourceUsageType
     */
    public com.moneygram.drs.ws.pci.SourceUsageTypeCode getSourceUsageType() {
        return sourceUsageType;
    }


    /**
     * Sets the sourceUsageType value for this EncryptAccountRequest.
     * 
     * @param sourceUsageType
     */
    public void setSourceUsageType(com.moneygram.drs.ws.pci.SourceUsageTypeCode sourceUsageType) {
        this.sourceUsageType = sourceUsageType;
    }


    /**
     * Gets the primaryAccountNumber value for this EncryptAccountRequest.
     * 
     * @return primaryAccountNumber
     */
    public java.lang.String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }


    /**
     * Sets the primaryAccountNumber value for this EncryptAccountRequest.
     * 
     * @param primaryAccountNumber
     */
    public void setPrimaryAccountNumber(java.lang.String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }


    /**
     * Gets the cardType value for this EncryptAccountRequest.
     * 
     * @return cardType
     */
    public com.moneygram.drs.ws.pci.CardType getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this EncryptAccountRequest.
     * 
     * @param cardType
     */
    public void setCardType(com.moneygram.drs.ws.pci.CardType cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the lastUsedDate value for this EncryptAccountRequest.
     * 
     * @return lastUsedDate
     */
    public java.util.Calendar getLastUsedDate() {
        return lastUsedDate;
    }


    /**
     * Sets the lastUsedDate value for this EncryptAccountRequest.
     * 
     * @param lastUsedDate
     */
    public void setLastUsedDate(java.util.Calendar lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }


    /**
     * Gets the userID value for this EncryptAccountRequest.
     * 
     * @return userID
     */
    public java.lang.String getUserID() {
        return userID;
    }


    /**
     * Sets the userID value for this EncryptAccountRequest.
     * 
     * @param userID
     */
    public void setUserID(java.lang.String userID) {
        this.userID = userID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EncryptAccountRequest)) return false;
        EncryptAccountRequest other = (EncryptAccountRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sourceSystemID==null && other.getSourceSystemID()==null) || 
             (this.sourceSystemID!=null &&
              this.sourceSystemID.equals(other.getSourceSystemID()))) &&
            ((this.sourceUsageCategory==null && other.getSourceUsageCategory()==null) || 
             (this.sourceUsageCategory!=null &&
              this.sourceUsageCategory.equals(other.getSourceUsageCategory()))) &&
            ((this.sourceUsageType==null && other.getSourceUsageType()==null) || 
             (this.sourceUsageType!=null &&
              this.sourceUsageType.equals(other.getSourceUsageType()))) &&
            ((this.primaryAccountNumber==null && other.getPrimaryAccountNumber()==null) || 
             (this.primaryAccountNumber!=null &&
              this.primaryAccountNumber.equals(other.getPrimaryAccountNumber()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.lastUsedDate==null && other.getLastUsedDate()==null) || 
             (this.lastUsedDate!=null &&
              this.lastUsedDate.equals(other.getLastUsedDate()))) &&
            ((this.userID==null && other.getUserID()==null) || 
             (this.userID!=null &&
              this.userID.equals(other.getUserID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSourceSystemID() != null) {
            _hashCode += getSourceSystemID().hashCode();
        }
        if (getSourceUsageCategory() != null) {
            _hashCode += getSourceUsageCategory().hashCode();
        }
        if (getSourceUsageType() != null) {
            _hashCode += getSourceUsageType().hashCode();
        }
        if (getPrimaryAccountNumber() != null) {
            _hashCode += getPrimaryAccountNumber().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getLastUsedDate() != null) {
            _hashCode += getLastUsedDate().hashCode();
        }
        if (getUserID() != null) {
            _hashCode += getUserID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EncryptAccountRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", ">EncryptAccountRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSystemID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sourceSystemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceUsageCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "SourceUsageCategoryCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceUsageType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sourceUsageType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "SourceUsageTypeCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "primaryAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.moneygram.com/PCIEncryptService_v3", "CardType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUsedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastUsedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
