package com.moneygram.drs.bo;

public class RealTimeValidationResponse extends BusinessObject {
	private static final long serialVersionUID = 1L;

	public enum Result {
		PASS, FAIL, ERROR
	};
	private int errorCode;
	private String errorString;
	private String offendingField;
	private Result result;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorString() {
		return errorString;
	}

	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}

	public String getOffendingField() {
		return offendingField;
	}

	public void setOffendingField(String offendingField) {
		this.offendingField = offendingField;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
}
