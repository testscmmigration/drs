package com.moneygram.drs.bo;

public class AuthenticationData extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private String authenticationCode;

	public String getAuthenticationCode() {
		return authenticationCode;
	}

	public void setAuthenticationCode(String authenticationCode) {
		this.authenticationCode = authenticationCode;
	}
}
