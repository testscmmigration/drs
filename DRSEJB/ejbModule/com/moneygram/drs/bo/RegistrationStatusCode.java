/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.bo;

import com.moneygram.common.mgEnum.Enum;

/**
 * @author T007
 * 
 */
public class RegistrationStatusCode extends Enum {
	private static final long serialVersionUID = 1L;
	public static final RegistrationStatusCode PENDING = new RegistrationStatusCode("PEN");
	public static final RegistrationStatusCode ACTIVE = new RegistrationStatusCode("ACT");
	public static final RegistrationStatusCode NOT_ACTIVE = new RegistrationStatusCode("NAT");
//	public static final RegistrationStatusCode UNK = new RegistrationStatusCode("UNK");

	private RegistrationStatusCode(String code) {
		super(code);
	}
}
