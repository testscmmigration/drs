package com.moneygram.drs.bo;

import java.io.Serializable;

/**
 * Copyright(c) 2005 Travelers Express
 * 
 * $Workfile: DeliveryOptionInfo.java $ $Revision: 1.4 $ $Author: a235 $ $Log:
 * DeliveryOptionInfo.java,v $ Revision 1.3 2008/09/26 20:23:47 a235 Initial
 * project addition -VJ
 * 
 * Revision 1.2 2008/09/22 21:07:43 a235 Initial project addition -VJ
 * 
 * Revision 1.1 2008/09/19 16:55:43 a118 US: API FQDOSForCountry() flow
 * 
 * Revision 1.3 2005/08/12 14:09:06 uid12451 Story Number AC 4.2 -dssOption,
 * change add new getter & setter for reflection
 * 
 * Revision 1.2 2005/08/03 19:05:28 t007 Make serializable for RPC calls.
 * 
 * Revision 1.1 2005/05/01 19:31:50 t007 Initial load of files.
 * 
 * 
 * Rev 1.1 Mar 02 2005 12:20:34 T357 Added code to translate delivery options.
 * 
 * Rev 1.0 Feb 28 2005 16:19:46 T357 Initial revision.
 */
public class DeliveryOptionInfo extends BusinessObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private int deliveryOptionID;
	private String deliveryOption;
	private String deliveryOptionName;
	private boolean dssOption;

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public int getDeliveryOptionID() {
		return deliveryOptionID;
	}

	public String getDeliveryOptionName() {
		return deliveryOptionName;
	}

	public void setDeliveryOption(String string) {
		deliveryOption = string;
	}

	public void setDeliveryOptionID(int id) {
		deliveryOptionID = id;
	}

	public void setDeliveryOptionName(String string) {
		deliveryOptionName = string;
	}

	public boolean isDSSOption() {
		return dssOption;
	}

	public void setDSSOption(boolean isDss) {
		dssOption = isDss;
	}

	public boolean getDssOption() {
		return dssOption;
	}

	public void setDssOption(boolean isDss) {
		dssOption = isDss;
	}

	public boolean isDssOption() {
		return dssOption;
	}
}
