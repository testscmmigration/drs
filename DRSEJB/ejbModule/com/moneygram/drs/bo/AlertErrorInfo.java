package com.moneygram.drs.bo;

import java.io.Serializable;
import com.moneygram.drs.constants.DRSConstants;

/**
 * Represents Alert info.
 * 
 * @author Prasad
 * 
 */
public class AlertErrorInfo extends BusinessObject implements Serializable,
		DRSConstants {

	private static final long serialVersionUID = 1L;

	private String errorCode;
	private String errorSource;
	private String errorCategoryCode;
	private String alertReasonCode;
	private boolean persistenceFlag;
	private ExtendedRegistrationFieldInfo registrationFieldData = new ExtendedRegistrationFieldInfo();

	/**
	 * @return the registrationFieldData
	 */
	public ExtendedRegistrationFieldInfo getRegistrationFieldData() {
		return registrationFieldData;
	}

	/**
	 * @param registrationFieldData
	 *            the registrationFieldData to set
	 */
	public void setRegistrationFieldData(
			ExtendedRegistrationFieldInfo registrationFieldData) {
		this.registrationFieldData = registrationFieldData;
	}

	public String getErrorCode() {
		return errorCode;

	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorSource() {

		return errorSource;
	}

	public void setErrorSource(String errorSource) {
		this.errorSource = errorSource;
	}

	public String getErrorCategoryCode() {

		return errorCategoryCode;
	}

	public void setErrorCategoryCode(String errorCategoryCode) {
		this.errorCategoryCode = errorCategoryCode;
	}

	public String getAlertReasonCode() {

		return alertReasonCode;
	}

	public void setAlertReasonCode(String alertReasonCode) {
		this.alertReasonCode = alertReasonCode;
	}

	public boolean isPersistenceFlag() {
		return persistenceFlag;
	}

	public boolean getPersistenceFlag() {

		return persistenceFlag;
	}

	public void setPersistenceFlag(boolean persistenceFlag) {
		this.persistenceFlag = persistenceFlag;
	}

	public void setDefaultErrorInfo() {
		this.setErrorCode(ERROR_CODE);
		this.setErrorCategoryCode(ERROR_CATEGORY_CODE);
		this.setErrorSource(ERROR_SOURCE);
		this.setAlertReasonCode(ALERT_REASON_CODE);
		this.setPersistenceFlag(PERSISTENCE_FLAG);
	}

}
