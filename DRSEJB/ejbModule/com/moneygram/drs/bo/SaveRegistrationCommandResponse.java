package com.moneygram.drs.bo;

public class SaveRegistrationCommandResponse implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String mgCustomerReceiveNumber;
	private int mgCustomerReceiveNumberVersion;
	private RegistrationStatusCode registrationStatusCode;
	private boolean verificationRequiredForUse;
	private boolean ofacStatus;
	private String ofacSourceID;
	private String accountNickname;
	private String accountNumberLastFour;
	private int receiverCustomerSequenceID;
	//Added for 30665j AGG3 - Send Infra and Rules
	private String ruleProcessAccountNumber;
	private String accountNumberSensitveID;

	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public int getMgCustomerReceiveNumberVersion() {
		return mgCustomerReceiveNumberVersion;
	}

	public void setMgCustomerReceiveNumberVersion(int mgCustomerReceiveNumberVersion) {
		this.mgCustomerReceiveNumberVersion = mgCustomerReceiveNumberVersion;
	}

	public String getOfacSourceID() {
		return ofacSourceID;
	}

	public void setOfacSourceID(String ofacSourceID) {
		this.ofacSourceID = ofacSourceID;
	}

	public boolean isOfacStatus() {
		return ofacStatus;
	}

	public void setOfacStatus(boolean ofacStatus) {
		this.ofacStatus = ofacStatus;
	}

	public RegistrationStatusCode getRegistrationStatusCode() {
		return registrationStatusCode;
	}

	public void setRegistrationStatusCode(RegistrationStatusCode registrationStatusCode) {
		this.registrationStatusCode = registrationStatusCode;
	}

	public boolean isVerificationRequiredForUse() {
		return verificationRequiredForUse;
	}

	public void setVerificationRequiredForUse(boolean verificationRequiredForUse) {
		this.verificationRequiredForUse = verificationRequiredForUse;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}

	public String getAccountNumberLastFour() {
		return accountNumberLastFour;
	}

	public void setAccountNumberLastFour(String accountNumberLastFour) {
		this.accountNumberLastFour = accountNumberLastFour;
	}

	public int getReceiverCustomerSequenceID() {
		return receiverCustomerSequenceID;
	}

	public void setReceiverCustomerSequenceID(int receiverCustomerSequenceID) {
		this.receiverCustomerSequenceID = receiverCustomerSequenceID;
	}
	
	public String getRuleProcessAccountNumber() {
		return ruleProcessAccountNumber;
	}

	public void setRuleProcessAccountNumber(String ruleProcessAccountNumber) {
		this.ruleProcessAccountNumber = ruleProcessAccountNumber;
	}

	public String getAccountNumberSensitveID() {
		return accountNumberSensitveID;
	}

	public void setAccountNumberSensitveID(String accountNumberSensitveID) {
		this.accountNumberSensitveID = accountNumberSensitveID;
	}
}
