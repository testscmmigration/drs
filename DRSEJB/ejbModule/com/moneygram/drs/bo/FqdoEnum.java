package com.moneygram.drs.bo;

import java.io.Serializable;
import com.moneygram.common.mgEnum.Enum;

public class FqdoEnum extends Enum implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final FqdoEnum FQDO = new FqdoEnum("fqdo");
	public static final FqdoEnum FULLFQDO = new FqdoEnum("fullFqdo");
	public static final FqdoEnum FULLFQDO_WITH_REGISTRATIONSTATUS = new FqdoEnum(
			"fullFqdoWithRegistrationStatus");

	private FqdoEnum(String value) {
		super(value);
	}

	public String getValue() {
		return name;
	}
}
