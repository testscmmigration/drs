/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author T007
 * 
 */
public class CustomerProfilePerson {
	private int sequenceID;
	private String BeneficiaryFlag;
	private String registrationProfileFlag;
	private String accountOwnerFlag;
	private String firstName;
	private String middleName;
	private String lastName;
	private String maternalName;
	private String phoneNumber;
	private String primaryPhoneCountryCode;
	private Boolean smsOptInEnabledFlag;
	private Boolean notificationOptInFlag;
	private List<Address> addresses = new ArrayList<Address>();

	public void addAddress(Address a) {
		addresses.add(a);
	}

	public void removeAddress(Address a) {
		addresses.remove(a);
	}

	public List<Address> getAddresses() {
//		return Collections.unmodifiableList(addresses);
		return addresses;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMaternalName() {
		return maternalName;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param list
	 */
	public void setAddresses(List<Address> list) {
		addresses = list;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMaternalName(String string) {
		maternalName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}

	/**
	 * 
	 * @param string
	 */
	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	public int getSequenceID() {
		return sequenceID;
	}

	public void setSequenceID(int sequenceID) {
		this.sequenceID = sequenceID;
	}

	public String getBeneficiaryFlag() {
		return BeneficiaryFlag;
	}

	public void setBeneficiaryFlag(String beneficiaryFlag) {
		BeneficiaryFlag = beneficiaryFlag;
	}

	public String getRegistrationProfileFlag() {
		return registrationProfileFlag;
	}

	public void setRegistrationProfileFlag(String registrationProfileFlag) {
		this.registrationProfileFlag = registrationProfileFlag;
	}

	public String getAccountOwnerFlag() {
		return accountOwnerFlag;
	}

	public void setAccountOwnerFlag(String accountOwnerFlag) {
		this.accountOwnerFlag = accountOwnerFlag;
	}

	/**
	 * Checks to see if the name is different for this person.
	 * 
	 * @param existingObject
	 *            The input object is the existing object. If we are null, then
	 *            we assume no change, since the request doesn't have to fill in
	 *            all data. However, if the exisitng object is null and we are
	 *            not, then we assume that the name HAS changed.
	 * @return
	 */
	public boolean isNameDifferent(CustomerProfilePerson existingObject) {
		if (existingObject == null)
			return true;
		if (this.firstName != null) {
			if (existingObject.firstName != null) {
				if (!existingObject.firstName.equalsIgnoreCase(this.firstName))
					return true;
			} else {
				return true;
			}
		}
		if (this.middleName != null) {
			if (existingObject.middleName != null) {
				if (!existingObject.middleName.equalsIgnoreCase(this.middleName))
					return true;
			} else {
				return true;
			}
		}
		if (this.lastName != null) {
			if (existingObject.lastName != null) {
				if (!existingObject.lastName.equalsIgnoreCase(this.lastName))
					return true;
			} else {
				return true;
			}
		}
		if (this.maternalName != null) {
			if (existingObject.maternalName != null) {
				if (!existingObject.maternalName.equalsIgnoreCase(this.maternalName))
					return true;
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("CustomerProfilePerson[");
		buffer.append("sequenceID = ").append(sequenceID);
		buffer.append("firstName = ").append(firstName);
		buffer.append(", middleName = ").append(middleName);
		buffer.append(", lastName = ").append(lastName);
		buffer.append(", maternalName = ").append(maternalName);
		buffer.append(", phoneNumber = ").append(phoneNumber);
		buffer.append(", primaryPhoneCountryCode = ").append(primaryPhoneCountryCode);
		buffer.append(", addresses = ").append(addresses);
		buffer.append("]");
		return buffer.toString();
	}

	public Boolean getSmsOptInEnabledFlag() {
		return smsOptInEnabledFlag;
	}

	public void setSmsOptInEnabledFlag(Boolean smsOptInEnabledFlag) {
		this.smsOptInEnabledFlag = smsOptInEnabledFlag;
	}

	public Boolean getNotificationOptInFlag() {
		return notificationOptInFlag;
	}

	public void setNotificationOptInFlag(Boolean notificationOptInFlag) {
		this.notificationOptInFlag = notificationOptInFlag;
	}

	public String getPrimaryPhoneCountryCode() {
		return primaryPhoneCountryCode;
	}

	public void setPrimaryPhoneCountryCode(String primaryPhoneCountryCode) {
		this.primaryPhoneCountryCode = primaryPhoneCountryCode;
	}
}
