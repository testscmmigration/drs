/**
 * QueryRegistrationByCustomerReceiveNumberResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.bo;

import java.util.Collection;

public class RegistrationData implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private FQDOInfo fqdoInfo;
	private java.lang.String ofacExceptionStateCode;
	private RegistrationStatusCode registrationStatus;
	private java.lang.String registrationSubStatus;
	private Collection registrationFieldInfo;
	private java.lang.String custTranFreqCode;
	private String AgentStatusCode;

	public RegistrationData() {
	}

	public RegistrationData(FQDOInfo fqdoInfo, java.lang.String ofacExceptionStateCode,
			RegistrationStatusCode registrationStatus, java.lang.String registrationSubStatus,
			Collection registrationFieldInfo,java.lang.String custTranFreqCode,String AgentStatusCode) {
		this.fqdoInfo = fqdoInfo;
		this.ofacExceptionStateCode = ofacExceptionStateCode;
		this.registrationStatus = registrationStatus;
		this.registrationSubStatus = registrationSubStatus;
		this.registrationFieldInfo = registrationFieldInfo;
		this.custTranFreqCode = custTranFreqCode;
		this.AgentStatusCode=AgentStatusCode;
	}

	/**
	 * Gets the fqdoInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return fqdoInfo
	 */
	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * Sets the fqdoInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param fqdoInfo
	 */
	public void setFqdoInfo(FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	/**
	 * Gets the ofacExceptionStateCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return ofacExceptionStateCode
	 */
	public java.lang.String getOfacExceptionStateCode() {
		return ofacExceptionStateCode;
	}

	/**
	 * Sets the ofacExceptionStateCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param ofacExceptionStateCode
	 */
	public void setOfacExceptionStateCode(java.lang.String ofacExceptionStateCode) {
		this.ofacExceptionStateCode = ofacExceptionStateCode;
	}

	/**
	 * Gets the registrationStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationStatus
	 */
	public RegistrationStatusCode getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * Sets the registrationStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationStatus
	 */
	public void setRegistrationStatus(RegistrationStatusCode registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	/**
	 * Gets the registrationSubStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationSubStatus
	 */
	public java.lang.String getRegistrationSubStatus() {
		return registrationSubStatus;
	}

	/**
	 * Sets the registrationSubStatus value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationSubStatus
	 */
	public void setRegistrationSubStatus(java.lang.String registrationSubStatus) {
		this.registrationSubStatus = registrationSubStatus;
	}

	/**
	 * Gets the registrationFieldInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return registrationFieldInfo
	 */
	public Collection getRegistrationFieldInfo() {
		return registrationFieldInfo;
	}

	/**
	 * Sets the registrationFieldInfo value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param registrationFieldInfo
	 */
	public void setRegistrationFieldInfo(Collection registrationFieldInfo) {
		this.registrationFieldInfo = registrationFieldInfo;
	}

	/**
	 * @return the custTranFreqCode
	 */
	public java.lang.String getCustTranFreqCode() {
		return custTranFreqCode;
	}

	/**
	 * @param custTranFreqCode the custTranFreqCode to set
	 */
	public void setCustTranFreqCode(java.lang.String custTranFreqCode) {
		this.custTranFreqCode = custTranFreqCode;
	}

	
	/**
	 * Gets the AgentStatusCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @return AgentStatusCode
	 */

	public String getAgentStatusCode() {
		return AgentStatusCode;
	}
	/**
	 * Sets the AgentStatusCode value for this
	 * QueryRegistrationByCustomerReceiveNumberResponse.
	 * 
	 * @param AgentStatusCode
	 */
	

	public void setAgentStatusCode(String agentStatusCode) {
		AgentStatusCode = agentStatusCode;
	}

}
