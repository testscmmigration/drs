/**
 * RegistrationInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.bo;

import java.math.BigInteger;

public class RegistrationInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String mgCustomerReceiveNumber;
	private BigInteger mgCustomerReceiveNumberVersion;
	private String receiverFirstName;
	private String receiverLastName;
	private String receiverPhoneNumber;
	private String creatorFirstName;
	private String creatorLastName;
	private FQDOInfo fqdoInfo;
	private String accountNickname;

	public RegistrationInfo() {
	}

	public RegistrationInfo(String mgCustomerReceiveNumber,
			BigInteger mgCustomerReceiveNumberVersion, String receiverFirstName,
			String receiverLastName, String receiverPhoneNumber, String creatorFirstName,
			String creatorLastName, FQDOInfo fqdoInfo, String accountNickname) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
		this.mgCustomerReceiveNumberVersion = mgCustomerReceiveNumberVersion;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.receiverPhoneNumber = receiverPhoneNumber;
		this.creatorFirstName = creatorFirstName;
		this.creatorLastName = creatorLastName;
		this.fqdoInfo = fqdoInfo;
		this.accountNickname = accountNickname;
	}

	public String getCreatorFirstName() {
		return creatorFirstName;
	}

	public void setCreatorFirstName(String creatorFirstName) {
		this.creatorFirstName = creatorFirstName;
	}

	public String getCreatorLastName() {
		return creatorLastName;
	}

	public void setCreatorLastName(String creatorLastName) {
		this.creatorLastName = creatorLastName;
	}

	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	public void setFqdoInfo(FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	public void setMgCustomerReceiveNumber(String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}

	public BigInteger getMgCustomerReceiveNumberVersion() {
		return mgCustomerReceiveNumberVersion;
	}

	public void setMgCustomerReceiveNumberVersion(BigInteger mgCustomerReceiveNumberVersion) {
		this.mgCustomerReceiveNumberVersion = mgCustomerReceiveNumberVersion;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverPhoneNumber() {
		return receiverPhoneNumber;
	}

	public void setReceiverPhoneNumber(String receiverPhoneNumber) {
		this.receiverPhoneNumber = receiverPhoneNumber;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}
}
