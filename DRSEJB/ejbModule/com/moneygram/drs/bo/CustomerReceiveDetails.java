package com.moneygram.drs.bo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerReceiveDetails", propOrder = {
    "referenceNumber",
    "poeTranId",
    "poeTranEventId",
    "customerReceiveNumber",
    "receiveAgentCustomerId",
    "customerReceiveNumberType",
    "customerReceiveNumberVersion",
    "customerReceiveNumberSequence",
    "transactionUseFlag"
})

@XmlRootElement(name = "customerReceiveDetails")
public class CustomerReceiveDetails extends BusinessObject implements java.io.Serializable{
	
	private String referenceNumber;
	private String poeTranId;
	private String poeTranEventId;
	private String customerReceiveNumber;	
	private String receiveAgentCustomerId;
	private String customerReceiveNumberType;
	private Integer customerReceiveNumberVersion;
    private Integer customerReceiveNumberSequence;
    private boolean transactionUseFlag;
    
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getPoeTranId() {
		return poeTranId;
	}
	public void setPoeTranId(String poeTranId) {
		this.poeTranId = poeTranId;
	}
	public String getPoeTranEventId() {
		return poeTranEventId;
	}
	public void setPoeTranEventId(String poeTranEventId) {
		this.poeTranEventId = poeTranEventId;
	}
	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}
	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}
	public String getReceiveAgentCustomerId() {
		return receiveAgentCustomerId;
	}
	public void setReceiveAgentCustomerId(String receiveAgentCustomerId) {
		this.receiveAgentCustomerId = receiveAgentCustomerId;
	}
	public String getCustomerReceiveNumberType() {
		return customerReceiveNumberType;
	}
	public void setCustomerReceiveNumberType(String customerReceiveNumberType) {
		this.customerReceiveNumberType = customerReceiveNumberType;
	}
	public Integer getCustomerReceiveNumberVersion() {
		return customerReceiveNumberVersion;
	}
	public void setCustomerReceiveNumberVersion(Integer customerReceiveNumberVersion) {
		this.customerReceiveNumberVersion = customerReceiveNumberVersion;
	}
	public Integer getCustomerReceiveNumberSequence() {
		return customerReceiveNumberSequence;
	}
	public void setCustomerReceiveNumberSequence(
			Integer customerReceiveNumberSequence) {
		this.customerReceiveNumberSequence = customerReceiveNumberSequence;
	}
	
    public boolean getTransactionUseFlag() {
		return transactionUseFlag;
	}
	public void setTransactionUseFlag(boolean transactionUseFlag) {
		this.transactionUseFlag = transactionUseFlag;
	}
	public void setNullsToEmptyString() {    	 	
    	referenceNumber 	= (referenceNumber==null ? "" : referenceNumber);    	
    	customerReceiveNumber = (customerReceiveNumber==null ? "" : customerReceiveNumber);    	
    	customerReceiveNumberType 	= (customerReceiveNumberType==null ? "" : customerReceiveNumberType);	    	    	
    	receiveAgentCustomerId = (receiveAgentCustomerId==null ? "" : receiveAgentCustomerId);    	
    	poeTranId      = (poeTranId==null ? "" : poeTranId);
    	poeTranEventId     = (poeTranEventId==null ? "" : poeTranEventId);    	
    }   
    
	public String toString() { 
        StringBuffer buf = new StringBuffer();
        buf.append("CustomerReceiveDetails = [");       
        buf.append(", referenceNumber = ").append(referenceNumber);  
        buf.append(", poeTranId = ").append(poeTranId);
        buf.append(", poeTranEventId = ").append(poeTranEventId);
        buf.append(", customerReceiveNumber = ").append(customerReceiveNumber);
        buf.append(", receiveAgentCustomerId = ").append(receiveAgentCustomerId);
        buf.append(", customerReceiveNumberType = ").append(customerReceiveNumberType);	        
        buf.append(", customerReceiveNumberVersion = ").append(customerReceiveNumberVersion);
        buf.append(", customerReceiveNumberSequence = ").append(customerReceiveNumberSequence);
        buf.append(", transactionUseFlag = ").append(transactionUseFlag); 
        buf.append(" ]\n"); 

        return buf.toString(); 
    }
    
}
