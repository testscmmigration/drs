package com.moneygram.drs.bo;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.moneygram.drs.constants.DRSConstants;

public class AlertRegistrationInfo extends BusinessObject implements
		Serializable, DRSConstants {
	private static final long serialVersionUID = 1L;
	private String alertActionCode;
	private String alertCategoryCode;
	private String alertSubCategoryCode;
	private List<AlertErrorInfo> errorData = new ArrayList<AlertErrorInfo>();

	public Collection<AlertErrorInfo> getErrorData() {
		return errorData;
	}

	public void setErrorData(Collection<AlertErrorInfo> errorData) {
		this.errorData.addAll(errorData);
	}

	public String getAlertActionCode() {
		return alertActionCode;

	}

	public void setAlertActionCode(String alertActionCode) {
		this.alertActionCode = alertActionCode;
	}

	public String getAlertCategoryCode() {
		return alertCategoryCode;

	}

	public void setAlertCategoryCode(String alertCategoryCode) {
		this.alertCategoryCode = alertCategoryCode;
	}

	public String getAlertSubCategoryCode() {
		return alertSubCategoryCode;

	}

	public void setAlertSubCategoryCode(String alertSubCategoryCode) {
		this.alertSubCategoryCode = alertSubCategoryCode;
	}

	public void setDefaultAlertInfo() {
		this.setAlertActionCode(ALERT_ACTION_CODE);
		this.setAlertCategoryCode(ALERT_CATEGORY_CODE);
		this.setAlertSubCategoryCode(ALERT_SUB_CATEGORY_CODE);
	}

}
