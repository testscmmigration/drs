package com.moneygram.drs.bo;

import java.io.Serializable;
import com.moneygram.drs.util.Reflector;

public abstract class BusinessObject implements Serializable {
	public BusinessObject() {
	}

	public String toString() {
		return Reflector.getInstance(this.getClass()).printBean(this);
	}
}