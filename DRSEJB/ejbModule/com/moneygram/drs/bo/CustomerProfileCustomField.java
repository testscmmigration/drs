/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.bo;

/**
 * @author T007
 * 
 */
public class CustomerProfileCustomField {
	private Integer attrId;
	private String xmlTag;
	private String value;

	public CustomerProfileCustomField() {
		super();
	}

	public CustomerProfileCustomField(String xmlTag, String value) {
		this.xmlTag = xmlTag;
		this.value = value;
	}

	public Integer getAttrId() {
		return attrId;
	}

	public void setAttrId(Integer id) {
		this.attrId = id;
	}
	
	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return
	 */
	public String getXmlTag() {
		return xmlTag;
	}

	/**
	 * @param i
	 */
	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}

	/**
	 * @param string
	 */
	public void setXmlTag(String string) {
		xmlTag = string;
	}
}
