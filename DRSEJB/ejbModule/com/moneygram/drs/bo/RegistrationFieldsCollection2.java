package com.moneygram.drs.bo;

import java.util.List;

public class RegistrationFieldsCollection2 extends RegistrationFieldsCollection{
	
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("rawtypes")
	public RegistrationFieldsCollection2(List registrationFields,
			String dccCardRange, boolean supplementalFlag,
			boolean saveRegistrationWithNewXmlTags) {
		super(registrationFields, dccCardRange, supplementalFlag,
				saveRegistrationWithNewXmlTags);
	}

}
