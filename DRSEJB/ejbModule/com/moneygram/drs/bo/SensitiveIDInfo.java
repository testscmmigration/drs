package com.moneygram.drs.bo;

public class SensitiveIDInfo extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private String customerReceiveNumber;
	private int customerReceiveNumberVersion;
	private String sensitiveID;
	//added for 30655 agg infra rules - july 2017
	private String ruleProcessAccountNumber;

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public int getCustomerReceiveNumberVersion() {
		return customerReceiveNumberVersion;
	}

	public void setCustomerReceiveNumberVersion(int customerReceiveNumberVersion) {
		this.customerReceiveNumberVersion = customerReceiveNumberVersion;
	}

	public String getSensitiveID() {
		return sensitiveID;
	}

	public void setSensitiveID(String status) {
		sensitiveID = status;
	}

	public String getRuleProcessAccountNumber() {
		return ruleProcessAccountNumber;
	}

	public void setRuleProcessAccountNumber(String ruleProcessAccountNumber) {
		this.ruleProcessAccountNumber = ruleProcessAccountNumber;
	}
}
