/*
 * Created on Feb 11, 2005
 *
 */
package com.moneygram.drs.bo;

import java.io.Serializable;
import com.moneygram.common.mgEnum.Enum;

/**
 * @author T007
 * 
 */
public class DataTypeCodeEnum extends Enum implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean isMinMaxCheckable;
	public static final DataTypeCodeEnum BOOLEAN = new DataTypeCodeEnum("boolean");
	public static final DataTypeCodeEnum STRING = new DataTypeCodeEnum("string", true);
	public static final DataTypeCodeEnum INT = new DataTypeCodeEnum("int", true);
	public static final DataTypeCodeEnum DECIMAL = new DataTypeCodeEnum("decimal", true);
	public static final DataTypeCodeEnum DATE = new DataTypeCodeEnum("date");
	public static final DataTypeCodeEnum DATETIME = new DataTypeCodeEnum("datetime");
	public static final DataTypeCodeEnum TIME = new DataTypeCodeEnum("time");
	public static final DataTypeCodeEnum TEXT = new DataTypeCodeEnum("text", true);
	public static final DataTypeCodeEnum COUNTRY_CODE = new DataTypeCodeEnum("cntrycode");
	public static final DataTypeCodeEnum ENUM = new DataTypeCodeEnum("enum");
	public static final DataTypeCodeEnum STRINGBOOL = new DataTypeCodeEnum("stringbool");

	private DataTypeCodeEnum(String xmlValue) {
		super(xmlValue, true);
	}

	private DataTypeCodeEnum(String xmlValue, boolean isValue) {
		this(xmlValue);
		this.isMinMaxCheckable = isValue;
	}

	/**
	 * @return
	 */
	public String getXmlValue() {
		return name;
	}

	public boolean isMinMaxCheckable() {
		return isMinMaxCheckable;
	}

	public String toString() {
		return getName();
	}
}
