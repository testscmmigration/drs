package com.moneygram.drs.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DirectedSendRegistrationInfo extends BusinessObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<ExtendedRegistrationFieldInfo> registrationFieldData = new ArrayList<ExtendedRegistrationFieldInfo>();
	private FQDOInfo fqdoInfo;
	private boolean supplementalFlag;
	
	public boolean isSupplementalFlag() {
		return supplementalFlag;
	}

	public void setSupplementalFlag(boolean supplementalFlag) {
		this.supplementalFlag = supplementalFlag;
	}

	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	public void setFqdoInfo(FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}

	public Collection<ExtendedRegistrationFieldInfo> getRegistrationFieldData() {
		return registrationFieldData;
	}

	public void setRegistrationFieldData(
			Collection<ExtendedRegistrationFieldInfo> registrationFieldData) {
		this.registrationFieldData.addAll(registrationFieldData);
	}
}
