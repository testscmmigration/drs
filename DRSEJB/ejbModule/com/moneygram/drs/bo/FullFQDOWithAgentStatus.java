package com.moneygram.drs.bo;

public class FullFQDOWithAgentStatus extends BusinessObject{
	
	private FQDOInfo fqdoInfo;
	private String AgentStatusCode;
	
	
	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}
	public void setFqdoInfo(FQDOInfo fqdoInfo) {
		this.fqdoInfo = fqdoInfo;
	}
	public String getAgentStatusCode() {
		return AgentStatusCode;
	}
	public void setAgentStatusCode(String agentStatusCode) {
		AgentStatusCode = agentStatusCode;
	}


}
