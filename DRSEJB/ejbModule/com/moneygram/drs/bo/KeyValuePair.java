/*
 * Created on Feb 10, 2005
 *
 */
package com.moneygram.drs.bo;

import java.io.Serializable;

/**
 * @author T007
 * 
 */
public class KeyValuePair implements Serializable {
	private static final long serialVersionUID = 1L;
	private String key;
	private String value;

	/**
	 * 
	 */
	public KeyValuePair() {
		super();
	}

	public KeyValuePair(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param string
	 */
	public void setKey(String string) {
		key = string;
	}

	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}
}
