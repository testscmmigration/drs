/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.bo;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author T007
 * 
 */
public class CustomerProfile extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private String customerReceiveNumber;
	private int customerReceiveNumberVersion;
	private String agentID;
	private int deliveryOptionID;
	private RegistrationStatusCode registrationStatus;
	private String ofacStatus;
	private String rcvAgentCustAcctName;
	private String rcvAgentCustAcctNbr;
	private String rcvAgentCustAcctNbrMask;
	private String rcvAgentBankID;
	private String registrationSubStatus;
	private String rcvAgentCustAcctNbrPanFlag = "N";
	private String accountNickname;
	private String custTranFreqCode;
	private String rcvAgentCustAccountBin;
	private int poeSourceSystem;
	private CustomerProfilePerson creator;
	private CustomerProfilePerson receiver;
	private Map<Integer, CustomerProfilePerson> jointAccountHolders = new HashMap<Integer, CustomerProfilePerson>();
	private Map<String, CustomerProfileCustomField> customFields = new HashMap<String, CustomerProfileCustomField>();
	private Map<String, CustomerProfileCustomField> customFieldsAllCaps = new HashMap<String, CustomerProfileCustomField>();
	private Map<Integer, CustomerProfileCustomField> customFieldsById = new HashMap<Integer, CustomerProfileCustomField>();
	private String dcpRcvAgentCustAcctNbr;
	//Added for 30665j AGG3 - Send Infra and Rules
	private String ruleProcessAccountNumber;
	private String accountNumberSensitveID;
	private String externalReceiverProfileId;
	//NOTE: accountNumberIsToken is only set during saveRegistration
	//so do not try to depend on it being set at other times.
	private boolean accountNumberIsToken;	
	//ADP-1535
	private String externalReceiverProfileType;


	

	/**
	 * @return
	 */
	public CustomerProfilePerson getCreator() {
		return creator;
	}

	/**
	 * @return
	 */
	public CustomerProfilePerson getReceiver() {
		return receiver;
	}

	/**
	 * @param person
	 */
	public void setCreator(CustomerProfilePerson person) {
		creator = person;
	}

	/**
	 * @param person
	 */
	public void setReceiver(CustomerProfilePerson person) {
		receiver = person;
	}

	public Collection<CustomerProfilePerson> getJointAccountHolders() {
		return Collections.unmodifiableCollection(jointAccountHolders.values());
	}

	public Set<Integer> getJointAccountHoldersIndexKeys() {
		return Collections.unmodifiableSet(jointAccountHolders.keySet());
	}

	/**
	 * 
	 * @param jah
	 * @param jahIndex
	 *            Index of jah (1 based).
	 */
	public void addJointAccountHolder(CustomerProfilePerson jah, int jahIndex) {
		jointAccountHolders.put(new Integer(jahIndex), jah);
	}

	public void addCustomFields(CustomerProfileCustomField custFlds, String xmlTag) {
		customFields.put(xmlTag, custFlds);
		customFieldsAllCaps.put(xmlTag.toUpperCase(), custFlds);
		Integer attrId = custFlds.getAttrId();
		if (attrId != null) {
			customFieldsById.put(attrId, custFlds);
		}
	}

	public CustomerProfileCustomField getCustomFieldByXMLTag(String tag) {
		return (CustomerProfileCustomField) customFields.get(tag);
	}
	
	public CustomerProfileCustomField getCustomFieldByXMLTagUpperCase(String tag) {
		return (CustomerProfileCustomField) customFieldsAllCaps.get(tag);
	}
	
	public CustomerProfileCustomField getCustomField(ExtendedRegistrationFieldInfo fieldInfo) {
		CustomerProfileCustomField result = customFieldsById.get(fieldInfo.getAttrID());
		if(result == null) {
			result = getCustomFieldByXMLTagUpperCase(fieldInfo.getXmlTag().toUpperCase());
		}
		return result;
	}

	public Collection <CustomerProfileCustomField> getCustomFields() {
		return Collections.unmodifiableCollection(customFields.values());
	}

	public void removeJointAccountHolder(String xmlTag) {
		jointAccountHolders.remove(xmlTag);
	}

	/**
	 * Get the joint account holder by index
	 * 
	 * @param index -
	 *            The index (remember 1-based!).
	 * @return
	 */
	public CustomerProfilePerson getJointAccountHolderByIndex(int index) {
		return (CustomerProfilePerson) jointAccountHolders.get(new Integer(index));
	}

	/**
	 * @return
	 */
	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	/**
	 * @param i
	 */
	public void setCustomerReceiveNumber(String i) {
		customerReceiveNumber = i;
	}

	/**
	 * @return
	 */
	public String getOfacStatus() {
		return ofacStatus;
	}

	/**
	 * @return
	 */
	public RegistrationStatusCode getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * @return
	 */
	public String getRegistrationSubStatus() {
		return registrationSubStatus;
	}

	/**
	 * @param b
	 */
	public void setOfacStatus(String value) {
		ofacStatus = value;
	}

	/**
	 * @param code
	 */
	public void setRegistrationStatus(RegistrationStatusCode code) {
		registrationStatus = code;
	}

	/**
	 * @param string
	 */
	public void setRegistrationSubStatus(String string) {
		registrationSubStatus = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgentBankID() {
		return rcvAgentBankID;
	}

	/**
	 * @return
	 */
	public String getRcvAgentCustAcctNbr() {
		return rcvAgentCustAcctNbr;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentBankID(String string) {
		rcvAgentBankID = string;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentCustAcctNbr(String string) {
		rcvAgentCustAcctNbr = string;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentCustAccountBin(String string) {
		rcvAgentCustAccountBin = string;
	}
	
	/**
	 * @return
	 */
	public String getRcvAgentCustAccountBin() {
		return rcvAgentCustAccountBin;
	}
	
	/**
	 * @return
	 */
	public String getRcvAgentCustAcctName() {
		return rcvAgentCustAcctName;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentCustAcctName(String string) {
		rcvAgentCustAcctName = string;
	}

	public boolean isAccountNumberIsToken() {
		return accountNumberIsToken;
	}

	public void setAccountNumberIsToken(boolean accountNumberIsToken) {
		this.accountNumberIsToken = accountNumberIsToken;
	}

	/**
	 * Compare the two customer profiles for any name changes. This is used to
	 * trigger OFAC checks
	 * 
	 * @param comparisonObj
	 * @return
	 */
	public boolean areNonCreatorNamesDifferent(CustomerProfile exisitngObject) {
		// Receiver
		if (this.getReceiver() != null
				&& this.getReceiver().isNameDifferent(exisitngObject.getReceiver()))
			return true;
		// Loop through the Joint Account Holders doing name comparisions.
		Iterator i = this.jointAccountHolders.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry entry = (Map.Entry) i.next();
			Integer index = (Integer) entry.getKey();
			CustomerProfilePerson cpp = (CustomerProfilePerson) entry.getValue();
			if (cpp == null)
				continue;
			CustomerProfilePerson existingCPP = exisitngObject.getJointAccountHolderByIndex(index
					.intValue());
			// If the JAH is new, then a check is required
			if (existingCPP == null)
				return true;
			if (cpp.isNameDifferent(existingCPP))
				return true;
		}
		return false;
	}

	/**
	 * @return
	 */
	public int getCustomerReceiveNumberVersion() {
		return customerReceiveNumberVersion;
	}

	/**
	 * @param i
	 */
	public void setCustomerReceiveNumberVersion(int i) {
		customerReceiveNumberVersion = i;
	}

	/**
	 * @return
	 */
	public String getRcvAgentCustAcctNbrMask() {
		return rcvAgentCustAcctNbrMask;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentCustAcctNbrMask(String string) {
		rcvAgentCustAcctNbrMask = string;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("CustomerProfile[");
		buffer.append("customerReceiveNumber = ").append(customerReceiveNumber);
		buffer.append(", customerReceiveNumberVersion = ").append(customerReceiveNumberVersion);
		buffer.append(", agentID = ").append(agentID);
		buffer.append(", deliveryOptionID = ").append(deliveryOptionID);
		buffer.append(", registrationStatus = ").append(registrationStatus);
		buffer.append(", ofacStatus = ").append(ofacStatus);
		buffer.append(", rcvAgentCustAcctName = ").append(rcvAgentCustAcctName);
		buffer.append(", rcvAgentBankID = ").append(rcvAgentBankID);
		buffer.append(", registrationSubStatus = ").append(registrationSubStatus);
		buffer.append(", accountNickname = ").append(accountNickname);
		buffer.append(", custTranFreqCode = ").append(custTranFreqCode);
		buffer.append(", poeSourceSystem = ").append(poeSourceSystem);
		buffer.append(", creator = ").append(creator);
		buffer.append(", receiver = ").append(receiver);
		buffer.append(", jointAccountHolders = ").append(jointAccountHolders);
		buffer.append(", customFields = ").append(customFields);
		buffer.append("]");
		return buffer.toString();
	}

	public String getRcvAgentCustAcctNbrPanFlag() {
		return rcvAgentCustAcctNbrPanFlag;
	}

	public void setRcvAgentCustAcctNbrPanFlag(String rcvAgentCustAcctNbrPanFlag) {
		this.rcvAgentCustAcctNbrPanFlag = rcvAgentCustAcctNbrPanFlag;
	}

	public String getAccountNickname() {
		return accountNickname;
	}

	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}
	

	public int getPoeSourceSystem() {
		return poeSourceSystem;
	}

	public void setPoeSourceSystem(int poeSourceSystem) {
		this.poeSourceSystem = poeSourceSystem;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public int getDeliveryOptionID() {
		return deliveryOptionID;
	}

	public void setDeliveryOptionID(int deliveryOptionID) {
		this.deliveryOptionID = deliveryOptionID;
	}

	/**
	 * @return the custTranFreqCode
	 */
	public String getCustTranFreqCode() {
		return custTranFreqCode;
	}

	/**
	 * @param custTranFreqCode the custTranFreqCode to set
	 */
	public void setCustTranFreqCode(String custTranFreqCode) {
		this.custTranFreqCode = custTranFreqCode;
	}
	
	public String getDcpRcvAgentCustAcctNbr() {
		return dcpRcvAgentCustAcctNbr;
	}

	public void setDcpRcvAgentCustAcctNbr(String dcpRcvAgentCustAcctNbr) {
		this.dcpRcvAgentCustAcctNbr = dcpRcvAgentCustAcctNbr;
	}

	public String getRuleProcessAccountNumber() {
		return ruleProcessAccountNumber;
	}

	public void setRuleProcessAccountNumber(String ruleProcessAccountNumber) {
		this.ruleProcessAccountNumber = ruleProcessAccountNumber;
	}

	public String getAccountNumberSensitveID() {
		return accountNumberSensitveID;
	}

	public void setAccountNumberSensitveID(String accountNumberSensitveID) {
		this.accountNumberSensitveID = accountNumberSensitveID;
	}
	
	public Address getReceiverAddress() {
		if(receiver != null) {
			List<Address> addresses = receiver.getAddresses();
			if(addresses != null && addresses.size() > 0) {
				return addresses.get(0);
			}
		}
		return null;
	}

	public String getExternalReceiverProfileId() {
		return externalReceiverProfileId;
	}

	public void setExternalReceiverProfileId(String externalReceiverProfileId) {
		this.externalReceiverProfileId = externalReceiverProfileId;
	}

	public void setExternalReceiverProfileType(String externalReceiverProfileType) {
		this.externalReceiverProfileType = externalReceiverProfileType;
	}

	public String getExternalReceiverProfileType() {
		return externalReceiverProfileType;
	}
}
