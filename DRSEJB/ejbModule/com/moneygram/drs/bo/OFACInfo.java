package com.moneygram.drs.bo;

public class OFACInfo extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private String customerReceiveNumber;
	private int customerReceiveNumberVersion;
	private String OFACStatus;

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	public int getCustomerReceiveNumberVersion() {
		return customerReceiveNumberVersion;
	}

	public void setCustomerReceiveNumberVersion(int customerReceiveNumberVersion) {
		this.customerReceiveNumberVersion = customerReceiveNumberVersion;
	}

	public String getOFACStatus() {
		return OFACStatus;
	}

	public void setOFACStatus(String status) {
		OFACStatus = status;
	}
}
