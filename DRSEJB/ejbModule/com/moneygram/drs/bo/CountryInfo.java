package com.moneygram.drs.bo;

public class CountryInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String countryCode;
	private java.lang.String countryName;
	private java.lang.String countryLegacyCode;
	private boolean sendActive;
	private boolean receiveActive;
	private boolean directedSendCountry;
	private boolean mgDirectedSendCountry;

	public CountryInfo() {
	}

	public CountryInfo(java.lang.String countryCode, java.lang.String countryName,
			java.lang.String countryLegacyCode, boolean sendActive, boolean receiveActive,
			boolean directedSendCountry, boolean mgDirectedSendCountry) {
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.countryLegacyCode = countryLegacyCode;
		this.sendActive = sendActive;
		this.receiveActive = receiveActive;
		this.directedSendCountry = directedSendCountry;
		this.mgDirectedSendCountry = mgDirectedSendCountry;
	}

	/**
	 * Gets the countryCode value for this CountryInfo.
	 * 
	 * @return countryCode
	 */
	public java.lang.String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the countryCode value for this CountryInfo.
	 * 
	 * @param countryCode
	 */
	public void setCountryCode(java.lang.String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the countryName value for this CountryInfo.
	 * 
	 * @return countryName
	 */
	public java.lang.String getCountryName() {
		return countryName;
	}

	/**
	 * Sets the countryName value for this CountryInfo.
	 * 
	 * @param countryName
	 */
	public void setCountryName(java.lang.String countryName) {
		this.countryName = countryName;
	}

	/**
	 * Gets the countryLegacyCode value for this CountryInfo.
	 * 
	 * @return countryLegacyCode
	 */
	public java.lang.String getCountryLegacyCode() {
		return countryLegacyCode;
	}

	/**
	 * Sets the countryLegacyCode value for this CountryInfo.
	 * 
	 * @param countryLegacyCode
	 */
	public void setCountryLegacyCode(java.lang.String countryLegacyCode) {
		this.countryLegacyCode = countryLegacyCode;
	}

	/**
	 * Gets the sendActive value for this CountryInfo.
	 * 
	 * @return sendActive
	 */
	public boolean isSendActive() {
		return sendActive;
	}

	/**
	 * Sets the sendActive value for this CountryInfo.
	 * 
	 * @param sendActive
	 */
	public void setSendActive(boolean sendActive) {
		this.sendActive = sendActive;
	}

	/**
	 * Gets the receiveActive value for this CountryInfo.
	 * 
	 * @return receiveActive
	 */
	public boolean isReceiveActive() {
		return receiveActive;
	}

	/**
	 * Sets the receiveActive value for this CountryInfo.
	 * 
	 * @param receiveActive
	 */
	public void setReceiveActive(boolean receiveActive) {
		this.receiveActive = receiveActive;
	}

	/**
	 * Gets the directedSendCountry value for this CountryInfo.
	 * 
	 * @return directedSendCountry
	 */
	public boolean isDirectedSendCountry() {
		return directedSendCountry;
	}

	/**
	 * Sets the directedSendCountry value for this CountryInfo.
	 * 
	 * @param directedSendCountry
	 */
	public void setDirectedSendCountry(boolean directedSendCountry) {
		this.directedSendCountry = directedSendCountry;
	}

	/**
	 * Gets the mgDirectedSendCountry value for this CountryInfo.
	 * 
	 * @return mgDirectedSendCountry
	 */
	public boolean isMgDirectedSendCountry() {
		return mgDirectedSendCountry;
	}

	/**
	 * Sets the mgDirectedSendCountry value for this CountryInfo.
	 * 
	 * @param mgDirectedSendCountry
	 */
	public void setMgDirectedSendCountry(boolean mgDirectedSendCountry) {
		this.mgDirectedSendCountry = mgDirectedSendCountry;
	}
}
