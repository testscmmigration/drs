/*
 * Created on Feb 11, 2005
 *
 */
package com.moneygram.drs.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.drs.util.ObjectUtils;

/**
 * The collection of registration field infos that is morphed to xml replies.
 * This class sorts the inital list into several hashtables keyed by xml tag for
 * quickness in iteration.
 * 
 * @author T007 (S. L. Schrader)
 * 
 */
public class RegistrationFieldsCollection extends BusinessObject {
	private static final long serialVersionUID = 1L;
	protected Map<String, List<ExtendedRegistrationFieldInfo>> fullRegistrationFieldMap = new HashMap<String, List<ExtendedRegistrationFieldInfo>>();
	protected List<ExtendedRegistrationFieldInfo> requiredList = new ArrayList<ExtendedRegistrationFieldInfo>();
	protected Map<String, ExtendedRegistrationFieldInfo> checkDigitMap = new HashMap<String, ExtendedRegistrationFieldInfo>();
	protected Map<String, ExtendedRegistrationFieldInfo> regExpMap = new HashMap<String, ExtendedRegistrationFieldInfo>();
	protected Map<String, ExtendedRegistrationFieldInfo> enumedList = new HashMap<String, ExtendedRegistrationFieldInfo>();
	protected Map<String, ExtendedRegistrationFieldInfo> readOnlyMap = new HashMap<String, ExtendedRegistrationFieldInfo>();
	protected Map<String, ExtendedRegistrationFieldInfo> confirmMap = new HashMap<String, ExtendedRegistrationFieldInfo>();
	
	// ADBOP-238 Support
	protected Map<String, List<ExtendedRegistrationFieldInfo>> xmlTagMap = new HashMap<String, List<ExtendedRegistrationFieldInfo>>();
	protected Map<String, List<ExtendedRegistrationFieldInfo>> newXmlTagMap = new HashMap<String, List<ExtendedRegistrationFieldInfo>>();
	
	private String dccCardRange;
	private boolean supplementalFieldExist;

	public RegistrationFieldsCollection(List<ExtendedRegistrationFieldInfo> registrationFields, String dccCardRange, boolean supplementalFlag,boolean saveRegistrationWithNewXmlTags) {
		buildLists(registrationFields,saveRegistrationWithNewXmlTags);
		requiredList = Collections.unmodifiableList(requiredList);
		enumedList = Collections.unmodifiableMap(enumedList);
		regExpMap = Collections.unmodifiableMap(regExpMap);
		confirmMap = Collections.unmodifiableMap(confirmMap);
		checkDigitMap = Collections.unmodifiableMap(checkDigitMap);
		this.dccCardRange = dccCardRange;
		this.supplementalFieldExist = supplementalFlag;
	}

	public boolean isSupplementalFieldExist() {
		return supplementalFieldExist;
	}

	public void setSupplementalFieldExist(boolean supplementalFieldExist) {
		this.supplementalFieldExist = supplementalFieldExist;
	}

	protected void buildLists(List entries, boolean saveRegistrationWithNewXmlTags) {
		ArrayList<ExtendedRegistrationFieldInfo> arrList =null;
		// For each entry, sort into a number of different lists based on
		// attributes
		for (int i = 0; i < entries.size(); ++i) {
			ExtendedRegistrationFieldInfo entry = (ExtendedRegistrationFieldInfo) entries.get(i);
			String xmlTag = entry.getXmlTag();
			ObjectUtils.addEntryToList(xmlTagMap, xmlTag, entry);
			String newXmlTag = entry.getNewXmlTag();
			ObjectUtils.addEntryToList(newXmlTagMap, newXmlTag, entry);
			loadAllMaps(entry,xmlTag,arrList);
			if (saveRegistrationWithNewXmlTags) {
				loadAllMaps(entry,newXmlTag,arrList);
			}
		}
	}

	protected void loadAllMaps (ExtendedRegistrationFieldInfo entry , String xmlTag, ArrayList<ExtendedRegistrationFieldInfo> arrList) {
		if (entry.isRequired()) {
			requiredList.add(entry);
		}
		if (entry.isEnumerated())
		{
			enumedList.put(xmlTag, entry);
		}
		if (entry.getValidationRegEx() != null
				&& entry.getValidationRegEx().trim().length() != 0)
		{
		    regExpMap.put(xmlTag, entry);
		}
		if (entry.isReadOnly())
		{
		    readOnlyMap.put(xmlTag, entry);
		}
		if (entry.isConfirmEntry())
		{
		    confirmMap.put(xmlTag, entry);
		}
		if (entry.getCheckDigitAlgorithm() != null
				&& entry.getCheckDigitAlgorithm().trim().length() != 0)
		{
			checkDigitMap.put(xmlTag, entry);
		}
		
		// Added for 56737 GR project - July-2016			
		if ((null != fullRegistrationFieldMap && !fullRegistrationFieldMap
				.isEmpty()) && fullRegistrationFieldMap.containsKey(xmlTag)) {
			fullRegistrationFieldMap.get(xmlTag).add(entry);
		} else {
			arrList = new ArrayList<ExtendedRegistrationFieldInfo>();
			arrList.add(entry);
			fullRegistrationFieldMap.put(xmlTag, arrList);
		}
	}

	public boolean isEntryReadOnly(String xmlTag) {
		return readOnlyMap.containsKey(xmlTag);
	}

	public List<ExtendedRegistrationFieldInfo> getRequiredFields() {
		return requiredList;
	}

	public Map getRegExpMap() {
		return regExpMap;
	}

	public String getRegExpString(String xmlTagName) {
		RegistrationFieldInfo rfi = (RegistrationFieldInfo) regExpMap.get(xmlTagName);
		if (rfi != null)
			return rfi.getValidationRegEx();
		return null;
	}

	public Collection getEnumeratedList() {
		return enumedList.values();
	}

	public String getCheckDigitAlg(String xmlTagName) {
		ExtendedRegistrationFieldInfo rfi = (ExtendedRegistrationFieldInfo) checkDigitMap
				.get(xmlTagName);
		if (rfi != null)
			return rfi.getCheckDigitAlgorithm();
		return null;
	}

	public Collection getEnueratedValuesList(String xmlTagName) {
		RegistrationFieldInfo rfi = (RegistrationFieldInfo) enumedList.get(xmlTagName);
		if (rfi != null)
			return rfi.getEnumeratedValues();
		return null;
	}

	public int getFullListCount() {
		return fullRegistrationFieldMap.size();
	}

	public Collection<List<ExtendedRegistrationFieldInfo>> getFullList() {
		return fullRegistrationFieldMap.values();
	}

	public List<ExtendedRegistrationFieldInfo> getInfoEntryOnFullList(String xmlTagName) {
		List<ExtendedRegistrationFieldInfo> rfi =  fullRegistrationFieldMap.get(xmlTagName);
			if(rfi == null)	
			{
				rfi=new ArrayList<ExtendedRegistrationFieldInfo>();
			}
		return rfi;
	}

	public RegistrationFieldInfo getInfoEntryOnConfirmList(String xmlTagName) {
		RegistrationFieldInfo rfi = (RegistrationFieldInfo) confirmMap.get(xmlTagName);
		return rfi;
	}

	public boolean isConfirmEntry(String xmlTagName) {
		return confirmMap.containsKey(xmlTagName);
	}

	public String getDccCardRange() {
		return dccCardRange;
	}

	public void setDccCardRange(String dccCardRange) {
		this.dccCardRange = dccCardRange;
	}

	public Map<String, List<ExtendedRegistrationFieldInfo>> getMutableXmlTagMap() {
		return new HashMap<String, List<ExtendedRegistrationFieldInfo>>(
				xmlTagMap);
	}
	
	public ExtendedRegistrationFieldInfo getFirstEntryForXmlTag(String xmlTag) {
		List<ExtendedRegistrationFieldInfo> list = xmlTagMap.get(xmlTag);
		if(list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	public ExtendedRegistrationFieldInfo getFirstEntryForNewXmlTag(String newXmlTag) {
		List<ExtendedRegistrationFieldInfo> list = newXmlTagMap.get(newXmlTag);
		if(list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
}
