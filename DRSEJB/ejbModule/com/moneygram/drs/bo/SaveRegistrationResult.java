package com.moneygram.drs.bo;

public class SaveRegistrationResult extends BusinessObject {
	private static final long serialVersionUID = 1L;

	private String accountNickname;
	private String accountNumberLastFour;
	private int receiverCustomerSequenceID;
	
	public String getAccountNickname() {
		return accountNickname;
	}
	
	public void setAccountNickname(String accountNickname) {
		this.accountNickname = accountNickname;
	}
	
	public String getAccountNumberLastFour() {
		return accountNumberLastFour;
	}
	
	public void setAccountNumberLastFour(String accountNumberLastFour) {
		this.accountNumberLastFour = accountNumberLastFour;
	}

	public int getReceiverCustomerSequenceID() {
		return receiverCustomerSequenceID;
	}

	public void setReceiverCustomerSequenceID(int receiverCustomerSequenceID) {
		this.receiverCustomerSequenceID = receiverCustomerSequenceID;
	}
}
