/**
 * DataTypeCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.moneygram.drs.bo;

public class DataTypeCode extends BusinessObject implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String _value_;
	private static java.util.HashMap<String, DataTypeCode> _table_ = new java.util.HashMap<String, DataTypeCode>();

	// Constructor
	protected DataTypeCode(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	}
	public static final java.lang.String _value1 = "boolean";
	public static final java.lang.String _value2 = "string";
	public static final java.lang.String _value3 = "int";
	public static final java.lang.String _value4 = "decimal";
	public static final java.lang.String _value5 = "date";
	public static final java.lang.String _value6 = "datetime";
	public static final java.lang.String _value7 = "time";
	public static final java.lang.String _value8 = "text";
	public static final java.lang.String _value9 = "cntrycode";
	public static final DataTypeCode value1 = new DataTypeCode(_value1);
	public static final DataTypeCode value2 = new DataTypeCode(_value2);
	public static final DataTypeCode value3 = new DataTypeCode(_value3);
	public static final DataTypeCode value4 = new DataTypeCode(_value4);
	public static final DataTypeCode value5 = new DataTypeCode(_value5);
	public static final DataTypeCode value6 = new DataTypeCode(_value6);
	public static final DataTypeCode value7 = new DataTypeCode(_value7);
	public static final DataTypeCode value8 = new DataTypeCode(_value8);
	public static final DataTypeCode value9 = new DataTypeCode(_value9);

	public java.lang.String getValue() {
		return _value_;
	}

	public static DataTypeCode fromValue(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		DataTypeCode enumeration = (DataTypeCode) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static DataTypeCode fromString(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

	public java.lang.Object readResolve() throws java.io.ObjectStreamException {
		return fromValue(_value_);
	}
}
