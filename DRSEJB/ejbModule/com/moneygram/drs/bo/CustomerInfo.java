package com.moneygram.drs.bo;

public class CustomerInfo extends BusinessObject implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private String deliveryOption;
	private String agentId;
	private CustomerProfile customerProfile;
	private boolean intraTransaction;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	public boolean isIntraTransaction() {
		return intraTransaction;
	}

	public void setIntraTransaction(boolean intraTransaction) {
		this.intraTransaction = intraTransaction;
	}
	
}
