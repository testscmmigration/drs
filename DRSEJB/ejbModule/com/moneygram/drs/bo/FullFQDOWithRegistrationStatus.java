/*
 * Created on Feb 28, 2005
 *
 */
package com.moneygram.drs.bo;

/**
 * @author T007
 * 
 */
public class FullFQDOWithRegistrationStatus extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private FQDOInfo fqdoInfo;
	private String registrationStatusCode;

	/**
	 * @return
	 */
	public FQDOInfo getFqdoInfo() {
		return fqdoInfo;
	}

	/**
	 * @return
	 */
	public String getRegistrationStatusCode() {
		return registrationStatusCode;
	}

	/**
	 * @param info
	 */
	public void setFqdoInfo(FQDOInfo info) {
		fqdoInfo = info;
	}

	/**
	 * @param string
	 */
	public void setRegistrationStatusCode(String string) {
		registrationStatusCode = string;
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("GetFQDOByCustomerReceiveNumberResponse[");
		buffer.append("fqdoInfo = ").append(fqdoInfo);
		buffer.append(", registrationStatusCode = ").append(registrationStatusCode);
		buffer.append("]");
		return buffer.toString();
	}
}
