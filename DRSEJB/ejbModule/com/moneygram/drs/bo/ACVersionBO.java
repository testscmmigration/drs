package com.moneygram.drs.bo;

import com.moneygram.drs.constants.DRSConstants;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.util.ObjectUtils;

/**
 * When not an AC call, version will be 0.
 * 
 * @author a210
 * 
 */
public class ACVersionBO {
	public static ACVersionBO create(String acVersion) {
		ACVersionBO acVersionBO = new ACVersionBO();
		acVersionBO.setAcVersion(acVersion);
		return acVersionBO;
	}

	private String acVersion;

	public String getAcVersion() {
		return acVersion;
	}

	public void setAcVersion(String acVersion) {
		this.acVersion = acVersion;
	}

	private boolean isACVersionLessThan1611(String acVersion) {
		return !ObjectUtils.isRequestGreaterThanOrEqualToVersion(acVersion,
				DRSConstants.API_VERSION_1611);
	}

	public boolean calculateHandleSuppressReEnterFields(
			SaveRegistrationRequest request) {
		// Dual account entry is only implemented in 1808 (or greater) or when
		// AC version supports.
		return !request.isSupportsReenterFields() || isACVersionLessThan1611(acVersion);
	}
}