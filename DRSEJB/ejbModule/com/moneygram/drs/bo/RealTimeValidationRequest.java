package com.moneygram.drs.bo;

import java.util.HashMap;
import java.util.Map;

import com.moneygram.drs.request.SaveRegistrationRequest;

public class RealTimeValidationRequest extends BusinessObject {
	private static final long serialVersionUID = 1L;
	private CustomerProfile customerProfile;
	private CustomerProfile existingProfile;
	private SaveRegistrationRequest saveRegistrationRequest;
	private boolean creating;
	protected Map<String, String> context = new HashMap<String, String>();

	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	public CustomerProfile getExistingProfile() {
		return existingProfile;
	}

	public void setExistingProfile(CustomerProfile existingProfile) {
		this.existingProfile = existingProfile;
	}

	public SaveRegistrationRequest getSaveRegistrationRequest() {
		return saveRegistrationRequest;
	}

	public void setSaveRegistrationRequest(SaveRegistrationRequest saveRegistrationRequest) {
		this.saveRegistrationRequest = saveRegistrationRequest;
	}
	
	public Map<String, String> getContext() {
		return context;
	}

	public void setContext(Map<String, String> context) {
		this.context = context;
	}

	public boolean isCreating() {
		return creating;
	}

	public void setCreating(boolean creating) {
		this.creating = creating;
	}
}
