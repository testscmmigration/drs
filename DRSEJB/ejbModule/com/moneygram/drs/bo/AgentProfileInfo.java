package com.moneygram.drs.bo;

import java.io.Serializable;

public class AgentProfileInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	protected String legacyAgentID;
    protected String legacyUnitOffice;
    protected String lmsAgentID;
    protected String parentAgentID;
    protected String mainOfficeID;
    protected String agentHierarchyLevel;
    protected String agentAddressLine1;
    protected String agentAddressLine2;
    protected String agentAddressLine3;
    protected String agentCity;
    protected String agentState;
    protected String agentZipCode;
    protected String agentIsoCountry;
    protected String agentIso2Country;
    protected String agentCountryName;
    protected String agentDMA;
    protected String agentTypeID;
    protected String agentStatusCode;
    protected String agentName;
    protected String agentDBA;
    protected String agentPhoneNumber;
    protected String agentTaxID;
    protected String agentTimeZone;
    protected String hqPartyID;
    protected Boolean isRetailCreditFlag;
    protected String custAcctSiteId;
    protected String agentCounty;

    /**
     * Gets the value of the legacyAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegacyAgentID() {
        return legacyAgentID;
    }

    /**
     * Sets the value of the legacyAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegacyAgentID(String value) {
        this.legacyAgentID = value;
    }

    /**
     * Gets the value of the legacyUnitOffice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegacyUnitOffice() {
        return legacyUnitOffice;
    }

    /**
     * Sets the value of the legacyUnitOffice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegacyUnitOffice(String value) {
        this.legacyUnitOffice = value;
    }

    /**
     * Gets the value of the lmsAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLmsAgentID() {
        return lmsAgentID;
    }

    /**
     * Sets the value of the lmsAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLmsAgentID(String value) {
        this.lmsAgentID = value;
    }

    /**
     * Gets the value of the parentAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentAgentID() {
        return parentAgentID;
    }

    /**
     * Sets the value of the parentAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentAgentID(String value) {
        this.parentAgentID = value;
    }

    /**
     * Gets the value of the mainOfficeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainOfficeID() {
        return mainOfficeID;
    }

    /**
     * Sets the value of the mainOfficeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainOfficeID(String value) {
        this.mainOfficeID = value;
    }

    /**
     * Gets the value of the agentHierarchyLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentHierarchyLevel() {
        return agentHierarchyLevel;
    }

    /**
     * Sets the value of the agentHierarchyLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentHierarchyLevel(String value) {
        this.agentHierarchyLevel = value;
    }

    /**
     * Gets the value of the agentAddressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentAddressLine1() {
        return agentAddressLine1;
    }

    /**
     * Sets the value of the agentAddressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentAddressLine1(String value) {
        this.agentAddressLine1 = value;
    }

    /**
     * Gets the value of the agentAddressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentAddressLine2() {
        return agentAddressLine2;
    }

    /**
     * Sets the value of the agentAddressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentAddressLine2(String value) {
        this.agentAddressLine2 = value;
    }

    /**
     * Gets the value of the agentAddressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentAddressLine3() {
        return agentAddressLine3;
    }

    /**
     * Sets the value of the agentAddressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentAddressLine3(String value) {
        this.agentAddressLine3 = value;
    }

    /**
     * Gets the value of the agentCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCity() {
        return agentCity;
    }

    /**
     * Sets the value of the agentCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCity(String value) {
        this.agentCity = value;
    }

    /**
     * Gets the value of the agentState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentState() {
        return agentState;
    }

    /**
     * Sets the value of the agentState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentState(String value) {
        this.agentState = value;
    }

    /**
     * Gets the value of the agentZipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentZipCode() {
        return agentZipCode;
    }

    /**
     * Sets the value of the agentZipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentZipCode(String value) {
        this.agentZipCode = value;
    }

    /**
     * Gets the value of the agentIsoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentIsoCountry() {
        return agentIsoCountry;
    }

    /**
     * Sets the value of the agentIsoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentIsoCountry(String value) {
        this.agentIsoCountry = value;
    }

    /**
     * Gets the value of the agentIso2Country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentIso2Country() {
        return agentIso2Country;
    }

    /**
     * Sets the value of the agentIso2Country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentIso2Country(String value) {
        this.agentIso2Country = value;
    }

    /**
     * Gets the value of the agentCountryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCountryName() {
        return agentCountryName;
    }

    /**
     * Sets the value of the agentCountryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCountryName(String value) {
        this.agentCountryName = value;
    }

    /**
     * Gets the value of the agentDMA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDMA() {
        return agentDMA;
    }

    /**
     * Sets the value of the agentDMA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDMA(String value) {
        this.agentDMA = value;
    }

    /**
     * Gets the value of the agentTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentTypeID() {
        return agentTypeID;
    }

    /**
     * Sets the value of the agentTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentTypeID(String value) {
        this.agentTypeID = value;
    }

    /**
     * Gets the value of the agentStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentStatusCode() {
        return agentStatusCode;
    }

    /**
     * Sets the value of the agentStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentStatusCode(String value) {
        this.agentStatusCode = value;
    }

    /**
     * Gets the value of the agentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * Sets the value of the agentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentName(String value) {
        this.agentName = value;
    }

    /**
     * Gets the value of the agentDBA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDBA() {
        return agentDBA;
    }

    /**
     * Sets the value of the agentDBA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDBA(String value) {
        this.agentDBA = value;
    }

    /**
     * Gets the value of the agentPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    /**
     * Sets the value of the agentPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentPhoneNumber(String value) {
        this.agentPhoneNumber = value;
    }

    /**
     * Gets the value of the agentTaxID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentTaxID() {
        return agentTaxID;
    }

    /**
     * Sets the value of the agentTaxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentTaxID(String value) {
        this.agentTaxID = value;
    }

    /**
     * Gets the value of the agentTimeZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentTimeZone() {
        return agentTimeZone;
    }

    /**
     * Sets the value of the agentTimeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentTimeZone(String value) {
        this.agentTimeZone = value;
    }

    /**
     * Gets the value of the hqPartyID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHQPartyID() {
        return hqPartyID;
    }

    /**
     * Sets the value of the hqPartyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHQPartyID(String value) {
        this.hqPartyID = value;
    }

    /**
     * Gets the value of the isRetailCreditFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsRetailCreditFlag() {
        return isRetailCreditFlag;
    }

    /**
     * Sets the value of the isRetailCreditFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRetailCreditFlag(Boolean value) {
        this.isRetailCreditFlag = value;
    }

    /**
     * Gets the value of the custAcctSiteId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustAcctSiteId() {
        return custAcctSiteId;
    }

    /**
     * Sets the value of the custAcctSiteId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustAcctSiteId(String value) {
        this.custAcctSiteId = value;
    }

    /**
     * Gets the value of the agentCounty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCounty() {
        return agentCounty;
    }

    /**
     * Sets the value of the agentCounty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCounty(String value) {
        this.agentCounty = value;
    }



}
