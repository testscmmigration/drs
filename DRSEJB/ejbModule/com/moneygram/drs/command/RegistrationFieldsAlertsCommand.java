/**
 * 
 */
package com.moneygram.drs.command;

/**
 * @author bai2
 *
 */
import java.util.ArrayList;



import java.util.Collection;
import java.util.List;

import java.util.Date;

import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.RegistrationFieldsAlertsRequest;
import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;


public class RegistrationFieldsAlertsCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		ObjectId objectId = null;
		AlertRegistrationInfo alertRegistrationInfo = null;
		RegistrationFieldsAlertsRequest alertRegistrationFieldsRequest = (RegistrationFieldsAlertsRequest) request;
		// TODO Make sure the validation is done in Stored proc... US
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(alertRegistrationFieldsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(alertRegistrationFieldsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(alertRegistrationFieldsRequest.getLanguage());
		boolean isRegistrationAllowed = PersistenceManagerFactory.getInstance().assertData(
				dsrCriteria);
		if (!isRegistrationAllowed) {
			throw new DRSException(DRSExceptionCodes.EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM,
					"Cannot use MoneyGram to register agentId:", "receiveAgentId", new Date(),
					" deliveryOption: " + alertRegistrationFieldsRequest.getDeliveryOption()
					+ alertRegistrationFieldsRequest.getReceiveAgentID(), CLIENT);
		}
		// Reading registration collection
		registrationFieldsInfoCriteria.setDeliveryOption(alertRegistrationFieldsRequest
				.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(alertRegistrationFieldsRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(alertRegistrationFieldsRequest
				.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(alertRegistrationFieldsRequest
				.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(alertRegistrationFieldsRequest
				.getReceiveCurrency());
		//FieldAlerts API is used by ESB for AC higher version. Should not suppress the reEnterFields
		registrationFieldsInfoCriteria.setSuppressReEnterFields(false);
		//registrationFieldsInfoCriteria.setRegFldsAlrtsCrit(true); //Used to derive the xml tag to display either partners field or GetAllField
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		RegistrationFieldsCollection registrationFieldsCollection = (RegistrationFieldsCollection) PersistenceManagerFactory
		.getInstance().read(objectId);
		if (registrationFieldsCollection == null) {
			throw new DRSException(DRSExceptionCodes.EC515_NO_FIELDS_FOUND_FOR_REGISTRATION,
					"No registration fields found for agent:", "receiveAgentId", new Date(),
					" deliveryOption: " + alertRegistrationFieldsRequest.getDeliveryOption()
					+ "Receive Agent Id: "
					+ alertRegistrationFieldsRequest.getReceiveAgentID(), CLIENT);
		}


		List<AlertErrorInfo> errorList = new ArrayList<AlertErrorInfo>();

		for(Collection<ExtendedRegistrationFieldInfo> registrationFieldData:registrationFieldsCollection
				.getFullList()){

			AlertErrorInfo errorInfo = new AlertErrorInfo();
			errorInfo.setDefaultErrorInfo();
			String newXmlTag = registrationFieldData.iterator().next().getNewXmlTag();  
			if (!StringUtility.isNullOrEmpty(newXmlTag)) {
				registrationFieldData.iterator().next().setXmlTag(newXmlTag.toUpperCase());
				errorInfo.setRegistrationFieldData(registrationFieldData.iterator().next());
				errorList.add(errorInfo);
			}			

		}

		alertRegistrationInfo = new AlertRegistrationInfo();
		alertRegistrationInfo.setDefaultAlertInfo();
		alertRegistrationInfo.setErrorData(errorList);			

		this.result = alertRegistrationInfo;
	}
}
