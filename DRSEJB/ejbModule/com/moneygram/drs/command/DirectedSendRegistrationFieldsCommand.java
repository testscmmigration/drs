package com.moneygram.drs.command;

import java.util.Collection;
import java.util.Date;
import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.DirectedSendRegistrationFieldsRequest;

public class DirectedSendRegistrationFieldsCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		FQDOCriteria fqdoCriteria = null;
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		ObjectId objectId = null;
		DirectedSendRegistrationInfo directedSendRegistrationInfo = null;
		DirectedSendRegistrationFieldsRequest directedSendRegistrationFieldsRequest = (DirectedSendRegistrationFieldsRequest) request;
		// TODO Make sure the validation is done in Stored proc... US
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(directedSendRegistrationFieldsRequest.getReceiveAgentID());
		dsrCriteria.setLanguage(directedSendRegistrationFieldsRequest.getLanguage());
		boolean isRegistrationAllowed = PersistenceManagerFactory.getInstance().assertData(
				dsrCriteria);
		if (!isRegistrationAllowed) {
			throw new DRSException(DRSExceptionCodes.EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM,
					"Cannot use MoneyGram to register agentId:", "receiveAgentId", new Date(),
					" deliveryOption: " + directedSendRegistrationFieldsRequest.getDeliveryOption()
							+ directedSendRegistrationFieldsRequest.getReceiveAgentID(), CLIENT);
		}
		// Reading registration collection
		registrationFieldsInfoCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest
				.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(directedSendRegistrationFieldsRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(directedSendRegistrationFieldsRequest
				.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(directedSendRegistrationFieldsRequest
				.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(directedSendRegistrationFieldsRequest
				.getReceiveCurrency());
		//SupressReEnter fields to true. This operation is used by lower AC versions. 
		registrationFieldsInfoCriteria.setSuppressReEnterFields(true);
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		RegistrationFieldsCollection registrationFieldsCollection = (RegistrationFieldsCollection) PersistenceManagerFactory
				.getInstance().read(objectId);
		if (registrationFieldsCollection == null) {
			throw new DRSException(DRSExceptionCodes.EC515_NO_FIELDS_FOUND_FOR_REGISTRATION,
					"No registration fields found for agent:", "receiveAgentId", new Date(),
					" deliveryOption: " + directedSendRegistrationFieldsRequest.getDeliveryOption()
							+ "Receive Agent Id: "
							+ directedSendRegistrationFieldsRequest.getReceiveAgentID(), CLIENT);
		}
		// Reading FQDOInfo data
		fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setReceiveCountry(directedSendRegistrationFieldsRequest.getReceiveCountry());
		fqdoCriteria.setDeliveryOption(directedSendRegistrationFieldsRequest.getDeliveryOption());
		fqdoCriteria.setReceiveAgentID(directedSendRegistrationFieldsRequest.getReceiveAgentID());
		fqdoCriteria.setReceiveCurrency(directedSendRegistrationFieldsRequest.getReceiveCurrency());
		fqdoCriteria.setLanguage(directedSendRegistrationFieldsRequest.getLanguage());
		fqdoCriteria.setFqdoEnum(FqdoEnum.FQDO);
		objectId = new ObjectId(FQDOInfo.class, fqdoCriteria);
		FQDOInfo fqdoInfo = (FQDOInfo) PersistenceManagerFactory.getInstance().read(objectId);
		directedSendRegistrationInfo = new DirectedSendRegistrationInfo();
		directedSendRegistrationInfo.setFqdoInfo(fqdoInfo);
		for(Collection<ExtendedRegistrationFieldInfo> registrationFieldData:registrationFieldsCollection
				.getFullList()){
		directedSendRegistrationInfo.setRegistrationFieldData(registrationFieldData);
		}
		
		directedSendRegistrationInfo.setSupplementalFlag(registrationFieldsCollection.isSupplementalFieldExist());
		
		this.result = directedSendRegistrationInfo;
	}
}
