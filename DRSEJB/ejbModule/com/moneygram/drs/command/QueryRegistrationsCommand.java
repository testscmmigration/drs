package com.moneygram.drs.command;

import java.util.List;
import com.moneygram.drs.criteria.QueryRegistrationsCriteria;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.request.QueryRegistrationsRequest;

public class QueryRegistrationsCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		QueryRegistrationsRequest queryRegistrationsRequest = (QueryRegistrationsRequest) request;
		QueryRegistrationsCriteria queryRegistrationsCriteria = new QueryRegistrationsCriteria();
		queryRegistrationsCriteria.setAccountNumberLast4(queryRegistrationsRequest
				.getAccountNumberLast4());
		queryRegistrationsCriteria.setDeliveryOption(queryRegistrationsRequest.getDeliveryOption());
		queryRegistrationsCriteria.setReceiveAgentID(queryRegistrationsRequest.getReceiveAgentID());
		queryRegistrationsCriteria.setReceiveCountry(queryRegistrationsRequest.getReceiveCountry());
		queryRegistrationsCriteria.setReceiveCurrency(queryRegistrationsRequest
				.getReceiveCurrency());
		queryRegistrationsCriteria.setReceiverFirstName(queryRegistrationsRequest
				.getReceiverFirstName());
		queryRegistrationsCriteria.setReceiverLastName(queryRegistrationsRequest
				.getReceiverLastName());
		queryRegistrationsCriteria.setReceiverPhoneNumber(queryRegistrationsRequest
				.getReceiverPhoneNumber());
		queryRegistrationsCriteria.setLanguage(queryRegistrationsRequest.getLanguage());
		List registrationInfo = PersistenceManagerFactory.getInstance().find(
				queryRegistrationsCriteria);
		this.result = registrationInfo;
	}
}
