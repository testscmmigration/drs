package com.moneygram.drs.command;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.moneygram.common.jms.MessagingException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.mgEnum.EnumRegistry;
import com.moneygram.common.util.PanIdInfo;
import com.moneygram.common.util.PanIdentification;
import com.moneygram.drs.bo.ACVersionBO;
import com.moneygram.drs.bo.AuthenticationData;
import com.moneygram.drs.bo.CustomerInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.bo.SaveRegistrationCommandResponse;
import com.moneygram.drs.bo.SaveRegistrationResult;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.constants.DRSConstants;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.AuthenticationCodeCriteria;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.get_send.CollectionsContext;
import com.moneygram.drs.get_send.FieldsCollectionRequest;
import com.moneygram.drs.get_send.FieldsCollectionService;
import com.moneygram.drs.get_send.SaveRegistrationCommandHelper;
import com.moneygram.drs.notification.NotificationManagerFactory;
import com.moneygram.drs.pci.PciTranslator;
import com.moneygram.drs.pciservicemanager.PCIServiceServiceFacade;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.persistence.PhoneCountryCodeInfoDAO;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.SaveRegistrationRequest;
import com.moneygram.drs.util.ObjectUtils;
import com.moneygram.drs.validators.SaveRegistrationValidator;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapper;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapperFactory;
import com.moneygram.drs.xtra.validators.RealTimeValidator;
import com.moneygram.drs.xtra.validators.XtraValidators;
import com.moneygram.drs.xtravalidators.factory.XtraValidatorsFactory;

public class SaveRegistrationCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;
	public String SERVER = "Server";
	private static Logger log = LogFactory.getInstance().getLogger(
			SaveRegistrationCommand.class);

	FieldsCollectionService fieldsCollectionService = new FieldsCollectionService();

	protected void doExecute() throws Exception {

		PciTranslator pciTranslator = new PciTranslator();
		CustomerInfo customerInfo = new CustomerInfo();
		RegistrationStatusCode statusCode = null;
		PanIdInfo panIdInfo = new PanIdInfo();

		SaveRegistrationRequest saveRegistrationRequest = (SaveRegistrationRequest) request;
		SaveRegistrationCommandHelper getSendHelper = new SaveRegistrationCommandHelper(
				saveRegistrationRequest,
				PersistenceManagerFactory
						.getDaoByType(PhoneCountryCodeInfoDAO.class));
		saveRegistrationRequest
				.setCustomerReceiveNumber(convertMGCRNtoUpperCase(saveRegistrationRequest
						.getCustomerReceiveNumber()));

		boolean creating = saveRegistrationRequest.getCustomerReceiveNumber() == null;
		String apiVersion = saveRegistrationRequest.getApiVersion();
		boolean saveRegistrationWithNewXmlTags = false;
		// Check whether the registration is allowed for the details in request
		// object
		boolean isRegistrationAllowed = checkRegistrationAllowed(saveRegistrationRequest);
		if (!isRegistrationAllowed) {
			throw new DRSException(
					DRSExceptionCodes.EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM,
					saveRegistrationRequest.getReceiveAgentID(),
					"receiveAgentId", new Date(),
					"Cannot use MoneyGram to register agentId", CLIENT);
		}
		StandardFieldXMLTagMapper fieldXMLTagMapperImpl = StandardFieldXMLTagMapperFactory
				.getInstance();
		if(!StringUtils.isEmpty(apiVersion)){
			saveRegistrationRequest.setAcVersionBO(ACVersionBO.create(apiVersion));
		}else{
			saveRegistrationRequest.setAcVersionBO(ACVersionBO.create("0"));
		}
		// Changes done for 30665i Send POE
		if (!StringUtils.isEmpty(apiVersion)
				&& ObjectUtils.isRequestGreaterThanOrEqualToVersion(apiVersion,
						DRSConstants.API_VERSION_1611)) {
			saveRegistrationWithNewXmlTags = true;
		}
		FieldsCollectionRequest fieldsCollectionRequest = createFieldsCollectionRequest(
				saveRegistrationRequest, saveRegistrationWithNewXmlTags);
		CollectionsContext collectionsContext = fieldsCollectionService
				.executeForContext(fieldsCollectionRequest);
		RegistrationFieldsCollection registrationFieldsCollection = collectionsContext.getMergedFieldsCollection();
		SaveRegistrationValidator.validateRegistrationRequest(
				saveRegistrationRequest, registrationFieldsCollection,
				saveRegistrationWithNewXmlTags);

		// Updating save registration request to old xml tag if api version in
		// request is greater than or equal to 1611 for further processing
		if (saveRegistrationWithNewXmlTags) {
			updateSaveRegistrationRequestWithOldTag(saveRegistrationRequest,
					registrationFieldsCollection);
		}
		getSendHelper
				.applyFieldsAfterOldTagUpdate(collectionsContext);
		if (saveRegistrationRequest.getRegistrationStatus() != null)
			statusCode = (RegistrationStatusCode) EnumRegistry.getByName(
					RegistrationStatusCode.class,
					saveRegistrationRequest.getRegistrationStatus());

		// fields mapped in StandardFieldXMLTagMapperImpl including StdRegFieldMappings.xml are mapped.
		CustomerProfile customerProfile;
		try {
			customerProfile = fieldXMLTagMapperImpl
					.convertIntoCustomerProfile(saveRegistrationRequest
							.getFieldValues());
		} catch (DRSException e) {
			throw new DRSException(
					DRSExceptionCodes.EC905_VALIDATION_PROCESSING_ERROR,
					saveRegistrationRequest.getReceiveCountry(),
					"Customer Profile", new Date(),
					"Failed to get converted customer profile", CLIENT);
		}
		customerProfile.setAccountNumberIsToken(saveRegistrationRequest.isAccountNumberIsToken());
		// Perform a PAN id check of accountToken.
		PanIdInfo accountPartnerTokenPanIdInfo = PanIdentification
				.isPanNumber(customerProfile.getRcvAgentCustAcctNbr());
		if (customerProfile.isAccountNumberIsToken() && accountPartnerTokenPanIdInfo.isPan())
			throw new DRSException(
					DRSExceptionCodes.EC355_INVALID_DATA,
					ObjectUtils.getMaskedString(customerProfile.getRcvAgentCustAcctNbr()),
					"accountPartnerToken", new Date(),
					"Account_PartnerToken contains an account number.", CLIENT);

		// Perform a PAN id check of accountBin.
		PanIdInfo accountBinPanIdInfo = PanIdentification
				.isPanNumber(customerProfile.getRcvAgentCustAccountBin());
		if (accountBinPanIdInfo.isPan())
			throw new DRSException(
					DRSExceptionCodes.EC355_INVALID_DATA,
					ObjectUtils.getMaskedString(customerProfile.getRcvAgentCustAccountBin()),
					"accountBin", new Date(),
					"Account_Bin contains an account number.", CLIENT);

		// Perform a PAN id check of accountMGIToken.
		PanIdInfo accountMGITokenPanIdInfo = PanIdentification
				.isPanNumber(customerProfile.getRuleProcessAccountNumber());
		if (accountMGITokenPanIdInfo.isPan())
			throw new DRSException(
					DRSExceptionCodes.EC355_INVALID_DATA,
					ObjectUtils.getMaskedString(customerProfile.getRuleProcessAccountNumber()),
					"accountMGIToken", new Date(),
					"Account_MGIToken contains an account number.", CLIENT);

		CustomerProfile existingProfile = new CustomerProfile();
		if (!creating) {
			CustomerProfileCriteria customerProfileCriteria = setDataForCPCriteria(saveRegistrationRequest);
			ObjectId objectId = new ObjectId(CustomerProfile.class,
					customerProfileCriteria);
			existingProfile = (CustomerProfile) PersistenceManagerFactory
					.getInstance().read(objectId);
			if ("Y".equalsIgnoreCase(StringUtils.trim(existingProfile
					.getRcvAgentCustAcctNbrPanFlag()))
					&& StringUtils.isEmpty(customerProfile
							.getRcvAgentCustAcctNbr())) {
				// IF PAn need to save it in PANDA again otherwise database
				// takes care of it
				customerProfile.setRcvAgentCustAcctNbr(PCIServiceServiceFacade
						.retrieveRRNCardNumber(existingProfile
								.getCustomerReceiveNumber(), Integer
								.toString(existingProfile
										.getCustomerReceiveNumberVersion())));
			}
		}

		getSendHelper.enrichCustProfile(customerProfile, existingProfile);

		// Validation to check whether the card number lies in the card range
		XtraValidators xtraValidators = XtraValidatorsFactory
				.getXtraValidator(registrationFieldsCollection
						.getDccCardRange());
		// pan validation
		if (xtraValidators != null) {
			xtraValidators.validate(saveRegistrationRequest, customerProfile,
					existingProfile, creating);
		}
		setCRNAndRegistrationStatus(
				xtraValidators instanceof RealTimeValidator, creating,
				saveRegistrationRequest.getCustomerReceiveNumber(), statusCode,
				customerProfile, saveRegistrationRequest.getLanguage());

		AuthenticationData authenticationData = getAuthenticationCode(saveRegistrationRequest);
		// Adding code which is required for the PCI compliance when the credit
		// card number
		// is entered.
		// To check whether card number is pan card or not
		String originalAccountNumber = customerProfile.getRcvAgentCustAcctNbr();
		panIdInfo = PanIdentification.isPanNumber(customerProfile
				.getRcvAgentCustAcctNbr());
		log.info("PanIdentification.. " + panIdInfo.isPan());
		if (panIdInfo.isPan()) {
			// Changes for 54351 E AD POE Improvements Nov 2015 Release.
			// Requirement is to display full Account Number in Receipts if PAN
			// is true so commenting below
			customerProfile.setRcvAgentCustAcctNbr(ObjectUtils
					.getMaskedString(customerProfile.getRcvAgentCustAcctNbr()));
			customerProfile.setRcvAgentCustAcctNbrPanFlag("Y");
		} else {
			customerProfile.setRcvAgentCustAcctNbrPanFlag("N");
		}

		if (customerProfile.getAccountNickname() == null) {
			customerProfile.setAccountNickname(existingProfile
					.getAccountNickname());
		}

		// Perform a PAN id check of the nickname.
		PanIdInfo nicknamePanIdInfo = PanIdentification
				.isPanNumber(customerProfile.getAccountNickname());
		if (nicknamePanIdInfo.isPan())
			throw new DRSException(
					DRSExceptionCodes.EC1700_ACCOUNT_NICKNAME_PAN_ID_CHECK_FAILED,
					ObjectUtils.getMaskedString(customerProfile.getAccountNickname()), "PROFILENICKNAME",
					new Date(), "Account Nickname contains an account number.",
					CLIENT);
		
		//Added as part of receiver directed changes
		customerProfile.setExternalReceiverProfileId(saveRegistrationRequest.getExternalReceiverProfileId());
		
		//ADP-1535  
		customerProfile.setExternalReceiverProfileType(saveRegistrationRequest.getExternalReceiverProfileType());

		customerInfo.setCustomerProfile(customerProfile);
		customerInfo.setAgentId(saveRegistrationRequest.getReceiveAgentID());
		customerInfo.setDeliveryOption(saveRegistrationRequest
				.getDeliveryOption());
		customerInfo.setIntraTransaction(saveRegistrationRequest
				.isIntraTransaction());

		customerProfile.setPoeSourceSystem(saveRegistrationRequest
				.getPoeSourceSystem());

		SaveRegistrationResult saveRegistrationResult = (SaveRegistrationResult) PersistenceManagerFactory
				.getInstance().save(customerInfo);

		// WLF Project 2014 - Removed OFAC Check
		// OFACstatus =
		// OFACValidatorImp.getInstance().validateOFAC(customerProfile,
		// existingProfile,
		// creating, saveRegistrationRequest.getLanguage());
		// if (OFACstatus != null) {
		// OFACInfo ofacInfo = new OFACInfo();
		// ofacInfo.setCustomerReceiveNumber(customerProfile.getCustomerReceiveNumber());
		// ofacInfo.setCustomerReceiveNumberVersion(customerProfile
		// .getCustomerReceiveNumberVersion());
		// ofacInfo.setOFACStatus(OFACstatus);
		// PersistenceManagerFactory.getInstance().save(ofacInfo);
		// }
		// WLF Project 2014

		// Get Abbreviated customer profile
		if (!creating) {
			CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
			customerProfileCriteria.setAdminUser(false);
			customerProfileCriteria.setCustomerReceiveNumber(customerProfile
					.getCustomerReceiveNumber());
			customerProfileCriteria.setProfileVersion(0);
			customerProfileCriteria.setAbbrCustomerProfile(true);
			ObjectId objectId = new ObjectId(CustomerProfile.class,
					customerProfileCriteria);
			customerProfile = (CustomerProfile) PersistenceManagerFactory
					.getInstance().read(objectId);
		} else {
			// customerProfile.setOfacStatus(OFACstatus);
		}
		SaveRegistrationCommandResponse commandResponse = convertToSaveRegistrationResponse(
				customerProfile, authenticationData.getAuthenticationCode(),
				saveRegistrationResult);
		// Adding code which is required for the PCI Encryption service call.
		SensitiveIDInfo sensID = pciTranslator.encryptCallOnPCI(
				panIdInfo.isPan(), originalAccountNumber,
				commandResponse.getMgCustomerReceiveNumber(),
				commandResponse.getMgCustomerReceiveNumberVersion(),
				Calendar.getInstance());
		// Sending a notification to queue.
		if (null != sensID) {
			commandResponse.setRuleProcessAccountNumber(sensID
					.getRuleProcessAccountNumber());
			commandResponse.setAccountNumberSensitveID(sensID.getSensitiveID());
		}
		try {
			if (!(customerProfile.getRegistrationStatus().equals(
					RegistrationStatusCode.ACTIVE) && customerProfile
					.getRegistrationSubStatus().equals("ACT"))) {
				NotificationManagerFactory.getInstance()
						.notifyDSSOfRegistrationChange(
								customerProfile.getCustomerReceiveNumber());
			}
		} catch (MessagingException mex) {
			throw new DRSException(
					DRSExceptionCodes.EC912_CANNOT_SEND_MQ_MESSAGE,
					saveRegistrationRequest.getReceiveCountry(),
					"saveRegistration", new Date(), "Cannot Send Mq message",
					CLIENT);
		}
		this.result = commandResponse;
	}

	private boolean checkRegistrationAllowed(
			SaveRegistrationRequest saveRegistrationRequest) throws Exception {
		AssertRegistrationCriteria asrCriteria = new AssertRegistrationCriteria();
		asrCriteria.setDeliveryOption(saveRegistrationRequest
				.getDeliveryOption());
		asrCriteria.setReceiveAgentID(saveRegistrationRequest
				.getReceiveAgentID());
		asrCriteria.setLanguage(saveRegistrationRequest.getLanguage());
		boolean isRegistrationAllowed = PersistenceManagerFactory.getInstance()
				.assertData(asrCriteria);
		return isRegistrationAllowed;
	}

	private AuthenticationData getAuthenticationCode(
			SaveRegistrationRequest saveRegistrationRequest)
			throws NotFoundException, DRSException {
		ObjectId objectId;
		AuthenticationCodeCriteria authenticationCodeCriteria = new AuthenticationCodeCriteria();
		authenticationCodeCriteria.setAgentId(saveRegistrationRequest
				.getReceiveAgentID());
		authenticationCodeCriteria.setDeliveryOption(saveRegistrationRequest
				.getDeliveryOption());
		objectId = new ObjectId(AuthenticationData.class,
				authenticationCodeCriteria);
		AuthenticationData authenticationData = (AuthenticationData) PersistenceManagerFactory
				.getInstance().read(objectId);
		return authenticationData;
	}

	private CustomerProfileCriteria setDataForCPCriteria(
			SaveRegistrationRequest saveRegistrationRequest) {
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(saveRegistrationRequest
				.isSuperUser());
		customerProfileCriteria
				.setCustomerReceiveNumber(saveRegistrationRequest
						.getCustomerReceiveNumber());
		customerProfileCriteria.setProfileVersion(0);
		return customerProfileCriteria;
	}

	/*
	 * protected SensitiveIDInfo encryptCallOnPCI(PanIdInfo panIdInfo, String
	 * originalAccountNumber, String mgCustomerReceiveNumber,int
	 * mgCustomerReceiveNumberVersion, Calendar cal) throws Exception {
	 * 
	 * SensitiveIDInfo sensID = new SensitiveIDInfo();
	 * sensID.setCustomerReceiveNumber(mgCustomerReceiveNumber);
	 * sensID.setCustomerReceiveNumberVersion(mgCustomerReceiveNumberVersion);
	 * if (panIdInfo.isPan()) { EncryptAccountRequest accountRequest = new
	 * EncryptAccountRequest();
	 * accountRequest.setPrimaryAccountNumber(originalAccountNumber);
	 * accountRequest.setSourceSystemID(mgCustomerReceiveNumber + "-" +
	 * mgCustomerReceiveNumberVersion);
	 * accountRequest.setSourceUsageType(SourceUsageTypeCode
	 * .RECEIVER_REGISTRATION_NUMBER);
	 * accountRequest.setSourceUsageCategory(SourceUsageCategoryCode.PROFILE);
	 * accountRequest.setLastUsedDate(cal); EncryptAccountResponse
	 * accountResponse = PCIServiceServiceFacade
	 * .encryptAccount(accountRequest); if
	 * (accountResponse.getResult().getValue(
	 * ).equals(EncryptAccountResponseCode._SUCCESS)) {
	 * log.debug("The account number is successfully encrypted");
	 * sensID.setSensitiveID(accountResponse.getSensitiveDataValueID()); //added
	 * for 30655 agg infra rules - july 2017
	 * //commandResponse.setAccountNumberSensitveID(sensID.getSensitiveID());
	 * sensID.setRuleProcessAccountNumber(String.valueOf(accountResponse.
	 * getAccountNumberHashCode()));
	 * PersistenceManagerFactory.getInstance().save(sensID); } else {
	 * log.error("Error Encrypting the account number"); throw new
	 * DRSException(DRSExceptionCodes.EC923_PCI_CALL_FAILURE, "",
	 * "PCIEncryption", new Date(), "PCI Encryption call failed", CLIENT); }
	 * }else{ //added for 30655 agg infra rules - july 2017
	 * if(!StringUtility.isNullOrEmpty(originalAccountNumber)){
	 * sensID.setRuleProcessAccountNumber(originalAccountNumber); } }
	 * //commandResponse
	 * .setRuleProcessAccountNumber(sensID.getRuleProcessAccountNumber());
	 * return sensID; }
	 */

	private FieldsCollectionRequest createFieldsCollectionRequest(
			SaveRegistrationRequest request,
			boolean saveRegistrationWithNewXmlTags) {
		FieldsInfoCriteria fieldsInfoCriteria = new FieldsInfoCriteria();
		fieldsInfoCriteria.setDeliveryOption(request.getDeliveryOption());
		fieldsInfoCriteria.setLanguage(request.getLanguage());
		fieldsInfoCriteria.setReceiveAgentId(request.getReceiveAgentID());
		fieldsInfoCriteria.setReceiveCountry(request.getReceiveCountry());
		fieldsInfoCriteria.setReceiveCurrency(request.getReceiveCurrency());
		fieldsInfoCriteria
				.setSaveRegistrationWithNewXmlTags(saveRegistrationWithNewXmlTags);

		// Does the caller support receiverPhoneCountryCode? Consider AC 1305.
		fieldsInfoCriteria.setSkipReceiverPhoneCountryCode(request
				.isSkipReceiverPhoneCountryCode());
		fieldsInfoCriteria.setSuppressReEnterFields(request.getAcVersionBO()
				.calculateHandleSuppressReEnterFields(request));
		//Added as part of receiver directed changes
		if(StringUtils.isNotEmpty(request.getExternalReceiverProfileId())){
			fieldsInfoCriteria.setExternalReceiverProfile(true);
		}

		FieldsCollectionRequest result = new FieldsCollectionRequest();
		result.setDaoCriteria(fieldsInfoCriteria);
		if (request.isCustomerReceiveNumberAvailable()) {
			result.setCustomerReceiveNumber(request.getCustomerReceiveNumber());
		}

		//If external profile id(UCP ID) is present skip the compliance logic
		//changes as part of Receiver directed flow
		
		// Removed  the logic to skip compliance if the request is from the receiver redirect flow.
//		if(!fieldsInfoCriteria.isExternalReceiverProfile()){  
			if (request.isSupportsComplianceFields()
					|| request.hasComplianceUpdates()){
			result.setSupportsComplianceFields(true);
				if (request.isCustomerReceiveNumberAvailable()) {
					// ** Use Case **: RRN Compliance Update
					// Only compliance related fields are supported for update
					result.setDisableRegistrationFields(true);
	
					/*
					 * TODO Select all phone fields and address fields by esb tag
					 * and cache Calculate complianceObjectTypes for update rather
					 * than client passing in.
					 */
					result.getComplianceObjectTypesForUpdate().addAll(
							request.getComplianceUpdates());
	
					// TODO If complianceObjectTypesForUpdate is missing, then throw
					// exception. Above TODO being completed removes this TODO
				} else {
					// ** Use Case **: Intra-Tran Creation with Reg and Compliance
					// Fields
					result.setDisableRegistrationFields(false);
				}
			} else {
				// ** Use Case **: Intra-Tran Creation
				// ** Use Case **: Direct RRN Creation
				// ** Use Case **: Updating RRN
				/*
				 * These use cases pre-date Get@Send. Callers are WebRRN GUI and
				 * Call Center
				 */
				result.setDisableRegistrationFields(false);
	
				/*
				 * Since this is not a compliance update, we don't want fields
				 * filtered by existing RRN. This facilitates non-RTPS who don't
				 * participate in compliance logic.
				 */
				result.setCustomerReceiveNumber(null);
			}
//		}
		return result;
	}

	/**
	 * This code upper cases any MG CRN
	 * 
	 * @param crn
	 *            The CRN to up case.
	 * @return THe upcased crn.
	 */
	private String convertMGCRNtoUpperCase(String crn) {
		if (crn == null || crn.length() < 2)
			return crn;
		// Check to make sure that this is an "mg" crn.
		String mgPart = crn.substring(0, 2);
		if (mgPart.equalsIgnoreCase("MG")) {
			return crn.toUpperCase();
		}
		return crn;
	}

	private void setCRNAndRegistrationStatus(boolean realtimeValidation,
			boolean creating, String crn, RegistrationStatusCode regStatus,
			CustomerProfile cp, String language) throws Exception {
		// If we are creating, we need to set the inital registration state.
		cp.setCustomerReceiveNumber(crn);
		if (realtimeValidation) {
			cp.setRegistrationStatus(RegistrationStatusCode.ACTIVE);
			cp.setRegistrationSubStatus("ACT");
		} else if (creating) {
			cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
			cp.setRegistrationSubStatus("INI");
		} else {
			// 4-16-05 : Set the registration status.
			if (regStatus != null) {
				// Check to see if an de-activation
				if (regStatus == RegistrationStatusCode.NOT_ACTIVE) {
					cp.setRegistrationStatus(RegistrationStatusCode.NOT_ACTIVE);
					cp.setRegistrationSubStatus("IAT");
				} else if (regStatus == RegistrationStatusCode.PENDING) {
					// Any registration change causes a transition to pending
					// ini
					// with the exception of of the NAT above
					cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
					cp.setRegistrationSubStatus("INI");
				} else if (regStatus != RegistrationStatusCode.ACTIVE) {
					// No other state change is acceptable, throw....
					throw new DRSException(
							DRSExceptionCodes.EC336_INVALID_REGISTRATION_STATUS,
							regStatus.getName(), "RegistrationStatus",
							new Date(), "Cannot set registration status", null);
				}
			} else {
				// Any registration change causes a transition to pending ini
				// with the exception of of the NAT above
				cp.setRegistrationStatus(RegistrationStatusCode.PENDING);
				cp.setRegistrationSubStatus("INI");
			}
		}
	}

	public SaveRegistrationCommandResponse convertToSaveRegistrationResponse(
			CustomerProfile profile, String verificationFlag,
			SaveRegistrationResult saveRegistrationResult) {
		SaveRegistrationCommandResponse saveRegistrationCommandResponse = new SaveRegistrationCommandResponse();
		saveRegistrationCommandResponse.setMgCustomerReceiveNumber(profile
				.getCustomerReceiveNumber());
		saveRegistrationCommandResponse
				.setMgCustomerReceiveNumberVersion(profile
						.getCustomerReceiveNumberVersion());
		saveRegistrationCommandResponse.setRegistrationStatusCode(profile
				.getRegistrationStatus());
		boolean verify = false;
		if (verificationFlag != null && verificationFlag.equals("R")) {
			verify = true;
		}
		saveRegistrationCommandResponse.setVerificationRequiredForUse(verify);

		boolean ofacCheck = true;
		// String ofacStatus = profile.getOfacStatus();
		// if (ofacStatus != null && ofacStatus.equalsIgnoreCase("F")) {
		// ofacCheck = false;
		// }
		/*
		 * Fixed against Defect #731 This code contains the call that makes the
		 * OFAC check. To set the OFAC flag to TRUE as though the OFAC check
		 * passed, and then let the service save the data as normal.
		 */
		saveRegistrationCommandResponse.setOfacStatus(ofacCheck);
		// if (!ofacCheck) {
		// saveRegistrationCommandResponse.setOfacSourceID(profile.getCustomerReceiveNumber());
		// }
		//
		saveRegistrationCommandResponse
				.setAccountNickname(saveRegistrationResult.getAccountNickname());
		saveRegistrationCommandResponse
				.setAccountNumberLastFour(saveRegistrationResult
						.getAccountNumberLastFour());
		saveRegistrationCommandResponse
				.setReceiverCustomerSequenceID(saveRegistrationResult
						.getReceiverCustomerSequenceID());
		return saveRegistrationCommandResponse;
	}

	private void updateSaveRegistrationRequestWithOldTag(
			SaveRegistrationRequest saveRegistrationRequest,
			RegistrationFieldsCollection registrationFieldsCollection) {
		ArrayList<KeyValuePair> saveRegistrationReqFieldValue = (ArrayList<KeyValuePair>) saveRegistrationRequest
				.getFieldValues();
		List<String> unsupportedKeys = new ArrayList<String>();
		for (KeyValuePair keyValue : saveRegistrationReqFieldValue) {
			String key = keyValue.getKey();
			List<ExtendedRegistrationFieldInfo> extendedRegistrationFieldInfoList = registrationFieldsCollection
					.getInfoEntryOnFullList(key);
			if (!extendedRegistrationFieldInfoList.isEmpty()) {
				for (ExtendedRegistrationFieldInfo list : extendedRegistrationFieldInfoList) {
					keyValue.setKey(list.getXmlTag());
				}
			} else {
				unsupportedKeys.add(key);
			}
		}
		if(log.isDebugEnabled()) {
			log.debug("Unsupported 1611+ keys: " + unsupportedKeys);
		}
	}
}
