package com.moneygram.drs.command;

import java.util.Collection;
import java.util.Date;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.DirectedSendRegistrationInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.constants.DRSConstants;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.get_send.FieldsCollectionRequest;
import com.moneygram.drs.get_send.FieldsCollectionService;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.DirectedSendFieldsRequest;

public class DirectedSendFieldsCommand extends AbstractCommand implements
		DRSConstants {
	private static final long serialVersionUID = 1L;
	private static Logger log = LogFactory.getInstance().getLogger(
			DirectedSendFieldsCommand.class);

	FieldsCollectionService fieldsCollectionService = new FieldsCollectionService();

	protected void doExecute() throws Exception {
		log.info("In DirectedSendFieldsCommand..");
		DirectedSendFieldsRequest directedSendFieldsRequest = (DirectedSendFieldsRequest) request;

		// TODO Murugan: Is this intra-tran flow only?
		validateRegistrationAllowed(directedSendFieldsRequest);

		RegistrationFieldsCollection fieldsCollection = getFieldsCollection(directedSendFieldsRequest);

		// TODO Murugan: Is this intra-tran flow only?
		FQDOInfo fqdoInfo = readFQDOInfo(directedSendFieldsRequest);

		DirectedSendRegistrationInfo directedSendRegistrationInfo = createResult(
				fieldsCollection, fqdoInfo);
		this.result = directedSendRegistrationInfo;
	}

	// TODO Does this belong in handleIntraTran?
	private void validateRegistrationAllowed(
			DirectedSendFieldsRequest directedSendFieldsRequest)
			throws Exception, DRSException {
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(directedSendFieldsRequest
				.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(directedSendFieldsRequest
				.getReceiveAgentID());
		dsrCriteria.setLanguage(directedSendFieldsRequest.getLanguage());
		boolean isRegistrationAllowed = PersistenceManagerFactory.getInstance()
				.assertData(dsrCriteria);

		// TODO Review if this makes sense for update rrn, if not then move into
		// handleIntraTran method
		if (!isRegistrationAllowed) {
			throw new DRSException(
					DRSExceptionCodes.EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM,
					"Cannot use MoneyGram to register agentId:",
					"receiveAgentId", new Date(), " deliveryOption: "
							+ directedSendFieldsRequest.getDeliveryOption()
							+ directedSendFieldsRequest.getReceiveAgentID(),
					CLIENT);
		}
	}

	private RegistrationFieldsCollection getFieldsCollection(
			DirectedSendFieldsRequest request) throws Exception {
		FieldsInfoCriteria daoCriteria = createDAOInput(request);

		FieldsCollectionRequest fieldsCollectionRequest = new FieldsCollectionRequest();
		fieldsCollectionRequest.setCustomerReceiveNumber(request
				.getCustomerReceiverNumber());
		fieldsCollectionRequest.setSupportsComplianceFields(request
				.isSupportsComplianceFields());

		fieldsCollectionRequest.setDaoCriteria(daoCriteria);

		if (fieldsCollectionRequest.isCustomerReceiveNumberAvailable()
				&& fieldsCollectionRequest.isSupportsComplianceFields()) {
			fieldsCollectionRequest.setDisableRegistrationFields(true);
		} else {
			fieldsCollectionRequest.setDisableRegistrationFields(request
					.isDisableRegistrationFields());
		}

		RegistrationFieldsCollection registrationFieldsCollection = fieldsCollectionService
				.execute(fieldsCollectionRequest);
		return registrationFieldsCollection;
	}

	private FieldsInfoCriteria createDAOInput(
			DirectedSendFieldsRequest directedSendFieldsRequest) {
		FieldsInfoCriteria registrationFieldsInfoCriteria = new FieldsInfoCriteria();
		registrationFieldsInfoCriteria
				.setDeliveryOption(directedSendFieldsRequest
						.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(directedSendFieldsRequest
				.getLanguage());
		registrationFieldsInfoCriteria
				.setReceiveAgentId(directedSendFieldsRequest
						.getReceiveAgentID());
		registrationFieldsInfoCriteria
				.setReceiveCountry(directedSendFieldsRequest
						.getReceiveCountry());
		registrationFieldsInfoCriteria
				.setReceiveCurrency(directedSendFieldsRequest
						.getReceiveCurrency());
		registrationFieldsInfoCriteria
				.setSkipReceiverPhoneCountryCode(directedSendFieldsRequest
						.isSkipReceiverPhoneCountryCode());
		registrationFieldsInfoCriteria.setSuppressReEnterFields(true);
		return registrationFieldsInfoCriteria;
	}

	private DirectedSendRegistrationInfo createResult(
			RegistrationFieldsCollection fieldsCollection, FQDOInfo fqdoInfo) {
		DirectedSendRegistrationInfo directedSendRegistrationInfo = new DirectedSendRegistrationInfo();
		directedSendRegistrationInfo.setFqdoInfo(fqdoInfo);
		if (fieldsCollection != null) {
			// TODO Murugan: May occur if not intra-tran and no fields to update
			// for RRN
			// Should we consider returning null instead? And skip getting
			// FQDO info?
			directedSendRegistrationInfo.setSupplementalFlag(fieldsCollection
					.isSupplementalFieldExist());
			for (Collection<ExtendedRegistrationFieldInfo> registrationFieldData : fieldsCollection
					.getFullList()) {
				directedSendRegistrationInfo
						.setRegistrationFieldData(registrationFieldData);
			}
		}
		return directedSendRegistrationInfo;
	}

	private FQDOInfo readFQDOInfo(
			DirectedSendFieldsRequest directedSendFieldsRequest)
			throws NotFoundException, DRSException {
		// Reading FQDOInfo data
		FQDOCriteria fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setReceiveCountry(directedSendFieldsRequest
				.getReceiveCountry());
		fqdoCriteria.setDeliveryOption(directedSendFieldsRequest
				.getDeliveryOption());
		fqdoCriteria.setReceiveAgentID(directedSendFieldsRequest
				.getReceiveAgentID());
		fqdoCriteria.setReceiveCurrency(directedSendFieldsRequest
				.getReceiveCurrency());
		fqdoCriteria.setLanguage(directedSendFieldsRequest.getLanguage());
		fqdoCriteria.setFqdoEnum(FqdoEnum.FQDO);
		ObjectId objectId = new ObjectId(FQDOInfo.class, fqdoCriteria);
		FQDOInfo fqdoInfo = (FQDOInfo) PersistenceManagerFactory.getInstance()
				.read(objectId);
		return fqdoInfo;
	}
}