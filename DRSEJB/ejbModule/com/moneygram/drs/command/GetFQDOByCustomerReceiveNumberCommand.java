package com.moneygram.drs.command;

import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.request.GetFQDOByCustomerReceiveNumberRequest;

public class GetFQDOByCustomerReceiveNumberCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		GetFQDOByCustomerReceiveNumberRequest getFQDOByCustomerReceiveNumberRequest = (GetFQDOByCustomerReceiveNumberRequest) request;
		FQDOCriteria fqdoByCustomerReceiverNumberCriteria = new FQDOCriteria();
		fqdoByCustomerReceiverNumberCriteria
				.setCustomerReceiveNumber(getFQDOByCustomerReceiveNumberRequest
						.getMgCustomerReceiveNumber());
		fqdoByCustomerReceiverNumberCriteria.setLanguage(getFQDOByCustomerReceiveNumberRequest
				.getLanguage());
		fqdoByCustomerReceiverNumberCriteria.setFqdoEnum(FqdoEnum.FULLFQDO_WITH_REGISTRATIONSTATUS);
		ObjectId objectId = new ObjectId(FullFQDOWithRegistrationStatus.class,
				fqdoByCustomerReceiverNumberCriteria);
		this.result = PersistenceManagerFactory.getInstance().read(objectId);
	}
}
