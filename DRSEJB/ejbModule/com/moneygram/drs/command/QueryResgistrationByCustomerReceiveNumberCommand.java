package com.moneygram.drs.command;

import java.util.Date;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.FullFQDOWithAgentStatus;
import com.moneygram.drs.bo.RegistrationData;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.pciservicemanager.PCIServiceServiceFacade;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.QueryRegistrationByCustomerReceiveNumberRequest;
import com.moneygram.drs.util.ObjectUtils;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapper;
import com.moneygram.drs.xmlTagMapper.StandardFieldXMLTagMapperFactory;

public class QueryResgistrationByCustomerReceiveNumberCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;
	
	private static Logger LOGGER = LogFactory.getInstance().getLogger(QueryResgistrationByCustomerReceiveNumberCommand.class);
	
	protected void doExecute() throws Exception {
		FQDOCriteria fqdoCriteria = null;
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = new RegistrationFieldsInfoCriteria();
		RegistrationData registrationData = new RegistrationData();
		QueryRegistrationByCustomerReceiveNumberRequest queryRegistrationByCustomerReceiveNumberRequest = (QueryRegistrationByCustomerReceiveNumberRequest) request;
		String customReceiveNumber = convertMGCRNtoUpperCase(queryRegistrationByCustomerReceiveNumberRequest
				.getMgCustomerReceiveNumber());
		queryRegistrationByCustomerReceiveNumberRequest
				.setMgCustomerReceiveNumber(customReceiveNumber);
		// Reading customerProfile data
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria.setAdminUser(queryRegistrationByCustomerReceiveNumberRequest
				.isSuperUser());
		customerProfileCriteria
				.setCustomerReceiveNumber(queryRegistrationByCustomerReceiveNumberRequest
						.getMgCustomerReceiveNumber());
		customerProfileCriteria.setProfileVersion(0);
		if(queryRegistrationByCustomerReceiveNumberRequest.isDisplayFullAccountNumber()){
			customerProfileCriteria.setDisplayFullAccountNumber(queryRegistrationByCustomerReceiveNumberRequest.isDisplayFullAccountNumber());
		}
		ObjectId objectId = new ObjectId(CustomerProfile.class, customerProfileCriteria);
		CustomerProfile customerProfile = (CustomerProfile) PersistenceManagerFactory.getInstance()
				.read(objectId);
		if (customerProfile == null) {
			throw new DRSException(
					DRSExceptionCodes.EC316_CUSTOMER_NOT_FOUND,
					"Customer receive number:" + customerProfileCriteria.getCustomerReceiveNumber(),
					"mgCustomerReceiveNumber", new Date(), "Customer is not found", CLIENT);
		}
		// Reading FQDOInfo data
		fqdoCriteria = new FQDOCriteria();
		fqdoCriteria.setCustomerReceiveNumber(queryRegistrationByCustomerReceiveNumberRequest
				.getMgCustomerReceiveNumber());
		fqdoCriteria.setLanguage(queryRegistrationByCustomerReceiveNumberRequest.getLanguage());
		fqdoCriteria.setFqdoEnum(FqdoEnum.FULLFQDO);
		objectId = new ObjectId(FQDOInfo.class, fqdoCriteria);
		// Modified as part of 61828 AD Quick Wins - GCC DS - 2016
		// It will return FQDO info along with agent status ["ACT"/"INT"]
		FullFQDOWithAgentStatus fqdoInfoWithAgentStatus = (FullFQDOWithAgentStatus) PersistenceManagerFactory.getInstance().read(objectId);
		FQDOInfo fqdoInfo=fqdoInfoWithAgentStatus.getFqdoInfo();
		// Reading Registration Field collections
		registrationFieldsInfoCriteria.setDeliveryOption(fqdoInfo.getDeliveryOption());
		registrationFieldsInfoCriteria.setLanguage(queryRegistrationByCustomerReceiveNumberRequest
				.getLanguage());
		registrationFieldsInfoCriteria.setReceiveAgentId(fqdoInfo.getReceiveAgentID());
		registrationFieldsInfoCriteria.setReceiveCountry(fqdoInfo.getReceiveCountry());
		registrationFieldsInfoCriteria.setReceiveCurrency(fqdoInfo.getReceiveCurrency());
		objectId = new ObjectId(RegistrationFieldsCollection.class, registrationFieldsInfoCriteria);
		RegistrationFieldsCollection registrationFieldsCollection = (RegistrationFieldsCollection) PersistenceManagerFactory
		.getInstance().read(objectId);
		// Parsing the XML and mapping with the registration fields
		StandardFieldXMLTagMapper fieldXMLTagMapperImpl = StandardFieldXMLTagMapperFactory
				.getInstance();
		fieldXMLTagMapperImpl.convertToRegistrationFieldInfos(customerProfile,
				registrationFieldsCollection);
		
		// Changes for 54351 E AD POE Improvements Nov 2015 Release. 
	    // Requirement is to display full Account Number in Receipts if PAN is true so commenting below
		
		String fullAccountNumber = null ;
		
		if ("Y".equalsIgnoreCase(customerProfile.getRcvAgentCustAcctNbrPanFlag())) {
			for (List<ExtendedRegistrationFieldInfo> registrationFieldList : registrationFieldsCollection.getFullList()) {
				for (RegistrationFieldInfo registrationField : registrationFieldList)
				{
					if (registrationField.getXmlTag() != null && "RECEIVERACCOUNTNUMBER".equalsIgnoreCase(registrationField.getXmlTag())) {
						if (!queryRegistrationByCustomerReceiveNumberRequest.isDisplayFullAccountNumber()) { 
							// Mask account number if full account number is not requested 
							registrationField.setFieldValue(ObjectUtils.getMaskedString(customerProfile.getRcvAgentCustAcctNbr()));
							LOGGER.info("Masked account number in the QueryRegistrationResponse as PanFlag is TRUE");
						}else {
							// Need to return full account number if it's PAN and display full account number is TRUE . Modifying this as part of AGG3 (Fixing prod bug due to compliance issue)
							fullAccountNumber =	decryptAccountNumberOnPCI (queryRegistrationByCustomerReceiveNumberRequest.getMgCustomerReceiveNumber(),String.valueOf(customerProfile.getCustomerReceiveNumberVersion()));
							registrationField.setFieldValue(fullAccountNumber);
							LOGGER.info("Full account number in the QueryRegistrationResponse as display full account number is TRUE");
						}
					}
				}
			}
		}
		// Setting the all the data to the object to be sent
		registrationData.setFqdoInfo(fqdoInfo);
		registrationData.setOfacExceptionStateCode(customerProfile.getOfacStatus());
		registrationData.setRegistrationStatus(customerProfile.getRegistrationStatus());
		registrationData.setRegistrationSubStatus(customerProfile.getRegistrationSubStatus());
		registrationData.setRegistrationFieldInfo(registrationFieldsCollection.getFullList());
		registrationData.setCustTranFreqCode(customerProfile.getCustTranFreqCode());
		registrationData.setAgentStatusCode(fqdoInfoWithAgentStatus.getAgentStatusCode());
		this.result = registrationData;
	}

	/**
	 * This code upper cases any MG CRN
	 * 
	 * @param crn
	 *            The CRN to up case.
	 * @return THe upcased crn.
	 */
	public static String convertMGCRNtoUpperCase(String crn) {
		if (crn == null || crn.length() < 2)
			return crn;
		// Check to make sure that this is an "mg" crn.
		String mgPart = crn.substring(0, 2);
		if (mgPart.equalsIgnoreCase("MG")) {
			return crn.toUpperCase();
		}
		return crn;
	}
	
	// This will return the decrypted account number 
	protected String decryptAccountNumberOnPCI( String rrnNumber, String profileVersion) throws Exception {
		String fullAccountNumber = null;
		try {
			fullAccountNumber = PCIServiceServiceFacade.retrieveRRNCardNumber(rrnNumber, profileVersion);
		}catch (Exception e) {
			LOGGER.error("Error Decrypting the account number" + e);
			throw new DRSException(DRSExceptionCodes.EC924_PCI_DECRYPT_CALL_FAILURE, "","PCIDecryption", new Date(), "PCI Decryption call failed", CLIENT);
		}
		return fullAccountNumber;
	}
		
}
