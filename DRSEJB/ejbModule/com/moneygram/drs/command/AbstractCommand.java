package com.moneygram.drs.command;

import com.moneygram.drs.request.CommandRequest;
import com.moneygram.drs.request.Request;

public abstract class AbstractCommand extends Request {
	protected CommandRequest request;
	protected Object result;
	public String CLIENT = "client";

	protected void preProcess() {
	}

	public void execute() throws Exception {
		preProcess();
		doExecute();
	}

	protected abstract void doExecute() throws Exception;

	public void initialize(CommandRequest request) {
		this.request = request;
	}

	public Object getResult() {
		return result;
	}

	protected void setResult(Object result) {
		this.result = result;
	}

	protected CommandRequest getRequest() {
		return request;
	}
}
