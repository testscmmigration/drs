package com.moneygram.drs.command;

import java.util.List;
import com.moneygram.drs.criteria.SimplifiedQueryRegistrationCriteria;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.request.SimplifiedQueryRegistrationRequest;

public class SimplifiedQueryRegistrationCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		SimplifiedQueryRegistrationRequest queryRequest = (SimplifiedQueryRegistrationRequest) request;
		SimplifiedQueryRegistrationCriteria criteria = new SimplifiedQueryRegistrationCriteria();
		criteria.setDeliveryOption(queryRequest.getDeliveryOption());
		criteria.setReceiveAgentID(queryRequest.getReceiveAgentID());
		criteria.setReceiveCountry(queryRequest.getReceiveCountry());
		criteria.setReceiveCurrency(queryRequest.getReceiveCurrency());
		criteria.setReceiverFirstName(queryRequest.getReceiverFirstName());
		criteria.setReceiverLastName(queryRequest.getReceiverLastName());
		criteria.setReceiverPhoneNumber(queryRequest.getReceiverPhoneNumber());
		criteria.setRegistrationCreatorFirstName(queryRequest.getRegistrationCreatorFirstName());
		criteria.setRegistrationCreatorLastName(queryRequest.getRegistrationCreatorLastName());
		criteria.setRegistrationCreatorPhoneNumber(queryRequest.getRegistrationCreatorPhoneNumber());
		criteria.setMgCustomerReceiveNumber(queryRequest.getMgCustomerReceiveNumber());
		criteria.setMaxRowsToReturn(queryRequest.getMaxRowsToReturn());
		criteria.setActiveRecordsOnly(queryRequest.getActiveRecordsOnly());

		List records = PersistenceManagerFactory.getInstance().find(criteria);
		this.result = records;
	}
}
