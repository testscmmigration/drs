package com.moneygram.drs.command;

import java.util.List;
import com.moneygram.drs.criteria.CountryInfoCriteria;
import com.moneygram.drs.persistence.PersistenceManagerFactory;

public class CountryInfoCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		CountryInfoCriteria criteria = new CountryInfoCriteria();
		List countryInfoList = PersistenceManagerFactory.getInstance().find(criteria);
		this.result = countryInfoList;
	}
}
