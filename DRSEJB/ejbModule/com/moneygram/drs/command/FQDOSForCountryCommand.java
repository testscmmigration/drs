package com.moneygram.drs.command;

import java.util.List;
import com.moneygram.drs.criteria.FQDOForCountryCriteria;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.request.FQDOsForCountryRequest;

public class FQDOSForCountryCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	protected void doExecute() throws Exception {
		FQDOsForCountryRequest fqdOsForCountryRequest = (FQDOsForCountryRequest) request;
		String receiveCountry = fqdOsForCountryRequest.getReceiveCountry();
		String language = fqdOsForCountryRequest.getLanguage();
		FQDOForCountryCriteria criteria = new FQDOForCountryCriteria();
		criteria.setReceiveCountry(receiveCountry);
		criteria.setLanguage(language);
		List fqdoList = PersistenceManagerFactory.getInstance().find(criteria);
		this.result = fqdoList;
	}
}
