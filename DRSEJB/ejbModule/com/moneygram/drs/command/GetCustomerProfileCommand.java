package com.moneygram.drs.command;

import java.util.Calendar;
import java.util.Date;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.CustomerProfile2;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.pci.PciTranslator;
import com.moneygram.drs.pciservicemanager.PCIServiceServiceFacade;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.CustomerProfileRequest2;

public class GetCustomerProfileCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = LogFactory.getInstance().getLogger(GetCustomerProfileCommand.class);

	protected void doExecute() throws Exception {
		String decryptedAccNumber = null;
		CustomerProfileRequest2 customerProfileRequest = (CustomerProfileRequest2) request;
		PciTranslator pciTranslator = new PciTranslator();
		SensitiveIDInfo sensID = new SensitiveIDInfo();
		String customReceiveNumber = convertMGCRNtoUpperCase(customerProfileRequest
				.getMgCustomerReceiveNumber());
		customerProfileRequest
				.setMgCustomerReceiveNumber(customReceiveNumber);
		// Reading customerProfile data
		CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
		customerProfileCriteria
				.setCustomerReceiveNumber(customerProfileRequest
						.getMgCustomerReceiveNumber());
		customerProfileCriteria.setProfileVersion(customerProfileRequest.getCustomerProfileVersionNumber());
		ObjectId objectId = new ObjectId(CustomerProfile2.class, customerProfileCriteria);
		CustomerProfile2 customerProfile = (CustomerProfile2) PersistenceManagerFactory.getInstance()
				.read(objectId);
		if (customerProfile == null) {
			throw new DRSException(
					DRSExceptionCodes.EC316_CUSTOMER_NOT_FOUND,
					"Customer receive number:" + customerProfileCriteria.getCustomerReceiveNumber(),
					"mgCustomerReceiveNumber", new Date(), "Customer is not found", CLIENT);
		}
		// Returning full account number based on new attribute displayFullAccountNumber if it's PAN - Prod fix as part of GR Phase 2 
		if (customerProfileRequest.isDisplayFullAccountNumber() && "Y".equalsIgnoreCase(customerProfile.getRcvAgentCustAcctNbrPanFlag())) {
			decryptedAccNumber = decryptAccountNumberOnPCI(customReceiveNumber,String.valueOf(customerProfile.getCustomerReceiveNumberVersion()));
			customerProfile.setDcpRcvAgentCustAcctNbr(decryptedAccNumber);
		}
	    //---- Added as part of Infra Rule Project
		// While retrieving the custInfo by RRN number , If cust's Account Number is "PAN", PCI call should happen to the RuleProcessAccountNumber
		boolean isPan = ("Y".equalsIgnoreCase(customerProfile
				.getRcvAgentCustAcctNbrPanFlag())) ? true : false;
		if (!isPan && customerProfile.getRuleProcessAccountNumber() == null) {
			customerProfile.setRuleProcessAccountNumber(customerProfile
					.getRcvAgentCustAcctNbr());
		} else if (isPan
				&& null == customerProfile.getRuleProcessAccountNumber()) {
			// Adding code which is required for the PCI Encryption service call.
			sensID = pciTranslator.encryptCallOnPCI(isPan,
					customerProfile.getRcvAgentCustAcctNbr(),
					customerProfile.getCustomerReceiveNumber(),
					customerProfile.getCustomerReceiveNumberVersion(),
					Calendar.getInstance());
			if (null != sensID) {
				customerProfile.setRuleProcessAccountNumber(sensID
						.getRuleProcessAccountNumber());
				customerProfile.setAccountNumberSensitveID(sensID
						.getSensitiveID());
			}

		}
		
		this.result = customerProfile;
	}

	/**
	 * This code upper cases any MG CRN
	 * 
	 * @param crn
	 *            The CRN to up case.
	 * @return THe upcased crn.
	 */
	public static String convertMGCRNtoUpperCase(String crn) {
		if (crn == null || crn.length() < 2)
			return crn;
		// Check to make sure that this is an "mg" crn.
		String mgPart = crn.substring(0, 2);
		if (mgPart.equalsIgnoreCase("MG")) {
			return crn.toUpperCase();
		}
		return crn;
	}
	
	// This will return the decrypted account number 
	protected String decryptAccountNumberOnPCI( String rrnNumber, String profileVersion) throws Exception {
		String fullAccountNumber = null;
		try {
			fullAccountNumber = PCIServiceServiceFacade.retrieveRRNCardNumber(rrnNumber, profileVersion);
		}catch (Exception e) {
			LOGGER.error("Error Decrypting the account number in get Customer Profile API" + e);
			throw new DRSException(DRSExceptionCodes.EC924_PCI_DECRYPT_CALL_FAILURE, "","PCIDecryption", new Date(), "PCI Decryption call failed", CLIENT);
		}
		return fullAccountNumber;
	}
		
}
