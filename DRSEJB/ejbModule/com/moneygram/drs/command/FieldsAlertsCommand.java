package com.moneygram.drs.command;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.get_send.AlertRegistrationInfoBuilder;
import com.moneygram.drs.get_send.FieldsCollectionRequest;
import com.moneygram.drs.get_send.FieldsCollectionService;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.request.FieldsAlertsRequest;

public class FieldsAlertsCommand extends AbstractCommand {
	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = LogFactory.getInstance().getLogger(
			FieldsAlertsCommand.class);

	FieldsCollectionService fieldsCollectionService = new FieldsCollectionService();
	
	protected void doExecute() throws Exception {

		FieldsAlertsRequest fieldsAlertsRequest = (FieldsAlertsRequest) request;

		if (fieldsAlertsRequest != null) {
			LOGGER.info(String
					.format("fieldsAlertsRequest: customerReceiveNumber=%s receiveAgentID=%s deliveryOption=%s externalReceiverProfile=%s receiveCountry=%s receiveCurrency=%s language=%s",
							fieldsAlertsRequest.getCustomerReceiveNumber(),
							fieldsAlertsRequest.getReceiveAgentID(),
							fieldsAlertsRequest.getDeliveryOption(),
							fieldsAlertsRequest.isExternalReceiverProfile(),
							fieldsAlertsRequest.getReceiveCountry(),
							fieldsAlertsRequest.getReceiveCurrency(),
							fieldsAlertsRequest.getLanguage()));
		}
		
		// TODO Murugan: Is this intra-tran flow only?
		validateRegistrationAllowed(fieldsAlertsRequest);

		FieldsInfoCriteria fieldsInfoCriteria = createDAOInput(fieldsAlertsRequest);

		
		// Reading registration collection
		RegistrationFieldsCollection registrationFieldsCollection = getFieldsCollection(
				fieldsAlertsRequest, fieldsInfoCriteria);
		


		AlertRegistrationInfo alertRegistrationInfo = AlertRegistrationInfoBuilder.convertToAgg3Structure(registrationFieldsCollection);
		

		this.result = alertRegistrationInfo;
	}

	private RegistrationFieldsCollection getFieldsCollection(
			FieldsAlertsRequest alertRegistrationFieldsRequest,
			FieldsInfoCriteria fieldsInfoCriteria) throws Exception {
		FieldsCollectionRequest fieldsCollectionRequest = new FieldsCollectionRequest();
		fieldsCollectionRequest
				.setCustomerReceiveNumber(alertRegistrationFieldsRequest
						.getCustomerReceiveNumber());
		fieldsCollectionRequest.setSupportsComplianceFields(true);
		
// Removed the logic to disable compliance if the request comes from the Receiver Directed flow		
//		if(alertRegistrationFieldsRequest.isExternalReceiverProfile()){
//			fieldsCollectionRequest.setSupportsComplianceFields(false);
//		}
		
		if(fieldsCollectionRequest.isCustomerReceiveNumberAvailable()) {
			// ** Use Case **: Compliance Fields to Collect for Existing RRN
			fieldsCollectionRequest.setDisableRegistrationFields(true);
		} else {
			// ** Use Case **: Intra Tran Reg and Compliance Fields to Collect
			fieldsCollectionRequest.setDisableRegistrationFields(false);
		}

		fieldsCollectionRequest.setDaoCriteria(fieldsInfoCriteria);

		RegistrationFieldsCollection registrationFieldsCollection = fieldsCollectionService
				.execute(fieldsCollectionRequest);
		return registrationFieldsCollection;
	}

	private FieldsInfoCriteria createDAOInput(
			FieldsAlertsRequest alertRegistrationFieldsRequest) {
		FieldsInfoCriteria fieldsInfoCriteria = new FieldsInfoCriteria();
		fieldsInfoCriteria.setDeliveryOption(alertRegistrationFieldsRequest
				.getDeliveryOption());
		fieldsInfoCriteria.setLanguage(alertRegistrationFieldsRequest
				.getLanguage());
		fieldsInfoCriteria.setReceiveAgentId(alertRegistrationFieldsRequest
				.getReceiveAgentID());
		fieldsInfoCriteria.setReceiveCountry(alertRegistrationFieldsRequest
				.getReceiveCountry());
		fieldsInfoCriteria.setReceiveCurrency(alertRegistrationFieldsRequest
				.getReceiveCurrency());
		fieldsInfoCriteria.setExternalReceiverProfile(alertRegistrationFieldsRequest
				.isExternalReceiverProfile());
		return fieldsInfoCriteria;
	}

	private void validateRegistrationAllowed(
			FieldsAlertsRequest alertRegistrationFieldsRequest)
			throws Exception, DRSException {
		AssertRegistrationCriteria dsrCriteria = new AssertRegistrationCriteria();
		dsrCriteria.setDeliveryOption(alertRegistrationFieldsRequest
				.getDeliveryOption());
		dsrCriteria.setReceiveAgentID(alertRegistrationFieldsRequest
				.getReceiveAgentID());
		dsrCriteria.setLanguage(alertRegistrationFieldsRequest.getLanguage());
		boolean isRegistrationAllowed = PersistenceManagerFactory.getInstance()
				.assertData(dsrCriteria);
		if (!isRegistrationAllowed) {
			throw new DRSException(
					DRSExceptionCodes.EC629_CANNOT_REGISTER_PROFILE_VIA_MONEYGRAM,
					"Cannot use MoneyGram to register agentId:",
					"receiveAgentId", new Date(), " deliveryOption: "
							+ alertRegistrationFieldsRequest
									.getDeliveryOption()
							+ alertRegistrationFieldsRequest
									.getReceiveAgentID(), CLIENT);
		}
	}
}
