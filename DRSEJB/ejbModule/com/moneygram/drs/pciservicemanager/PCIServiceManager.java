package com.moneygram.drs.pciservicemanager;

import java.rmi.RemoteException;
import com.moneygram.drs.ws.pci.EncryptAccountRequest;
import com.moneygram.drs.ws.pci.EncryptAccountResponse;

public interface PCIServiceManager {
	public EncryptAccountResponse encryptAccount(EncryptAccountRequest request)
			throws RemoteException;

	public String retrieveRRNCardNumber(String rrnNumber, String version) throws Exception;
}
