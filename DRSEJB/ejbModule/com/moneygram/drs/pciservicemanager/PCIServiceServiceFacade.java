package com.moneygram.drs.pciservicemanager;

import com.moneygram.drs.ws.pci.EncryptAccountRequest;
import com.moneygram.drs.ws.pci.EncryptAccountResponse;

public class PCIServiceServiceFacade {
	protected static PCIServiceManager pciServiceManager;

	private static PCIServiceManager getPCIServiceManager() throws Exception {
		if (pciServiceManager == null) {
			pciServiceManager = new PCIServiceManagerImpl();
		}
		return pciServiceManager;
	}

	public static EncryptAccountResponse encryptAccount(EncryptAccountRequest request)
			throws Exception {
		return getPCIServiceManager().encryptAccount(request);
	}

	public static String retrieveRRNCardNumber(String rrnNumber, String version) throws Exception {
		return getPCIServiceManager().retrieveRRNCardNumber(rrnNumber, version);
	}
}
