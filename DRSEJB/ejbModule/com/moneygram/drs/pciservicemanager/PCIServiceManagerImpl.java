package com.moneygram.drs.pciservicemanager;

import java.net.URL;
import java.rmi.RemoteException;
import java.security.InvalidParameterException;
import java.util.Calendar;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.ws.pci.EncryptAccountRequest;
import com.moneygram.drs.ws.pci.EncryptAccountResponse;
import com.moneygram.drs.ws.pci.PCIEncryptServiceSOAP_v3Stub;
import com.moneygram.drs.ws.pci.PCIEncryptService_ServiceLocator;
import com.moneygram.drs.ws.pci.decrypt.AccountBySourceIdRequest;
import com.moneygram.drs.ws.pci.decrypt.AccountBySourceIdResponse;
import com.moneygram.drs.ws.pci.decrypt.PCIDecryptService_PortType;
import com.moneygram.drs.ws.pci.decrypt.PCIDecryptService_ServiceLocator;
import com.moneygram.drs.ws.pci.decrypt.SourceUsageCategoryCode;
import com.moneygram.drs.ws.pci.decrypt.SourceUsageTypeCode;
import com.moneygram.ree.lib.Config;

public class PCIServiceManagerImpl implements PCIServiceManager {
	private static Logger log = LogFactory.getInstance().getLogger(PCIServiceManagerImpl.class);
	PCIEncryptServiceSOAP_v3Stub encryptServiceProxy;
	PCIDecryptService_PortType decryptService;

	public PCIServiceManagerImpl() throws Exception {
		super();
		Context ctx = new InitialContext();
		int pciServiceTimeOut = 2500;
		Object object = ctx.lookup("java:comp/env/rep/ResourceReference");
		Config config = (Config) object;
		String _url = (String) config.getAttribute("PCIServiceURL");
		pciServiceTimeOut = new Integer(config.getAttribute("pciTimeOut").toString()).intValue();
		if (_url == null) {
			throw new Exception("Failed to obtain the value of the PCI Service URL");
		}
		URL encryptURL = new URL(_url);
		PCIEncryptService_ServiceLocator locator = new PCIEncryptService_ServiceLocator();
		encryptServiceProxy = new PCIEncryptServiceSOAP_v3Stub(encryptURL, locator);
		encryptServiceProxy.setTimeout(pciServiceTimeOut);
		URL decryptEndpoint = new URL((String) config.getAttribute("PCIDecryptServiceURL"));
		String userName = (String) config.getAttribute("PCIUserName");
		String password = (String) config.getAttribute("PCIPswd");
		decryptService = new PCIDecryptService_ServiceLocator()
				.getPCIDecryptServiceSOAP(decryptEndpoint);
		// provide the service account userID & password for authentication
		((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
		((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);
		((Stub) decryptService).setTimeout(pciServiceTimeOut);
	}

	public EncryptAccountResponse encryptAccount(EncryptAccountRequest request)
			throws RemoteException {
		if (request == null) {
			throw new InvalidParameterException("Can not process null EncryptAccoutRequest.");
		}
		return encryptServiceProxy.encryptAccount(request);
	}

	public String retrieveRRNCardNumber(String rrnNumber, String version) throws Exception {
		AccountBySourceIdRequest request = new AccountBySourceIdRequest();
		request.setSourceSystemID(rrnNumber + "-" + version);
		request.setSourceUsageCategory(SourceUsageCategoryCode.PROFILE);
		request.setSourceUsageType(SourceUsageTypeCode.RECEIVER_REGISTRATION_NUMBER);
		request.setLastUsedDate(Calendar.getInstance());
		request.setUserID("DRS");
		log.debug("Calling PCI Decrypt Service to retrieve RRN card number...");
		AccountBySourceIdResponse response;
		response = decryptService.getAccountBySourceID(request);
		log.debug("PCI Decrypt Service call completed...");
		return response.getPrimaryAccountNumber();
	}
}
