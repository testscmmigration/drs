package com.moneygram.drs.get_send;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.TransactionAttributeType;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

/**
 * Removes {@link RegistrationFieldsCollection}s from
 * {@link ComplianceFieldsCollections} when the existing CustomerProfile has all
 * required fields for the object Type(address or phone).
 */
public class UpdateRRNComplianceFieldsCollectionsFilter {

	public static String getDynamicValue(CustomerProfile customerProfile,
			ExtendedRegistrationFieldInfo fieldInfo) {
		CustomerProfileCustomField customFieldByXMLTag = customerProfile
				.getCustomField(fieldInfo);
		return customFieldByXMLTag == null ? null : customFieldByXMLTag
				.getValue();
	}

	private final CustomerProfileSimpleDAO dao;

	public UpdateRRNComplianceFieldsCollectionsFilter(
			CustomerProfileSimpleDAO dao) {
		super();
		this.dao = dao;
	}

	/**
	 * Field collection for phone or address should only be sent if a valid
	 * phone or valid address doesn't already exist.
	 * 
	 * After this method executes, values in {@link ComplianceFieldsCollections}
	 * may be set to null if the field collection is not needed.
	 * 
	 * @param customerReceiveNumber
	 * @param complianceFieldsCollections
	 *            Contains a receiver address and receiver phone set of fields
	 * @throws DRSException
	 * @throws NotFoundException
	 */
	public void process(UpdateRRNComplianceFieldsContext context)
			throws DRSException, NotFoundException {
		FieldsCollectionRequest request = context.getFieldsCollectionRequest();
		if (request.isCustomerReceiveNumberAvailable()) {
			CustomerProfileCriteria customerProfileCriteria = new CustomerProfileCriteria();
			customerProfileCriteria.setCustomerReceiveNumber(request
					.getCustomerReceiveNumber());
			CustomerProfile customerProfile = dao
					.readCustomerProfile(customerProfileCriteria);
			if (customerProfile != null) {
				CustomerProfilePerson receiver = customerProfile.getReceiver();
				if (receiver != null) {
					Address address = getAddress(receiver);
					doProcess(context, customerProfile, receiver, address);
				}
			}
		}
	}

	private Address getAddress(CustomerProfilePerson receiver) {
		@SuppressWarnings("unchecked")
		List<Address> addresses = receiver.getAddresses();
		if (addresses.size() > 0) {
			return addresses.get(0);
		}
		return null;
	}

	private void doProcess(UpdateRRNComplianceFieldsContext context,
			CustomerProfile customerProfile, CustomerProfilePerson receiver,
			Address address) {
		ComplianceFieldsCollections cfp = context
				.getComplianceFieldsCollections();
		if (!context.arePhoneFieldsRequiredBecauseOfSaveRegistrationInput()
				&& !isCustomerProfileMissingRequiredComplianceFields(
						cfp.getPhoneFieldsCollection(), customerProfile,
						receiver, ESB_PersonTag.class)) {
			// Phone fields are not needed
			cfp.setPhoneFieldsCollection(null);
		}

		if (!context.areAddressFieldsRequiredBecauseOfSaveRegistrationInput()) {
			if (address != null) {
				if (!isCustomerProfileMissingRequiredComplianceFields(
						cfp.getAddressFieldsCollection(), customerProfile,
						address, ESB_AddressTag.class)) {
					cfp.setAddressFieldsCollection(null);
				}
			}
		}
	}

	/**
	 * 
	 * @param fieldsCollection
	 *            The fields defined for the type of object (phone | adress)
	 *            that need to be collected for Get@Send story
	 * @param customerProfile
	 *            Allows access to dynamic fields on customerProfile for dynamic
	 *            attribute types
	 * @param t
	 *            Contains values on a java bean that need to be checked for
	 *            existence. This must not be null
	 * @param cl
	 *            The enum class to help statically access fields on argument t
	 * @return The passed in fieldsCollection or null if these field collections
	 *         are not needed
	 */
	private <T, E extends Enum<E> & IAccessor<T>> boolean isCustomerProfileMissingRequiredComplianceFields(
			RegistrationFieldsCollection fieldsCollection,
			CustomerProfile customerProfile, T t, Class<E> cl) {
		if (fieldsCollection != null) {
			@SuppressWarnings("unchecked")
			List<ExtendedRegistrationFieldInfo> requiredFields = (List<ExtendedRegistrationFieldInfo>) fieldsCollection
					.getRequiredFields();
			for (ExtendedRegistrationFieldInfo fieldInfo : requiredFields) {
				if (isRequiredValueMissing(fieldInfo, customerProfile, t, cl)) {
					return true;
				}
			}
		}
		return false;
	}

	private <T, E extends Enum<E> & IAccessor<T>> boolean isRequiredValueMissing(
			ExtendedRegistrationFieldInfo fieldInfo,
			CustomerProfile customerProfile, T t, Class<E> cl) {
		// TODO IGNORE PrimaryPhoneCountryCode if version is 1305|1512?
		if (TransactionAttributeType.TRAN_ONLY_TYPE.equals(fieldInfo
				.getAttrType())) {
			// We should never get these, ignore them
			// TODO Log warning once per partner?
			return false;
		}
		String value;
		if (TransactionAttributeType.STANDARD_TYPE.equals(fieldInfo
				.getAttrType())) {
			IAccessor<T> accessor = Enum.valueOf(cl, fieldInfo.getNewXmlTag());
			if (accessor == null) {
				// TODO LOG WARNING, compliance item is configured as required
				// but unknown how to check for standard value
				return false;
			}
			value = accessor.getValue(t);
		} else {
			value = getDynamicValue(customerProfile, fieldInfo);
		}
		return StringUtils.isBlank(value);
	}
}