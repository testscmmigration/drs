package com.moneygram.drs.get_send;

import com.moneygram.drs.bo.Address;

public enum ESB_AddressTag implements IAccessor<Address> {

	receiverAddress(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			return address.getAddrLine1();
		}
	}),
	//
	receiverCity(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			return address.getCity();
		}
	}),

	receiverCountrySubdivisionCode(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			// NOTE: Agg3 started mapping CountrySubdivisionCode to state. The
			// Get@Send project did not have scope to
			// "fix" this this issue.

			// As a workaround, Get@Send code create column
			// CUST_PROFILE_ADDRESS#SUBDIV and stores the value in both columns
			// for 1611 clients.
			return address.getState();
		}
	}),

	// TODO There is no standard DRS field mapping, but logically is state
	receiverAccountIssueStatePartnerField(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			return address.getState();
		}
	}),
	//
	receiverPostalCode(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			return address.getPostalCode();
		}
	}),
	//
	receiverCountry(new IAccessor<Address>() {
		@Override
		public String getValue(Address address) {
			return address.getCntryCode();
		}
	});

	private final IAccessor<Address> esbTagActions;

	private ESB_AddressTag(IAccessor<Address> esbTagActions) {
		this.esbTagActions = esbTagActions;
	}

	public String getValue(Address address) {
		return esbTagActions.getValue(address);
	}
}
