package com.moneygram.drs.get_send;

import com.moneygram.drs.bo.CustomerProfilePerson;

public enum ESB_PersonTag implements IAccessor<CustomerProfilePerson> {

	receiverPrimaryPhone(new IAccessor<CustomerProfilePerson>() {
		@Override
		public String getValue(CustomerProfilePerson receiver) {
			return receiver.getPhoneNumber();
		}
	}),

	// TODO I see this newly configured in tran_attr, is this a valid gffp
	// mapping? 1512 only?
	receiverPrimaryPhoneCountryCode(new IAccessor<CustomerProfilePerson>() {
		@Override
		public String getValue(CustomerProfilePerson receiver) {
			return receiver.getPrimaryPhoneCountryCode();
		}
	}),

	// TODO I see this newly configured in tran_attr, is this a valid gffp
	// mapping? 1512 only?
	receiverPrimaryPhoneSmsEnabled(new IAccessor<CustomerProfilePerson>() {
		@Override
		public String getValue(CustomerProfilePerson receiver) {
			return receiver.getSmsOptInEnabledFlag() == null ? null : receiver
					.getSmsOptInEnabledFlag().toString();
		}
	});

	private final IAccessor<CustomerProfilePerson> esbTagActions;

	private ESB_PersonTag(IAccessor<CustomerProfilePerson> esbTagActions) {
		this.esbTagActions = esbTagActions;
	}

	public String getValue(CustomerProfilePerson customerProfilePerson) {
		return esbTagActions.getValue(customerProfilePerson);
	}
}
