package com.moneygram.drs.get_send;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.AlertErrorInfo;
import com.moneygram.drs.bo.AlertRegistrationInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;

public class AlertRegistrationInfoBuilder {
	public static AlertRegistrationInfo convertToAgg3Structure(
			RegistrationFieldsCollection registrationFieldsCollection) {
		List<AlertErrorInfo> errorList = new ArrayList<AlertErrorInfo>();

		if (registrationFieldsCollection != null) {
			for (Collection<ExtendedRegistrationFieldInfo> registrationFieldData : registrationFieldsCollection
					.getFullList()) {
				AlertErrorInfo errorInfo = new AlertErrorInfo();
				errorInfo.setDefaultErrorInfo();
				String newXmlTag = registrationFieldData.iterator().next()
						.getNewXmlTag();
				if (!StringUtility.isNullOrEmpty(newXmlTag)) {
					registrationFieldData.iterator().next()
							.setXmlTag(newXmlTag.toUpperCase());
					errorInfo.setRegistrationFieldData(registrationFieldData
							.iterator().next());
					errorList.add(errorInfo);
				}
			}
		}
		AlertRegistrationInfo alertRegistrationInfo = new AlertRegistrationInfo();
		alertRegistrationInfo.setDefaultAlertInfo();
		alertRegistrationInfo.setErrorData(errorList);
		return alertRegistrationInfo;
	}
}
