package com.moneygram.drs.get_send;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.cache.PhoneCountryCodeCache;
import com.moneygram.drs.command.SaveRegistrationCommand;
import com.moneygram.drs.persistence.PhoneCountryCodeInfoDAO;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.request.SaveRegistrationRequest;

public class SaveRegistrationCommandHelper {
	private static Logger LOGGER = LogFactory.getInstance().getLogger(
			SaveRegistrationCommandHelper.class);

	static ApplicationContext appContext = new ClassPathXmlApplicationContext(
			"applicationContextEhCache.xml");

	private final SaveRegistrationRequest saveRegistrationRequest;

	/**
	 * This is not supported by a DRS mapping
	 */
	private String senderPrimaryPhoneCountryCode;
	/**
	 * This is not supported by a DRS mapping
	 */
	private String receiverCountrySubdivisionCode;

	CollectionsContext collectionsContext;

	private Map<String, KeyValuePair> oldTagToKeyValuePair;

	PhoneCountryCodeCache phoneCountryCodeCache = (PhoneCountryCodeCache) appContext
			.getBean("getPhoneCountryCode");

	/**
	 * Finds key/value Get@Send processing inputs in
	 * {@link SaveRegistrationRequest#getFieldValues()}, sets as bean values on
	 * {@link SaveRegistrationRequest}, and removes the entry from
	 * {@link SaveRegistrationRequest#getFieldValues()}.
	 * 
	 * These values are only used by RTPS when detecting a compliance update is
	 * required.
	 * 
	 * @param saveRegistrationRequest
	 */
	public SaveRegistrationCommandHelper(
			SaveRegistrationRequest saveRegistrationRequest,
			PhoneCountryCodeInfoDAO phoneCountryCodeInfoDAO) {
		this.saveRegistrationRequest = saveRegistrationRequest;

		Iterator<KeyValuePair> kvs = saveRegistrationRequest.getFieldValues()
				.iterator();

		while (kvs.hasNext()) {
			KeyValuePair kv = kvs.next();
			String key = kv.getKey();
			if ("complianceUpdates".equals(key)) {
				String[] values = kv.getValue().split(",");
				saveRegistrationRequest
						.setComplianceUpdates(new ArrayList<String>(Arrays
								.asList(values)));
				kvs.remove();
			} else if ("supportsComplianceFields".equals(key)) {
				saveRegistrationRequest.setSupportsComplianceFields(Boolean
						.parseBoolean(kv.getValue()));
				kvs.remove();
			} else if ("skipReceiverPhoneCountryCode".equals(key)) {
				saveRegistrationRequest.setSkipReceiverPhoneCountryCode(Boolean
						.parseBoolean(kv.getValue()));
				kvs.remove();
			} else if ("senderPrimaryPhoneCountryCode".equals(key)) {
				senderPrimaryPhoneCountryCode = kv.getValue();
			} else if ("receiverCountrySubdivisionCode".equals(key)) {
				receiverCountrySubdivisionCode = kv.getValue();
			}
		}
		LOGGER.info(String
				.format("complianceUpdates=%s supportsComplianceFields=%s skipReceiverPhoneCountryCode=%s",
						saveRegistrationRequest.getComplianceUpdates(),
						saveRegistrationRequest.isSupportsComplianceFields(),
						saveRegistrationRequest
								.isSkipReceiverPhoneCountryCode()));
		if (saveRegistrationRequest.hasComplianceUpdates()
				&& StringUtils.isEmpty(saveRegistrationRequest
						.getCustomerReceiveNumber())) {
			// TODO Murugan: throw new
			// ...("CustomerReceiveNumber is required for ComplianceUpdates")
		}
	}

	/**
	 * At this point, {@link SaveRegistrationCommand} has already updated
	 * {@link SaveRegistrationRequest#getFieldValues()} to replace all 1611 esb
	 * tags to the DRS attr names.
	 * 
	 * @param registrationFieldsCollection
	 */
	public void applyFieldsAfterOldTagUpdate(
			CollectionsContext collectionsContext) {
		this.collectionsContext = collectionsContext;
		oldTagToKeyValuePair = new HashMap<String, KeyValuePair>();
		for (KeyValuePair fieldValue : saveRegistrationRequest.getFieldValues()) {
			oldTagToKeyValuePair.put(fieldValue.getKey(), fieldValue);
		}
	}

	/**
	 * Returns the the {@link SaveRegistrationRequest} field value given the ESB
	 * tag name. This method should only be called after
	 * {@link #applyFieldsAfterOldTagUpdate(RegistrationFieldsCollection)}
	 * 
	 * @param newTagName
	 * @return
	 */
	public String getSRRValueFromNewTagName(String newTagName) {
		ExtendedRegistrationFieldInfo fieldInfo = collectionsContext
				.getFirstEntryForNewXmlTag(newTagName);

		if (fieldInfo != null) {
			String oldXmlTag = fieldInfo.getXmlTag();
			KeyValuePair field = oldTagToKeyValuePair.get(oldXmlTag);
			if (field != null) {
				return field.getValue();

			}
		}
		return null;
	}

	/**
	 * Apply Get@Send processing to the profile that is about to be saved
	 * 
	 * @param profileToSave
	 *            {@link SaveRegistrationRequest} data converted to a
	 *            {@link CustomerProfile}
	 * @param existingProfile
	 *            will never be null, but will be empty for intra-tran
	 * @throws DRSException
	 */
	public void enrichCustProfile(CustomerProfile profileToSave,
			CustomerProfile existingProfile) throws DRSException {
		if (senderPrimaryPhoneCountryCode != null
				&& profileToSave.getCreator() != null
				&& profileToSave.getCreator().getPhoneNumber() != null) {
			profileToSave.getCreator().setPrimaryPhoneCountryCode(
					senderPrimaryPhoneCountryCode);
		}

		CustomerProfilePerson receiverToSave = profileToSave.getReceiver();
		String inPhone = getSRRValueFromNewTagName(ESB_PersonTag.receiverPrimaryPhone
				.name());
		if (inPhone != null) {
			// TODO Dean: Only if compliance is turned on?

			if (receiverToSave == null) {
				receiverToSave = new CustomerProfilePerson();
			}
			receiverToSave.setSmsOptInEnabledFlag(true);
			updateReceiverPrimaryPhoneCountryCodeIfNeeded(receiverToSave,
					existingProfile);
			if (!StringUtils.isEmpty(receiverToSave
					.getPrimaryPhoneCountryCode())) {
				// If phone was a custom value, then the receiver object may be
				// null. Update if there is a value to save.
				profileToSave.setReceiver(receiverToSave);
			}
		}

		if (receiverToSave != null) {
			// This returns custom or standard field
			if (receiverCountrySubdivisionCode != null) {
				// receiverCountrySubdivisionCode is not supported in Address
				// table through fieldInfo mapping, so we will explicitly set
				List<Address> addresses = receiverToSave.getAddresses();
				if (addresses != null && addresses.size() > 0) {
					Address address = addresses.get(0);
					address.setCountrySubDivCode(receiverCountrySubdivisionCode);
				}
			}
		}
	}

	private void updateReceiverPrimaryPhoneCountryCodeIfNeeded(
			CustomerProfilePerson receiverToSave,
			CustomerProfile existingProfile) throws DRSException {
		// If user did not pass in value
		if (StringUtils.isEmpty(receiverToSave.getPrimaryPhoneCountryCode())) {
			// Find fieldInfo for receiverCountry
			ExtendedRegistrationFieldInfo receiverCountry_fieldInfo = collectionsContext
					.getFirstEntryForNewXmlTag(ESB_AddressTag.receiverCountry
							.name());

			// If field info exists (for compliance configuration, it always
			// should)
			String receiverPrimaryPhoneCountryCode = null;
			if (receiverCountry_fieldInfo != null) {
				receiverPrimaryPhoneCountryCode = deriveReceiverPrimaryPhoneCountryCode(
						receiverCountry_fieldInfo, existingProfile);
			}
			receiverToSave
					.setPrimaryPhoneCountryCode(receiverPrimaryPhoneCountryCode);
		}
	}

	private String deriveReceiverPrimaryPhoneCountryCode(
			ExtendedRegistrationFieldInfo receiverCountry_fieldInfo,
			CustomerProfile existingProfile) throws DRSException {
		// Get country from request
		String addressCountryToUseForConversion = getSRRValueFromNewTagName(ESB_AddressTag.receiverCountry
				.name());

		// If country from request not available, use country available
		// in existingProfile
		if (addressCountryToUseForConversion == null && existingProfile != null) {
			Address existingReceiverAddress = existingProfile
					.getReceiverAddress();
			if (existingReceiverAddress != null) {
				// The value may be standard or custom, so use FieldUtil
				// to access
				addressCountryToUseForConversion = FieldUtil.getValue(
						receiverCountry_fieldInfo, existingProfile,
						existingReceiverAddress, ESB_AddressTag.class);
			}
		}

		String result = null;
		if (addressCountryToUseForConversion != null) {
			// A value was found in the input or existing profile
			result = convertToPhoneCountryCode(addressCountryToUseForConversion);
		}
		return result;
	}

	/**
	 * 
	 * @param country
	 * @return A phone country code
	 * @throws DRSException
	 */
	private String convertToPhoneCountryCode(String country)
			throws DRSException {
		if(StringUtils.isEmpty(country)){
			return null;
		}
		return phoneCountryCodeCache.getCountryToPhoneCountryCode(country);
	}
}