package com.moneygram.drs.get_send;

import java.util.Date;

import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfile2;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class CustomerProfileSimpleDAO {

	public static final String CLIENT = "client";

	public CustomerProfile readCustomerProfile(
			CustomerProfileCriteria customerProfileCriteria)
			throws NotFoundException, DRSException {
		ObjectId objectId = new ObjectId(CustomerProfile2.class,
				customerProfileCriteria);
		CustomerProfile2 customerProfile = (CustomerProfile2) PersistenceManagerFactory
				.getInstance().read(objectId);
		if (customerProfile == null) {
			throw new DRSException(DRSExceptionCodes.EC316_CUSTOMER_NOT_FOUND,
					"Customer receive number:"
							+ customerProfileCriteria
									.getCustomerReceiveNumber(),
					"mgCustomerReceiveNumber", new Date(),
					"Customer is not found", CLIENT);
		}
		return customerProfile;
	}
}
