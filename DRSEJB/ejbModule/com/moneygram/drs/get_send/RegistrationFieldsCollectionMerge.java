package com.moneygram.drs.get_send;

import java.util.List;
import java.util.Map;

import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.util.ObjectUtils;

public class RegistrationFieldsCollectionMerge {

	/**
	 * Merges the 2 collections into 1.
	 * 
	 * If multiple {@link RegistrationFieldInfo}s exist for the same xmlTag in
	 * different sets, then the returned
	 * {@link RegistrationFieldInfo#isRequired()} for that xmlTag will be set to
	 * true if any of the values in any set are true.
	 * 
	 * @param reg
	 *            May be null
	 * @param address
	 *            May be null
	 * @param phone
	 *            May be null
	 * @return A new collection merging all together. Highest visibility is
	 *         preferred when common field exists
	 */
	public RegistrationFieldsCollection merge(RegistrationFieldsCollection reg,
			RegistrationFieldsCollection address,
			RegistrationFieldsCollection phone,
			boolean saveRegistrationWithNewXmlTags) {
		/*
		 * NOTE RegistrationFieldsCollection has multiple values per field
		 * (Map<String, List<ExtendedRegistrationFieldInfo>>
		 * fullRegistrationFieldMap) to facilitate multiple languages being
		 * returned. This input for iv_language is comma separated like en,fr
		 */
		if (reg != null) {
			if ((address == null || address.getFullListCount() == 0)
					&& (phone == null || phone.getFullListCount() == 0)) {
				return reg;
			}
			Map<String, List<ExtendedRegistrationFieldInfo>> regFieldMap = reg
					.getMutableXmlTagMap();
			if (address != null && address.getFullListCount() > 0) {
				merge(regFieldMap, address.getMutableXmlTagMap());
			}
			if (phone != null && phone.getFullListCount() > 0) {
				merge(regFieldMap, phone.getMutableXmlTagMap());
			}
			return createNewRegistrationFieldsCollection(reg,
					saveRegistrationWithNewXmlTags, regFieldMap);
		} else {
			if (phone == null) {
				return address;
			}
			if (address == null) {
				return phone;
			}
			Map<String, List<ExtendedRegistrationFieldInfo>> addressFieldMap = address
					.getMutableXmlTagMap();
			merge(addressFieldMap, phone.getMutableXmlTagMap());
			return createNewRegistrationFieldsCollection(address,
					saveRegistrationWithNewXmlTags, addressFieldMap);
		}
	}

	RegistrationFieldsCollection createNewRegistrationFieldsCollection(
			RegistrationFieldsCollection original,
			boolean saveRegistrationWithNewXmlTags,
			Map<String, List<ExtendedRegistrationFieldInfo>> newFields) {
		List<ExtendedRegistrationFieldInfo> fields = ObjectUtils
				.flatten(newFields.values());
		return new RegistrationFieldsCollection(fields,
				original.getDccCardRange(),
				original.isSupplementalFieldExist(),
				saveRegistrationWithNewXmlTags);
	}

	void merge(Map<String, List<ExtendedRegistrationFieldInfo>> toUpdate,
			Map<String, List<ExtendedRegistrationFieldInfo>> mergeFrom) {
		for (Map.Entry<String, List<ExtendedRegistrationFieldInfo>> entry : mergeFrom
				.entrySet()) {
			String key = entry.getKey();
			List<ExtendedRegistrationFieldInfo> toUpdateInfos = toUpdate
					.get(key);
			if (toUpdateInfos == null || toUpdateInfos.size() == 0) {
				toUpdate.put(key, entry.getValue());
			} else {
				List<ExtendedRegistrationFieldInfo> mergeFromInfos = entry
						.getValue();
				if (mergeFromInfos != null && mergeFromInfos.size() > 0) {
					boolean required = toUpdateInfos.get(0).isRequired()
							|| mergeFromInfos.get(0).isRequired();
					updateRequired(toUpdateInfos, required);
				}
			}
		}
	}

	private void updateRequired(
			List<ExtendedRegistrationFieldInfo> toUpdateInfos, boolean required) {
		for (ExtendedRegistrationFieldInfo info : toUpdateInfos) {
			info.setRequired(required);
		}
	}
}