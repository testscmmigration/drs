package com.moneygram.drs.get_send;

import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;

public class CollectionsContext {

	private RegistrationFieldsCollection addressFieldsCollection;
	private RegistrationFieldsCollection phoneFieldsCollection;
	private RegistrationFieldsCollection mergedFieldsCollection;

	public RegistrationFieldsCollection getAddressFieldsCollection() {
		return addressFieldsCollection;
	}

	public void setAddressFieldsCollection(
			RegistrationFieldsCollection addressFieldsCollection) {
		this.addressFieldsCollection = addressFieldsCollection;
	}

	public RegistrationFieldsCollection getPhoneFieldsCollection() {
		return phoneFieldsCollection;
	}

	public void setPhoneFieldsCollection(
			RegistrationFieldsCollection phoneFieldsCollection) {
		this.phoneFieldsCollection = phoneFieldsCollection;
	}

	public RegistrationFieldsCollection getMergedFieldsCollection() {
		return mergedFieldsCollection;
	}

	public void setMergedFieldsCollection(
			RegistrationFieldsCollection mergedFieldsCollection) {
		this.mergedFieldsCollection = mergedFieldsCollection;
	}

	public ExtendedRegistrationFieldInfo getFirstEntryForNewXmlTag(
			String newTagName) {
		ExtendedRegistrationFieldInfo fieldInfo = null;
		if (mergedFieldsCollection != null) {
			fieldInfo = mergedFieldsCollection
					.getFirstEntryForNewXmlTag(newTagName);
		}
		if (fieldInfo == null && addressFieldsCollection != null) {
			fieldInfo = addressFieldsCollection
					.getFirstEntryForNewXmlTag(newTagName);
		}
		if (fieldInfo == null && phoneFieldsCollection != null) {
			fieldInfo = phoneFieldsCollection
					.getFirstEntryForNewXmlTag(newTagName);
		}
		return fieldInfo;
	}
}
