package com.moneygram.drs.get_send;

import java.util.Date;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationFieldsCollection2;
import com.moneygram.drs.constants.DRSConstants;
import com.moneygram.drs.criteria.FieldsInfoCriteria;
import com.moneygram.drs.persistence.ObjectId;
import com.moneygram.drs.persistence.PersistenceManager;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class FieldsCollectionService implements DRSConstants {
	public static final String CLIENT = "client";
	private static final Logger log = LogFactory.getInstance().getLogger(
			FieldsCollectionService.class);

	RegistrationFieldsCollectionMerge fieldsCollectionMerge = new RegistrationFieldsCollectionMerge();

	UpdateRRNComplianceFieldsCollectionsFilter updateRRNComplianceFieldsFilter = new UpdateRRNComplianceFieldsCollectionsFilter(
			new CustomerProfileSimpleDAO());

	PersistenceManager persistenceManager = PersistenceManagerFactory
			.getInstance();

	public RegistrationFieldsCollection execute(FieldsCollectionRequest request)
			throws Exception {
		CollectionsContext executeForContext = executeForContext(request);
		return executeForContext.getMergedFieldsCollection();
	}

	public CollectionsContext executeForContext(FieldsCollectionRequest request)
			throws Exception {
		log.info(String
				.format("FieldsCollectionService.execute complianceObjectTypesForUpdate=%s customerReceiveNumber=%s receiveAgentId=%s deliveryOption=%s",
						request.getComplianceObjectTypesForUpdate(), request
								.getCustomerReceiveNumber(), request
								.getDaoCriteria().getReceiveAgentId(), request
								.getDaoCriteria().getDeliveryOption()));

		RegistrationFieldsCollection fieldsCollection = handleIntraTran(request);

		CollectionsContext context = new CollectionsContext();
		RegistrationFieldsCollection mergedFieldsCollection = handleComplianceFields(
				request, fieldsCollection, context);
		context.setMergedFieldsCollection(mergedFieldsCollection);

		return context;
	}

	private RegistrationFieldsCollection handleIntraTran(
			FieldsCollectionRequest request) throws NotFoundException,
			DRSException {
		RegistrationFieldsCollection registrationFieldsCollection = null;

		if (!request.isDisableRegistrationFields()) {
			FieldsInfoCriteria daoCriteria = request.getDaoCriteria();
			ObjectId objectId = new ObjectId(
					RegistrationFieldsCollection.class, daoCriteria);
			// RegistrationFieldsCollection
			registrationFieldsCollection = (RegistrationFieldsCollection) persistenceManager
					.read(objectId);
			if (registrationFieldsCollection == null) {
				throw new DRSException(
						DRSExceptionCodes.EC515_NO_FIELDS_FOUND_FOR_REGISTRATION,
						"No registration fields found for agent:", daoCriteria
								.getReceiveAgentId(), new Date(),
						" deliveryOption: " + daoCriteria.getDeliveryOption()
								+ "Receive Agent Id: "
								+ daoCriteria.getReceiveAgentId(), CLIENT);
			}
		}
		return registrationFieldsCollection;
	}

	/**
	 * If compliance fields are required, they are loaded and merged with
	 * registrationFieldsCollection.
	 * 
	 * @param request
	 * @param registrationFieldsCollection
	 *            Registration related fields, may be null
	 * @return The merged set, or registrationFieldsCollection if compliance
	 *         fields are not needed.
	 * @throws NotFoundException
	 * @throws DRSException
	 */
	private RegistrationFieldsCollection handleComplianceFields(
			FieldsCollectionRequest request,
			RegistrationFieldsCollection registrationFieldsCollection,
			CollectionsContext collectionsContext) throws NotFoundException,
			DRSException {

		/*
		 * Why load compliance fields even when compliance isn't being used?
		 * Because we want to support phone country code derive logic in all
		 * scenarios, and to do that we need meta-data about address and phone
		 * fields even when the client isn't requesting compliance fields.
		 * 
		 * TODO: Low priority optimization - CollectionsContext could be updated
		 * to lazily load address or phone fields collections. The main issue
		 * with existing DRS architecture is throwing of checked exceptions
		 * which impacts trying to perform this type of refactoring.
		 */
		if (request.isSupportsComplianceFields()) {
			ObjectId objectId = new ObjectId(RegistrationFieldsCollection2.class,
				request.getDaoCriteria());
			ComplianceFieldsCollections complianceFieldsCollection = loadGetSendFieldCollections(objectId);
			collectionsContext
				.setAddressFieldsCollection(complianceFieldsCollection
						.getAddressFieldsCollection());
			collectionsContext.setPhoneFieldsCollection(complianceFieldsCollection
				.getPhoneFieldsCollection());

			// If updating RRN, then remove fields for collection if already
			// stored in CustomerProfile
			UpdateRRNComplianceFieldsContext context = new UpdateRRNComplianceFieldsContext(
					request, complianceFieldsCollection);
			updateRRNComplianceFieldsFilter.process(context);

			RegistrationFieldsCollection mergedFieldsCollection = fieldsCollectionMerge
					.merge(registrationFieldsCollection,
							complianceFieldsCollection
									.getAddressFieldsCollection(),
							complianceFieldsCollection
									.getPhoneFieldsCollection(), request
									.getDaoCriteria()
									.isSaveRegistrationWithNewXmlTags());
			return mergedFieldsCollection;
		} else {
			// If compliance fields are not supported, then nothing to do
			return registrationFieldsCollection;
		}
	}

	private ComplianceFieldsCollections loadGetSendFieldCollections(
			ObjectId objectId) throws NotFoundException, DRSException {
		ComplianceFieldsCollections fieldsCollection = new ComplianceFieldsCollections();

		RegistrationFieldsCollection addressGetSendFieldsCollection = (RegistrationFieldsCollection) persistenceManager
				.read(objectId, DRSConstants.ADDR_COMP_ATTR_PRC);
		fieldsCollection
				.setAddressFieldsCollection(addressGetSendFieldsCollection);

		RegistrationFieldsCollection phoneGetSendFieldsCollection = (RegistrationFieldsCollection) persistenceManager
				.read(objectId, DRSConstants.PHONE_COMP_ATTR_PRC);
		fieldsCollection.setPhoneFieldsCollection(phoneGetSendFieldsCollection);
		return fieldsCollection;
	}
}