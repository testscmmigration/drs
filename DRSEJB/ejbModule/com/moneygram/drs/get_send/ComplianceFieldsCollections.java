package com.moneygram.drs.get_send;

import com.moneygram.drs.bo.RegistrationFieldsCollection;

public class ComplianceFieldsCollections {
	RegistrationFieldsCollection addressFieldsCollection;
	RegistrationFieldsCollection phoneFieldsCollection;

	public RegistrationFieldsCollection getAddressFieldsCollection() {
		return addressFieldsCollection;
	}

	public void setAddressFieldsCollection(
			RegistrationFieldsCollection addressFieldsCollection) {
		this.addressFieldsCollection = addressFieldsCollection;
	}

	public RegistrationFieldsCollection getPhoneFieldsCollection() {
		return phoneFieldsCollection;
	}

	public void setPhoneFieldsCollection(
			RegistrationFieldsCollection phoneFieldsCollection) {
		this.phoneFieldsCollection = phoneFieldsCollection;
	}
}
