package com.moneygram.drs.get_send;

import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.TransactionAttributeType;

public class FieldUtil {
	public static String getDynamicValue(CustomerProfile customerProfile,
			ExtendedRegistrationFieldInfo fieldInfo) {
		CustomerProfileCustomField customFieldByXMLTag = customerProfile
				.getCustomField(fieldInfo);
		return customFieldByXMLTag == null ? null : customFieldByXMLTag
				.getValue();
	}

	public static <T, E extends Enum<E> & IAccessor<T>> String getValue(
			ExtendedRegistrationFieldInfo fieldInfo,
			CustomerProfile customerProfile, T t, Class<E> cl) {
		// TODO IGNORE PrimaryPhoneCountryCode if version is 1305|1512?
		if (TransactionAttributeType.TRAN_ONLY_TYPE.equals(fieldInfo
				.getAttrType())) {
			// We should never get these, ignore them
			// TODO Log warning once per partner?
			return null;
		}
		String value;
		if (TransactionAttributeType.STANDARD_TYPE.equals(fieldInfo
				.getAttrType())) {
			IAccessor<T> accessor = Enum.valueOf(cl, fieldInfo.getNewXmlTag());
			if (accessor == null) {
				// TODO LOG WARNING, compliance item is configured as required
				// but unknown how to check for standard value
				return null;
			}
			value = accessor.getValue(t);
		} else {
			value = getDynamicValue(customerProfile, fieldInfo);
		}
		return value;
	}
}
