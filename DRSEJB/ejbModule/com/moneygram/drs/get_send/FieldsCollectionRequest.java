package com.moneygram.drs.get_send;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.moneygram.drs.criteria.FieldsInfoCriteria;

public class FieldsCollectionRequest {
	public static final String RECEIVER_PHONE = "RECEIVER_PHONE";
	public static final String RECEIVER_ADDRESS = "RECEIVER_ADDRESS";
	
	// Defaults prior to Get@Send story
	boolean supportsComplianceFields = false;
	boolean disableRegistrationFields = false;

	String customerReceiveNumber;
	FieldsInfoCriteria daoCriteria;

	private final List<String> complianceObjectTypesForUpdate = new ArrayList<String>();

	/**
	 * Specifies if address fields and/or phone fields should be loaded. Valid
	 * values are Maybe RECEIVER_ADDRESS or RECEIVER_PHONE. When empty, both
	 * should be loaded.
	 * 
	 * @return
	 */
	public List<String> getComplianceObjectTypesForUpdate() {
		return complianceObjectTypesForUpdate;
	}

	public boolean isDisableRegistrationFields() {
		return disableRegistrationFields;
	}

	public void setDisableRegistrationFields(boolean supportsRegistrationFields) {
		this.disableRegistrationFields = supportsRegistrationFields;
	}

	public String getCustomerReceiveNumber() {
		return customerReceiveNumber;
	}

	/**
	 * The value should not always be set when an customerReceiveNumber exists.
	 * 
	 * When set, compliance fields to collect are filtered by examining existing
	 * CustomerProfile and determining if all compliance address or all
	 * compliance phone fields already exist. This is used during fieldsAlerts
	 * and directedSendFields requests when sending data back to POE.
	 * 
	 * When performing an update during save registration, this value should be
	 * set to null as not to filter the compliance fields.
	 * 
	 * When RTPS is determining how to format a saveRegistration request for
	 * compliance update, this value should also be set to null as not to filter
	 * fields.
	 * 
	 * @param customerReceiveNumber
	 */
	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}
	
	public boolean isCustomerReceiveNumberAvailable() {
		return StringUtils.isNotBlank(customerReceiveNumber);
	}

	public boolean isSupportsComplianceFields() {
		return supportsComplianceFields;
	}

	public void setSupportsComplianceFields(boolean supportsComplianceFields) {
		this.supportsComplianceFields = supportsComplianceFields;
	}

	public FieldsInfoCriteria getDaoCriteria() {
		return daoCriteria;
	}

	public void setDaoCriteria(FieldsInfoCriteria daoCriteria) {
		this.daoCriteria = daoCriteria;
	}

}
