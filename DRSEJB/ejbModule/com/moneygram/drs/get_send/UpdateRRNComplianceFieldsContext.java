package com.moneygram.drs.get_send;

public class UpdateRRNComplianceFieldsContext {
	FieldsCollectionRequest fieldsCollectionRequest;

	ComplianceFieldsCollections complianceFieldsCollections;

	public UpdateRRNComplianceFieldsContext(
			FieldsCollectionRequest fieldsCollectionRequest,
			ComplianceFieldsCollections complianceFieldsCollections) {
		super();
		this.fieldsCollectionRequest = fieldsCollectionRequest;
		this.complianceFieldsCollections = complianceFieldsCollections;
	}

	public FieldsCollectionRequest getFieldsCollectionRequest() {
		return fieldsCollectionRequest;
	}

	public ComplianceFieldsCollections getComplianceFieldsCollections() {
		return complianceFieldsCollections;
	}

	public boolean arePhoneFieldsRequiredBecauseOfSaveRegistrationInput() {
		return fieldsCollectionRequest.getComplianceObjectTypesForUpdate()
				.contains("RECEIVER_PHONE");
	}

	public boolean areAddressFieldsRequiredBecauseOfSaveRegistrationInput() {
		return fieldsCollectionRequest.getComplianceObjectTypesForUpdate()
				.contains("RECEIVER_ADDRESS");
	}
}