package com.moneygram.drs.get_send;

interface IAccessor<T> {
	String getValue(T t);
}