package com.moneygram.drs.cache;

import com.moneygram.drs.profiles.DRSException;


public interface PhoneCountryCodeCache  {
	
	public String getCountryToPhoneCountryCode(String countryCode) throws DRSException;

}
