package com.moneygram.drs.cache;

import java.util.Iterator;
import java.util.Map;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.persistence.PhoneCountryCodeInfoDAO;
import com.moneygram.drs.profiles.DRSException;

public class PhoneCountryCodeCacheImpl implements PhoneCountryCodeCache {
	
	private static Logger LOGGER = LogFactory.getInstance().getLogger(
			PhoneCountryCodeCacheImpl.class);
	
	private Ehcache phoneCountryCodeCache;
	
	@Override
	public String getCountryToPhoneCountryCode(String countryCode) throws DRSException {
		Map<String, String> phoneCountryCodeMap = null; 
		if(phoneCountryCodeCache.getKeysWithExpiryCheck().isEmpty()) {
			synchronized(phoneCountryCodeCache) {
				LOGGER.info("Load phoneCountryCodeCache from DAO ");
				PhoneCountryCodeInfoDAO phoneCountryCodeInfoDAO = new PhoneCountryCodeInfoDAO();
				phoneCountryCodeMap = phoneCountryCodeInfoDAO.loadCountryToPhoneCountryCode();
				populatePhoneCountryCodeCache(phoneCountryCodeMap);
				return phoneCountryCodeMap.get(countryCode);
			}
		} else {
			LOGGER.info("Load phoneCountryCode from cache ");
			Element element = phoneCountryCodeCache.get(countryCode);
			if(element!=null){
				return (String)element.getValue();
			}
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void populatePhoneCountryCodeCache(Map<String, String> phoneCountryCodeMap){
		 Iterator it = phoneCountryCodeMap.entrySet().iterator();
		 while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        Element element = new Element(pair.getKey(), pair.getValue());
		        phoneCountryCodeCache.put(element);
		 }
	}

	public void setPhoneCountryCodeCache(Ehcache phoneCountryCodeCache) {
		this.phoneCountryCodeCache = phoneCountryCodeCache;
	}

}
