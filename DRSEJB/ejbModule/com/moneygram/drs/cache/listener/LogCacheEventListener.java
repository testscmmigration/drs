package com.moneygram.drs.cache.listener;


import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * Cache Event Listener to log all cache events
 */
final class LogCacheEventListener implements CacheEventListener {
	private static Logger LOGGER = LogFactory.getInstance().getLogger(
			LogCacheEventListener.class);
	/**
	 * Method dispose.
	 * @see net.sf.ehcache.event.CacheEventListener#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	/**
	 * Method notifyElementEvicted.
	 * Called when elements is removed from cache due the eviction policy
	 * @param arg0 Ehcache
	 * @param arg1 Element
	 * @see net.sf.ehcache.event.CacheEventListener#notifyElementEvicted(Ehcache, Element)
	 */
	@Override
	public void notifyElementEvicted(Ehcache arg0, Element arg1) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT EVICTED : "+arg1.toString());
		}
	}

	/**
	 * Method notifyElementExpired.
	 * Called when an element expires in the cache
	 * @param arg0 Ehcache
	 * @param arg1 Element
	 * @see net.sf.ehcache.event.CacheEventListener#notifyElementExpired(Ehcache, Element)
	 */
	@Override
	public void notifyElementExpired(Ehcache arg0, Element arg1) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT EXPIRED : "+arg1.toString());
		}
	}

	/**
	 * Method notifyElementPut.
	 * Called when a new element is put in cache
	 * @param arg0 Ehcache
	 * @param arg1 Element
	 * @throws CacheException
	 * @see net.sf.ehcache.event.CacheEventListener#notifyElementPut(Ehcache, Element)
	 */
	@Override
	public void notifyElementPut(Ehcache arg0, Element arg1)
			throws CacheException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT PUT : "+arg1.toString());
		}
	}

	/**
	 * Method notifyElementRemoved.
	 * Called when an element is removed from cache
	 * @param arg0 Ehcache
	 * @param arg1 Element
	 * @throws CacheException
	 * @see net.sf.ehcache.event.CacheEventListener#notifyElementRemoved(Ehcache, Element)
	 */
	@Override
	public void notifyElementRemoved(Ehcache arg0, Element arg1)
			throws CacheException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT REMOVED : "+arg1.toString());
		}
	}

	/**
	 * Method notifyElementUpdated.
	 * Called when a cache element is updated
	 * @param arg0 Ehcache
	 * @param arg1 Element
	 * @throws CacheException
	 * @see net.sf.ehcache.event.CacheEventListener#notifyElementUpdated(Ehcache, Element)
	 */
	@Override
	public void notifyElementUpdated(Ehcache arg0, Element arg1)
			throws CacheException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT UPDATED : "+arg1.toString());
		}

	}

	/**
	 * Method notifyRemoveAll.
	 * Called when all elements all removed from the cache
	 * @param arg0 Ehcache
	 * @see net.sf.ehcache.event.CacheEventListener#notifyRemoveAll(Ehcache)
	 */
	@Override
	public void notifyRemoveAll(Ehcache arg0) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("CACHE NAME : "+arg0.getName()+" : ELEMENT REMOVED : ALL");
		}
	}
	
	/**
	 * Method clone.
	 * Called when an listener is cloned
	 * @return Object
	 * @throws CloneNotSupportedException
	 * @see net.sf.ehcache.event.CacheEventListener#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Singleton instance");
	}

}
