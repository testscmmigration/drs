package com.moneygram.drs.messagebean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerReceiveDetails;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.util.JAXBUtil;

/**
 * Message-Driven Bean implementation class for: CustomerReceiveDetailsListenerBean
 *
 */
@MessageDriven(mappedName = "jms/TD.TO.DRS.REQ", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable") })
public class CustomerReceiveDetailsListenerBean implements MessageListener {

	private static Logger log = LogFactory.getInstance().getLogger(CustomerReceiveDetailsListenerBean.class);
    /**
     * Default constructor. 
     */
    public CustomerReceiveDetailsListenerBean() {
        // TODO Auto-generated constructor stub    	
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        // TODO Auto-generated method stub
    	try{
	    	
    		TextMessage tm = (TextMessage) message;
			String customerRRNMessage = tm.getText();
			log.info("onMessage(): raw inbound message: ["+ customerRRNMessage + "]");
			
			CustomerReceiveDetails details = new CustomerReceiveDetails();
			
			details = JAXBUtil.convertFromXml(customerRRNMessage, CustomerReceiveDetails.class);	
			
			log.info("onMessage(): CustomerReceiveDetails : ["+ details + "]");
			
			updateCustomerTransactionUseFlag(details);
			
    	}catch (JMSException ex) {
			log.error("Could not process JMS message (message error)", ex);
		} catch (Exception ex) {
			log.error("Unexpected exception.", ex);
		} catch (Throwable ex) {
			log.error("Unexpected exception.", ex);
		}       
    }
    
    private void updateCustomerTransactionUseFlag(CustomerReceiveDetails details){
    	try{
    		
    		boolean customerProfile = (boolean) PersistenceManagerFactory.getInstance().updateTransactionUseFlag(details);
    		    		
    	}catch(Exception e){
    		log.error("Exception during updateCustomerTransactionUseFlag : ",e);    		
    	}
    }
    
    
}
