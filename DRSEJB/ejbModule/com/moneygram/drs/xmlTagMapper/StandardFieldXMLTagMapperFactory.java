package com.moneygram.drs.xmlTagMapper;

public class StandardFieldXMLTagMapperFactory {
	public static StandardFieldXMLTagMapper getInstance() throws Exception {
		StandardFieldXMLTagMapper fieldXMLTagMapper = new StandardFieldXMLTagMapperImpl();
		return fieldXMLTagMapper;
	}
}
