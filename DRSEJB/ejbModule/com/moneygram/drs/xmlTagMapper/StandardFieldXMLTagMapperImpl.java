/*
 * Created on Feb 22, 2005
 *
 */
package com.moneygram.drs.xmlTagMapper;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import com.moneygram.common.mgEnum.EnumRegistry;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.KeyValuePair;
import com.moneygram.drs.bo.RegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;

/**
 * This class implements the standard profile mappings between xml tagged name
 * value pairs and the customer profile object used for persistence.
 * 
 * @author T007
 * 
 */
public class StandardFieldXMLTagMapperImpl implements StandardFieldXMLTagMapper {
	public static final String XML_TAG_CAT_ELEM = "XMLTagCategory";
	public static final String XML_TAG_ELEM = "XmlTag";
	public static final String NAME_ATTR = "name";
	public static final String TYPE_ATTR = "type";
	public static final String CLASS_ATTR = "class";
	public static final String FIELD_NAME_ATTR = "classFieldName";
	public static final String PREFIXES_ATTR = "prefixes";
	public static final String END_INDEX_ATTR = "endIndex";
	public static final String OPTIONAL_ATTR = "optional";
	public static final String ENUMERATION_ATTR = "enumerationClass";
	public static final String PROFILE_ATTR_VAL = "customerProfile";
	public static final String PERSON_ATTR_VAL = "customerProfilePerson";
	public static final String ADDRESS_ATTR_VAL = "address";
	public static final String CONFIRM_FIELD_POSTFIX = "Verification";
	public static final String FIRST_NAME_POSTFIX = "FIRSTNAME";
	public static final String LAST_NAME_POSTFIX = "LASTNAME";
	public static final String SECOND_POSTFIX = "2ND";
	public static final String STATE_POSTFIX = "STATE";
	public static final String COUNTRY_POSTFIX = "COUNTRY";
	public static final String CREATOR_PREFIX_VAL = "CREATOR";
	public static final String RECEIVER_PREFIX_VAL = "RECEIVER";
	public static final String JAH_PREFIX_VAL = "JAH";
	private static final int NO_INDEX_NEEDED = -1;
	// private static final Logger log =
	// LogFactory.getInstance().getLogger(StandardFieldXMLTagMapperImpl.class);
	private Map<String, XMLMapEntry> xmlTagToCustomerProfileMap = new HashMap<String, XMLMapEntry>();
	private Map<String, XMLMapEntry> customerProfileToXMLTagMap = new HashMap<String, XMLMapEntry>();

	public StandardFieldXMLTagMapperImpl() throws Exception {
		buildMaps();
	}

	private void buildMaps() throws Exception {
		InputStream is = null;
		String classFieldName = null;
		String className = null;
		try {
			// TODO US need to take care of all the general exceptions
			// 2. Need to move this xml out to a common folder.
			is = getClass().getResourceAsStream("/com/moneygram/drs/xmlTagMapper/StdRegFieldMappings.xml");
			Document doc;
			try {
				doc = new SAXBuilder().build(is);
			} catch (JDOMException e) {
				throw new Exception("Improperly formatted standard config file", e);
			}
			Element rootElement = doc.getRootElement();
			List xmlCatElems = rootElement.getChildren();
			Iterator xmlTagCategoryIter = xmlCatElems.iterator();
			while (xmlTagCategoryIter.hasNext()) {
				Element xmlCatElem = (Element) xmlTagCategoryIter.next();
				className = xmlCatElem.getAttributeValue(CLASS_ATTR);
				String type = xmlCatElem.getAttributeValue(TYPE_ATTR);
				String prefixes = xmlCatElem.getAttributeValue(PREFIXES_ATTR);
				String endIndexStr = xmlCatElem.getAttributeValue(END_INDEX_ATTR);
				int endIndex = NO_INDEX_NEEDED;
				if (endIndexStr != null)
					endIndex = Integer.parseInt(endIndexStr);
				String[] splitPrefix = { "" };
				if (prefixes != null) {
					StringTokenizer st = new StringTokenizer(prefixes, ",", false);
					splitPrefix = new String[st.countTokens()];
					int splitIndex = 0;
					while (st.hasMoreTokens()) {
						splitPrefix[splitIndex] = st.nextToken();
						++splitIndex;
					}
				}
				Class currentClass = Class.forName(className);
				List xmlTags = xmlCatElem.getChildren(XML_TAG_ELEM);
				Iterator xmlTagIter = xmlTags.iterator();
				while (xmlTagIter.hasNext()) {
					Element xmlTagElem = (Element) xmlTagIter.next();
					String xmlTagName = xmlTagElem.getAttributeValue(NAME_ATTR);
					classFieldName = xmlTagElem.getAttributeValue(FIELD_NAME_ATTR);
					Field classField = currentClass.getDeclaredField(classFieldName);
					classField.setAccessible(true);
					String optionalStr = xmlTagElem.getAttributeValue(OPTIONAL_ATTR);
					boolean optional = false;
					if (optionalStr != null && optionalStr.equalsIgnoreCase("true"))
						optional = true;
					String enumerationStr = xmlTagElem.getAttributeValue(ENUMERATION_ATTR);
					for (int prefixIndex = 0; prefixIndex < splitPrefix.length; ++prefixIndex) {
						if (endIndex == NO_INDEX_NEEDED) {
							String fullXmlTagName = (splitPrefix[prefixIndex] + xmlTagName)
									.toUpperCase();
							// Given the type, get the field, make the entry
							XMLMapEntry xme = new XMLMapEntry(currentClass, classField,
									fullXmlTagName, type, splitPrefix[prefixIndex], optional,
									enumerationStr);
							xmlTagToCustomerProfileMap.put(fullXmlTagName, xme);
							if("RECEIVERADDRESS1".equals(fullXmlTagName)) {
								xmlTagToCustomerProfileMap.put("RECEIVERADDRESS", xme);
							}
							if("RECEIVERPHONE".equals(fullXmlTagName)) {
								xmlTagToCustomerProfileMap.put("RECEIVERPRIMARYPHONE", xme);
							}
							customerProfileToXMLTagMap.put(type + splitPrefix[prefixIndex]
									+ classFieldName, xme);
						} else {
							for (int i = 1; i <= endIndex; ++i) {
								String fullXmlTagName = (splitPrefix[prefixIndex] + i + xmlTagName)
										.toUpperCase();
								// Given the type, get the field, make the entry
								XMLMapEntry xme = new XMLMapEntry(currentClass, classField,
										fullXmlTagName, type, splitPrefix[prefixIndex], i,
										optional, enumerationStr);
								xmlTagToCustomerProfileMap.put(fullXmlTagName, xme);
								customerProfileToXMLTagMap.put(type + splitPrefix[prefixIndex]
										+ classFieldName + i, xme);
							}
						}
					}
				}
			}
		} catch (IOException iex) {
			throw new Exception("Failed to read standard field configuration file", iex);
		} catch (ClassNotFoundException cnfex) {
			throw new Exception();
		} catch (NoSuchFieldException nsfex) {
			throw new Exception("Could not find field: " + classFieldName + " on class: "
					+ className);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException iex) {/* Do nothing */
				}
			}
		}
	}

	private CustomerProfilePerson createCreator(CustomerProfile cp) {
		if (cp.getCreator() != null)
			return cp.getCreator();
		CustomerProfilePerson newPerson = new CustomerProfilePerson();
		cp.setCreator(newPerson);
		return newPerson;
	}

	private Address createAddress(CustomerProfilePerson cpp) {
		Address add;
		List l = cpp.getAddresses();
		if (l.size() == 0) {
			add = new Address();
			cpp.addAddress(add);
			return add;
		}
		return (Address) l.get(0);
	}

	private Address createCreatorAddress(CustomerProfile cp) {
		CustomerProfilePerson cpp = createCreator(cp);
		return createAddress(cpp);
	}

	private CustomerProfilePerson createReceiver(CustomerProfile cp) {
		if (cp.getReceiver() != null)
			return cp.getReceiver();
		CustomerProfilePerson newPerson = new CustomerProfilePerson();
		cp.setReceiver(newPerson);
		return newPerson;
	}

	private Address createReceiverAddress(CustomerProfile cp) {
		CustomerProfilePerson cpp = createReceiver(cp);
		return createAddress(cpp);
	}

	/**
	 * @param retProfile
	 * @param i
	 * @return
	 */
	private CustomerProfilePerson createJAH(CustomerProfile retProfile, int index) throws Exception {
		CustomerProfilePerson cpp = retProfile.getJointAccountHolderByIndex(index);
		if (cpp == null) {
			cpp = new CustomerProfilePerson();
			retProfile.addJointAccountHolder(cpp, index);
		}
		return cpp;
	}

	private Address createJAHAddress(CustomerProfile retProfile, int index) throws Exception {
		CustomerProfilePerson cpp = createJAH(retProfile, index);
		return createAddress(cpp);
	}

	/**
	 * Given a key value pair set, convert into a CustomerProfile class. This
	 * class is used by the persistence layer to read/write customer profiles.
	 * 
	 * This particular implementation uses our mapping XML file to match the tag
	 * names to the field in CustomerProfile via reflection.
	 * 
	 * @param keyValues
	 *            The key value pairs to convert.
	 * @return The built customer profile.
	 * 
	 * @see com.moneygram.rts.model.dss.mapping.StandardFieldXMLTagMapper#convertIntoCustomerProfile(java.util.Map)
	 */
	public CustomerProfile convertIntoCustomerProfile(Collection keyValues) throws Exception {
		CustomerProfile retProfile = new CustomerProfile();
		Iterator entryIter = keyValues.iterator();
		while (entryIter.hasNext()) {
			KeyValuePair entry = (KeyValuePair) entryIter.next();
			String fieldValueXMLTag = ((String) entry.getKey()).toUpperCase();
			String fieldValue = (entry.getValue());
			XMLMapEntry customerProfileEntry = (XMLMapEntry) xmlTagToCustomerProfileMap
					.get(fieldValueXMLTag);
			// See if this a non-standard field (ie there is no entry)
			if (customerProfileEntry == null) {
				// log.debug("Field: "+fieldValueXMLTag+" is custom. Value:
				// "+fieldValue);
				retProfile.addCustomFields(new CustomerProfileCustomField(fieldValueXMLTag,
						fieldValue), fieldValueXMLTag);
				continue;
			}
			Field customerProfileField = customerProfileEntry.getFieldToSet();
			int configType = customerProfileEntry.getType();
			Object objectToSetFieldOn;
			switch (configType) {
			case XMLMapEntry.CREATOR:
				objectToSetFieldOn = createCreator(retProfile);
				break;
			case XMLMapEntry.CREATOR_ADDR:
				objectToSetFieldOn = createCreatorAddress(retProfile);
				break;
			case XMLMapEntry.RECEIVER:
				objectToSetFieldOn = createReceiver(retProfile);
				break;
			case XMLMapEntry.RECEIVER_ADDR:
				objectToSetFieldOn = createReceiverAddress(retProfile);
				break;
			case XMLMapEntry.JAH:
				objectToSetFieldOn = createJAH(retProfile, customerProfileEntry.getIndex());
				break;
			case XMLMapEntry.JAH_ADDR:
				objectToSetFieldOn = createJAHAddress(retProfile, customerProfileEntry.getIndex());
				break;
			case XMLMapEntry.CUST_PROFILE:
				objectToSetFieldOn = retProfile;
				break;
			default:
				throw new Exception("Unknown xml tag type: " + configType);
			}
			try {
				if (customerProfileEntry.isEnumeration()) {
					EnumRegistry.getByName(Class
							.forName(customerProfileEntry.getEnumerationClass()), fieldValue);
				} else
					customerProfileField.set(objectToSetFieldOn, fieldValue);
			} catch (IllegalAccessException iex) {
				throw new Exception("Could not set value on field: " + configType + " to: "
						+ fieldValue);
			} catch (IllegalArgumentException iaex) {
				throw new Exception("Could not set value on field type: " + configType + " to: "
						+ fieldValue + " on object: " + objectToSetFieldOn);
			} catch (ClassNotFoundException cnfex) {
				throw new Exception("Could not find class: "
						+ customerProfileEntry.getEnumerationClass());
			}
		}
		return retProfile;
	}

	private String getXMLTagForField(String type, String prefix, String field, int index) {
		XMLMapEntry xme = null;
		if (index != NO_INDEX_NEEDED)
			xme = (XMLMapEntry) customerProfileToXMLTagMap.get(type + prefix + field + index);
		else
			xme = (XMLMapEntry) customerProfileToXMLTagMap.get(type + prefix + field);
		if (xme != null)
			return xme.getXmlTag();
		return null;
	}

	private void introspectObject(Class profClass, Object dataObject, String type, String prefix,
			int index, Map<String, Object> mapToAddTo) throws Exception {
		if (dataObject == null)
			return;
		Field[] fields = profClass.getDeclaredFields();
		int i = 0;
		try {
			for (; i < fields.length; ++i) {
				fields[i].setAccessible(true);
				String xmlTag = getXMLTagForField(type, prefix, fields[i].getName(), index);
				if (xmlTag != null) {
					mapToAddTo.put(xmlTag, fields[i].get(dataObject));
				}
			}
		} catch (IllegalAccessException iaex) {
			throw new Exception("Failed to access property: " + fields[i].getName());
		}
	}

	private void processPerson(CustomerProfilePerson cpp, String prefix,
			Map<String, Object> mapToAddTo) throws Exception {
		processPerson(cpp, prefix, NO_INDEX_NEEDED, mapToAddTo);
	}

	private void processPerson(CustomerProfilePerson cpp, String prefix, int index,
			Map<String, Object> mapToAddTo) throws Exception {
		if (cpp != null) {
			introspectObject(CustomerProfilePerson.class, cpp, PERSON_ATTR_VAL, prefix, index,
					mapToAddTo);
			List l = cpp.getAddresses();
			if (l != null) {
				for (int i = 0; i < l.size(); ++i) {
					introspectObject(Address.class, l.get(i), ADDRESS_ATTR_VAL, prefix, index,
							mapToAddTo);
				}
			}
		}
	}

	/**
	 * Given a CustomerProfile, convert back to a name-value tag set with the
	 * name being the xml tag name, and the value being the set value of the
	 * field.
	 * 
	 * This particular implementation uses reflection magic to introspect the
	 * various objects (customer profile, person, address) that make up the the
	 * Customer Profile, and then uses our XML configuration to get the
	 * appropriate XML tag name to set as the key in the map.
	 * 
	 * @param profile
	 *            The customer profile to extract from
	 * @return The key value pair map.
	 * 
	 * @see com.moneygram.rts.model.dss.mapping.StandardFieldXMLTagMapper#convertToMap(com.moneygram.rts.model.dss.CustomerProfile)
	 */
	public Map convertToMap(CustomerProfile profile) throws Exception {
		// Class profClass = profile.getClass();
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		introspectObject(CustomerProfile.class, profile, PROFILE_ATTR_VAL, "", NO_INDEX_NEEDED,
				retMap);
		processPerson(profile.getCreator(), CREATOR_PREFIX_VAL, retMap);
		processPerson(profile.getReceiver(), RECEIVER_PREFIX_VAL, retMap);
		Iterator jahs = profile.getJointAccountHoldersIndexKeys().iterator();
		while (jahs.hasNext()) {
			Integer key = (Integer) jahs.next();
			CustomerProfilePerson jah = profile.getJointAccountHolderByIndex(key.intValue());
			processPerson(jah, JAH_PREFIX_VAL, key.intValue(), retMap);
		}
		Iterator customFieldIter = profile.getCustomFields().iterator();
		while (customFieldIter.hasNext()) {
			CustomerProfileCustomField customField = (CustomerProfileCustomField) customFieldIter
					.next();
			retMap.put(customField.getXmlTag().toUpperCase(), customField.getValue());
		}
		return retMap;
	}

	/**
	 * Get all known xml tag names
	 * 
	 * @see com.moneygram.rts.model.dss.mapping.StandardFieldXMLTagMapper#getAllStandardXMLTagNames()
	 */
	public Set getAllStandardXMLTagNames() {
		return Collections.unmodifiableSet(xmlTagToCustomerProfileMap.keySet());
	}

	/**
	 * Match up profile values with the registration field infos and set values.
	 * 
	 * @see com.moneygram.rts.model.dss.mapping.StandardFieldXMLTagMapper#convertToRegistrationFieldInfos(com.moneygram.rts.model.dss.CustomerProfile,
	 *      com.moneygram.rts.model.dss.RegistrationFieldsCollection)
	 */
	public void convertToRegistrationFieldInfos(CustomerProfile profile,
			RegistrationFieldsCollection rfc) throws Exception {
		Map m = convertToMap(profile);
		Iterator valueIterator = m.entrySet().iterator();
		while (valueIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) valueIterator.next();
			String key = (String) entry.getKey();
			XMLMapEntry xmle = (XMLMapEntry) xmlTagToCustomerProfileMap.get(key);
			
			List<ExtendedRegistrationFieldInfo> rfiList = rfc.getInfoEntryOnFullList(key);
			
			for(ExtendedRegistrationFieldInfo rfi:rfiList ){
			if (rfi == null) {
				// Some fields, such as bank id and account number, will only be
				// set
				// on certain delivery options, we need to let them through if
				// they
				// are optional.
				// 4-5-05: Add additional condition that if the value was null,
				// we ignore the problem.
				if ((xmle == null || xmle.isOptional() == false) && entry.getValue() != null)
					throw new Exception("Cannot find field: " + key + " in registration fields");
			} else {
				// SLS 4-7-05: Find the hidden match and set its value too...
				// SLS 4-28-05: Hidden is not a good indicator of confirm
				// fields. Therefore, do the
				// check regardless.
				// Make sure we aren't the confirm field. If so, set nothing.
				if (!rfi.getXmlTag().endsWith(CONFIRM_FIELD_POSTFIX)) {
					List<ExtendedRegistrationFieldInfo> hiddenRfiList = rfc.getInfoEntryOnFullList(rfi.getXmlTag()
							+ CONFIRM_FIELD_POSTFIX);
					if (hiddenRfiList != null){
						for(ExtendedRegistrationFieldInfo hiddenRfi:hiddenRfiList)
						{
						((RegistrationFieldInfo) hiddenRfi).setFieldValue((String) entry.getValue());
						}
					}
				} else
					continue; // We are the hidden extra field, don't set
				// anything.
				rfi.setFieldValue((String) entry.getValue());
			}
		}
		}
	}

	/**
	 * @see com.moneygram.rts.model.dss.mapping.StandardFieldXMLTagMapper#convertToSaveRegistrationResponse(com.moneygram.rts.model.dss.CustomerProfile,
	 *      java.lang.String,
	 *      com.moneygram.rts.message.dss.SaveRegistrationResponse)
	 */
	public class XMLMapEntry {
		public static final int CUST_PROFILE = 0;
		public static final int CREATOR = 1;
		public static final int CREATOR_ADDR = 2;
		public static final int RECEIVER = 3;
		public static final int RECEIVER_ADDR = 4;
		public static final int JAH = 5;
		public static final int JAH_ADDR = 6;
		public Class clazz;
		public Field fieldToSet;
		public String xmlTag;
		public int type;
		public int index;
		public boolean optional;
		public String enumerationClass;

		public XMLMapEntry(Class clazz, Field fieldToSet, String xmlTag, String type,
				String prefix, boolean required, String enumerationClass) {
			this(clazz, fieldToSet, xmlTag, type, prefix, NO_INDEX_NEEDED, required,
					enumerationClass);
		}

		public XMLMapEntry(Class clazz, Field fieldToSet, String xmlTag, String type,
				String prefix, int index, boolean required, String enumerationClass) {
			this.clazz = clazz;
			this.fieldToSet = fieldToSet;
			this.xmlTag = xmlTag;
			this.index = index;
			this.optional = required;
			this.enumerationClass = enumerationClass;
			if (type.equals(PERSON_ATTR_VAL)) {
				if (prefix.equals(CREATOR_PREFIX_VAL))
					this.type = CREATOR;
				else if (prefix.equals(RECEIVER_PREFIX_VAL))
					this.type = RECEIVER;
				else if (prefix.equals(JAH_PREFIX_VAL))
					this.type = JAH;
			} else if (type.equals(ADDRESS_ATTR_VAL)) {
				if (prefix.equals(CREATOR_PREFIX_VAL))
					this.type = CREATOR_ADDR;
				else if (prefix.equals(RECEIVER_PREFIX_VAL))
					this.type = RECEIVER_ADDR;
				else if (prefix.equals(JAH_PREFIX_VAL))
					this.type = JAH_ADDR;
			} else if (type.equals(PROFILE_ATTR_VAL))
				this.type = CUST_PROFILE;
		}

		/**
		 * @return
		 */
		public Field getFieldToSet() {
			return fieldToSet;
		}

		/**
		 * @return
		 */
		public String getXmlTag() {
			return xmlTag;
		}

		/**
		 * @param field
		 */
		public void setFieldToSet(Field field) {
			fieldToSet = field;
		}

		/**
		 * @param string
		 */
		public void setXmlTag(String string) {
			xmlTag = string;
		}

		/**
		 * @return
		 */
		public int getType() {
			return type;
		}

		/**
		 * @return
		 */
		public Class getClazz() {
			return clazz;
		}

		/**
		 * @param class1
		 */
		public void setClazz(Class class1) {
			clazz = class1;
		}

		/**
		 * @return
		 */
		public int getIndex() {
			return index;
		}

		/**
		 * @return
		 */
		public boolean isOptional() {
			return optional;
		}

		/**
		 * @return
		 */
		public String getEnumerationClass() {
			return enumerationClass;
		}

		public boolean isEnumeration() {
			return enumerationClass != null;
		}

		/**
		 * toString methode: creates a String representation of the object
		 * 
		 * @return the String representation
		 * @author info.vancauwenberge.tostring plugin
		 * 
		 */
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("XMLMapEntry[");
			buffer.append("clazz = ").append(StringUtility.getClassNameNoPackage(clazz.getName()));
			buffer.append(", fieldToSet = ").append(fieldToSet.getName());
			buffer.append(", xmlTag = ").append(xmlTag);
			buffer.append(", type = ").append(type);
			buffer.append(", index = ").append(index);
			buffer.append(", optional = ").append(optional);
			if (enumerationClass != null)
				buffer.append(", enumerationClass = ").append(enumerationClass);
			buffer.append("]\n");
			return buffer.toString();
		}
	}

	/**
	 * toString methode: creates a String representation of the object
	 * 
	 * @return the String representation
	 * @author info.vancauwenberge.tostring plugin
	 * 
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("StandardFieldXMLTagMapperImpl[");
		buffer.append("\nxmlTagToCustomerProfileMap = ").append(xmlTagToCustomerProfileMap);
		buffer.append("\n\ncustomerProfileToXMLTagMap = ").append(customerProfileToXMLTagMap);
		buffer.append("]");
		return buffer.toString();
	}
}
