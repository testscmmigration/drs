/*
 * Created on May 10, 2005
 *
 */
package com.moneygram.drs.xmlTagMapper;

/**
 * @author T007
 * 
 */
public interface StandardFieldTags {
	public static final String CONFIRM_FIELD_POSTFIX = "Verification";
	public static final String FIRST_NAME_POSTFIX = "FIRSTNAME";
	public static final String LAST_NAME_POSTFIX = "LASTNAME";
	public static final String SECOND_POSTFIX = "2ND";
	public static final String STATE_POSTFIX = "STATE";
	public static final String COUNTRY_POSTFIX = "COUNTRY";
	public static final String CREATOR_PREFIX_VAL = "CREATOR";
	public static final String RECEIVER_PREFIX_VAL = "RECEIVER";
	public static final String JAH_PREFIX_VAL = "JAH";
	// Used for throwing exception
	public static final String CLIENT = "Client";
}
