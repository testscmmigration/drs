package com.moneygram.drs.xmlTagMapper;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.RegistrationFieldsCollection;

public interface StandardFieldXMLTagMapper {
	/**
	 * Given a key value pair set, convert into a CustomerProfile class. This
	 * class is used by the persistence layer to read/write customer profiles.
	 * 
	 * @param keyValues
	 *            The key value pairs to convert.
	 * @return The built customer profile.
	 */
	public CustomerProfile convertIntoCustomerProfile(Collection keyValues) throws Exception;

	/**
	 * Given a key value pair set, convert into a CustomerProfile class. This
	 * class is used by the persistence layer to read/write customer profiles.
	 * 
	 * @param keyValues
	 *            The key value pairs to convert.
	 * @return The built customer profile.
	 */
	public Map convertToMap(CustomerProfile profile) throws Exception;

	/**
	 * Get all known xml tag names
	 */
	public Set getAllStandardXMLTagNames();

	public void convertToRegistrationFieldInfos(CustomerProfile profile,
			RegistrationFieldsCollection rfc) throws Exception;
}
