package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.AuthenticationData;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.criteria.AuthenticationCodeCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.util.DAOUtil;

public class AuthenticantionInfoDAO extends BaseDao implements ReadDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(AuthenticantionInfoDAO.class);

	public BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException {
		AuthenticationCodeCriteria authenticationCodeCriteria = (AuthenticationCodeCriteria) objectId
				.getId();
		AuthenticationData authenticationData = getPrflPreAuthCode(authenticationCodeCriteria);
		return authenticationData;
	}

	private AuthenticationData getPrflPreAuthCode(
			AuthenticationCodeCriteria authenticationCodeCriteria) throws DRSException {
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet res = null;
		AuthenticationData authenticationData = null;
		try {
			con = getConnection();
			final String prc = "prc_get_prfl_pre_auth_code(?, ?, ?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			stmt = con.prepareCall(sql);
			stmt.setString(1, authenticationCodeCriteria.getDeliveryOption());
			DAOUtil.setParameter(stmt, authenticationCodeCriteria.getAgentId(), 2);
			stmt.registerOutParameter(3, OracleTypes.VARCHAR);
			stmt.execute();
			String prflPreAuthCode = stmt.getString(3);
			authenticationData = new AuthenticationData();
			authenticationData.setAuthenticationCode(prflPreAuthCode);
			return authenticationData;
		} catch (SQLException se) {
			log.error("The exception thrown when reading the authentication code" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "Delivery Option",
					authenticationCodeCriteria.getDeliveryOption(), new Date(),
					"Error reading authentication code", null);
		} finally {
			close(res, stmt, con);
		} // end-try
	}

	@Override
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		// TODO Auto-generated method stub
		return null;
	}
}
