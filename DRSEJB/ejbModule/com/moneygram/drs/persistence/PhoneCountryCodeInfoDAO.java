package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class PhoneCountryCodeInfoDAO extends BaseDao {
	private static Logger log = LogFactory.getInstance().getLogger(
			PhoneCountryCodeInfoDAO.class);

	public Map<String, String> loadCountryToPhoneCountryCode()
			throws DRSException {
		Connection conn = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		Map<String, String> countryToPhoneCountryCode = new HashMap<String, String>();
		try {
			conn = getConnection();
			callableStatement = conn
					.prepareCall("{ call pkg_drs.prc_get_phn_cntry_code(?) }");
			callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
			callableStatement.execute();
			resultSet = (ResultSet) callableStatement.getObject(1);
			while (resultSet.next()) {
				String country = resultSet.getString("iso_cntry_alpha3_code");
				String phoneCountryCode = resultSet.getString("phn_cntry_code");
				countryToPhoneCountryCode.put(country, phoneCountryCode);
			}
		} catch (SQLException se) {
			log.debug("Exception during abbreviated customer profile"
					+ se.getMessage());
			// TODO Test exception
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE,
					"Unable to load country to phone country code mapping", "",
					new Date(), null, null, se);
		} finally {
			close(resultSet, callableStatement, conn);
		}
		return countryToPhoneCountryCode;
	}
}
