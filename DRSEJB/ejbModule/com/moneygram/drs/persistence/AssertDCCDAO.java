package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.criteria.AssertDCCCriteria;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class AssertDCCDAO extends BaseDao implements AssertDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(AssertDCCDAO.class);

	public boolean assertData(Criteria criteria) throws Exception {
		AssertDCCCriteria assertDccCriteria = (AssertDCCCriteria) criteria;
		return getData(assertDccCriteria);
	}

	private boolean getData(AssertDCCCriteria assertDccCriteria) throws DRSException {
		Connection con = null;
		boolean result = false;
		CallableStatement stmt = null;
		try {
			con = getConnection();
			final String sql = ("{? = call +" + DB_PACKAGE + ".fcn_get_valid_card(?, ?, ?)}");
			stmt = con.prepareCall(sql);
			int i = 1;
			int iResult = i;
			stmt.registerOutParameter(i++, OracleTypes.VARCHAR);
			stmt.setString(i++, assertDccCriteria.getAgentId());
			stmt.setString(i++, assertDccCriteria.getCurrency());
			stmt.setString(i++, assertDccCriteria.getAccountNumber());
			stmt.execute();
			String outString = stmt.getString(iResult);
			if (outString == null) {
				throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "AgentId:",
						assertDccCriteria.getAgentId(), new Date(),
						"Failed to validate card range in  database", null);
			}
			if (outString.charAt(0) == 'Y') {
				result = true;
			} else {
				result = false;
			}
			return result;
		} catch (SQLException se) {
			log.info("Exception in validating card range" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "AgentId:",
					assertDccCriteria.getAgentId(), new Date(),
					"Failed to validate card range in  database", null);
		} finally {
			close(stmt, con);
		} // end-try
	}
}