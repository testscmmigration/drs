package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.ReceiverInformation;
import com.moneygram.drs.bo.RegistrationStatus;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.criteria.SimplifiedQueryRegistrationCriteria;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.util.DAOUtil;

public class SimplifiedQueryRegistrationDAO extends BaseDao implements FindDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static final int MAX_RECORDS_TO_RETURN = 50;
	private static Logger log = LogFactory.getInstance().getLogger(
			SimplifiedQueryRegistrationDAO.class);

	public List find(Criteria criteria) throws Exception {
		SimplifiedQueryRegistrationCriteria queryCriteria = (SimplifiedQueryRegistrationCriteria) criteria;
		return getRecords(queryCriteria);
	}

	private List getRecords(SimplifiedQueryRegistrationCriteria queryCriteria)
			throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		List<ReceiverInformation> records = new ArrayList<ReceiverInformation>();
		String deliveryOption = queryCriteria.getDeliveryOption();
		String agentID = queryCriteria.getReceiveAgentID();
		String receiveCountry = queryCriteria.getReceiveCountry();
		String receiveCurrency = queryCriteria.getReceiveCurrency();
		try {
			String rcvrFirstName;
			String rcvrLastName;
			String rcvrPhNbr;
			String creatorFirstName;
			String creatorLastName;
			String creatorPhNbr;
			String mgCustomerReceiveNumber;

			rcvrFirstName = convertToSearchParm(queryCriteria.getReceiverFirstName(), 
					"(simplifiedQueryRegistration) Receiver First Name ",
					queryCriteria.getLanguage());
			rcvrLastName = convertToSearchParm(queryCriteria.getReceiverLastName(), 
					"(simplifiedQueryRegistration) Receiver Last Name ",
					queryCriteria.getLanguage());
			rcvrPhNbr = convertToSearchParm(queryCriteria.getReceiverPhoneNumber(), 
					"(simplifiedQueryRegistration) Receiver Phone Number ",
					queryCriteria.getLanguage());
			creatorFirstName = convertToSearchParm(queryCriteria.getRegistrationCreatorFirstName(),
					"(simplifiedQueryRegistration) Creator First Name ",
					queryCriteria.getLanguage());
			creatorLastName = convertToSearchParm(queryCriteria.getRegistrationCreatorLastName(),
					"(simplifiedQueryRegistration) Creator Last Name ",
					queryCriteria.getLanguage());
			creatorPhNbr = convertToSearchParm(queryCriteria.getRegistrationCreatorPhoneNumber(),
					"(simplifiedQueryRegistration) Creator Phone Number ",
					queryCriteria.getLanguage());
			mgCustomerReceiveNumber = convertToSearchParm(queryCriteria.getMgCustomerReceiveNumber(),
					"(simplifiedQueryRegistration) Customer Receive Number ",
					queryCriteria.getLanguage());
			
			Boolean activeRecordsOnly = queryCriteria.getActiveRecordsOnly();
			if (activeRecordsOnly == null) {
				activeRecordsOnly = Boolean.FALSE;
			}
			
			Integer maxRowsToReturn = queryCriteria.getMaxRowsToReturn();
			if (maxRowsToReturn == null || maxRowsToReturn <= 0 || maxRowsToReturn > MAX_RECORDS_TO_RETURN) {
				maxRowsToReturn = MAX_RECORDS_TO_RETURN;
			}

			con = getConnection();
			final String prc = "prc_get_registration_by_names(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			cstmt.setString(1, deliveryOption);
			cstmt.setString(2, agentID);
			DAOUtil.setString(cstmt, 3, receiveCountry, true);
			DAOUtil.setString(cstmt, 4, receiveCurrency, true);
			DAOUtil.setString(cstmt, 5, rcvrFirstName, true);
			DAOUtil.setString(cstmt, 6, rcvrLastName, true);
			DAOUtil.setString(cstmt, 7, rcvrPhNbr, true);
			DAOUtil.setString(cstmt, 8, creatorFirstName, true);
			DAOUtil.setString(cstmt, 9, creatorLastName, true);
			DAOUtil.setString(cstmt, 10, creatorPhNbr, true);
			DAOUtil.setString(cstmt, 11, mgCustomerReceiveNumber, true);
			DAOUtil.setString(cstmt, 12, activeRecordsOnly ? "Y" : "N", true);
			cstmt.setInt(13, maxRowsToReturn);
			cstmt.registerOutParameter(14, OracleTypes.CURSOR);
			
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(14);
			while (rs.next()) {
				RegistrationStatus status = RegistrationStatus.fromValue(rs.getString("prfl_stat_code"));

				ReceiverInformation receiverInformation = new ReceiverInformation();
				receiverInformation.setMgCustomerReceiveNumber(rs.getString("cust_rcv_nbr"));
				receiverInformation.setAccountNickname(rs.getString("acct_nkname"));
				receiverInformation.setReceiverPhoneNumber(rs.getString("cust_ph_nbr"));
				receiverInformation.setReceiverFirstName(rs.getString("cust_frst_name"));
				receiverInformation.setReceiverLastName(rs.getString("cust_last_name"));
				receiverInformation.setAccountNumberLastFour(rs.getString("rcv_agent_cust_acct_mask_nbr"));
				receiverInformation.setRegistrationStatusCode(status);
				records.add(receiverInformation);
			}
			return records;
		} catch (SQLException sqle) {
			log.info("Exception during reading name records", sqle);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "receiveCountry",
					receiveCountry, new Date(), "Error occurred during reading name records", null);
		} finally {
			close(rs, cstmt, con);
		}
	}

	private String convertToSearchParm(String parm, String errorHelpText, String lang)
			throws DRSException {
		int flagIndex = -1;
		if (parm != null) {
			flagIndex = parm.indexOf('*');
			if (flagIndex >= 0) {
				if (flagIndex < parm.length() - 1) {
					throw new DRSException(
							DRSExceptionCodes.EC628_WILDCARDS_NOT_AT_END_OF_STRINGS,
							"receiverFirstName",
							errorHelpText,
							new Date(),
							"WildCards not at the end of Strings, (simplifiedQueryRegistration) Reciver Last Name ",
							SERVER);
				} else {
					parm = parm.replace('*', '%');
				}
			}
		}
		return parm;
	}
}
