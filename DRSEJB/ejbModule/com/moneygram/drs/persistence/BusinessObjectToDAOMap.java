package com.moneygram.drs.persistence;

import java.util.HashMap;
import java.util.Map;

import com.moneygram.drs.bo.AuthenticationData;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.CustomerInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfile2;
import com.moneygram.drs.bo.CustomerReceiveDetails;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.bo.OFACInfo;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.RegistrationFieldsCollection2;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.criteria.AssertDCCCriteria;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.CountryInfoCriteria;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.criteria.FQDOForCountryCriteria;
import com.moneygram.drs.criteria.QueryRegistrationsCriteria;
import com.moneygram.drs.criteria.SimplifiedQueryRegistrationCriteria;

public class BusinessObjectToDAOMap {
	private static Map<Class<?>, BaseDao> businessObjectToDAOMap = new HashMap<Class<?>, BaseDao>();
	private static Map<Class<?>, BaseDao> criteriaToDaoMap = new HashMap<Class<?>, BaseDao>();
	private static Map<Class<?>, BaseDao> daoToDaoMap = new HashMap<Class<?>, BaseDao>();
	static {
		businessObjectToDAOMap.put(FullFQDOWithRegistrationStatus.class, new FQDODao());
		businessObjectToDAOMap.put(CustomerProfile.class, new CustomerProfileDAO());
		businessObjectToDAOMap.put(CustomerProfile2.class, new CustomerProfileDAO2());
		businessObjectToDAOMap.put(FQDOInfo.class, new FQDODao());
		businessObjectToDAOMap.put(RegistrationFieldsCollection.class, new RegistrationFieldsDAO());
		businessObjectToDAOMap.put(RegistrationFieldsCollection2.class, new DirectedSendFieldsDao());
		businessObjectToDAOMap.put(AuthenticationData.class, new AuthenticantionInfoDAO());
		businessObjectToDAOMap.put(CustomerInfo.class, new SaveRegistrationDAO());
		businessObjectToDAOMap.put(OFACInfo.class, new SaveOFACDAO());
		businessObjectToDAOMap.put(SensitiveIDInfo.class, new SaveSensitiveIDDAO());
		businessObjectToDAOMap.put(RealTimeValidationRequest.class, new RealTimeDAO());		
		businessObjectToDAOMap.put(CustomerReceiveDetails.class, new SaveRegistrationDAO());
		criteriaToDaoMap.put(FQDOForCountryCriteria.class, new FQDOForCountryDAO());
		criteriaToDaoMap.put(QueryRegistrationsCriteria.class, new QueryRegistrationsDAO());
		criteriaToDaoMap.put(CountryInfoCriteria.class, new CountryInfoDAO());
		criteriaToDaoMap.put(AssertRegistrationCriteria.class, new AssertRegistrationDAO());
		criteriaToDaoMap.put(AssertDCCCriteria.class, new AssertDCCDAO());
		criteriaToDaoMap.put(SimplifiedQueryRegistrationCriteria.class,
				new SimplifiedQueryRegistrationDAO());
		
		daoToDaoMap.put(PhoneCountryCodeInfoDAO.class, new PhoneCountryCodeInfoDAO());
	}
	
	public static final <T extends BaseDao> T getDaoByType(Class<T> type) {
		return type.cast(daoToDaoMap.get(type));
	}

	private BusinessObjectToDAOMap() {
	}

	public static SaveDao getSaveDAO(BusinessObject businessObject) {
		return (SaveDao) businessObjectToDAOMap.get(businessObject.getClass());
	}
	
	public static SaveDao getSaveDAOForUpdate(BusinessObject businessObject) {
		return (SaveDao) businessObjectToDAOMap.get(businessObject.getClass());
	}

	public static ReadDao getReadDAO(Class objectClass) {
		Object dao = businessObjectToDAOMap.get(objectClass);
		if (dao == null) {
			throw new IllegalArgumentException("Read Dao not registered for class: "
					+ objectClass.getName());
		}
		return (ReadDao) dao;
	}

	public static FindDao getFindDAO(Criteria criteria) {
		return (FindDao) criteriaToDaoMap.get(criteria.getClass());
	}

	public static AssertDao getAssertDAO(Criteria criteria) {
		return (AssertDao) criteriaToDaoMap.get(criteria.getClass());
	}
}
