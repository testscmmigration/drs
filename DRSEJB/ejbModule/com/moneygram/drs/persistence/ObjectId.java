package com.moneygram.drs.persistence;

public class ObjectId {
	private Class objectClass;
	private Object id;

	public ObjectId(Class objectClass, Object id) {
		this.objectClass = objectClass;
		this.id = id;
	}

	public Class getObjectClass() {
		return this.objectClass;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}
}
