package com.moneygram.drs.persistence;

import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.CustomerReceiveDetails;

public interface SaveDao {
	public BusinessObject save(BusinessObject businessObject) throws Exception;
	public boolean updateTransactionUseFlag(BusinessObject businessObject) throws Exception;
}
