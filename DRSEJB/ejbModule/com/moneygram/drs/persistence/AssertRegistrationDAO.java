package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.criteria.AssertRegistrationCriteria;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class AssertRegistrationDAO extends BaseDao implements AssertDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(AssertRegistrationDAO.class);

	public boolean assertData(Criteria criteria) throws Exception {
		AssertRegistrationCriteria assertRegistrationCriteria = (AssertRegistrationCriteria) criteria;
		return getData(assertRegistrationCriteria);
	}

	private boolean getData(AssertRegistrationCriteria assertRegistrationCriteria)
			throws DRSException {
		Connection con = null;
		boolean result = false;
		CallableStatement stmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_registration_code(?, ?, ?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			stmt = con.prepareCall(sql);
			int i = 1;
			stmt.setString(i++, assertRegistrationCriteria.getReceiveAgentID());
			stmt.setString(i++, assertRegistrationCriteria.getDeliveryOption());
			int iResult = i++;
			stmt.registerOutParameter(iResult, OracleTypes.VARCHAR);
			stmt.execute();
			String outString = stmt.getString(iResult);
			if (outString == null) {
				throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "AgentId:",
						assertRegistrationCriteria.getReceiveAgentID(), new Date(),
						"Failed to retreive a registration status from the database", null);
			}
			if (outString.charAt(0) == 'R' || outString.charAt(0) == 'O') {
				result = true;
			} else {
				result = false;
			}
			return result;
		} catch (SQLException se) {
			log.error("The exception thrown when reading the assert registration data" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "AgentId:",
					assertRegistrationCriteria.getReceiveAgentID(), new Date(),
					"Error occurred during reading registration status", null);
		} finally {
			close(stmt, con);
		} // end-try
	}
}
