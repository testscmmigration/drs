package com.moneygram.drs.persistence;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Provides helper methods to lookup JNDI entries Implements the Factory pattern
 * 
 * @author Tim
 */
public class ServiceLocator {
	// private static final Logger log =
	// LogFactory.getInstance().getLogger(ServiceLocator.class);
	/** Holds a reference to the one and only <code>ServiceLocator</code> */
	private static ServiceLocator _instance = null;
	/** The initial context fro JNDI lookup */
	private InitialContext _ic;

	/** Default constructor that is called only by <code>getInstance()</code> */
	private ServiceLocator() throws Exception {
		_ic = new InitialContext();
	}

	/**
	 * Instantiates a <code>ServiceLocator</code> object
	 * 
	 * @return A reference to the object
	 * @throws Exception
	 */
	public static ServiceLocator getInstance() throws Exception {
		if (null == _instance) {
			synchronized (ServiceLocator.class) {
				_instance = new ServiceLocator();
			}
		}
		return _instance;
	}

	/**
	 * Lookup the TPE DataSource from JNDI
	 * 
	 * @return The TPEDataSource
	 * @throws NamingException
	 * @throws Exception
	 */
	public DataSource getDataSource() throws NamingException {
		String dsName = "java:comp/env/jdbc/TPEDataSource";
		// log.debug("Connecting to " + dsName);
		return (DataSource) _ic.lookup(dsName);
	}
}
