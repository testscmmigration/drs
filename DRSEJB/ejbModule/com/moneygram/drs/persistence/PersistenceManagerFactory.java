package com.moneygram.drs.persistence;

import java.util.List;

import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public class PersistenceManagerFactory implements PersistenceManager {
	private static PersistenceManager instance;

	PersistenceManagerFactory() {
	}

	public static PersistenceManager getInstance() {
		if (null == instance) {
			instance = new PersistenceManagerFactory();
		}
		return instance;
	}

	public BusinessObject save(BusinessObject businessObject) throws Exception {
		return save(businessObject, BusinessObjectToDAOMap.getSaveDAO(businessObject));
	}

	private BusinessObject save(BusinessObject businessObject, SaveDao dao) throws Exception {
		return dao.save(businessObject);
	}

	public BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException {
		return read(objectId, BusinessObjectToDAOMap.getReadDAO(objectId.getObjectClass()));
	}
	
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		return read(objectId, BusinessObjectToDAOMap.getReadDAO(objectId.getObjectClass()),procCall);
	}

	private BusinessObject read(ObjectId objectId, ReadDao dao) throws NotFoundException,
			DRSException {
		return dao.read(objectId);
	}
	
	private BusinessObject read(ObjectId objectId, ReadDao dao,String procCall) throws NotFoundException,
		DRSException {
			return dao.read(objectId,procCall);
	}

	public List find(Criteria criteria) throws Exception {
		return find(criteria, BusinessObjectToDAOMap.getFindDAO(criteria));
	}

	private List find(Criteria criteria, FindDao dao) throws Exception {
		return dao.find(criteria);
	}

	public boolean assertData(Criteria criteria) throws Exception {
		return assertData(criteria, BusinessObjectToDAOMap.getAssertDAO(criteria));
	}

	private boolean assertData(Criteria criteria, AssertDao assertDao) throws Exception {
		return assertDao.assertData(criteria);
	}
	public boolean updateTransactionUseFlag(BusinessObject businessObject, SaveDao dao) throws Exception{
		return dao.updateTransactionUseFlag(businessObject); 
	}
	
	public boolean updateTransactionUseFlag(BusinessObject businessObject) throws Exception {		
		return updateTransactionUseFlag(businessObject,BusinessObjectToDAOMap.getSaveDAOForUpdate(businessObject)); 
	}
	
	public static <T extends BaseDao> T getDaoByType(Class<T> type) {
		return BusinessObjectToDAOMap.getDaoByType(type);
	}
}