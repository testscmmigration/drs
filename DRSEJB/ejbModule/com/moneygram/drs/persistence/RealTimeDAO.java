package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.RealTimeValidationRequest;
import com.moneygram.drs.criteria.RealTimeInfoCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class RealTimeDAO extends BaseDao implements ReadDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static final String CALL_TYPE = "WEB";
	private static Logger log = LogFactory.getInstance().getLogger(RegistrationFieldsDAO.class);

	public BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException {
		RealTimeValidationRequest request = new RealTimeValidationRequest();
		RealTimeInfoCriteria criteria = (RealTimeInfoCriteria) objectId.getId();
		HashMap<String, String> commMap = new HashMap<String, String>();
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			final String prc = "prc_get_group_info_by_agt(?,?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			int i = 1;
			cstmt.setString(i++, criteria.getAgentId());
			cstmt.setString(i++, CALL_TYPE);
			int result = i++;
			cstmt.registerOutParameter(result, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) (cstmt.getObject(result));
			while (rs.next()) {
				commMap.put(rs.getString("parm_name"), rs.getString("parm_val"));
			}
			request.setContext(commMap);
			return request;
		} catch (SQLException se) {
			log.info("Exception when reading the registration fields" + se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "Agent ID:", criteria
					.getAgentId(), new Date(), "Error occurred during reading RealTime Info",
					SERVER);
		} finally {
			close(rs, cstmt, con);
		} // end-try
	}

	@Override
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		// TODO Auto-generated method stub
		return null;
	}
}
