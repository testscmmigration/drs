package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.mgEnum.EnumRegistry;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.CustomerInfo;
import com.moneygram.drs.bo.CustomerProfile;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.CustomerReceiveDetails;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.bo.SaveRegistrationResult;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.util.DAOUtil;
import com.moneygram.drs.util.IntegerCompare;

public class SaveRegistrationDAO extends BaseDao implements SaveDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static final int CREATOR = 1;
	private static final int RECEIVER = 2;
	protected static final int JOINTACCHLDR = 3;
	private static final String DB_ENCRYP_PACKAGE = "pkg_drs_save_registration";
	private static final String DB_DECRYP_PACKAGE = "pkg_drs_get_registration";
	private static final String INTRA_TRAN = "INTRA TRAN";
	private static final String NON_INTRA_TRAN = "NOT INTRA TRAN";
	private static Logger log = LogFactory.getInstance().getLogger(SaveRegistrationDAO.class);

	public BusinessObject save(BusinessObject businessObject) throws Exception {
		CustomerInfo customerInfo = (CustomerInfo) businessObject;
		SaveRegistrationResult result = saveRegistration(customerInfo.getCustomerProfile(), customerInfo.getAgentId(), customerInfo
				.getDeliveryOption(),customerInfo.isIntraTransaction());
		return result;
	}

	private SaveRegistrationResult saveRegistration(CustomerProfile profile, String agent_id, String dlvr_optn_id, boolean intraTransaction)
			throws DRSException {
		if (profile.getCustomerReceiveNumber() == null
				|| profile.getCustomerReceiveNumber().length() < 1) {
			String newRcvNbr = getNewCustomerReceiverNumber();
			int prfl_ver = 1;
			int cust_seq_id = 0;
			// First store info in the customer profile table
			profile.setCustomerReceiveNumber(newRcvNbr);
			profile.setCustomerReceiveNumberVersion(prfl_ver);
			SaveRegistrationResult result = insertCustomerProfile(profile, prfl_ver, agent_id, dlvr_optn_id,intraTransaction);
			cust_seq_id++;
			if (profile.getCreator() != null) {
				insertCustPrflPerAddr(profile.getCreator(), profile.getCustomerReceiveNumber(),
						prfl_ver, cust_seq_id, CREATOR);
			}
			cust_seq_id++;
			if (profile.getReceiver() != null) {
				insertCustPrflPerAddr(profile.getReceiver(), profile.getCustomerReceiveNumber(),
						prfl_ver, cust_seq_id, RECEIVER);
				result.setReceiverCustomerSequenceID(cust_seq_id);
			}
			Set<Integer> keySet = profile.getJointAccountHoldersIndexKeys();
			int jah_cust_seq_id = 0;
			if (keySet != null) {
				List<Integer> keyList = Arrays.asList(keySet.toArray(new Integer[keySet.size()]));
				Collections.sort(keyList, new IntegerCompare<Integer>());
				for (Iterator<Integer> iter = keyList.iterator(); iter.hasNext();) {
					int key = iter.next().intValue();
					jah_cust_seq_id = key + cust_seq_id;
					CustomerProfilePerson person = profile.getJointAccountHolderByIndex(key);
					if (person != null) {
						insertCustPrflPerAddr(person, profile.getCustomerReceiveNumber(), prfl_ver,
								jah_cust_seq_id, JOINTACCHLDR);
					}
				}
			}
			insertCustPrflData(profile.getCustomFields(), profile.getCustomerReceiveNumber(),
					prfl_ver, agent_id, dlvr_optn_id);
			return result;
		} else {
			SaveRegistrationResult result = editRegistration(profile, agent_id, dlvr_optn_id,intraTransaction);
			return result;
		}
	}

	private String genrateNextSeq() throws DRSException {
		int sum = 0;
		boolean odd = true;
		StringBuilder tempData = new StringBuilder(getNextSeqData());
		if (tempData.length() < 7)
		// insert just enough zeros at the beginning to make the value the desired length.
			tempData.insert(0, "0000000", 0, 7-tempData.length());
		String data = tempData.toString();
		for (int i = data.length() - 1; i >= 0; i--) {
			int digit = Integer.parseInt(data.substring(i, i + 1));
			if (odd) {
				int tSum = digit * 2;
				if (tSum >= 10) {
					tSum = (tSum % 10) + 1;
				}
				sum += tSum;
			} else
				sum += digit;
			odd = !odd;
		}
		int dataEnd = ((((((sum / 10) + 1) * 10) - sum) % 10) + 5) % 10;
		return "MG" + data + dataEnd;
	}

	private String getNextSeqData() throws DRSException {
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet res = null;
		try {
			con = getConnection();
			final String prc = "prc_get_next_cust_rcv_seq(?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			stmt = con.prepareCall(sql);
			stmt.registerOutParameter(1, OracleTypes.NUMERIC);
			stmt.execute();
			String nextSeq = stmt.getString(1);
			return nextSeq;
		} catch (SQLException se) {
			log.info("The exception thrown when saving data : on getting next seq data" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error Saving data", SERVER);
		} finally {
			close(res, stmt, con);
		} // end-try
	}

	private SaveRegistrationResult insertCustomerProfile(CustomerProfile profile, int prfl_ver_nbr, String agent_id,
			String dlvr_optn_id, boolean intraTransaction) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		String reas_code = "00001";
		// Calculating Profile reason code
		if (profile.getRegistrationStatus() != null
				&& profile.getRegistrationStatus().equals(RegistrationStatusCode.NOT_ACTIVE)) {
			reas_code = "00401";
		}
		try {
			con = getConnection();
			final String prc = 
				"prc_iu_cust_profile(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			final String sql = "call " + DB_ENCRYP_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			int i = 1;
			// TODO.. need to check y RRN is taking a space...
			cstmt.setString(i++, profile.getCustomerReceiveNumber().trim());
			cstmt.setInt(i++, prfl_ver_nbr);
			cstmt.setString(i++, agent_id);
			cstmt.setString(i++, dlvr_optn_id);
			RegistrationStatusCode statusCode = profile.getRegistrationStatus();
			String statusCodeString = null;
			if (statusCode != null)
				statusCodeString = statusCode.getName();
			DAOUtil.setString(cstmt, i++, statusCodeString, true);
			DAOUtil.setString(cstmt, i++, profile.getRegistrationSubStatus(), true);
			DAOUtil.setString(cstmt, i++, profile.getOfacStatus(), true);
			DAOUtil.setString(cstmt, i++, profile.getRcvAgentCustAcctNbr(), true);
			DAOUtil.setString(cstmt, i++, profile.getRcvAgentCustAcctName(), true);
			DAOUtil.setString(cstmt, i++, profile.getRcvAgentBankID(), true);
			DAOUtil.setString(cstmt, i++, reas_code, true);
			DAOUtil.setString(cstmt, i++, profile.getRcvAgentCustAcctNbrPanFlag(), true);
			cstmt.setInt(i++, profile.getPoeSourceSystem());
			DAOUtil.setString(cstmt, i++, profile.getAccountNickname(), true);
			if(intraTransaction)
				DAOUtil.setString(cstmt, i++,INTRA_TRAN,true);
			else
				DAOUtil.setString(cstmt, i++,NON_INTRA_TRAN,true);
			
			//Changes as part of receiver directed flow
			DAOUtil.setString(cstmt, i++, profile.getExternalReceiverProfileId(), true);
			
			//ADP-1535
			DAOUtil.setString(cstmt, i++, profile.getExternalReceiverProfileType(), true);
			if (profile.isAccountNumberIsToken()) {
				DAOUtil.setString(cstmt, i++, profile.getRcvAgentCustAccountBin(), true);
			} else {
				DAOUtil.setString(cstmt, i++, null, true);
			}
		    if(profile.getRcvAgentCustAcctNbr() == null ||
		    		profile.getRuleProcessAccountNumber() == null ||
		    		(profile.getRcvAgentCustAcctNbr().equalsIgnoreCase(profile.getRuleProcessAccountNumber()))) {
				DAOUtil.setString(cstmt, i++, null, true);
			} else {
				DAOUtil.setString(cstmt, i++, profile.getRuleProcessAccountNumber(), true);
			}
			int accountMaskPos = i++;
			
			cstmt.registerOutParameter(accountMaskPos, OracleTypes.VARCHAR);
			cstmt.execute();
			
			String accountNumberLastFour = cstmt.getString(accountMaskPos);
			
			SaveRegistrationResult result = new SaveRegistrationResult();
			result.setAccountNickname(profile.getAccountNickname());
			result.setAccountNumberLastFour(accountNumberLastFour);
			return result;
		} catch (SQLException se) {
			log.info("Exception when inserting or updating customer profile" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "Agent ID:", agent_id,
					new Date(), "Error occurred during saving Registration fields", SERVER, se);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	/**
	 * @param person
	 * @param i
	 *            Flag indicates what type of person
	 */
	protected void insertCustPrflPerAddr(CustomerProfilePerson person, String rcv_nbr,
			int prfl_ver_nbr, int cust_seq_id, int flag) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_iud_cust_profile_prs_addr(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			final String sql = "call " + DB_ENCRYP_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			int i = 1;
			cstmt.setString(i++, rcv_nbr.trim());
			cstmt.setInt(i++, prfl_ver_nbr);
			cstmt.setInt(i++, cust_seq_id);
			if (flag == CREATOR) {
				cstmt.setString(i++, "N");
				cstmt.setString(i++, "Y");
				cstmt.setString(i++, "N");
			} else if (flag == RECEIVER) {
				cstmt.setString(i++, "Y");
				cstmt.setString(i++, "N");
				cstmt.setString(i++, "Y");
			} else if (flag == JOINTACCHLDR) {
				cstmt.setString(i++, "N");
				cstmt.setString(i++, "N");
				cstmt.setString(i++, "Y");
			}
			DAOUtil.setString(cstmt, i++, person.getFirstName(), true);
			DAOUtil.setString(cstmt, i++, person.getMiddleName(), true);
			DAOUtil.setString(cstmt, i++, person.getLastName(), true);
			DAOUtil.setString(cstmt, i++, person.getMaternalName(), true);
			DAOUtil.setString(cstmt, i++, person.getPhoneNumber(), true);
			setAddress(cstmt, i, person,flag);
			cstmt.execute();
		} catch (SQLException se) {
			log.info("Exception when inserting " + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", rcv_nbr,
					new Date(), "Error occurred during saving Registration fields", SERVER);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	/**
	 * @param cstmt
	 * @param i
	 * @param list
	 *            Sets the address
	 */
	private void setAddress(CallableStatement cstmt, int i, CustomerProfilePerson person,int flag) throws SQLException {
		List list = person.getAddresses();
		Address addr = list.isEmpty() ? new Address() : (Address) list.get(0);
		DAOUtil.setString(cstmt, i++, addr.getAddrLine1(), true);
		DAOUtil.setString(cstmt, i++, addr.getAddrLine2(), true);
		DAOUtil.setString(cstmt, i++, addr.getAddrLine3(), true);
		DAOUtil.setString(cstmt, i++, addr.getCity(), true);
		DAOUtil.setString(cstmt, i++, addr.getState(), true);
		DAOUtil.setString(cstmt, i++, addr.getPostalCode(), true);
		DAOUtil.setString(cstmt, i++, addr.getCntryCode(), true);
		DAOUtil.setString(cstmt, i++, addr.getDeliveryInstructions1(), true);
		DAOUtil.setString(cstmt, i++, addr.getDeliveryInstructions2(), true);
		DAOUtil.setString(cstmt, i++, addr.getDeliveryInstructions3(), true);
		//Changes as part of ADBOP-692 May 18 release	
		//For Receiver, if PhoneNumber is present, send smsEnabled as Y. will revisit this in Jun18
		if (flag == RECEIVER
				&& (person.getPhoneNumber() != null || person
						.getSmsOptInEnabledFlag() != null
						&& person.getSmsOptInEnabledFlag())) {
			DAOUtil.setString(cstmt, i++, "Y", true); //iv_cust_phn_sms_enbl_flag
		} //For sender and other type of persons, smsEnabled will be null always. 
		else {
			DAOUtil.setString(cstmt, i++, null, false); //iv_cust_phn_sms_enbl_flag 
		}
		//NotfCnstFlag will be null for all type of persons. 
		DAOUtil.setString(cstmt, i++, null, false); //iv_notf_opt_in_cnsnt_flag
		DAOUtil.setString(cstmt, i++, addr.getCountrySubDivCode(), true);//iv_cnsmr_addr_subdiv
		DAOUtil.setString(cstmt, i++, person.getPrimaryPhoneCountryCode(), true);
	}

	/**
	 * @param collection
	 * @param string
	 * @param prfl_ver
	 * @param agent_id
	 * @param dlvr_optn_id
	 *            insert into data into the cust_profile_data table
	 */
	private void insertCustPrflData(Collection collection, String rcv_nbr, int prfl_ver,
			String agent_id, String dlvr_optn_id) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_agent_tran_cnfg_id(?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			cstmt.setString(1, agent_id);
			cstmt.registerOutParameter(2, OracleTypes.NUMERIC);
			cstmt.execute();
			int tranCnfgID = cstmt.getInt(2);
			final String prc_data = "prc_iud_cust_profile_data(?, ?, ?, ?, ?, ?)";
			final String sql_data = "call " + DB_ENCRYP_PACKAGE + "." + prc_data;
			cstmt = con.prepareCall(sql_data);
			int i = 1;
			cstmt.setString(i++, rcv_nbr.trim());
			cstmt.setInt(i++, prfl_ver);
			cstmt.setInt(i++, tranCnfgID);
			for (Iterator iter = collection.iterator(); iter.hasNext();) {
				i = 4;
				CustomerProfileCustomField field = (CustomerProfileCustomField) iter.next();
				cstmt.setString(i++, field.getXmlTag());
				cstmt.setNull(i++, OracleTypes.NUMERIC);
				DAOUtil.setString(cstmt, i++, field.getValue(), true);
				cstmt.execute();
			}
		} catch (SQLException se) {
			log.info("Exception thrown when saving customer profile" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error occurred during saving Registration fields", SERVER);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	/**
	 * @param profile
	 * @return This function copies the existing profile to a new version and
	 *         makes the required changes
	 */
	private SaveRegistrationResult editRegistration(CustomerProfile profile, String agent_id, String dlvr_optn_id, boolean intraTransaction)
			throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_cust_prfl_ver(?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			cstmt.setString(1, profile.getCustomerReceiveNumber());
			cstmt.registerOutParameter(2, OracleTypes.NUMERIC);
			cstmt.execute();
			int prfl_ver = cstmt.getInt(2);
			final String prc_copy = "prc_copy_cust_profile(?, ?, ?)";
			final String sql_copy = "call " + DB_ENCRYP_PACKAGE + "." + prc_copy;
			cstmt = con.prepareCall(sql_copy);
			int i = 1;
			cstmt.setString(i++, profile.getCustomerReceiveNumber());
			cstmt.setInt(i++, prfl_ver);
			prfl_ver++; // Increment version to new version
			cstmt.setInt(i++, prfl_ver);
			cstmt.execute();
			profile.setCustomerReceiveNumberVersion(prfl_ver);
			int cust_seq_id = 0;
			// First store info in the customer profile table
			SaveRegistrationResult result = insertCustomerProfile(profile, prfl_ver, agent_id, dlvr_optn_id,intraTransaction);
			cust_seq_id++;
			if (profile.getCreator() != null) {
				insertCustPrflPerAddr(profile.getCreator(), profile.getCustomerReceiveNumber(),
						prfl_ver, cust_seq_id, CREATOR);
			}
			cust_seq_id++;
			if (profile.getReceiver() != null) {
				insertCustPrflPerAddr(profile.getReceiver(), profile.getCustomerReceiveNumber(),
						prfl_ver, cust_seq_id, RECEIVER);
			}
			Set<Integer> keySet = profile.getJointAccountHoldersIndexKeys();
			int jah_cust_seq_id = 0;
			if (keySet != null) {
				List<Integer> keyList = Arrays.asList(keySet.toArray(new Integer[keySet.size()]));
				Collections.sort(keyList, new IntegerCompare<Integer>());
				for (Iterator<Integer> iter = keyList.iterator(); iter.hasNext();) {
					int key = ((Integer) iter.next()).intValue();
					jah_cust_seq_id = key + cust_seq_id;
					CustomerProfilePerson person = profile.getJointAccountHolderByIndex(key);
					if (person != null) {
						insertCustPrflPerAddr(person, profile.getCustomerReceiveNumber(), prfl_ver,
								jah_cust_seq_id, JOINTACCHLDR);
					}
				}
			}
			insertCustPrflData(profile.getCustomFields(), profile.getCustomerReceiveNumber(),
					prfl_ver, agent_id, dlvr_optn_id);
						
			return result;
		} catch (SQLException se) {
			log.info("Exception thrown on editing registrations" + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error occurred during saving Registration fields", SERVER);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	/**
	 * @param profileID
	 * @param profileVersion
	 * @return Profile ValueObject This Function returns the customer profile
	 *         for a given profileID and version adminuser enables it to decrypt
	 *         any encrypted data
	 * 
	 */
	public CustomerProfile getCustomerProfile(String profileID, int profileVersion,
			boolean adminUser) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet profileRS = null;
		ResultSet profilePersonRS = null;
		ResultSet profileDataRS = null;
		try {
			con = getConnection();
			final String prc = "prc_get_cust_profile(?, ?, ?, ?, ?)";
			String sql = "call " + DB_PACKAGE + "." + prc;
			if (adminUser) {
				sql = "call " + DB_DECRYP_PACKAGE + "." + prc;
			}
			cstmt = con.prepareCall(sql);
			cstmt.setString(1, profileID);
			if (profileVersion < 1) {
				cstmt.setNull(2, OracleTypes.NUMERIC);
			} else {
				cstmt.setInt(2, profileVersion);
			}
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.registerOutParameter(5, OracleTypes.CURSOR);
			cstmt.execute();
			profileRS = (ResultSet) cstmt.getObject(3);
			profilePersonRS = (ResultSet) cstmt.getObject(4);
			profileDataRS = (ResultSet) cstmt.getObject(5);
			CustomerProfile profile = null;
			if (profileRS.next()) {
				profile = getCustomerProfileObject(profileRS, profilePersonRS, profileDataRS);
			}
			return profile;
		} catch (SQLException se) {
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error occurred during saving Registration fields", SERVER);
		} finally {
			ResultSet[] rs = { profileRS, profilePersonRS, profileDataRS };
			close(rs, cstmt, con);
		} // end-try
	}

	private CustomerProfile getCustomerProfileObject(ResultSet profileRS,
			ResultSet profilePersonRS, ResultSet profileDataRS) throws SQLException {
		CustomerProfile profile = new CustomerProfile();
		profile.setCustomerReceiveNumber(profileRS.getString("pfl_cust_rcv_nbr"));
		profile.setCustomerReceiveNumberVersion(profileRS.getInt("pfl_cust_prfl_ver_nbr"));
		/*
		 * if (profileRS.getString("pfl_ofac_excp_state_code") == null) {
		 * profile.setOfacStatus(false); } else { profile.setOfacStatus(
		 * profileRS.getString("pfl_ofac_excp_state_code").charAt(0) == 'Y'); }
		 */
		profile.setOfacStatus(profileRS.getString("pfl_ofac_excp_state_code"));
		profile.setRegistrationStatus((RegistrationStatusCode) EnumRegistry.getByName(
				RegistrationStatusCode.class, profileRS.getString("pfl_prfl_stat_code")));
		profile.setRegistrationSubStatus(profileRS.getString("pfl_prfl_sub_stat_code"));
		profile.setRcvAgentCustAcctNbr(profileRS.getString("pfl_rcv_agent_cust_acct_nbr"));
		profile.setRcvAgentBankID(profileRS.getString("pfl_rcv_agent_bank_id"));
		profile.setRcvAgentCustAcctName(profileRS.getString("pfl_rcv_agent_cust_acct_name"));
		int accountHolderIndex = 0;
		while ((profilePersonRS != null) && (profilePersonRS.next())) {
			CustomerProfilePerson accountHolder = new CustomerProfilePerson();
			accountHolder.setFirstName(profilePersonRS.getString("per_cust_frst_name"));
			accountHolder.setLastName(profilePersonRS.getString("per_cust_last_name"));
			accountHolder.setMiddleName(profilePersonRS.getString("per_cust_mid_name"));
			accountHolder.setMaternalName(profilePersonRS.getString("per_cust_matrnl_name"));
			accountHolder.setPhoneNumber(profilePersonRS.getString("per_cust_ph_nbr"));
			Address accountHolderAddress = new Address();
			accountHolderAddress.setAddrLine1(profilePersonRS.getString("addr_addr_line1_text"));
			accountHolderAddress.setAddrLine2(profilePersonRS.getString("addr_addr_line2_text"));
			accountHolderAddress.setAddrLine3(profilePersonRS.getString("addr_addr_line3_text"));
			accountHolderAddress.setCity(profilePersonRS.getString("addr_addr_city_name"));
			accountHolderAddress.setCntryCode(profilePersonRS.getString("addr_addr_cntry_id"));
			accountHolderAddress.setDeliveryInstructions1(profilePersonRS
					.getString("addr_dlvr_instr1_text"));
			accountHolderAddress.setDeliveryInstructions2(profilePersonRS
					.getString("addr_dlvr_instr2_text"));
			accountHolderAddress.setDeliveryInstructions3(profilePersonRS
					.getString("addr_dlvr_instr3_text"));
			accountHolderAddress.setPostalCode(profilePersonRS.getString("addr_addr_postal_code"));
			accountHolderAddress.setState(profilePersonRS.getString("addr_addr_state_name"));
			accountHolder.addAddress(accountHolderAddress);
			if (profilePersonRS.getString("per_rgst_prfl_flag").charAt(0) == 'Y') {
				profile.setCreator(accountHolder);
			} else if (profilePersonRS.getString("per_benef_flag").charAt(0) == 'Y') {
				profile.setReceiver(accountHolder);
			} else {
				accountHolderIndex = profilePersonRS.getInt("per_cust_seq_id") - 2;
				profile.addJointAccountHolder(accountHolder, accountHolderIndex);
			}
		}
		while ((profileDataRS != null) && profileDataRS.next()) {
			CustomerProfileCustomField item = new CustomerProfileCustomField();
			// item.setAttrID(profileDataRS.getInt("data_attr_id"));
			item.setValue(profileDataRS.getString("data_attr_val"));
			item.setXmlTag(profileDataRS.getString("data_pos_xml_tag"));
			profile.addCustomFields(item, item.getXmlTag());
		}
		return profile;
	}

	private String getNewCustomerReceiverNumber() throws DRSException {
		String availableRcvNbr = null;
		int tryCount = 0;
		do {
			String newRcvNbr = genrateNextSeq();
			Connection con = null;
			CallableStatement cstmt = null;
			tryCount++;
			try {
				con = getConnection();
				final String prc = "prc_get_cust_prfl_ver(?,?)";
				final String sql = "call " + DB_PACKAGE + "." + prc;
				cstmt = con.prepareCall(sql);
				cstmt.setString(1, newRcvNbr);
				cstmt.registerOutParameter(2, OracleTypes.NUMERIC);
				cstmt.execute();
				int prfl_ver = cstmt.getInt(2);
				if (prfl_ver == 0)
					availableRcvNbr = newRcvNbr;
				// else availableRcvNbr stays null;
			} catch (SQLException se) {
				log.info("Exception thrown on editing registrations" + se);
				throw new DRSException(
						DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null,
						new Date(),
						"Error occurred during saving Registration fields",
						SERVER);
			} finally {
				close(cstmt, con);
			} // end-try
		} while (availableRcvNbr == null && tryCount < 9);
		if (availableRcvNbr == null)
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE,
					"Exceeded retry limit for available cust_rcv_nbr",
					"mgCustomerReceiveNumber", new Date(),
					"Error occurred trying to create a new cust_rcv_nbr value",
					SERVER);
		return availableRcvNbr;
	}
	
	@Override
	public boolean updateTransactionUseFlag(BusinessObject businessObject)throws Exception {
		
		CustomerReceiveDetails details = (CustomerReceiveDetails) businessObject;		
		log.info("inbound Object : ["+details+"]");
		Connection con = null;
		CallableStatement stmt = null;		
		int rowsAffected = 0;
		try {
			con = getConnection();
			final String prc = "prc_u_cust_prfl_flg(?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			stmt = con.prepareCall(sql);
			stmt.setString(1, details.getCustomerReceiveNumber());
			stmt.setString(2, details.getCustomerReceiveNumberVersion().toString());			
			rowsAffected = stmt.executeUpdate();					
			
			log.info("No of rows affected : "+rowsAffected);
			
		} catch (Exception se) {
			log.info("Exception thrown on update transactional use flag : " + se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error occurred during update transactional use flag ", SERVER);
		} finally {			
			close(stmt, con);
		} 		
		return (rowsAffected > 0 ? true : false);
	}
}
