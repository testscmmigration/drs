package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.OFACInfo;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class SaveOFACDAO extends BaseDao implements SaveDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(SaveOFACDAO.class);

	public BusinessObject save(BusinessObject businessObject) throws Exception {
		OFACInfo ofInfo = (OFACInfo) businessObject;
		insertCustPrflOfacActn(ofInfo.getCustomerReceiveNumber(), ofInfo
				.getCustomerReceiveNumberVersion(), ofInfo.getOFACStatus());
		return null;
	}

	public void insertCustPrflOfacActn(String cust_rcv_nbr, int prfl_ver_nbr,
			String ofac_excp_state_code) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_i_cust_profile_ofac_action(?, ?, ?, ?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			int i = 1;
			cstmt.setString(i++, cust_rcv_nbr);
			cstmt.setInt(i++, prfl_ver_nbr);
			cstmt.setString(i++, ofac_excp_state_code);
			cstmt.setTimestamp(i++, new Timestamp(Calendar.getInstance().getTime().getTime()));
			cstmt.execute();
		} catch (SQLException se) {
			log.info("The exception when saving ofac data" + se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error Saving data", SERVER);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	@Override
	public boolean updateTransactionUseFlag(BusinessObject businessObject)
			throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
}
