package com.moneygram.drs.persistence;

import java.util.List;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.CustomerReceiveDetails;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public interface PersistenceManager {
	public abstract BusinessObject save(BusinessObject businessObject) throws Exception;

	public abstract BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException;
	
	public abstract BusinessObject read(ObjectId objectId,String procCall) throws NotFoundException, DRSException;

	public abstract List find(Criteria criteria) throws Exception;

	public abstract boolean assertData(Criteria criteria) throws Exception;
	
	public boolean updateTransactionUseFlag(BusinessObject businessObject) throws Exception;
}