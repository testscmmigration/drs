package com.moneygram.drs.persistence;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.mgEnum.EnumRegistry;
import com.moneygram.drs.bo.Address;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.CustomerProfile2;
import com.moneygram.drs.bo.CustomerProfileCustomField;
import com.moneygram.drs.bo.CustomerProfilePerson;
import com.moneygram.drs.bo.RegistrationStatusCode;
import com.moneygram.drs.criteria.CustomerProfileCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class CustomerProfileDAO2 extends BaseDao implements ReadDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(
			CustomerProfileDAO.class);

	public BusinessObject read(ObjectId objectId) throws NotFoundException,
			DRSException {
		CustomerProfile2 customerProfile = null;
		CustomerProfileCriteria customerProfileCriteria = (CustomerProfileCriteria) objectId
				.getId();

		if (!customerProfileCriteria.isAbbrCustomerProfile()) {
			customerProfile = getCustomerProfile(
					customerProfileCriteria.getCustomerReceiveNumber(),
					customerProfileCriteria.getProfileVersion());
		}

		return customerProfile;
	}

	/**
	 * @param profileID
	 * @param profileVersion
	 * @return Profile ValueObject This Function returns the customer profile
	 * 
	 */
	public CustomerProfile2 getCustomerProfile(String profileID,
			int profileVersion) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_customer_profile_info(?, ?, ?, ?, ?, ?)";
			String sql = "call " + DB_PACKAGE + "." + prc;

			cstmt = con.prepareCall(sql);
			cstmt.setString(1, profileID);
			if (profileVersion < 1) {
				cstmt.setNull(2, OracleTypes.NUMERIC);
			} else {
				cstmt.setInt(2, profileVersion);
			}
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.registerOutParameter(5, OracleTypes.CURSOR);
			cstmt.registerOutParameter(6, OracleTypes.CURSOR);

			cstmt.execute();
			ResultSet profileRS = (ResultSet) cstmt.getObject(3);
			ResultSet profilePersonRS = (ResultSet) cstmt.getObject(4);
			ResultSet profilePersonAddressRS = (ResultSet) cstmt.getObject(5);
			ResultSet profileDataRS = (ResultSet) cstmt.getObject(6);
			CustomerProfile2 profile = null;
			if (profileRS.next()) {
				profile = getCustomerProfileObject(profileRS, profilePersonRS,
						profilePersonAddressRS, profileDataRS);
			}
			profileRS.close();
			profilePersonRS.close();
			profilePersonAddressRS.close();
			profileDataRS.close();
			return profile;
		} catch (SQLException se) {
			se.printStackTrace();
			log.error(
					"Exception during reading customer profile"
							+ se.getMessage(), se);
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE,
					"Profile ID:", profileID, new Date(),
					"Error occurred during reading CustomerProfile", null);
		} finally {
			close(cstmt, con);
		}
	}

	private CustomerProfile2 getCustomerProfileObject(ResultSet profileRS,
			ResultSet profilePersonRS, ResultSet profilePersonAddressRS,
			ResultSet profileDataRS) throws SQLException {
		CustomerProfile2 profile = new CustomerProfile2();
		profile.setCustomerReceiveNumber(profileRS
				.getString("pfl_cust_rcv_nbr"));
		profile.setCustomerReceiveNumberVersion(profileRS
				.getInt("pfl_cust_prfl_ver_nbr"));
		profile.setAgentID(profileRS.getString("pfl_agent_id"));
		profile.setDeliveryOptionID(profileRS.getInt("pfl_dlvr_optn_id"));
		profile.setOfacStatus(profileRS.getString("pfl_ofac_excp_state_code"));
		profile.setRegistrationStatus((RegistrationStatusCode) EnumRegistry
				.getByName(RegistrationStatusCode.class,
						profileRS.getString("pfl_prfl_stat_code")));
		profile.setRegistrationSubStatus(profileRS
				.getString("pfl_prfl_sub_stat_code"));
		profile.setRcvAgentCustAcctNbr(profileRS
				.getString("pfl_rcv_agent_cust_acct_nbr"));
		profile.setRcvAgentCustAcctNbrMask(profileRS
				.getString("pfl_rcv_agt_cust_acct_mask_nbr"));
		profile.setRcvAgentBankID(profileRS.getString("pfl_rcv_agent_bank_id"));
		profile.setRcvAgentCustAcctName(profileRS
				.getString("pfl_rcv_agent_cust_acct_name"));
		profile.setRcvAgentCustAcctNbrPanFlag(profileRS
				.getString("pfl_rcv_cust_acct_nbr_pan_flag"));
		// profile.setAccountNickname(profileRS.getString("pfl_acct_nkname"));
		// added for 30665j agg infra rules.
		profile.setRuleProcessAccountNumber(profileRS
				.getString("pfl_rule_prcs_acct_nbr"));
		profile.setAccountNumberSensitveID(profileRS
				.getString("pfl_sens_data_val_id"));
		Map<BigDecimal, Address> addressMap = new HashMap<BigDecimal, Address>();
		while (profilePersonAddressRS != null && profilePersonAddressRS.next()) {
			Address accountHolderAddress = new Address();

			accountHolderAddress.setAddrLine1(profilePersonAddressRS
					.getString("addr_addr_line1_text"));
			accountHolderAddress.setAddrLine2(profilePersonAddressRS
					.getString("addr_addr_line2_text"));
			accountHolderAddress.setAddrLine3(profilePersonAddressRS
					.getString("addr_addr_line3_text"));
			accountHolderAddress.setCity(profilePersonAddressRS
					.getString("addr_addr_city_name"));
			accountHolderAddress.setCntryCode(profilePersonAddressRS
					.getString("addr_addr_cntry_id"));
			accountHolderAddress
					.setDeliveryInstructions1(profilePersonAddressRS
							.getString("addr_dlvr_instr1_text"));
			accountHolderAddress
					.setDeliveryInstructions2(profilePersonAddressRS
							.getString("addr_dlvr_instr2_text"));
			accountHolderAddress
					.setDeliveryInstructions3(profilePersonAddressRS
							.getString("addr_dlvr_instr3_text"));
			accountHolderAddress.setPostalCode(profilePersonAddressRS
					.getString("addr_addr_postal_code"));
			accountHolderAddress.setState(profilePersonAddressRS
					.getString("addr_addr_state_name"));
			accountHolderAddress.setCountrySubDivCode(profilePersonAddressRS
					.getString("addr_addr_subdiv_code"));
			addressMap.put(profilePersonAddressRS.getBigDecimal("addr_cust_seq_id"),
					accountHolderAddress);
			//accountHolderAddress.setAccountNumberHashCode(profilePersonAddressRS
			//		.getInt(columnName));
		}

		int accountHolderIndex = 0;
		while ((profilePersonRS != null) && (profilePersonRS.next())) {
			CustomerProfilePerson accountHolder = new CustomerProfilePerson();
			accountHolder.setSequenceID(profilePersonRS
					.getInt("per_cust_seq_id"));
			accountHolder.setFirstName(profilePersonRS
					.getString("per_cust_frst_name"));
			accountHolder.setLastName(profilePersonRS
					.getString("per_cust_last_name"));
			accountHolder.setMiddleName(profilePersonRS
					.getString("per_cust_mid_name"));
			accountHolder.setMaternalName(profilePersonRS
					.getString("per_cust_matrnl_name"));
			accountHolder.setPhoneNumber(profilePersonRS
					.getString("per_cust_ph_nbr"));
			accountHolder.setAccountOwnerFlag(profilePersonRS.getString("per_acct_ownr_flag"));
			accountHolder.setBeneficiaryFlag(profilePersonRS.getString("per_benef_flag"));
			accountHolder.setRegistrationProfileFlag(profilePersonRS.getString("per_rgst_prfl_flag"));
			//Changes as part of ADBOP-692
			accountHolder.setPrimaryPhoneCountryCode(profilePersonRS.getString("per_cust_phn_cntry_code"));
			if (profilePersonRS.getString("per_cust_phn_sms_enbl_flag") != null && profilePersonRS.getString("per_cust_phn_sms_enbl_flag").charAt(0) == 'Y'){
				accountHolder.setSmsOptInEnabledFlag(true);
			}
			if (profilePersonRS.getString("per_notf_opt_in_cnsnt_flag") != null && profilePersonRS.getString("per_notf_opt_in_cnsnt_flag").charAt(0) == 'Y'){
				accountHolder.setNotificationOptInFlag(true);
			}	
			Address address = addressMap.get(profilePersonRS
					.getBigDecimal("per_cust_seq_id"));
			if (address != null) {
				accountHolder.addAddress(address);
			}

			if (profilePersonRS.getString("per_rgst_prfl_flag").charAt(0) == 'Y') {
				profile.setCreator(accountHolder);
			} else if (profilePersonRS.getString("per_benef_flag").charAt(0) == 'Y') {
				profile.setReceiver(accountHolder);
			} else {
				accountHolderIndex = profilePersonRS.getInt("per_cust_seq_id") - 2;
				profile.addJointAccountHolder(accountHolder, accountHolderIndex);
			}
		}

		while ((profileDataRS != null) && profileDataRS.next()) {
			CustomerProfileCustomField item = new CustomerProfileCustomField();
			item.setAttrId(profileDataRS.getInt("data_attr_id"));
			item.setValue(profileDataRS.getString("data_attr_val"));
			item.setXmlTag(profileDataRS.getString("data_agent_field_name"));
			profile.addCustomFields(item, item.getXmlTag());
		}
		return profile;
	}

	@Override
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		// TODO Auto-generated method stub
		return null;
	}
}
