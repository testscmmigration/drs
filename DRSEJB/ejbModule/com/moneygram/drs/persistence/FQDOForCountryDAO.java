package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.criteria.FQDOForCountryCriteria;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class FQDOForCountryDAO extends BaseDao implements FindDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(FQDOForCountryDAO.class);

	public List find(Criteria criteria) throws Exception {
		FQDOForCountryCriteria fQDOForCountryCriteria = (FQDOForCountryCriteria) criteria;
		Connection con = null;
		ResultSet rs = null;
		CallableStatement stmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_fqdos_for_country(?,?,?)";
			final String sql = "{ call " + DB_PACKAGE + "." + prc + " }";
			stmt = con.prepareCall(sql);
			int i = 1;
			String language = null == fQDOForCountryCriteria.getLanguage() ? this.language
					: fQDOForCountryCriteria.getLanguage();
			stmt.setString(i++, fQDOForCountryCriteria.getReceiveCountry());
			stmt.setString(i++, language);
			int iResult = i++;
			stmt.registerOutParameter(iResult, OracleTypes.CURSOR);
			stmt.execute();
			List<FQDOInfo> result = new ArrayList<FQDOInfo>();
			rs = (ResultSet) (stmt.getObject(iResult));
			while (rs.next()) {
				FQDOInfo info = new FQDOInfo();
				copy(rs, info, language);
				result.add(info);
			}
			return result;
		} catch (SQLException e) {
			log.info("Exception during reading FQDOs for country" + e.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "fQDOsForCountry",
					fQDOForCountryCriteria.getReceiveCountry(), new Date(),
					"Error reading FQDOs for country", null);
		} finally {
			close(rs, stmt, con);
		}// end-try
	}

	private void copy(ResultSet rs, FQDOInfo info, String language) throws Exception {
		info.setReceiveCountry(rs.getString("receive_country"));
		info.setDeliveryOption(rs.getString("delivery_option"));
		info.setDeliveryOptionDisplayName(rs.getString("delivery_option_display_name"));
		info.setReceiveAgentAbbreviation(rs.getString("receive_agent_abbreviation"));
		info.setReceiveAgentID(rs.getString("receive_agent_id"));
		info.setReceiveAgentName(rs.getString("receive_agent_name"));
		info.setReceiveCurrency(rs.getString("receive_currency"));
		info.setRegistrationAuthorizationText(rs.getString("registration_authorization"));
		info.setSpeedOfDeliveryText(rs.getString("speed_of_delivery_text"));
	}
}
