package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;
import com.moneygram.drs.bo.CountryInfo;
import com.moneygram.drs.criteria.Criteria;

public class CountryInfoDAO extends BaseDao implements FindDao {
	public List find(Criteria criteria) throws Exception {
		Connection conn = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		List<CountryInfo> countryInfoList = new ArrayList<CountryInfo>();
		try {
			conn = getConnection();
			callableStatement = conn.prepareCall("{ call pkg_drs.prc_get_country(?) }");
			callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
			callableStatement.execute();
			resultSet = (ResultSet) callableStatement.getObject(1);
			while (resultSet.next()) {
				CountryInfo countryInfo = new CountryInfo();
				countryInfo.setCountryCode(resultSet.getString("iso_cntry"));
				countryInfo.setCountryName(resultSet.getString("cntry_name"));
				countryInfo.setCountryLegacyCode(resultSet.getString("mf_cntry"));
				String mgRegistrationAllowed = resultSet.getString("mg_ds_reg_cntry");
				mgRegistrationAllowed = (mgRegistrationAllowed) == null ? null
						: mgRegistrationAllowed.trim();
				countryInfo.setMgDirectedSendCountry("Y".equalsIgnoreCase(mgRegistrationAllowed));
				countryInfoList.add(countryInfo);
			}
		} finally {
			close(resultSet, callableStatement, conn);
		}
		return countryInfoList;
	}
}
