package com.moneygram.drs.persistence;

import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.RegistrationInfo;
import com.moneygram.drs.criteria.Criteria;
import com.moneygram.drs.criteria.QueryRegistrationsCriteria;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.util.DAOUtil;

public class QueryRegistrationsDAO extends BaseDao implements FindDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(QueryRegistrationsDAO.class);

	public List find(Criteria criteria) throws Exception {
		QueryRegistrationsCriteria queryRegistrationsCriteria = (QueryRegistrationsCriteria) criteria;
		List result = getData(queryRegistrationsCriteria);
		return result;
	}

	private List getData(QueryRegistrationsCriteria queryRegistrationsCriteria) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String receiverFirstName = queryRegistrationsCriteria.getReceiverFirstName();
		String receiverLastName = queryRegistrationsCriteria.getReceiverLastName();
		String receiverPhoneNumber = queryRegistrationsCriteria.getReceiverPhoneNumber();
		String deliveryOption = queryRegistrationsCriteria.getDeliveryOption();
		String agentID = queryRegistrationsCriteria.getReceiveAgentID();
		String receiveCountry = queryRegistrationsCriteria.getReceiveCountry();
		try {
			if (receiverFirstName != null) {
				if (receiverFirstName.indexOf('*') >= 0) {
					if (receiverFirstName.indexOf('*') < receiverFirstName.length() - 1) {
						throw new DRSException(
								DRSExceptionCodes.EC628_WILDCARDS_NOT_AT_END_OF_STRINGS,
								"receiverFirstName",
								receiverFirstName,
								new Date(),
								"WildCards not at the end of Strings, (queryRegistration) Reciver First Name ",
								SERVER);
					} else {
						receiverFirstName = receiverFirstName.replace('*', '%');
					}
				}
			}
			if (receiverLastName != null) {
				if (receiverLastName.indexOf('*') >= 0) {
					if (receiverLastName.indexOf('*') < receiverLastName.length() - 1) {
						throw new DRSException(
								DRSExceptionCodes.EC628_WILDCARDS_NOT_AT_END_OF_STRINGS,
								"receiverFirstName",
								receiverLastName,
								new Date(),
								"WildCards not at the end of Strings, (queryRegistration) Reciver Last Name ",
								SERVER);
					} else {
						receiverLastName = receiverLastName.replace('*', '%');
					}
				}
			}
			if (receiverPhoneNumber != null) {
				if (receiverPhoneNumber.indexOf('*') >= 0) {
					if (receiverPhoneNumber.indexOf('*') < receiverPhoneNumber.length() - 1) {
						throw new DRSException(
								DRSExceptionCodes.EC628_WILDCARDS_NOT_AT_END_OF_STRINGS,
								"receiverPhoneNumber",
								receiverPhoneNumber,
								new Date(),
								"WildCards not at the end of Strings, (queryRegistration) Reciver Phone Number ",
								SERVER);
					} else {
						receiverPhoneNumber = receiverPhoneNumber.replace('*', '%');
					}
				}
			}
			con = getConnection();
			final String prc = "prc_get_registrations(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			if (deliveryOption != null) {
				cstmt.setString(1, deliveryOption);
			} else {
				cstmt.setNull(1, OracleTypes.VARCHAR);
			}
			if (agentID == null) {
				cstmt.setNull(2, Types.NUMERIC); // agentId is num in dB
			} else {
				cstmt.setString(2, agentID);
			}
			DAOUtil.setParameter(cstmt, receiveCountry, 3);
			DAOUtil.setParameter(cstmt, queryRegistrationsCriteria.getReceiveCurrency(), 4);
			DAOUtil.setParameter(cstmt, receiverFirstName, 5);
			DAOUtil.setParameter(cstmt, receiverLastName, 6);
			DAOUtil.setParameter(cstmt, receiverPhoneNumber, 7);
			DAOUtil.setParameter(cstmt, queryRegistrationsCriteria.getAccountNumberLast4(), 8);
			String language = null == queryRegistrationsCriteria.getLanguage() ? this.language
					: queryRegistrationsCriteria.getLanguage();
			DAOUtil.setParameter(cstmt, language, 9);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(10);
			return setDataFromResultSet(rs, language);
		} catch (Exception excep) {
			log.info("Exception during reading registration fields" + excep.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "receiveCountry",
					receiveCountry, new Date(),
					"Error occurred during reading registration details", null);
		} finally {
			close(rs, cstmt, con);
		}
	}

	private List setDataFromResultSet(ResultSet rs, String language) throws SQLException, Exception {
		RegistrationInfo registrationInfo = new RegistrationInfo();
		FQDOInfo fqdoInfo = new FQDOInfo();
		HashMap<String, RegistrationInfo> registrationsMap = new HashMap<String, RegistrationInfo>();
		Collection<RegistrationInfo> registrationList;
		List<RegistrationInfo> registrationInfoList = new ArrayList<RegistrationInfo>();
		while (rs.next()) {
			String custRcvNbr = rs.getString("cust_rcv_nbr");
			if (registrationsMap.containsKey(custRcvNbr)) {
				registrationInfo = (RegistrationInfo) registrationsMap.get(custRcvNbr);
				if (rs.getString("benef_flag").charAt(0) == 'Y') {
					registrationInfo.setReceiverFirstName(rs.getString("cust_frst_name"));
					registrationInfo.setReceiverLastName(rs.getString("cust_last_name"));
					registrationInfo.setReceiverPhoneNumber(rs.getString("cust_ph_nbr"));
				}
				if (rs.getString("rgst_prfl_flag").charAt(0) == 'Y') {
					registrationInfo.setCreatorFirstName(rs.getString("cust_frst_name"));
					registrationInfo.setCreatorLastName(rs.getString("cust_last_name"));
				}
			} else {
				registrationInfo = new RegistrationInfo();
				registrationsMap.put(custRcvNbr, registrationInfo);
				registrationInfo.setMgCustomerReceiveNumber(custRcvNbr);
				registrationInfo.setMgCustomerReceiveNumberVersion(new BigInteger(rs
						.getString("cust_prfl_ver_nbr")));
				registrationInfo.setAccountNickname(rs.getString("acct_nkname"));
								
				// missing status and substaus ask lisa
				if (rs.getString("benef_flag").charAt(0) == 'Y') {
					registrationInfo.setReceiverFirstName(rs.getString("cust_frst_name"));
					registrationInfo.setReceiverLastName(rs.getString("cust_last_name"));
					registrationInfo.setReceiverPhoneNumber(rs.getString("cust_ph_nbr"));
				}
				if (rs.getString("rgst_prfl_flag").charAt(0) == 'Y') {
					registrationInfo.setCreatorFirstName(rs.getString("cust_frst_name"));
					registrationInfo.setCreatorLastName(rs.getString("cust_last_name"));
				}
				fqdoInfo = new FQDOInfo();
				fqdoInfo.setReceiveCountry(rs.getString("iso_cntry"));
				String deliveryOptionID = rs.getString("dlvr_optn_id");
				if (deliveryOptionID == null || deliveryOptionID.trim().length() == 0) {
					throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE,
							"deliveryOptionID", deliveryOptionID, new Date(),
							"Unknown returned delivery option", null);
				}
				fqdoInfo.setDeliveryOption(deliveryOptionID);
				fqdoInfo.setReceiveAgentID(rs.getString("agent_id"));
				fqdoInfo.setReceiveCurrency(rs.getString("iso_currency_code"));
				fqdoInfo.setReceiveAgentName(rs.getString("name"));
				fqdoInfo.setReceiveAgentAbbreviation(rs.getString("agent_abbr_name"));
				// Agent
				fqdoInfo.setDeliveryOptionDisplayName(rs.getString("dlvr_optn_text"));
				// receive_option
				fqdoInfo.setRegistrationAuthorizationText(rs.getString("reg_auth_text"));
				// mainoffice_receive_option
				fqdoInfo.setSpeedOfDeliveryText(rs.getString("dlvr_time_text"));
				// mainoffice_receive_option
				registrationInfo.setFqdoInfo(fqdoInfo);
			}
		}
		registrationList = registrationsMap.values();
		registrationInfoList.addAll(registrationList);
		return registrationInfoList;
	}
}
