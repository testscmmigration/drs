package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.mgEnum.EnumRegistry;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.DataTypeCodeEnum;
import com.moneygram.drs.bo.EnumeratedValueInfo;
import com.moneygram.drs.bo.ExtendedRegistrationFieldInfo;
import com.moneygram.drs.bo.RegistrationFieldsCollection;
import com.moneygram.drs.bo.TransactionAttributeType;
import com.moneygram.drs.criteria.RegistrationFieldsInfoCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.xmlTagMapper.StandardFieldTags;

public class RegistrationFieldsDAO extends BaseDao implements ReadDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(
			RegistrationFieldsDAO.class);

	public BusinessObject read(ObjectId objectId) throws NotFoundException,
			DRSException {
		RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria = (RegistrationFieldsInfoCriteria) objectId
				.getId();
		RegistrationFieldsCollection registrationFieldsCollection = getFQDORegistrationFields(registrationFieldsInfoCriteria);
		return registrationFieldsCollection;
	}

	public RegistrationFieldsCollection getFQDORegistrationFields(
			RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria)
			throws DRSException {
		List<ExtendedRegistrationFieldInfo> fullRFCList = new ArrayList<ExtendedRegistrationFieldInfo>();
		HashMap<Integer, List<ExtendedRegistrationFieldInfo>> attrMap = new HashMap<Integer, List<ExtendedRegistrationFieldInfo>>();
		HashMap<Integer, ExtendedRegistrationFieldInfo> confAttrMap = new HashMap<Integer, ExtendedRegistrationFieldInfo>();
		List<ExtendedRegistrationFieldInfo> arrList;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String cardRange = null;
		RegistrationFieldsCollection registrationFieldsCollection = null;

		// PROCEDURE prc_get_reg_info
		// (iv_receive_country IN VARCHAR2,
		// iv_delivery_option IN receive_option.pos_xml_tag_text%TYPE,
		// iv_receive_agent_id IN agent.agent_id%TYPE,
		// iv_receive_currency IN VARCHAR2,
		// iv_language IN locale.iso_lang_mnemonic%TYPE,
		// iv_suppln_fields_rtn_flag IN VARCHAR2,
		// ov_reg_info_cv OUT registration_info_cv_type,
		// ov_enum_cv OUT tran_attr_list_cv_type,
		// ov_xtra_vldn_type_code OUT agent_tran_cnfg.xtra_vldn_type_code%TYPE
		// ov_suppln_fields_avl_flag OUT VARCHAR2
		// )

		try {
			con = getConnection();
			final String prc = "prc_get_reg_info(?,?,?,?,?,?,?,?,?,?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);

			cstmt.setString("iv_receive_country",
					registrationFieldsInfoCriteria.getReceiveCountry());
			cstmt.setString("iv_delivery_option",
					registrationFieldsInfoCriteria.getDeliveryOption());
			cstmt.setString("iv_receive_agent_id",
					registrationFieldsInfoCriteria.getReceiveAgentId());
			cstmt.setString("iv_receive_currency",
					registrationFieldsInfoCriteria.getReceiveCurrency());
//			cstmt.setString("iv_external_receiver_profile",
//					(registrationFieldsInfoCriteria.isExternalReceiverProfile()?"Y":"N"));
			String language = null == registrationFieldsInfoCriteria
					.getLanguage() ? this.language
					: registrationFieldsInfoCriteria.getLanguage();
			log.info("Input language for prc_get_reg_info " + language);
			cstmt.setString("iv_language", language);
			cstmt.setString("iv_suppln_fields_rtn_flag", "N");

			cstmt.registerOutParameter("ov_reg_info_cv", OracleTypes.CURSOR);
			cstmt.registerOutParameter("ov_enum_cv", OracleTypes.CURSOR);
			cstmt.registerOutParameter("ov_xtra_vldn_type_code",
					OracleTypes.VARCHAR);
			cstmt.registerOutParameter("ov_suppln_fields_avl_flag",
					OracleTypes.VARCHAR);

			cstmt.execute();
			rs = (ResultSet) (cstmt.getObject("ov_reg_info_cv"));
			while (rs.next()) {
				ExtendedRegistrationFieldInfo result = buildRecord(rs,
						confAttrMap, language, registrationFieldsInfoCriteria);

				// Added for 56737 GR project
				// to handle multiple languages
				if ((null != attrMap && !attrMap.isEmpty())
						&& attrMap.containsKey(result.getAttrID())) {
					attrMap.get(result.getAttrID()).add(result);
				} else {
					arrList = new ArrayList<ExtendedRegistrationFieldInfo>();
					arrList.add(result);
					attrMap.put(result.getAttrID(), arrList);
				}
			}
			rs.close();
			rs = (ResultSet) (cstmt.getObject("ov_enum_cv"));
			while (rs.next()) {
				EnumeratedValueInfo data = new EnumeratedValueInfo();
				Integer key = new Integer(rs.getInt("attr_id"));
				data.setValue(rs.getString("attr_val"));
				data.setLabel(rs.getString("attr_val_text"));
				List<ExtendedRegistrationFieldInfo> resultList = attrMap
						.get(key);
				for (ExtendedRegistrationFieldInfo result : resultList) {
					result.addEnumeratedValue(data);
				}
				if (confAttrMap.containsKey(key)) {
					ExtendedRegistrationFieldInfo confResult = (ExtendedRegistrationFieldInfo) confAttrMap
							.get(key);
					confResult.addEnumeratedValue(data);
				}
			}
			cardRange = (String) (cstmt.getString("ov_xtra_vldn_type_code"));

			for (Map.Entry<Integer, List<ExtendedRegistrationFieldInfo>> entry : attrMap
					.entrySet()) {
				fullRFCList.addAll(entry.getValue());

			}
			fullRFCList.addAll(confAttrMap.values());

			boolean supplementalFlag = "Y".equalsIgnoreCase(cstmt
					.getString("ov_suppln_fields_avl_flag"));

			registrationFieldsCollection = new RegistrationFieldsCollection(
					fullRFCList, cardRange, supplementalFlag,registrationFieldsInfoCriteria.isSaveRegistrationWithNewXmlTags());
			return registrationFieldsCollection;
		} catch (SQLException se) {
			log.info("Exception when reading the registration fields"
					+ se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE,
					"Agent ID:",
					registrationFieldsInfoCriteria.getReceiveAgentId(),
					new Date(),
					"Error occurred during reading Registration fields", SERVER);
		} finally {
			close(rs, cstmt, con);
		} // end-try
	}

	private ExtendedRegistrationFieldInfo buildRecord(ResultSet rs,
			HashMap<Integer, ExtendedRegistrationFieldInfo> confAttrMap,
			String language,
			RegistrationFieldsInfoCriteria registrationFieldsInfoCriteria)
			throws SQLException, DRSException {
		ExtendedRegistrationFieldInfo valueObject = new ExtendedRegistrationFieldInfo();
		valueObject.setAttrID(rs.getInt("attr_id"));
		valueObject.setAttrName(rs.getString("attr_name"));
		char attrTypeCode = rs.getString("attr_type_code").charAt(0);
		valueObject.setAttrType(TransactionAttributeType
				.getTypeFromCode(attrTypeCode));
		valueObject.setNewXmlTag(rs.getString("tag_name"));
		valueObject.setXmlTag(rs.getString("pos_xml_tag_text").toUpperCase());
		//Changes done for 30665i Send POE - To get the Old xml tag name
	//	valueObject.setOldXmlTag(rs.getString("pos_xml_tag_text")
	//			.toUpperCase());
		String dataType = rs.getString("data_typ_code");
		if (dataType.equalsIgnoreCase(DataTypeCodeEnum.ENUM.getName())) {
			valueObject.setEnumerated(true);
			dataType = DataTypeCodeEnum.STRING.getName();
		}
		valueObject.setDataTypeCode((DataTypeCodeEnum) EnumRegistry.getByName(
				DataTypeCodeEnum.class, dataType));
		// TODO do we need to throw an exception for bad enum value US
		valueObject.setHidden(rs.getString("input_mask_flag").charAt(0) == 'Y');
		valueObject.setEncrypt(rs.getString("encryp_flag").charAt(0) == 'Y');
		valueObject.setRequired(rs.getString("attr_req_flag").charAt(0) == 'R');
		valueObject.setDisplayOrder(new Integer(rs.getInt("attr_seq_id") * 2));
		valueObject.setFieldLabel(rs.getString("pos_lbl_tran_text"));
		valueObject.setExampleFormat(rs.getString("pos_fmt_example_text"));
		// PRF 54351: to add ExampleFormatID and ExtendedHelpTextID
		int exampleFormatID = rs.getInt("pos_fmt_example_text_id");
		if (exampleFormatID != -1)
			valueObject.setExampleFormatID(new Integer(exampleFormatID));
		int extendedHelpTextID = rs.getInt("pos_extd_hlp_text_id");
		if (extendedHelpTextID != -1)
			valueObject.setExtendedHelpTextID(new Integer(extendedHelpTextID));
		valueObject.setFieldMin(new Integer(rs.getInt("min_lgth_qty")));
		valueObject.setFieldMax(new Integer(rs.getInt("max_lgth_qty")));
		valueObject.setFieldScale(new Integer(rs.getInt("num_scal_lgth_qty")));
		valueObject.setValidationRegEx(rs.getString("vldn_exprs_text"));
		valueObject.setCheckDigitAlgorithm(rs.getString("chk_dgt_alg_abbr"));
		valueObject.setDefaultValue(rs.getString("attr_dflt_val"));
		valueObject.setCategory(rs.getString("text_cat_code"));
		// Added for 56737 GR project
		// to handle multiple languages
		if (!StringUtility.isNullOrEmpty(language)) {
			valueObject.setLanguageCode(rs.getString("lang_code"));
		}

		String includeFlag = rs.getString("itra_tran_rgstn_displ_flag");
		if (includeFlag == null || includeFlag.equalsIgnoreCase("Y")) {
			valueObject.setIncludeInInterTransaction(true);
		} else {
			valueObject.setIncludeInInterTransaction(false);
		}

		// TODO do we need this attribute..
		// valueObject.setAgentFldName(rs.getString("agent_fld_name"));
		String pos_conf_lbl_text = rs.getString("pos_conf_lbl_tran_text");
		//Evaluate reenter field logic only if it is higher AC version
		if (!registrationFieldsInfoCriteria.isSuppressReEnterFields()
				&& pos_conf_lbl_text != null
				&& pos_conf_lbl_text.trim().length() > 0) {
			valueObject.setConfirmEntry(true);
			ExtendedRegistrationFieldInfo dupValueObject;
			try {
				dupValueObject = (ExtendedRegistrationFieldInfo) valueObject
						.deepClone();
			} catch (Exception ex) {
				throw new DRSException(
						DRSExceptionCodes.EC901_DATABASE_FAILURE,
						"Problem doing a deep clone of object", null,
						new Date(), null, SERVER);
			}
			dupValueObject.setConfirmEntry(true);
			dupValueObject.setFieldLabel(pos_conf_lbl_text);
			dupValueObject.setXmlTag(valueObject.getXmlTag()
					+ StandardFieldTags.CONFIRM_FIELD_POSTFIX);
			dupValueObject.setDisplayOrder(new Integer(valueObject
					.getDisplayOrder().intValue() + 1));
			dupValueObject.setDataTypeCode(valueObject.getDataTypeCodeEnum());
			//Added as part of ADP-1146 dual account entry changes
			dupValueObject.setNewXmlTag(rs.getString("re_enter_tag_name"));
			Integer key = new Integer(dupValueObject.getAttrID());
			confAttrMap.put(key, dupValueObject);
		}
		return valueObject;
	}

	@Override
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		// TODO Auto-generated method stub
		return null;
	}
}
