package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.FQDOInfo;
import com.moneygram.drs.bo.FqdoEnum;
import com.moneygram.drs.bo.FullFQDOWithAgentStatus;
import com.moneygram.drs.bo.FullFQDOWithRegistrationStatus;
import com.moneygram.drs.criteria.FQDOCriteria;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class FQDODao extends BaseDao implements ReadDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(FQDODao.class);

	public BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException {
		BusinessObject businessObject = null;
		FQDOCriteria fqdoCriteria = (FQDOCriteria) objectId.getId();
		if (fqdoCriteria.getFqdoEnum().equals(FqdoEnum.FQDO)) {
			businessObject = getFQDO(fqdoCriteria);
		} else if (fqdoCriteria.getFqdoEnum().equals(FqdoEnum.FULLFQDO_WITH_REGISTRATIONSTATUS)) {
			businessObject = (FullFQDOWithRegistrationStatus) getFullFqdo(fqdoCriteria);
		} else if (fqdoCriteria.getFqdoEnum().equals(FqdoEnum.FULLFQDO)) {
			businessObject = (FullFQDOWithAgentStatus) getFullFqdo(fqdoCriteria);
		}
		return businessObject;
	}

	@SuppressWarnings("null")
	private Object getFullFqdo(FQDOCriteria fqdoCriteria) throws NotFoundException, DRSException {
		FullFQDOWithRegistrationStatus fullFQDOWithRegistrationStatus = null;
		FullFQDOWithAgentStatus fullFQDOWithAgentStatus = null;
		FQDOInfo fqdoInfo = new FQDOInfo();
		Connection con = null;
		ResultSet rs = null;
		CallableStatement stmt = null;
		Object object = null;
		try {
			con = getConnection();
			final String prc = "prc_get_full_fqdo(?,?,?)";
			final String sql = "{ call " + DB_PACKAGE + "." + prc + " }";
			stmt = con.prepareCall(sql);
			int i = 1;
			stmt.setString(i++, fqdoCriteria.getCustomerReceiveNumber());
			String language = null == fqdoCriteria.getLanguage() ? this.language : fqdoCriteria
					.getLanguage();
			if(null!=language && language.contains(","))
			{
			String splitLangugae[] = language.split(",");
			language=splitLangugae[0];
			}
			stmt.setString(i++, language);
			int iResult = i++;
			stmt.registerOutParameter(iResult, OracleTypes.CURSOR);
			stmt.execute();
			rs = (ResultSet) (stmt.getObject(iResult));
			if (rs.next()) {
				if (fqdoCriteria.getFqdoEnum().equals(FqdoEnum.FULLFQDO_WITH_REGISTRATIONSTATUS)) {
					fullFQDOWithRegistrationStatus = new FullFQDOWithRegistrationStatus();
					copyWithRegistrationStatus(rs, fullFQDOWithRegistrationStatus, fqdoInfo,
							language);
					fullFQDOWithRegistrationStatus.setFqdoInfo(fqdoInfo);
					object = fullFQDOWithRegistrationStatus;
				} else if (fqdoCriteria.getFqdoEnum().equals(FqdoEnum.FULLFQDO)) {
					fullFQDOWithAgentStatus = new FullFQDOWithAgentStatus();
					copyWithAgentStatus(rs, fullFQDOWithAgentStatus, fqdoInfo,
							language);
					fullFQDOWithAgentStatus.setFqdoInfo(fqdoInfo);
					object = fullFQDOWithAgentStatus;
				}
				return object;
			} else {
				throw new DRSException(DRSExceptionCodes.EC625_MG_CUSTOMER_RCV_NUMBER_NOT_FOUND,
						"Customer ID: " + fqdoCriteria.getCustomerReceiveNumber() + " not found",
						"mgCustomerReceiveNumber", new Date(), "fqdoCriteria", SERVER);
			}
		} catch (SQLException se) {
			log.info("Exception during reading FQDO" + se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "fQDOsForCountry",
					fqdoCriteria.getReceiveCountry(), new Date(),
					"Error occurred during reading Full FQDO with registration status", SERVER);
		} finally {
			close(rs, stmt, con);
		} // end-try
	}

	private void copyWithRegistrationStatus(ResultSet rs,
			FullFQDOWithRegistrationStatus fullFQDOWithRegistrationStatus, FQDOInfo info,
			String language) throws SQLException {
		// Copy the FQDO stuff
		copy(rs, info, language);
		fullFQDOWithRegistrationStatus.setRegistrationStatusCode(rs
				.getString("registration_status"));
	}

	private FQDOInfo getFQDO(FQDOCriteria fqdoCriteria) throws DRSException {
		Connection con = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_get_fqdo_for_agent(?,?,?,?,?,?)";
			final String sql = "{ call " + DB_PACKAGE + "." + prc + " }";
			cstmt = con.prepareCall(sql);
			int i = 1;
			cstmt.setString(i++, fqdoCriteria.getReceiveCountry());
			cstmt.setString(i++, fqdoCriteria.getDeliveryOption());
			cstmt.setString(i++, fqdoCriteria.getReceiveAgentID());
			cstmt.setString(i++, fqdoCriteria.getReceiveCurrency());
			String language = null == fqdoCriteria.getLanguage() ? this.language : fqdoCriteria
					.getLanguage();
			cstmt.setString(i++, language);
			int iResult = i++;
			cstmt.registerOutParameter(iResult, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) (cstmt.getObject(iResult));
			FQDOInfo result = new FQDOInfo();
			if (rs.next()) {
				copy(rs, result, language);
			}
			return result;
		} catch (SQLException se) {
			log.info("Exception during reading FQDO for agent" + se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "fQDOsForCountry",
					fqdoCriteria.getReceiveCountry(), new Date(), "Error reading fqdo for agent",
					SERVER);
		} finally {
			close(rs, cstmt, con);
		} // end-try
	}

	private void copy(ResultSet rs, FQDOInfo info, String language) throws SQLException {
		info.setReceiveCountry(rs.getString("receive_country"));
		String deliveryOptionID = rs.getString("delivery_option");
		info.setDeliveryOption(deliveryOptionID);
		info.setDeliveryOptionDisplayName(rs.getString("delivery_option_display_name"));
		info.setReceiveAgentAbbreviation(rs.getString("receive_agent_abbreviation"));
		info.setReceiveAgentID(rs.getString("receive_agent_id"));
		info.setReceiveAgentName(rs.getString("receive_agent_name"));
		info.setReceiveCurrency(rs.getString("receive_currency"));
		info.setRegistrationAuthorizationText(rs.getString("registration_authorization"));
		info.setSpeedOfDeliveryText(rs.getString("speed_of_delivery_text"));
	}
	
	private void copyWithAgentStatus(ResultSet rs,
			FullFQDOWithAgentStatus fullFQDOWithAgentStatus, FQDOInfo info,
			String language) throws SQLException {
		// Copy the FQDO stuff
		copy(rs, info, language);
		//"ACT"-Active Agent
		//"IAT"-InActive Agent
		fullFQDOWithAgentStatus.setAgentStatusCode((rs.getString("ptnr_active_flag").equalsIgnoreCase("y"))? "ACT":"IAT");
	}

	@Override
	public BusinessObject read(ObjectId objectId, String procCall)
			throws NotFoundException, DRSException {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
