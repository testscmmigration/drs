package com.moneygram.drs.persistence;

import java.util.List;
import com.moneygram.drs.criteria.Criteria;

public interface FindDao {
	public List find(Criteria criteria) throws Exception;
}
