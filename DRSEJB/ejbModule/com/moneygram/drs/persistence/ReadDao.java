package com.moneygram.drs.persistence;

import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.persistence.exception.NotFoundException;
import com.moneygram.drs.profiles.DRSException;

public interface ReadDao {
	public BusinessObject read(ObjectId objectId) throws NotFoundException, DRSException;
	public BusinessObject read(ObjectId objectId,String procCall) throws NotFoundException, DRSException;
}
