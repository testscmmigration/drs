package com.moneygram.drs.persistence;

import com.moneygram.drs.criteria.Criteria;

public interface AssertDao {
	public boolean assertData(Criteria criteria) throws Exception;
}
