package com.moneygram.drs.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.drs.bo.BusinessObject;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;

public class SaveSensitiveIDDAO extends BaseDao implements SaveDao {
	private static final String DB_PACKAGE = "pkg_drs";
	private static Logger log = LogFactory.getInstance().getLogger(SaveSensitiveIDDAO.class);

	public BusinessObject save(BusinessObject businessObject) throws Exception {
		SensitiveIDInfo sensitiveIDInfo = (SensitiveIDInfo) businessObject;
		insertCustPrflSensitiveIDInfo(sensitiveIDInfo.getCustomerReceiveNumber(), sensitiveIDInfo
				.getCustomerReceiveNumberVersion(), sensitiveIDInfo.getSensitiveID(), sensitiveIDInfo.getRuleProcessAccountNumber());
		return null;
	}

	public void insertCustPrflSensitiveIDInfo(String cust_rcv_nbr, int prfl_ver_nbr,
			String sensitiveID, String hashCode) throws DRSException {
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			con = getConnection();
			final String prc = "prc_u_sens_data_val_id(?, ?, ?, ?)";
			final String sql = "call " + DB_PACKAGE + "." + prc;
			cstmt = con.prepareCall(sql);
			int i = 1;
			cstmt.setString(i++, cust_rcv_nbr);
			cstmt.setInt(i++, prfl_ver_nbr);
			cstmt.setString(i++, sensitiveID);
			cstmt.setString(i++,hashCode);
			cstmt.execute();
		} catch (SQLException se) {
			log.info("The exception when saving sens_data_val_id data" + se.getMessage());
			throw new DRSException(DRSExceptionCodes.EC901_DATABASE_FAILURE, "", null, new Date(),
					"Error Saving data", SERVER);
		} finally {
			close(cstmt, con);
		} // end-try
	}

	@Override
	public boolean updateTransactionUseFlag(BusinessObject businessObject)
			throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
}
