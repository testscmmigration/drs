package com.moneygram.drs.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

public class BaseDao {
	private static Logger log = LogFactory.getInstance().getLogger(BaseDao.class);
	public String language = "ENG";
	public String SERVER = "Server";

	public Connection getConnection() {
		try {
			return ServiceLocator.getInstance().getDataSource().getConnection();
		} catch (Exception e) {
			log.error("Fatal error getting Datasource or Connection.", e);
			throw new RuntimeException("Fatal error connecting to database.", e);
		}
	}

	public void close(ResultSet[] results, Statement stmt, Connection conn) {
		try {
			if (results != null) {
				for (int i = 0; i < results.length; i++) {
					if (results[i] != null) {
						results[i].close();
					}
				}
			}
		} catch (SQLException e) {
			/* ignore */
			log.error("Error during closing ResultSets ", e);
		}
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			/* ignore */
			log.error("Error during closing Statement ", e);
		}
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			/* ignore */
			log.error("Error during closing Connection ", e);
		}
	}

	public void close(ResultSet results, Statement stmt, Connection conn) {
		close(new ResultSet[] { results }, stmt, conn);
	}

	public void close(Statement stmt, Connection conn) {
		close((ResultSet[]) null, stmt, conn);
	}
}
