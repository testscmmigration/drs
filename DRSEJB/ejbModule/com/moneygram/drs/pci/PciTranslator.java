package com.moneygram.drs.pci;

import java.util.Calendar;
import java.util.Date;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.StringUtility;
import com.moneygram.drs.bo.SensitiveIDInfo;
import com.moneygram.drs.pciservicemanager.PCIServiceServiceFacade;
import com.moneygram.drs.persistence.PersistenceManagerFactory;
import com.moneygram.drs.profiles.DRSException;
import com.moneygram.drs.profiles.DRSExceptionCodes;
import com.moneygram.drs.ws.pci.EncryptAccountRequest;
import com.moneygram.drs.ws.pci.EncryptAccountResponse;
import com.moneygram.drs.ws.pci.EncryptAccountResponseCode;
import com.moneygram.drs.ws.pci.SourceUsageCategoryCode;
import com.moneygram.drs.ws.pci.SourceUsageTypeCode;

public class PciTranslator {
	private static Logger log = LogFactory.getInstance().getLogger(
			PciTranslator.class);

	public SensitiveIDInfo encryptCallOnPCI(boolean isPan,
			String originalAccountNumber, String mgCustomerReceiveNumber,
			int mgCustomerReceiveNumberVersion, Calendar cal) throws Exception {

		SensitiveIDInfo sensID = new SensitiveIDInfo();
		sensID.setCustomerReceiveNumber(mgCustomerReceiveNumber);
		sensID.setCustomerReceiveNumberVersion(mgCustomerReceiveNumberVersion);
		if (isPan) {
			EncryptAccountRequest accountRequest = new EncryptAccountRequest();
			accountRequest.setPrimaryAccountNumber(originalAccountNumber);
			accountRequest.setSourceSystemID(mgCustomerReceiveNumber + "-"
					+ mgCustomerReceiveNumberVersion);
			accountRequest
					.setSourceUsageType(SourceUsageTypeCode.RECEIVER_REGISTRATION_NUMBER);
			accountRequest
					.setSourceUsageCategory(SourceUsageCategoryCode.PROFILE);
			accountRequest.setLastUsedDate(cal);
			EncryptAccountResponse accountResponse = PCIServiceServiceFacade
					.encryptAccount(accountRequest);
			if (accountResponse.getResult().getValue()
					.equals(EncryptAccountResponseCode._SUCCESS)) {
				log.debug("The account number is successfully encrypted");
				sensID.setSensitiveID(accountResponse.getSensitiveDataValueID());
				// added for 30655 agg infra rules - july 2017
				sensID.setRuleProcessAccountNumber(String
						.valueOf(accountResponse.getAccountNumberHashCode()));
				PersistenceManagerFactory.getInstance().save(sensID);
			} else {
				log.error("Error Encrypting the account number");
				throw new DRSException(
						DRSExceptionCodes.EC923_PCI_CALL_FAILURE, "",
						"PCIEncryption", new Date(),
						"PCI Encryption call failed", "client");
			}
		} else {
			// added for 30655 agg infra rules - july 2017
			if (!StringUtility.isNullOrEmpty(originalAccountNumber)) {
				sensID.setRuleProcessAccountNumber(originalAccountNumber);
			}
		}
		return sensID;
	}

}
