## Setup the Server

AdminConfig.reset()

import base
import utils

node = base.node
server = base.server
security = base.security

## Setup Environment Specific properties
def Environment_WebSphereVariables():
	if utils.env:
		node.setVarsFromProps(utils.env)

## Setup Certs for the Environment
certificateAlias = "dev"
certificateFilePath = utils.serverLibPath("certs/" + utils.env + "/pci.cer")
base64Encoded = "false"

def listCerts():
	for k, v in node.listTrustStore().items():
		print "NodeDefaultTrustStore->%s: %s" % (k, str(v))
		
def delCert(alias = certificateAlias):
	node.deleteTrustCertificate(alias)

def addCert():	
	node.addTrustCertificate(certificateAlias, certificateFilePath, base64Encoded)

def Security_Certs():
	delCert()
	addCert()
	listCerts()
	
def SSL_TLSv2():
	sslConfig = security.findChild("SSLConfig", "alias", "NodeDefaultSSLSettings")
	setting =  sslConfig.child("SecureSocketLayer")
	setting.setAttr("sslProtocol", "SSL_TLSv2")

def OracleXAJDBCProvider():
	oracleDriverPath = utils.serverLibPath("oracle-driver/11.2.0.3.0")
	node.setVars({
		"ORACLE_JDBC_DRIVER_PATH": oracleDriverPath,	
	})
	server.OracleXAJDBCProvider({
	})

def OracleJDBCProvider():
	oracleDriverPath = utils.serverLibPath("oracle-driver/11.2.0.3.0")
	node.setVars({
		"ORACLE_JDBC_DRIVER_PATH": oracleDriverPath,	
	})
	server.OracleJDBCProvider({
	})
	

Environment_WebSphereVariables()
SSL_TLSv2()
#Security_Certs()
OracleXAJDBCProvider()

AdminConfig.save()