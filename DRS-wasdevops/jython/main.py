########################
## Reset any pending configuration items
AdminConfig.reset()

#########################################################################
## Import reusable modules, setup common variables

## base module is used with WebSphere Base product
import base
import utils
import fast_deploy

cell = base.cell
node = base.node
server = base.server
security = base.security

#########################################################################
## Server Configuration

jvm = server.getJVM()
jvm.systemProperties({
	#"key": "value"
})

#########################################################################
## Application Resource Configuration

###############################
## Setup JDBC Objects
tpeAlias = "TPE"
tpeJAAS = security.JAASAuthData({
	"alias": tpeAlias, 
	"userId": "objects", 
	"password": "objects"
})

oracleDriver = server.getOracleXAJDBCProvider()
#TODO Create auth alias for Container Managed
tpeDataSource = oracleDriver.Oracle11gDS({
	"jndiName": "jdbc/TPEDataSource", 
	"authDataAlias": tpeAlias,
	"xaRecoveryAuthAlias": tpeAlias,
	"URL": "${tpe.jdbc}"
})

#################################
## Create app properties as 
## resource environment provider

rep = server.ResourceEnvironmentProvider({
	"name": "MGIResourceEnvironmentProvider"
})

configFactory = rep.Referenceable({
	"factoryClassname": "com.moneygram.ree.lib.ConfigFactory",
	"classname": "com.moneygram.ree.lib.Config"
}) 

rep.ResourceEnvEntry({
	"jndiName": "rep/ResourceReference",
	"referenceable": configFactory.getId(),
	"_customProps": {
		"PCIServiceURL": "${pci.encrypt.v3.endpoint}",
		"pciTimeOut": "3000",
		"PCIUserName": "pandaServiceRRN",
		"PCIPswd": "2uMEbEb6",
		"PCIDecryptServiceURL": "${pci.decrypt.v1.endpoint}",
		"mfTimeout": "30000"
	}
})

#################################
## MQ Configuration

ah = fast_deploy.AppHelper(server, "drs")

ah.getSIBus()

ah.resolveAll(ah.SIBQueue, [
	{
		"identifier": "TD.TO.DRS.REQ",
	},
	{
		"identifier": "DSS.NOTIFY.REQ"
	}
])


ah.resolveAll(ah.SIBJMSQueue, [
	{
		"jndiName": "jms/TD.TO.DRS.REQ",
		"QueueName": "TD.TO.DRS.REQ"
	},
	{
		"jndiName": "jms/DSS.NOTIFY.REQ",
		"QueueName": "DSS.NOTIFY.REQ"
	}
])

ah.resolveAll(ah.SIBJMSQCF, [
	{
		"jndiName": "jms/TD.TO.DRS.REQ.QCF"
	},
	{
		"jndiName": "jms/DSS.NOTIFY.REQ.QCF"
	}
])

ah.resolveAll(ah.SIBJMSActivationSpec,
[
	{
		"jndiName": "jms/tdMessageActivationSpec",
		"destinationJndiName": "jms/TD.TO.DRS.REQ"
	}
])

#################################
## Save the configuration
AdminConfig.save()
print "AdminConfig.save() complete"